(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pagination"],{

/***/ "./assets/pagination.js":
/*!******************************!*\
  !*** ./assets/pagination.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {__webpack_require__(/*! core-js/modules/es.array.find.js */ "./node_modules/core-js/modules/es.array.find.js");

__webpack_require__(/*! core-js/modules/es.array.slice.js */ "./node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.parse-int.js */ "./node_modules/core-js/modules/es.parse-int.js");

!function (e) {
  e.fn.pagination = function (a) {
    function t(t) {
      var s = e("." + r.contents + ".current").children().length,
          l = Math.ceil(s / r.items),
          o = '<ul id="page-navi">\t<li><a href="#" class="previos">' + r.previous + "</a></li>";

      for (i = 0; i < l; i++) {
        o += '\t<li><a href="#">' + (i + 1) + "</a></li>";
      }

      o += '\t<li><a href="#" class="next">' + r.next + "</a></li></ul>";
      var c = t;
      0 == t ? (c = parseInt(e("#page-navi li a.current").html())) - 1 != 0 && c-- : t == l + 1 && (c = parseInt(e("#page-navi li a.current").html())) + 1 != l + 1 && c++, t = c, 0 == s && (o = ""), e("#page-navi").remove(), "top" == r.position ? e("." + r.contents + ".current").before(o) : e("." + r.contents + ".current").after(o), e("#page-navi li a").removeClass("current"), e("#page-navi li a").eq(t).addClass("current"), e("#page-navi li a").removeClass("disable"), c = parseInt(e("#page-navi li a.current").html()), c - 1 == 0 && e("#page-navi li a.previos").addClass("disable"), c == l && e("#page-navi li a.next").addClass("disable");
      var u = a.items * (t - 1),
          d = a.items * t;
      t == l && (d = s), e("." + r.contents + ".current").children().hide(), e("." + r.contents + ".current").children().slice(u, d).fadeIn(a.time), 1 == r.scroll && e("html,body").animate({
        scrollTop: n
      }, 0);
    }

    var r = {
      items: 5,
      contents: "contents",
      previous: "Previous&raquo;",
      next: "&laquo;Next",
      time: 800,
      start: 1,
      position: "bottom",
      scroll: !0
    },
        r = e.extend(r, a);
    e(this).addClass("jquery-tab-pager-tabbar"), $tab = e(this).find("li");
    var n = 0;
    !function () {
      var a = r.start - 1;
      $tab.eq(a).addClass("current"), e("." + r.contents).hide().eq(a).show().addClass("current"), t(1);
    }(), $tab.click(function () {
      var a = $tab.index(this);
      $tab.removeClass("current"), e(this).addClass("current"), e("." + r.contents).removeClass("current").hide().eq(a).addClass("current").fadeIn(r.time), t(1);
    }), e(document).on("click", "#page-navi li a", function () {
      return !e(this).hasClass("disable") && (t(e("#page-navi li a").index(this)), !1);
    }), e(window).on("load scroll", function () {
      n = e(window).scrollTop();
    });
  };
}(jQuery); // $('#pagination-container').pagination({
//     dataSource: [1, 2, 3, 4, 5, 6, 7],
//     callback: function(data, pagination) {
//         // template method of yourself
//         var html = template(data);
//         $('#data-container').html(html);
//     }
// })
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./node_modules/core-js/internals/array-method-has-species-support.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/internals/array-method-has-species-support.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var fails = __webpack_require__(/*! ../internals/fails */ "./node_modules/core-js/internals/fails.js");
var wellKnownSymbol = __webpack_require__(/*! ../internals/well-known-symbol */ "./node_modules/core-js/internals/well-known-symbol.js");
var V8_VERSION = __webpack_require__(/*! ../internals/engine-v8-version */ "./node_modules/core-js/internals/engine-v8-version.js");

var SPECIES = wellKnownSymbol('species');

module.exports = function (METHOD_NAME) {
  // We can't use this feature detection in V8 since it causes
  // deoptimization and serious performance degradation
  // https://github.com/zloirock/core-js/issues/677
  return V8_VERSION >= 51 || !fails(function () {
    var array = [];
    var constructor = array.constructor = {};
    constructor[SPECIES] = function () {
      return { foo: 1 };
    };
    return array[METHOD_NAME](Boolean).foo !== 1;
  });
};


/***/ }),

/***/ "./node_modules/core-js/internals/create-property.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/internals/create-property.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var toPrimitive = __webpack_require__(/*! ../internals/to-primitive */ "./node_modules/core-js/internals/to-primitive.js");
var definePropertyModule = __webpack_require__(/*! ../internals/object-define-property */ "./node_modules/core-js/internals/object-define-property.js");
var createPropertyDescriptor = __webpack_require__(/*! ../internals/create-property-descriptor */ "./node_modules/core-js/internals/create-property-descriptor.js");

module.exports = function (object, key, value) {
  var propertyKey = toPrimitive(key);
  if (propertyKey in object) definePropertyModule.f(object, propertyKey, createPropertyDescriptor(0, value));
  else object[propertyKey] = value;
};


/***/ }),

/***/ "./node_modules/core-js/internals/engine-user-agent.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/internals/engine-user-agent.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getBuiltIn = __webpack_require__(/*! ../internals/get-built-in */ "./node_modules/core-js/internals/get-built-in.js");

module.exports = getBuiltIn('navigator', 'userAgent') || '';


/***/ }),

/***/ "./node_modules/core-js/internals/engine-v8-version.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/internals/engine-v8-version.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(/*! ../internals/global */ "./node_modules/core-js/internals/global.js");
var userAgent = __webpack_require__(/*! ../internals/engine-user-agent */ "./node_modules/core-js/internals/engine-user-agent.js");

var process = global.process;
var versions = process && process.versions;
var v8 = versions && versions.v8;
var match, version;

if (v8) {
  match = v8.split('.');
  version = match[0] + match[1];
} else if (userAgent) {
  match = userAgent.match(/Edge\/(\d+)/);
  if (!match || match[1] >= 74) {
    match = userAgent.match(/Chrome\/(\d+)/);
    if (match) version = match[1];
  }
}

module.exports = version && +version;


/***/ }),

/***/ "./node_modules/core-js/internals/number-parse-int.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/internals/number-parse-int.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(/*! ../internals/global */ "./node_modules/core-js/internals/global.js");
var trim = __webpack_require__(/*! ../internals/string-trim */ "./node_modules/core-js/internals/string-trim.js").trim;
var whitespaces = __webpack_require__(/*! ../internals/whitespaces */ "./node_modules/core-js/internals/whitespaces.js");

var $parseInt = global.parseInt;
var hex = /^[+-]?0[Xx]/;
var FORCED = $parseInt(whitespaces + '08') !== 8 || $parseInt(whitespaces + '0x16') !== 22;

// `parseInt` method
// https://tc39.es/ecma262/#sec-parseint-string-radix
module.exports = FORCED ? function parseInt(string, radix) {
  var S = trim(String(string));
  return $parseInt(S, (radix >>> 0) || (hex.test(S) ? 16 : 10));
} : $parseInt;


/***/ }),

/***/ "./node_modules/core-js/internals/string-trim.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/internals/string-trim.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var requireObjectCoercible = __webpack_require__(/*! ../internals/require-object-coercible */ "./node_modules/core-js/internals/require-object-coercible.js");
var whitespaces = __webpack_require__(/*! ../internals/whitespaces */ "./node_modules/core-js/internals/whitespaces.js");

var whitespace = '[' + whitespaces + ']';
var ltrim = RegExp('^' + whitespace + whitespace + '*');
var rtrim = RegExp(whitespace + whitespace + '*$');

// `String.prototype.{ trim, trimStart, trimEnd, trimLeft, trimRight }` methods implementation
var createMethod = function (TYPE) {
  return function ($this) {
    var string = String(requireObjectCoercible($this));
    if (TYPE & 1) string = string.replace(ltrim, '');
    if (TYPE & 2) string = string.replace(rtrim, '');
    return string;
  };
};

module.exports = {
  // `String.prototype.{ trimLeft, trimStart }` methods
  // https://tc39.es/ecma262/#sec-string.prototype.trimstart
  start: createMethod(1),
  // `String.prototype.{ trimRight, trimEnd }` methods
  // https://tc39.es/ecma262/#sec-string.prototype.trimend
  end: createMethod(2),
  // `String.prototype.trim` method
  // https://tc39.es/ecma262/#sec-string.prototype.trim
  trim: createMethod(3)
};


/***/ }),

/***/ "./node_modules/core-js/internals/whitespaces.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/internals/whitespaces.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// a string of all valid unicode whitespaces
// eslint-disable-next-line max-len
module.exports = '\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';


/***/ }),

/***/ "./node_modules/core-js/modules/es.array.slice.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/modules/es.array.slice.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var isObject = __webpack_require__(/*! ../internals/is-object */ "./node_modules/core-js/internals/is-object.js");
var isArray = __webpack_require__(/*! ../internals/is-array */ "./node_modules/core-js/internals/is-array.js");
var toAbsoluteIndex = __webpack_require__(/*! ../internals/to-absolute-index */ "./node_modules/core-js/internals/to-absolute-index.js");
var toLength = __webpack_require__(/*! ../internals/to-length */ "./node_modules/core-js/internals/to-length.js");
var toIndexedObject = __webpack_require__(/*! ../internals/to-indexed-object */ "./node_modules/core-js/internals/to-indexed-object.js");
var createProperty = __webpack_require__(/*! ../internals/create-property */ "./node_modules/core-js/internals/create-property.js");
var wellKnownSymbol = __webpack_require__(/*! ../internals/well-known-symbol */ "./node_modules/core-js/internals/well-known-symbol.js");
var arrayMethodHasSpeciesSupport = __webpack_require__(/*! ../internals/array-method-has-species-support */ "./node_modules/core-js/internals/array-method-has-species-support.js");
var arrayMethodUsesToLength = __webpack_require__(/*! ../internals/array-method-uses-to-length */ "./node_modules/core-js/internals/array-method-uses-to-length.js");

var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('slice');
var USES_TO_LENGTH = arrayMethodUsesToLength('slice', { ACCESSORS: true, 0: 0, 1: 2 });

var SPECIES = wellKnownSymbol('species');
var nativeSlice = [].slice;
var max = Math.max;

// `Array.prototype.slice` method
// https://tc39.es/ecma262/#sec-array.prototype.slice
// fallback for not array-like ES3 strings and DOM objects
$({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT || !USES_TO_LENGTH }, {
  slice: function slice(start, end) {
    var O = toIndexedObject(this);
    var length = toLength(O.length);
    var k = toAbsoluteIndex(start, length);
    var fin = toAbsoluteIndex(end === undefined ? length : end, length);
    // inline `ArraySpeciesCreate` for usage native `Array#slice` where it's possible
    var Constructor, result, n;
    if (isArray(O)) {
      Constructor = O.constructor;
      // cross-realm fallback
      if (typeof Constructor == 'function' && (Constructor === Array || isArray(Constructor.prototype))) {
        Constructor = undefined;
      } else if (isObject(Constructor)) {
        Constructor = Constructor[SPECIES];
        if (Constructor === null) Constructor = undefined;
      }
      if (Constructor === Array || Constructor === undefined) {
        return nativeSlice.call(O, k, fin);
      }
    }
    result = new (Constructor === undefined ? Array : Constructor)(max(fin - k, 0));
    for (n = 0; k < fin; k++, n++) if (k in O) createProperty(result, n, O[k]);
    result.length = n;
    return result;
  }
});


/***/ }),

/***/ "./node_modules/core-js/modules/es.parse-int.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/modules/es.parse-int.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var parseIntImplementation = __webpack_require__(/*! ../internals/number-parse-int */ "./node_modules/core-js/internals/number-parse-int.js");

// `parseInt` method
// https://tc39.es/ecma262/#sec-parseint-string-radix
$({ global: true, forced: parseInt != parseIntImplementation }, {
  parseInt: parseIntImplementation
});


/***/ })

},[["./assets/pagination.js","runtime","vendors~app~pagecustom~pagination","vendors~app~pagination"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvcGFnaW5hdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9pbnRlcm5hbHMvYXJyYXktbWV0aG9kLWhhcy1zcGVjaWVzLXN1cHBvcnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvaW50ZXJuYWxzL2NyZWF0ZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9pbnRlcm5hbHMvZW5naW5lLXVzZXItYWdlbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvaW50ZXJuYWxzL2VuZ2luZS12OC12ZXJzaW9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2ludGVybmFscy9udW1iZXItcGFyc2UtaW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2ludGVybmFscy9zdHJpbmctdHJpbS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9pbnRlcm5hbHMvd2hpdGVzcGFjZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbW9kdWxlcy9lcy5hcnJheS5zbGljZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9tb2R1bGVzL2VzLnBhcnNlLWludC5qcyJdLCJuYW1lcyI6WyJlIiwiZm4iLCJwYWdpbmF0aW9uIiwiYSIsInQiLCJzIiwiciIsImNvbnRlbnRzIiwiY2hpbGRyZW4iLCJsZW5ndGgiLCJsIiwiTWF0aCIsImNlaWwiLCJpdGVtcyIsIm8iLCJwcmV2aW91cyIsImkiLCJuZXh0IiwiYyIsInBhcnNlSW50IiwiaHRtbCIsInJlbW92ZSIsInBvc2l0aW9uIiwiYmVmb3JlIiwiYWZ0ZXIiLCJyZW1vdmVDbGFzcyIsImVxIiwiYWRkQ2xhc3MiLCJ1IiwiZCIsImhpZGUiLCJzbGljZSIsImZhZGVJbiIsInRpbWUiLCJzY3JvbGwiLCJhbmltYXRlIiwic2Nyb2xsVG9wIiwibiIsInN0YXJ0IiwiZXh0ZW5kIiwiJHRhYiIsImZpbmQiLCJzaG93IiwiY2xpY2siLCJpbmRleCIsImRvY3VtZW50Iiwib24iLCJoYXNDbGFzcyIsIndpbmRvdyIsImpRdWVyeSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsQ0FBQyxVQUFTQSxDQUFULEVBQVc7QUFBQ0EsR0FBQyxDQUFDQyxFQUFGLENBQUtDLFVBQUwsR0FBZ0IsVUFBU0MsQ0FBVCxFQUFXO0FBQUMsYUFBU0MsQ0FBVCxDQUFXQSxDQUFYLEVBQWE7QUFBQyxVQUFJQyxDQUFDLEdBQUNMLENBQUMsQ0FBQyxNQUFJTSxDQUFDLENBQUNDLFFBQU4sR0FBZSxVQUFoQixDQUFELENBQTZCQyxRQUE3QixHQUF3Q0MsTUFBOUM7QUFBQSxVQUFxREMsQ0FBQyxHQUFDQyxJQUFJLENBQUNDLElBQUwsQ0FBVVAsQ0FBQyxHQUFDQyxDQUFDLENBQUNPLEtBQWQsQ0FBdkQ7QUFBQSxVQUN2REMsQ0FBQyxHQUFDLDBEQUF3RFIsQ0FBQyxDQUFDUyxRQUExRCxHQUFtRSxXQURkOztBQUMwQixXQUFJQyxDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUNOLENBQVYsRUFBWU0sQ0FBQyxFQUFiO0FBQWdCRixTQUFDLElBQUUsd0JBQXNCRSxDQUFDLEdBQUMsQ0FBeEIsSUFBMkIsV0FBOUI7QUFBaEI7O0FBQTBERixPQUFDLElBQUUsb0NBQWtDUixDQUFDLENBQUNXLElBQXBDLEdBQXlDLGdCQUE1QztBQUE2RCxVQUFJQyxDQUFDLEdBQUNkLENBQU47QUFBUSxXQUFHQSxDQUFILEdBQUssQ0FBQ2MsQ0FBQyxHQUFDQyxRQUFRLENBQUNuQixDQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2Qm9CLElBQTdCLEVBQUQsQ0FBWCxJQUFrRCxDQUFsRCxJQUFxRCxDQUFyRCxJQUF3REYsQ0FBQyxFQUE5RCxHQUFpRWQsQ0FBQyxJQUFFTSxDQUFDLEdBQUMsQ0FBTCxJQUFRLENBQUNRLENBQUMsR0FBQ0MsUUFBUSxDQUFDbkIsQ0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJvQixJQUE3QixFQUFELENBQVgsSUFBa0QsQ0FBbEQsSUFBcURWLENBQUMsR0FBQyxDQUEvRCxJQUFrRVEsQ0FBQyxFQUFwSSxFQUF1SWQsQ0FBQyxHQUFDYyxDQUF6SSxFQUEySSxLQUFHYixDQUFILEtBQU9TLENBQUMsR0FBQyxFQUFULENBQTNJLEVBQXdKZCxDQUFDLENBQUMsWUFBRCxDQUFELENBQWdCcUIsTUFBaEIsRUFBeEosRUFBaUwsU0FBT2YsQ0FBQyxDQUFDZ0IsUUFBVCxHQUFrQnRCLENBQUMsQ0FBQyxNQUFJTSxDQUFDLENBQUNDLFFBQU4sR0FBZSxVQUFoQixDQUFELENBQTZCZ0IsTUFBN0IsQ0FBb0NULENBQXBDLENBQWxCLEdBQXlEZCxDQUFDLENBQUMsTUFBSU0sQ0FBQyxDQUFDQyxRQUFOLEdBQWUsVUFBaEIsQ0FBRCxDQUE2QmlCLEtBQTdCLENBQW1DVixDQUFuQyxDQUExTyxFQUFnUmQsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ5QixXQUFyQixDQUFpQyxTQUFqQyxDQUFoUixFQUE0VHpCLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCMEIsRUFBckIsQ0FBd0J0QixDQUF4QixFQUEyQnVCLFFBQTNCLENBQW9DLFNBQXBDLENBQTVULEVBQTJXM0IsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ5QixXQUFyQixDQUFpQyxTQUFqQyxDQUEzVyxFQUF1WlAsQ0FBQyxHQUFDQyxRQUFRLENBQUNuQixDQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2Qm9CLElBQTdCLEVBQUQsQ0FBamEsRUFBdWNGLENBQUMsR0FBQyxDQUFGLElBQUssQ0FBTCxJQUFRbEIsQ0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkIyQixRQUE3QixDQUFzQyxTQUF0QyxDQUEvYyxFQUFnZ0JULENBQUMsSUFBRVIsQ0FBSCxJQUFNVixDQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQjJCLFFBQTFCLENBQW1DLFNBQW5DLENBQXRnQjtBQUFvakIsVUFBSUMsQ0FBQyxHQUFDekIsQ0FBQyxDQUFDVSxLQUFGLElBQVNULENBQUMsR0FBQyxDQUFYLENBQU47QUFBQSxVQUFvQnlCLENBQUMsR0FBQzFCLENBQUMsQ0FBQ1UsS0FBRixHQUFRVCxDQUE5QjtBQUFnQ0EsT0FBQyxJQUFFTSxDQUFILEtBQU9tQixDQUFDLEdBQUN4QixDQUFULEdBQVlMLENBQUMsQ0FBQyxNQUFJTSxDQUFDLENBQUNDLFFBQU4sR0FBZSxVQUFoQixDQUFELENBQTZCQyxRQUE3QixHQUF3Q3NCLElBQXhDLEVBQVosRUFBMkQ5QixDQUFDLENBQUMsTUFBSU0sQ0FBQyxDQUFDQyxRQUFOLEdBQWUsVUFBaEIsQ0FBRCxDQUE2QkMsUUFBN0IsR0FBd0N1QixLQUF4QyxDQUE4Q0gsQ0FBOUMsRUFBZ0RDLENBQWhELEVBQW1ERyxNQUFuRCxDQUEwRDdCLENBQUMsQ0FBQzhCLElBQTVELENBQTNELEVBQTZILEtBQUczQixDQUFDLENBQUM0QixNQUFMLElBQWFsQyxDQUFDLENBQUMsV0FBRCxDQUFELENBQWVtQyxPQUFmLENBQXVCO0FBQUNDLGlCQUFTLEVBQUNDO0FBQVgsT0FBdkIsRUFBcUMsQ0FBckMsQ0FBMUk7QUFBa0w7O0FBQUEsUUFBSS9CLENBQUMsR0FBQztBQUFDTyxXQUFLLEVBQUMsQ0FBUDtBQUFTTixjQUFRLEVBQUMsVUFBbEI7QUFBNkJRLGNBQVEsRUFBQyxpQkFBdEM7QUFBd0RFLFVBQUksRUFBQyxhQUE3RDtBQUEyRWdCLFVBQUksRUFBQyxHQUFoRjtBQUFvRkssV0FBSyxFQUFDLENBQTFGO0FBQTRGaEIsY0FBUSxFQUFDLFFBQXJHO0FBQThHWSxZQUFNLEVBQUMsQ0FBQztBQUF0SCxLQUFOO0FBQUEsUUFBK0g1QixDQUFDLEdBQUNOLENBQUMsQ0FBQ3VDLE1BQUYsQ0FBU2pDLENBQVQsRUFBV0gsQ0FBWCxDQUFqSTtBQUErSUgsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMkIsUUFBUixDQUFpQix5QkFBakIsR0FBNENhLElBQUksR0FBQ3hDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXlDLElBQVIsQ0FBYSxJQUFiLENBQWpEO0FBQW9FLFFBQUlKLENBQUMsR0FBQyxDQUFOO0FBQVEsS0FBQyxZQUFVO0FBQUMsVUFBSWxDLENBQUMsR0FBQ0csQ0FBQyxDQUFDZ0MsS0FBRixHQUFRLENBQWQ7QUFBZ0JFLFVBQUksQ0FBQ2QsRUFBTCxDQUFRdkIsQ0FBUixFQUFXd0IsUUFBWCxDQUFvQixTQUFwQixHQUErQjNCLENBQUMsQ0FBQyxNQUFJTSxDQUFDLENBQUNDLFFBQVAsQ0FBRCxDQUFrQnVCLElBQWxCLEdBQXlCSixFQUF6QixDQUE0QnZCLENBQTVCLEVBQStCdUMsSUFBL0IsR0FBc0NmLFFBQXRDLENBQStDLFNBQS9DLENBQS9CLEVBQXlGdkIsQ0FBQyxDQUFDLENBQUQsQ0FBMUY7QUFBOEYsS0FBekgsRUFBRCxFQUE2SG9DLElBQUksQ0FBQ0csS0FBTCxDQUFXLFlBQVU7QUFBQyxVQUFJeEMsQ0FBQyxHQUFDcUMsSUFBSSxDQUFDSSxLQUFMLENBQVcsSUFBWCxDQUFOO0FBQXVCSixVQUFJLENBQUNmLFdBQUwsQ0FBaUIsU0FBakIsR0FBNEJ6QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVEyQixRQUFSLENBQWlCLFNBQWpCLENBQTVCLEVBQXdEM0IsQ0FBQyxDQUFDLE1BQUlNLENBQUMsQ0FBQ0MsUUFBUCxDQUFELENBQWtCa0IsV0FBbEIsQ0FBOEIsU0FBOUIsRUFBeUNLLElBQXpDLEdBQWdESixFQUFoRCxDQUFtRHZCLENBQW5ELEVBQXNEd0IsUUFBdEQsQ0FBK0QsU0FBL0QsRUFBMEVLLE1BQTFFLENBQWlGMUIsQ0FBQyxDQUFDMkIsSUFBbkYsQ0FBeEQsRUFBaUo3QixDQUFDLENBQUMsQ0FBRCxDQUFsSjtBQUFzSixLQUFuTSxDQUE3SCxFQUFrVUosQ0FBQyxDQUFDNkMsUUFBRCxDQUFELENBQVlDLEVBQVosQ0FBZSxPQUFmLEVBQXVCLGlCQUF2QixFQUF5QyxZQUFVO0FBQUMsYUFBTSxDQUFDOUMsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRK0MsUUFBUixDQUFpQixTQUFqQixDQUFELEtBQStCM0MsQ0FBQyxDQUFDSixDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQjRDLEtBQXJCLENBQTJCLElBQTNCLENBQUQsQ0FBRCxFQUFvQyxDQUFDLENBQXBFLENBQU47QUFBNkUsS0FBakksQ0FBbFUsRUFBcWM1QyxDQUFDLENBQUNnRCxNQUFELENBQUQsQ0FBVUYsRUFBVixDQUFhLGFBQWIsRUFBMkIsWUFBVTtBQUFDVCxPQUFDLEdBQUNyQyxDQUFDLENBQUNnRCxNQUFELENBQUQsQ0FBVVosU0FBVixFQUFGO0FBQXdCLEtBQTlELENBQXJjO0FBQXFnQixHQUR6cUQ7QUFDMHFELENBRHRyRCxDQUN1ckRhLE1BRHZyRCxDQUFELEMsQ0FLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEs7Ozs7Ozs7Ozs7OztBQ1pBLFlBQVksbUJBQU8sQ0FBQyxxRUFBb0I7QUFDeEMsc0JBQXNCLG1CQUFPLENBQUMsNkZBQWdDO0FBQzlELGlCQUFpQixtQkFBTyxDQUFDLDZGQUFnQzs7QUFFekQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7Ozs7O0FDbEJhO0FBQ2Isa0JBQWtCLG1CQUFPLENBQUMsbUZBQTJCO0FBQ3JELDJCQUEyQixtQkFBTyxDQUFDLHVHQUFxQztBQUN4RSwrQkFBK0IsbUJBQU8sQ0FBQywrR0FBeUM7O0FBRWhGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1RBLGlCQUFpQixtQkFBTyxDQUFDLG1GQUEyQjs7QUFFcEQ7Ozs7Ozs7Ozs7OztBQ0ZBLGFBQWEsbUJBQU8sQ0FBQyx1RUFBcUI7QUFDMUMsZ0JBQWdCLG1CQUFPLENBQUMsNkZBQWdDOztBQUV4RDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7QUNuQkEsYUFBYSxtQkFBTyxDQUFDLHVFQUFxQjtBQUMxQyxXQUFXLG1CQUFPLENBQUMsaUZBQTBCO0FBQzdDLGtCQUFrQixtQkFBTyxDQUFDLGlGQUEwQjs7QUFFcEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7QUNiRCw2QkFBNkIsbUJBQU8sQ0FBQywyR0FBdUM7QUFDNUUsa0JBQWtCLG1CQUFPLENBQUMsaUZBQTBCOztBQUVwRDtBQUNBO0FBQ0E7O0FBRUEsc0JBQXNCLGdEQUFnRDtBQUN0RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esd0JBQXdCLHNCQUFzQjtBQUM5QztBQUNBO0FBQ0Esd0JBQXdCLHFCQUFxQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQzNCQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNGYTtBQUNiLFFBQVEsbUJBQU8sQ0FBQyx1RUFBcUI7QUFDckMsZUFBZSxtQkFBTyxDQUFDLDZFQUF3QjtBQUMvQyxjQUFjLG1CQUFPLENBQUMsMkVBQXVCO0FBQzdDLHNCQUFzQixtQkFBTyxDQUFDLDZGQUFnQztBQUM5RCxlQUFlLG1CQUFPLENBQUMsNkVBQXdCO0FBQy9DLHNCQUFzQixtQkFBTyxDQUFDLDZGQUFnQztBQUM5RCxxQkFBcUIsbUJBQU8sQ0FBQyx5RkFBOEI7QUFDM0Qsc0JBQXNCLG1CQUFPLENBQUMsNkZBQWdDO0FBQzlELG1DQUFtQyxtQkFBTyxDQUFDLDJIQUErQztBQUMxRiw4QkFBOEIsbUJBQU8sQ0FBQyxpSEFBMEM7O0FBRWhGO0FBQ0EsdURBQXVELDhCQUE4Qjs7QUFFckY7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUcsZ0ZBQWdGO0FBQ25GO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxTQUFTO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7OztBQ2hERCxRQUFRLG1CQUFPLENBQUMsdUVBQXFCO0FBQ3JDLDZCQUE2QixtQkFBTyxDQUFDLDJGQUErQjs7QUFFcEU7QUFDQTtBQUNBLEdBQUcsMkRBQTJEO0FBQzlEO0FBQ0EsQ0FBQyIsImZpbGUiOiJwYWdpbmF0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIWZ1bmN0aW9uKGUpe2UuZm4ucGFnaW5hdGlvbj1mdW5jdGlvbihhKXtmdW5jdGlvbiB0KHQpe3ZhciBzPWUoXCIuXCIrci5jb250ZW50cytcIi5jdXJyZW50XCIpLmNoaWxkcmVuKCkubGVuZ3RoLGw9TWF0aC5jZWlsKHMvci5pdGVtcyksXHJcbm89Jzx1bCBpZD1cInBhZ2UtbmF2aVwiPlxcdDxsaT48YSBocmVmPVwiI1wiIGNsYXNzPVwicHJldmlvc1wiPicrci5wcmV2aW91cytcIjwvYT48L2xpPlwiO2ZvcihpPTA7aTxsO2krKylvKz0nXFx0PGxpPjxhIGhyZWY9XCIjXCI+JysoaSsxKStcIjwvYT48L2xpPlwiO28rPSdcXHQ8bGk+PGEgaHJlZj1cIiNcIiBjbGFzcz1cIm5leHRcIj4nK3IubmV4dCtcIjwvYT48L2xpPjwvdWw+XCI7dmFyIGM9dDswPT10PyhjPXBhcnNlSW50KGUoXCIjcGFnZS1uYXZpIGxpIGEuY3VycmVudFwiKS5odG1sKCkpKS0xIT0wJiZjLS06dD09bCsxJiYoYz1wYXJzZUludChlKFwiI3BhZ2UtbmF2aSBsaSBhLmN1cnJlbnRcIikuaHRtbCgpKSkrMSE9bCsxJiZjKyssdD1jLDA9PXMmJihvPVwiXCIpLGUoXCIjcGFnZS1uYXZpXCIpLnJlbW92ZSgpLFwidG9wXCI9PXIucG9zaXRpb24/ZShcIi5cIityLmNvbnRlbnRzK1wiLmN1cnJlbnRcIikuYmVmb3JlKG8pOmUoXCIuXCIrci5jb250ZW50cytcIi5jdXJyZW50XCIpLmFmdGVyKG8pLGUoXCIjcGFnZS1uYXZpIGxpIGFcIikucmVtb3ZlQ2xhc3MoXCJjdXJyZW50XCIpLGUoXCIjcGFnZS1uYXZpIGxpIGFcIikuZXEodCkuYWRkQ2xhc3MoXCJjdXJyZW50XCIpLGUoXCIjcGFnZS1uYXZpIGxpIGFcIikucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlXCIpLGM9cGFyc2VJbnQoZShcIiNwYWdlLW5hdmkgbGkgYS5jdXJyZW50XCIpLmh0bWwoKSksYy0xPT0wJiZlKFwiI3BhZ2UtbmF2aSBsaSBhLnByZXZpb3NcIikuYWRkQ2xhc3MoXCJkaXNhYmxlXCIpLGM9PWwmJmUoXCIjcGFnZS1uYXZpIGxpIGEubmV4dFwiKS5hZGRDbGFzcyhcImRpc2FibGVcIik7dmFyIHU9YS5pdGVtcyoodC0xKSxkPWEuaXRlbXMqdDt0PT1sJiYoZD1zKSxlKFwiLlwiK3IuY29udGVudHMrXCIuY3VycmVudFwiKS5jaGlsZHJlbigpLmhpZGUoKSxlKFwiLlwiK3IuY29udGVudHMrXCIuY3VycmVudFwiKS5jaGlsZHJlbigpLnNsaWNlKHUsZCkuZmFkZUluKGEudGltZSksMT09ci5zY3JvbGwmJmUoXCJodG1sLGJvZHlcIikuYW5pbWF0ZSh7c2Nyb2xsVG9wOm59LDApfXZhciByPXtpdGVtczo1LGNvbnRlbnRzOlwiY29udGVudHNcIixwcmV2aW91czpcIlByZXZpb3VzJnJhcXVvO1wiLG5leHQ6XCImbGFxdW87TmV4dFwiLHRpbWU6ODAwLHN0YXJ0OjEscG9zaXRpb246XCJib3R0b21cIixzY3JvbGw6ITB9LHI9ZS5leHRlbmQocixhKTtlKHRoaXMpLmFkZENsYXNzKFwianF1ZXJ5LXRhYi1wYWdlci10YWJiYXJcIiksJHRhYj1lKHRoaXMpLmZpbmQoXCJsaVwiKTt2YXIgbj0wOyFmdW5jdGlvbigpe3ZhciBhPXIuc3RhcnQtMTskdGFiLmVxKGEpLmFkZENsYXNzKFwiY3VycmVudFwiKSxlKFwiLlwiK3IuY29udGVudHMpLmhpZGUoKS5lcShhKS5zaG93KCkuYWRkQ2xhc3MoXCJjdXJyZW50XCIpLHQoMSl9KCksJHRhYi5jbGljayhmdW5jdGlvbigpe3ZhciBhPSR0YWIuaW5kZXgodGhpcyk7JHRhYi5yZW1vdmVDbGFzcyhcImN1cnJlbnRcIiksZSh0aGlzKS5hZGRDbGFzcyhcImN1cnJlbnRcIiksZShcIi5cIityLmNvbnRlbnRzKS5yZW1vdmVDbGFzcyhcImN1cnJlbnRcIikuaGlkZSgpLmVxKGEpLmFkZENsYXNzKFwiY3VycmVudFwiKS5mYWRlSW4oci50aW1lKSx0KDEpfSksZShkb2N1bWVudCkub24oXCJjbGlja1wiLFwiI3BhZ2UtbmF2aSBsaSBhXCIsZnVuY3Rpb24oKXtyZXR1cm4hZSh0aGlzKS5oYXNDbGFzcyhcImRpc2FibGVcIikmJih0KGUoXCIjcGFnZS1uYXZpIGxpIGFcIikuaW5kZXgodGhpcykpLCExKX0pLGUod2luZG93KS5vbihcImxvYWQgc2Nyb2xsXCIsZnVuY3Rpb24oKXtuPWUod2luZG93KS5zY3JvbGxUb3AoKX0pfX0oalF1ZXJ5KTtcclxuXHJcblxyXG5cclxuLy8gJCgnI3BhZ2luYXRpb24tY29udGFpbmVyJykucGFnaW5hdGlvbih7XHJcbi8vICAgICBkYXRhU291cmNlOiBbMSwgMiwgMywgNCwgNSwgNiwgN10sXHJcbi8vICAgICBjYWxsYmFjazogZnVuY3Rpb24oZGF0YSwgcGFnaW5hdGlvbikge1xyXG4vLyAgICAgICAgIC8vIHRlbXBsYXRlIG1ldGhvZCBvZiB5b3Vyc2VsZlxyXG4vLyAgICAgICAgIHZhciBodG1sID0gdGVtcGxhdGUoZGF0YSk7XHJcbi8vICAgICAgICAgJCgnI2RhdGEtY29udGFpbmVyJykuaHRtbChodG1sKTtcclxuLy8gICAgIH1cclxuLy8gfSkiLCJ2YXIgZmFpbHMgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvZmFpbHMnKTtcbnZhciB3ZWxsS25vd25TeW1ib2wgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvd2VsbC1rbm93bi1zeW1ib2wnKTtcbnZhciBWOF9WRVJTSU9OID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2VuZ2luZS12OC12ZXJzaW9uJyk7XG5cbnZhciBTUEVDSUVTID0gd2VsbEtub3duU3ltYm9sKCdzcGVjaWVzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKE1FVEhPRF9OQU1FKSB7XG4gIC8vIFdlIGNhbid0IHVzZSB0aGlzIGZlYXR1cmUgZGV0ZWN0aW9uIGluIFY4IHNpbmNlIGl0IGNhdXNlc1xuICAvLyBkZW9wdGltaXphdGlvbiBhbmQgc2VyaW91cyBwZXJmb3JtYW5jZSBkZWdyYWRhdGlvblxuICAvLyBodHRwczovL2dpdGh1Yi5jb20vemxvaXJvY2svY29yZS1qcy9pc3N1ZXMvNjc3XG4gIHJldHVybiBWOF9WRVJTSU9OID49IDUxIHx8ICFmYWlscyhmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGFycmF5ID0gW107XG4gICAgdmFyIGNvbnN0cnVjdG9yID0gYXJyYXkuY29uc3RydWN0b3IgPSB7fTtcbiAgICBjb25zdHJ1Y3RvcltTUEVDSUVTXSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB7IGZvbzogMSB9O1xuICAgIH07XG4gICAgcmV0dXJuIGFycmF5W01FVEhPRF9OQU1FXShCb29sZWFuKS5mb28gIT09IDE7XG4gIH0pO1xufTtcbiIsIid1c2Ugc3RyaWN0JztcbnZhciB0b1ByaW1pdGl2ZSA9IHJlcXVpcmUoJy4uL2ludGVybmFscy90by1wcmltaXRpdmUnKTtcbnZhciBkZWZpbmVQcm9wZXJ0eU1vZHVsZSA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9vYmplY3QtZGVmaW5lLXByb3BlcnR5Jyk7XG52YXIgY3JlYXRlUHJvcGVydHlEZXNjcmlwdG9yID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2NyZWF0ZS1wcm9wZXJ0eS1kZXNjcmlwdG9yJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKG9iamVjdCwga2V5LCB2YWx1ZSkge1xuICB2YXIgcHJvcGVydHlLZXkgPSB0b1ByaW1pdGl2ZShrZXkpO1xuICBpZiAocHJvcGVydHlLZXkgaW4gb2JqZWN0KSBkZWZpbmVQcm9wZXJ0eU1vZHVsZS5mKG9iamVjdCwgcHJvcGVydHlLZXksIGNyZWF0ZVByb3BlcnR5RGVzY3JpcHRvcigwLCB2YWx1ZSkpO1xuICBlbHNlIG9iamVjdFtwcm9wZXJ0eUtleV0gPSB2YWx1ZTtcbn07XG4iLCJ2YXIgZ2V0QnVpbHRJbiA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9nZXQtYnVpbHQtaW4nKTtcblxubW9kdWxlLmV4cG9ydHMgPSBnZXRCdWlsdEluKCduYXZpZ2F0b3InLCAndXNlckFnZW50JykgfHwgJyc7XG4iLCJ2YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2dsb2JhbCcpO1xudmFyIHVzZXJBZ2VudCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9lbmdpbmUtdXNlci1hZ2VudCcpO1xuXG52YXIgcHJvY2VzcyA9IGdsb2JhbC5wcm9jZXNzO1xudmFyIHZlcnNpb25zID0gcHJvY2VzcyAmJiBwcm9jZXNzLnZlcnNpb25zO1xudmFyIHY4ID0gdmVyc2lvbnMgJiYgdmVyc2lvbnMudjg7XG52YXIgbWF0Y2gsIHZlcnNpb247XG5cbmlmICh2OCkge1xuICBtYXRjaCA9IHY4LnNwbGl0KCcuJyk7XG4gIHZlcnNpb24gPSBtYXRjaFswXSArIG1hdGNoWzFdO1xufSBlbHNlIGlmICh1c2VyQWdlbnQpIHtcbiAgbWF0Y2ggPSB1c2VyQWdlbnQubWF0Y2goL0VkZ2VcXC8oXFxkKykvKTtcbiAgaWYgKCFtYXRjaCB8fCBtYXRjaFsxXSA+PSA3NCkge1xuICAgIG1hdGNoID0gdXNlckFnZW50Lm1hdGNoKC9DaHJvbWVcXC8oXFxkKykvKTtcbiAgICBpZiAobWF0Y2gpIHZlcnNpb24gPSBtYXRjaFsxXTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHZlcnNpb24gJiYgK3ZlcnNpb247XG4iLCJ2YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2dsb2JhbCcpO1xudmFyIHRyaW0gPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvc3RyaW5nLXRyaW0nKS50cmltO1xudmFyIHdoaXRlc3BhY2VzID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3doaXRlc3BhY2VzJyk7XG5cbnZhciAkcGFyc2VJbnQgPSBnbG9iYWwucGFyc2VJbnQ7XG52YXIgaGV4ID0gL15bKy1dPzBbWHhdLztcbnZhciBGT1JDRUQgPSAkcGFyc2VJbnQod2hpdGVzcGFjZXMgKyAnMDgnKSAhPT0gOCB8fCAkcGFyc2VJbnQod2hpdGVzcGFjZXMgKyAnMHgxNicpICE9PSAyMjtcblxuLy8gYHBhcnNlSW50YCBtZXRob2Rcbi8vIGh0dHBzOi8vdGMzOS5lcy9lY21hMjYyLyNzZWMtcGFyc2VpbnQtc3RyaW5nLXJhZGl4XG5tb2R1bGUuZXhwb3J0cyA9IEZPUkNFRCA/IGZ1bmN0aW9uIHBhcnNlSW50KHN0cmluZywgcmFkaXgpIHtcbiAgdmFyIFMgPSB0cmltKFN0cmluZyhzdHJpbmcpKTtcbiAgcmV0dXJuICRwYXJzZUludChTLCAocmFkaXggPj4+IDApIHx8IChoZXgudGVzdChTKSA/IDE2IDogMTApKTtcbn0gOiAkcGFyc2VJbnQ7XG4iLCJ2YXIgcmVxdWlyZU9iamVjdENvZXJjaWJsZSA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9yZXF1aXJlLW9iamVjdC1jb2VyY2libGUnKTtcbnZhciB3aGl0ZXNwYWNlcyA9IHJlcXVpcmUoJy4uL2ludGVybmFscy93aGl0ZXNwYWNlcycpO1xuXG52YXIgd2hpdGVzcGFjZSA9ICdbJyArIHdoaXRlc3BhY2VzICsgJ10nO1xudmFyIGx0cmltID0gUmVnRXhwKCdeJyArIHdoaXRlc3BhY2UgKyB3aGl0ZXNwYWNlICsgJyonKTtcbnZhciBydHJpbSA9IFJlZ0V4cCh3aGl0ZXNwYWNlICsgd2hpdGVzcGFjZSArICcqJCcpO1xuXG4vLyBgU3RyaW5nLnByb3RvdHlwZS57IHRyaW0sIHRyaW1TdGFydCwgdHJpbUVuZCwgdHJpbUxlZnQsIHRyaW1SaWdodCB9YCBtZXRob2RzIGltcGxlbWVudGF0aW9uXG52YXIgY3JlYXRlTWV0aG9kID0gZnVuY3Rpb24gKFRZUEUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uICgkdGhpcykge1xuICAgIHZhciBzdHJpbmcgPSBTdHJpbmcocmVxdWlyZU9iamVjdENvZXJjaWJsZSgkdGhpcykpO1xuICAgIGlmIChUWVBFICYgMSkgc3RyaW5nID0gc3RyaW5nLnJlcGxhY2UobHRyaW0sICcnKTtcbiAgICBpZiAoVFlQRSAmIDIpIHN0cmluZyA9IHN0cmluZy5yZXBsYWNlKHJ0cmltLCAnJyk7XG4gICAgcmV0dXJuIHN0cmluZztcbiAgfTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICAvLyBgU3RyaW5nLnByb3RvdHlwZS57IHRyaW1MZWZ0LCB0cmltU3RhcnQgfWAgbWV0aG9kc1xuICAvLyBodHRwczovL3RjMzkuZXMvZWNtYTI2Mi8jc2VjLXN0cmluZy5wcm90b3R5cGUudHJpbXN0YXJ0XG4gIHN0YXJ0OiBjcmVhdGVNZXRob2QoMSksXG4gIC8vIGBTdHJpbmcucHJvdG90eXBlLnsgdHJpbVJpZ2h0LCB0cmltRW5kIH1gIG1ldGhvZHNcbiAgLy8gaHR0cHM6Ly90YzM5LmVzL2VjbWEyNjIvI3NlYy1zdHJpbmcucHJvdG90eXBlLnRyaW1lbmRcbiAgZW5kOiBjcmVhdGVNZXRob2QoMiksXG4gIC8vIGBTdHJpbmcucHJvdG90eXBlLnRyaW1gIG1ldGhvZFxuICAvLyBodHRwczovL3RjMzkuZXMvZWNtYTI2Mi8jc2VjLXN0cmluZy5wcm90b3R5cGUudHJpbVxuICB0cmltOiBjcmVhdGVNZXRob2QoMylcbn07XG4iLCIvLyBhIHN0cmluZyBvZiBhbGwgdmFsaWQgdW5pY29kZSB3aGl0ZXNwYWNlc1xuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG1heC1sZW5cbm1vZHVsZS5leHBvcnRzID0gJ1xcdTAwMDlcXHUwMDBBXFx1MDAwQlxcdTAwMENcXHUwMDBEXFx1MDAyMFxcdTAwQTBcXHUxNjgwXFx1MjAwMFxcdTIwMDFcXHUyMDAyXFx1MjAwM1xcdTIwMDRcXHUyMDA1XFx1MjAwNlxcdTIwMDdcXHUyMDA4XFx1MjAwOVxcdTIwMEFcXHUyMDJGXFx1MjA1RlxcdTMwMDBcXHUyMDI4XFx1MjAyOVxcdUZFRkYnO1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyICQgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvZXhwb3J0Jyk7XG52YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvaXMtb2JqZWN0Jyk7XG52YXIgaXNBcnJheSA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9pcy1hcnJheScpO1xudmFyIHRvQWJzb2x1dGVJbmRleCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy90by1hYnNvbHV0ZS1pbmRleCcpO1xudmFyIHRvTGVuZ3RoID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3RvLWxlbmd0aCcpO1xudmFyIHRvSW5kZXhlZE9iamVjdCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy90by1pbmRleGVkLW9iamVjdCcpO1xudmFyIGNyZWF0ZVByb3BlcnR5ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2NyZWF0ZS1wcm9wZXJ0eScpO1xudmFyIHdlbGxLbm93blN5bWJvbCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy93ZWxsLWtub3duLXN5bWJvbCcpO1xudmFyIGFycmF5TWV0aG9kSGFzU3BlY2llc1N1cHBvcnQgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvYXJyYXktbWV0aG9kLWhhcy1zcGVjaWVzLXN1cHBvcnQnKTtcbnZhciBhcnJheU1ldGhvZFVzZXNUb0xlbmd0aCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hcnJheS1tZXRob2QtdXNlcy10by1sZW5ndGgnKTtcblxudmFyIEhBU19TUEVDSUVTX1NVUFBPUlQgPSBhcnJheU1ldGhvZEhhc1NwZWNpZXNTdXBwb3J0KCdzbGljZScpO1xudmFyIFVTRVNfVE9fTEVOR1RIID0gYXJyYXlNZXRob2RVc2VzVG9MZW5ndGgoJ3NsaWNlJywgeyBBQ0NFU1NPUlM6IHRydWUsIDA6IDAsIDE6IDIgfSk7XG5cbnZhciBTUEVDSUVTID0gd2VsbEtub3duU3ltYm9sKCdzcGVjaWVzJyk7XG52YXIgbmF0aXZlU2xpY2UgPSBbXS5zbGljZTtcbnZhciBtYXggPSBNYXRoLm1heDtcblxuLy8gYEFycmF5LnByb3RvdHlwZS5zbGljZWAgbWV0aG9kXG4vLyBodHRwczovL3RjMzkuZXMvZWNtYTI2Mi8jc2VjLWFycmF5LnByb3RvdHlwZS5zbGljZVxuLy8gZmFsbGJhY2sgZm9yIG5vdCBhcnJheS1saWtlIEVTMyBzdHJpbmdzIGFuZCBET00gb2JqZWN0c1xuJCh7IHRhcmdldDogJ0FycmF5JywgcHJvdG86IHRydWUsIGZvcmNlZDogIUhBU19TUEVDSUVTX1NVUFBPUlQgfHwgIVVTRVNfVE9fTEVOR1RIIH0sIHtcbiAgc2xpY2U6IGZ1bmN0aW9uIHNsaWNlKHN0YXJ0LCBlbmQpIHtcbiAgICB2YXIgTyA9IHRvSW5kZXhlZE9iamVjdCh0aGlzKTtcbiAgICB2YXIgbGVuZ3RoID0gdG9MZW5ndGgoTy5sZW5ndGgpO1xuICAgIHZhciBrID0gdG9BYnNvbHV0ZUluZGV4KHN0YXJ0LCBsZW5ndGgpO1xuICAgIHZhciBmaW4gPSB0b0Fic29sdXRlSW5kZXgoZW5kID09PSB1bmRlZmluZWQgPyBsZW5ndGggOiBlbmQsIGxlbmd0aCk7XG4gICAgLy8gaW5saW5lIGBBcnJheVNwZWNpZXNDcmVhdGVgIGZvciB1c2FnZSBuYXRpdmUgYEFycmF5I3NsaWNlYCB3aGVyZSBpdCdzIHBvc3NpYmxlXG4gICAgdmFyIENvbnN0cnVjdG9yLCByZXN1bHQsIG47XG4gICAgaWYgKGlzQXJyYXkoTykpIHtcbiAgICAgIENvbnN0cnVjdG9yID0gTy5jb25zdHJ1Y3RvcjtcbiAgICAgIC8vIGNyb3NzLXJlYWxtIGZhbGxiYWNrXG4gICAgICBpZiAodHlwZW9mIENvbnN0cnVjdG9yID09ICdmdW5jdGlvbicgJiYgKENvbnN0cnVjdG9yID09PSBBcnJheSB8fCBpc0FycmF5KENvbnN0cnVjdG9yLnByb3RvdHlwZSkpKSB7XG4gICAgICAgIENvbnN0cnVjdG9yID0gdW5kZWZpbmVkO1xuICAgICAgfSBlbHNlIGlmIChpc09iamVjdChDb25zdHJ1Y3RvcikpIHtcbiAgICAgICAgQ29uc3RydWN0b3IgPSBDb25zdHJ1Y3RvcltTUEVDSUVTXTtcbiAgICAgICAgaWYgKENvbnN0cnVjdG9yID09PSBudWxsKSBDb25zdHJ1Y3RvciA9IHVuZGVmaW5lZDtcbiAgICAgIH1cbiAgICAgIGlmIChDb25zdHJ1Y3RvciA9PT0gQXJyYXkgfHwgQ29uc3RydWN0b3IgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXR1cm4gbmF0aXZlU2xpY2UuY2FsbChPLCBrLCBmaW4pO1xuICAgICAgfVxuICAgIH1cbiAgICByZXN1bHQgPSBuZXcgKENvbnN0cnVjdG9yID09PSB1bmRlZmluZWQgPyBBcnJheSA6IENvbnN0cnVjdG9yKShtYXgoZmluIC0gaywgMCkpO1xuICAgIGZvciAobiA9IDA7IGsgPCBmaW47IGsrKywgbisrKSBpZiAoayBpbiBPKSBjcmVhdGVQcm9wZXJ0eShyZXN1bHQsIG4sIE9ba10pO1xuICAgIHJlc3VsdC5sZW5ndGggPSBuO1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cbn0pO1xuIiwidmFyICQgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvZXhwb3J0Jyk7XG52YXIgcGFyc2VJbnRJbXBsZW1lbnRhdGlvbiA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9udW1iZXItcGFyc2UtaW50Jyk7XG5cbi8vIGBwYXJzZUludGAgbWV0aG9kXG4vLyBodHRwczovL3RjMzkuZXMvZWNtYTI2Mi8jc2VjLXBhcnNlaW50LXN0cmluZy1yYWRpeFxuJCh7IGdsb2JhbDogdHJ1ZSwgZm9yY2VkOiBwYXJzZUludCAhPSBwYXJzZUludEltcGxlbWVudGF0aW9uIH0sIHtcbiAgcGFyc2VJbnQ6IHBhcnNlSW50SW1wbGVtZW50YXRpb25cbn0pO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==