(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/app.js":
/*!***********************!*\
  !*** ./assets/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.find.js */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.string.split.js */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.string.starts-with.js */ "./node_modules/core-js/modules/es.string.starts-with.js");
/* harmony import */ var core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var owl_carousel_dist_assets_owl_carousel_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! owl.carousel/dist/assets/owl.carousel.css */ "./node_modules/owl.carousel/dist/assets/owl.carousel.css");
/* harmony import */ var owl_carousel_dist_assets_owl_carousel_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(owl_carousel_dist_assets_owl_carousel_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! owl.carousel */ "./node_modules/owl.carousel/dist/owl.carousel.js");
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(owl_carousel__WEBPACK_IMPORTED_MODULE_7__);








__webpack_require__(/*! ./styles/app.css */ "./assets/styles/app.css");

var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

__webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");



window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 15 || document.documentElement.scrollTop > 15) {
    $('.navbar').addClass("fixed-top");
    $('.bg-emp-transparent').addClass('bg-emp-blue').removeClass('bg-emp-transparent');
    $('.navbar-brand').addClass("oval-emp").removeClass('oval-empTransparent');
    ;
  } else {
    $('.navbar ').removeClass("fixed-top");
    $('.navbar').addClass('bg-emp-transparent').removeClass('bg-emp-blue');
    $('.navbar-brand').addClass("oval-empTransparent").removeClass('oval-emp');
    ;
  }
}

window.onload = function (event) {
  if (document.documentElement.scrollTop > 25) {
    //$('.navbar').addClass("fixed-top");
    $('.navbar').addClass('bg-emp-blue');
  }
}; //Hamburger menu change on click


$(document).ready(function () {
  $('#ham-icon').click(function () {
    $('.icon-ham').toggleClass('active');
    console.log("icon clicked"); // if ($('navbar-collapse').hasClass('show')){
    //   $('body').css('overflow-y','hidden !important');
    // };
  });
});
$(document).ready(function () {
  var one = $('.owl-fine-homes');
  var two = $('.owl-carousel');
  var three = $('.owl-prop-videos');
  var four = $('#owl-portals');
  one.owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"],
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  }); // calling owl carousel-------------------------------------------------------------------------------------------------

  two.owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"],
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      },
      1600: {
        items: 3
      }
    }
  });
  three.owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"],
    autoplay: false,
    autoplayHoverPause: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });
});
$(document).ready(function () {
  $("#owl-portals").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-caret-left'></i>", "<i class='fa fa-caret-right'></i>"],
    autoplay: false,
    autoplayHoverPause: false,
    responsive: {
      0: {
        items: 3
      },
      600: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  });
}); //------------------------------------------------------------------------------------------------------------------=-----
//$( "#cap_one" ).append( "<p>Test</p>" );

$(document).delegate("cap_one", "click", function () {
  window.location = $(this).find("a").attr("href");
  console.log("botton is clicked");
});
$('#carousel-home').on('slide.bs.carousel', function () {
  console.log('item-changed');
  $('.carousel-caption').hide();
}).on('slid.bs.carousel', function (e) {
  $('.active .carousel-caption').fadeIn('slow');
});

if ($(window).width() <= 767) {
  console.log('mobile device');
}

$(window).resize(function () {
  if ($('#service-icon-container').width() <= 767) {
    // is mobile device
    console.log('mobile device');
  }
});
var $item = $('.service-img-class'),
    //Cache your DOM selector
visible = 2,
    //Set the number of items that will be visible
index = 0,
    //Starting index
endIndex = $item.length / visible - 1; //End index

$('#arrow-img-right').click(function () {
  if (index < endIndex) {
    index++;
    $item.animate({
      'left': '-=200px'
    });
  }
});
$('#arrow-img-left').click(function () {
  if (index > 0) {
    index--;
    $item.animate({
      'left': '+=200px'
    });
  }
}); // Enable Carousel Controls

$(".left").click(function () {
  $("#carousel-properties").carousel("prev");
}); //-------------------------------------------Animate on scroll------------------------------------------------------------

var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = window_top_position + window_height;
  $.each($animation_elements, function () {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = element_top_position + element_height; //check to see if this current container is within viewport

    if (element_bottom_position >= window_top_position && element_top_position <= window_bottom_position) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll', check_if_in_view); //-------------------------------------------Animate on scroll------------------------------------------------------------
//-------------------------------------------Number animation------------------------------------------------------------

var section = document.querySelector('.numbers');
var hasEntered = false;
window.addEventListener('scroll', function (e) {
  var shouldAnimate = window.scrollY + window.innerHeight >= section.offsetTop;

  if (shouldAnimate && !hasEntered) {
    hasEntered = true;
    $('.value').each(function () {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 4000,
        easing: 'swing',
        step: function step(now) {
          $(this).text(Math.ceil(now));
        }
      });
    });
  }
}); // mortgage calculater
// function calculateMortgage(p, r, n) {
//   r = percentToDecimal(r);
//   n = yearsToMonths(n);
//   var pmt = (r * p) / (1 - (Math.pow((1 + r), (-n))));
//   return parseFloat(pmt.toFixed(2));
// }
// function percentToDecimal(percent) {
//   return (percent / 12) / 100;
// }
// function yearsToMonths(year) {
//   return year * 12;
// }
// var htmlEl = document.getElementById("outMonthly");
// function postPayments(pmt) {
//   htmlEl.innerText = "£" + pmt;
// }
// var btn = document.getElementById("btnCalculate");
// btn.onclick = function() {
//   var cost = document.getElementById("inCost").value.replace('£', '');
//   var downPayment = document.getElementById("inDown").value.replace('£', '');
//   var interest = document.getElementById("inInterest").value.replace('%', '');
//   var term = document.getElementById("inTerm").value.replace(' years', '');
//   if (cost == "" && downPayment == "" && interest == "" && term == "") {
//     htmlEl.innerText = "Please fill out all fields.";
//     return false;
//   }
//   if (cost < 0 || cost == "" || isNaN(cost)) {
//     htmlEl.innerText = "Please enter a valid loan amount.";
//     return false;
//   }
//   if (downPayment < 0 || downPayment == "" || isNaN(downPayment)) {
//     htmlEl.innerText = "Please enter a valid down payment.";
//     return false;
//   }
//   if (interest < 0 || interest == "" || isNaN(interest)) {
//     htmlEl.innerText = "Please enter a valid interest rate.";
//     return false;
//   }
//   if (term < 0 || term == "" || isNaN(term)) {
//     htmlEl.innerText = "Please enter a valid length of loan.";
//     return false;
//   }
//   var amountBorrowed = cost - downPayment;
//   var pmt = calculateMortgage(amountBorrowed, interest, term);
//   postPayments(pmt);
// };
// jQuery(document).ready(function( $ ) {
//   $("#google-reviews").googlePlaces({
//        placeId: 'ChIJvzn34HQTdkgRtelV0I7eRLk' //Find placeID @: https://developers.google.com/places/place-id
//      , render: ['reviews']
//      , min_rating:4
//      , max_rows:5
//      ,rotateTime: false
//      , shorten_names: true
//   });
// });
// $(document).ready(function() {
// videos = $("video"); 
// for(video of videos) {
//   video.pause(); 
// }
// });

$('#myBtn').click(function () {
  var dots = document.getElementById("dots");
  var moreText = document.getElementsByClassName("more");
  var btnText = document.getElementById("myBtn");
  console.log('close');

  if (dots.style.display === "none") {
    dots.style.display = "block";
    btnText.innerHTML = "Read more";

    for (var i = 0; i < moreText.length; i++) {
      moreText[i].style.display = "none";
    }
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";

    for (var _i = 0; _i < moreText.length; _i++) {
      moreText[_i].style.display = "block";
    }
  }
});
/* common fuctions */

function el(selector) {
  return document.querySelector(selector);
}

function els(selector) {
  return document.querySelectorAll(selector);
}

function on(selector, event, action) {
  els(selector).forEach(function (e) {
    return e.addEventListener(event, action);
  });
}

function cookie(name) {
  var c = document.cookie.split('; ').find(function (cookie) {
    return cookie && cookie.startsWith(name + '=');
  });
  return c ? c.split('=')[1] : false;
}
/* popup button hanler */


on('.cookie-popup button', 'click', function () {
  el('.cookie-popup').classList.add('cookie-popup--accepted');
  document.cookie = "cookie-accepted=true";
});
/* popup init hanler */

if (cookie('cookie-accepted') !== "true") {
  el('.cookie-popup').classList.add('cookie-popup--not-accepted');
}
/* page buttons handlers */


function _reset() {
  document.cookie = 'cookie-accepted=false';
  document.location.reload();
}

function _switchMode(cssClass) {
  el('.cookie-popup').classList.toggle(cssClass);
}

$(function () {
  $('.modal-footer').click(function () {
    console.log('close');
  });
});
console.log('Hello Webpack Encore! Edit me in assets/app.js');

/***/ }),

/***/ "./assets/styles/app.css":
/*!*******************************!*\
  !*** ./assets/styles/app.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/app.js","runtime","vendors~app~pagecustom~pagination","vendors~app~pagination","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvYXBwLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9zdHlsZXMvYXBwLmNzcyJdLCJuYW1lcyI6WyJyZXF1aXJlIiwiJCIsIndpbmRvdyIsIm9uc2Nyb2xsIiwic2Nyb2xsRnVuY3Rpb24iLCJkb2N1bWVudCIsImJvZHkiLCJzY3JvbGxUb3AiLCJkb2N1bWVudEVsZW1lbnQiLCJhZGRDbGFzcyIsInJlbW92ZUNsYXNzIiwib25sb2FkIiwiZXZlbnQiLCJyZWFkeSIsImNsaWNrIiwidG9nZ2xlQ2xhc3MiLCJjb25zb2xlIiwibG9nIiwib25lIiwidHdvIiwidGhyZWUiLCJmb3VyIiwib3dsQ2Fyb3VzZWwiLCJsb29wIiwibWFyZ2luIiwibmF2IiwibmF2VGV4dCIsImF1dG9wbGF5IiwiYXV0b3BsYXlIb3ZlclBhdXNlIiwicmVzcG9uc2l2ZSIsIml0ZW1zIiwiZGVsZWdhdGUiLCJsb2NhdGlvbiIsImZpbmQiLCJhdHRyIiwib24iLCJoaWRlIiwiZSIsImZhZGVJbiIsIndpZHRoIiwicmVzaXplIiwiJGl0ZW0iLCJ2aXNpYmxlIiwiaW5kZXgiLCJlbmRJbmRleCIsImxlbmd0aCIsImFuaW1hdGUiLCJjYXJvdXNlbCIsIiRhbmltYXRpb25fZWxlbWVudHMiLCIkd2luZG93IiwiY2hlY2tfaWZfaW5fdmlldyIsIndpbmRvd19oZWlnaHQiLCJoZWlnaHQiLCJ3aW5kb3dfdG9wX3Bvc2l0aW9uIiwid2luZG93X2JvdHRvbV9wb3NpdGlvbiIsImVhY2giLCIkZWxlbWVudCIsImVsZW1lbnRfaGVpZ2h0Iiwib3V0ZXJIZWlnaHQiLCJlbGVtZW50X3RvcF9wb3NpdGlvbiIsIm9mZnNldCIsInRvcCIsImVsZW1lbnRfYm90dG9tX3Bvc2l0aW9uIiwidHJpZ2dlciIsInNlY3Rpb24iLCJxdWVyeVNlbGVjdG9yIiwiaGFzRW50ZXJlZCIsImFkZEV2ZW50TGlzdGVuZXIiLCJzaG91bGRBbmltYXRlIiwic2Nyb2xsWSIsImlubmVySGVpZ2h0Iiwib2Zmc2V0VG9wIiwicHJvcCIsIkNvdW50ZXIiLCJ0ZXh0IiwiZHVyYXRpb24iLCJlYXNpbmciLCJzdGVwIiwibm93IiwiTWF0aCIsImNlaWwiLCJkb3RzIiwiZ2V0RWxlbWVudEJ5SWQiLCJtb3JlVGV4dCIsImdldEVsZW1lbnRzQnlDbGFzc05hbWUiLCJidG5UZXh0Iiwic3R5bGUiLCJkaXNwbGF5IiwiaW5uZXJIVE1MIiwiaSIsImVsIiwic2VsZWN0b3IiLCJlbHMiLCJxdWVyeVNlbGVjdG9yQWxsIiwiYWN0aW9uIiwiZm9yRWFjaCIsImNvb2tpZSIsIm5hbWUiLCJjIiwic3BsaXQiLCJzdGFydHNXaXRoIiwiY2xhc3NMaXN0IiwiYWRkIiwiX3Jlc2V0IiwicmVsb2FkIiwiX3N3aXRjaE1vZGUiLCJjc3NDbGFzcyIsInRvZ2dsZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7O0FBQ0NBLG1CQUFPLENBQUUsaURBQUYsQ0FBUDs7QUFHRCxJQUFNQyxDQUFDLEdBQUdELG1CQUFPLENBQUMsb0RBQUQsQ0FBakI7O0FBRUFBLG1CQUFPLENBQUMsZ0VBQUQsQ0FBUDs7QUFFQTs7QUFHQUUsTUFBTSxDQUFDQyxRQUFQLEdBQWtCLFlBQVc7QUFBQ0MsZ0JBQWM7QUFBRyxDQUEvQzs7QUFFQSxTQUFTQSxjQUFULEdBQTBCO0FBQ3hCLE1BQUlDLFFBQVEsQ0FBQ0MsSUFBVCxDQUFjQyxTQUFkLEdBQTBCLEVBQTFCLElBQWdDRixRQUFRLENBQUNHLGVBQVQsQ0FBeUJELFNBQXpCLEdBQXFDLEVBQXpFLEVBQTZFO0FBRzNFTixLQUFDLENBQUMsU0FBRCxDQUFELENBQWFRLFFBQWIsQ0FBc0IsV0FBdEI7QUFLQVIsS0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJRLFFBQXpCLENBQWtDLGFBQWxDLEVBQWlEQyxXQUFqRCxDQUE2RCxvQkFBN0Q7QUFDQVQsS0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQlEsUUFBbkIsQ0FBNEIsVUFBNUIsRUFBd0NDLFdBQXhDLENBQW9ELHFCQUFwRDtBQUEyRTtBQUU1RSxHQVhELE1BV007QUFHSlQsS0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjUyxXQUFkLENBQTBCLFdBQTFCO0FBR0FULEtBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYVEsUUFBYixDQUFzQixvQkFBdEIsRUFBNENDLFdBQTVDLENBQXdELGFBQXhEO0FBQ0FULEtBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJRLFFBQW5CLENBQTRCLHFCQUE1QixFQUFtREMsV0FBbkQsQ0FBK0QsVUFBL0Q7QUFBMkU7QUFFNUU7QUFHRjs7QUFFRFIsTUFBTSxDQUFDUyxNQUFQLEdBQWdCLFVBQUNDLEtBQUQsRUFBVztBQUN6QixNQUFHUCxRQUFRLENBQUNHLGVBQVQsQ0FBeUJELFNBQXpCLEdBQXFDLEVBQXhDLEVBQTJDO0FBRXpDO0FBQ0FOLEtBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYVEsUUFBYixDQUFzQixhQUF0QjtBQUVEO0FBRUYsQ0FSRCxDLENBZUE7OztBQUNBUixDQUFDLENBQUNJLFFBQUQsQ0FBRCxDQUFZUSxLQUFaLENBQWtCLFlBQVU7QUFDMUJaLEdBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZWEsS0FBZixDQUFxQixZQUFVO0FBQzdCYixLQUFDLENBQUMsV0FBRCxDQUFELENBQWVjLFdBQWYsQ0FBMkIsUUFBM0I7QUFDQUMsV0FBTyxDQUFDQyxHQUFSLENBQVksY0FBWixFQUY2QixDQUc3QjtBQUVBO0FBQ0E7QUFDRCxHQVBEO0FBUUQsQ0FURDtBQVlBaEIsQ0FBQyxDQUFDSSxRQUFELENBQUQsQ0FBWVEsS0FBWixDQUFrQixZQUFXO0FBRzNCLE1BQUlLLEdBQUcsR0FBR2pCLENBQUMsQ0FBQyxpQkFBRCxDQUFYO0FBQ0EsTUFBSWtCLEdBQUcsR0FBR2xCLENBQUMsQ0FBQyxlQUFELENBQVg7QUFDQSxNQUFJbUIsS0FBSyxHQUFHbkIsQ0FBQyxDQUFDLGtCQUFELENBQWI7QUFDQSxNQUFJb0IsSUFBSSxHQUFHcEIsQ0FBQyxDQUFDLGNBQUQsQ0FBWjtBQUVBaUIsS0FBRyxDQUFDSSxXQUFKLENBQWdCO0FBQ2RDLFFBQUksRUFBRSxJQURRO0FBRWRDLFVBQU0sRUFBRSxFQUZNO0FBR2RDLE9BQUcsRUFBRSxJQUhTO0FBSWRDLFdBQU8sRUFBRSxDQUNQLGtDQURPLEVBRVAsbUNBRk8sQ0FKSztBQVFkQyxZQUFRLEVBQUUsSUFSSTtBQVNkQyxzQkFBa0IsRUFBRSxJQVROO0FBVWRDLGNBQVUsRUFBRTtBQUNWLFNBQUc7QUFDREMsYUFBSyxFQUFFO0FBRE4sT0FETztBQUlWLFdBQUs7QUFDSEEsYUFBSyxFQUFFO0FBREosT0FKSztBQU9WLFlBQU07QUFDSkEsYUFBSyxFQUFFO0FBREg7QUFQSTtBQVZFLEdBQWhCLEVBUjJCLENBaUM3Qjs7QUFHQVgsS0FBRyxDQUFDRyxXQUFKLENBQWdCO0FBQ2RDLFFBQUksRUFBRSxJQURRO0FBRWRDLFVBQU0sRUFBRSxFQUZNO0FBR2RDLE9BQUcsRUFBRSxJQUhTO0FBSWRDLFdBQU8sRUFBRSxDQUNQLGtDQURPLEVBRVAsbUNBRk8sQ0FKSztBQVFkQyxZQUFRLEVBQUUsSUFSSTtBQVNkQyxzQkFBa0IsRUFBRSxJQVROO0FBVWRDLGNBQVUsRUFBRTtBQUNWLFNBQUc7QUFDREMsYUFBSyxFQUFFO0FBRE4sT0FETztBQUlWLFdBQUs7QUFDSEEsYUFBSyxFQUFFO0FBREosT0FKSztBQU9WLFlBQU07QUFDSkEsYUFBSyxFQUFFO0FBREgsT0FQSTtBQVVWLFlBQU07QUFDSkEsYUFBSyxFQUFFO0FBREg7QUFWSTtBQVZFLEdBQWhCO0FBMEJBVixPQUFLLENBQUNFLFdBQU4sQ0FBa0I7QUFDaEJDLFFBQUksRUFBRSxJQURVO0FBRWhCQyxVQUFNLEVBQUUsRUFGUTtBQUdoQkMsT0FBRyxFQUFFLElBSFc7QUFJaEJDLFdBQU8sRUFBRSxDQUNQLGtDQURPLEVBRVAsbUNBRk8sQ0FKTztBQVFoQkMsWUFBUSxFQUFFLEtBUk07QUFTaEJDLHNCQUFrQixFQUFFLEtBVEo7QUFVaEJDLGNBQVUsRUFBRTtBQUNWLFNBQUc7QUFDREMsYUFBSyxFQUFFO0FBRE4sT0FETztBQUlWLFdBQUs7QUFDSEEsYUFBSyxFQUFFO0FBREosT0FKSztBQU9WLFlBQU07QUFDSkEsYUFBSyxFQUFFO0FBREg7QUFQSTtBQVZJLEdBQWxCO0FBeUJDLENBdkZEO0FBeUZBN0IsQ0FBQyxDQUFDSSxRQUFELENBQUQsQ0FBWVEsS0FBWixDQUFrQixZQUFZO0FBQzVCWixHQUFDLENBQUMsY0FBRCxDQUFELENBQWtCcUIsV0FBbEIsQ0FBOEI7QUFHOUJDLFFBQUksRUFBRSxJQUh3QjtBQUk5QkMsVUFBTSxFQUFFLEVBSnNCO0FBSzlCQyxPQUFHLEVBQUUsSUFMeUI7QUFNOUJDLFdBQU8sRUFBRSxDQUNQLGtDQURPLEVBRVAsbUNBRk8sQ0FOcUI7QUFVOUJDLFlBQVEsRUFBRSxLQVZvQjtBQVc5QkMsc0JBQWtCLEVBQUUsS0FYVTtBQVk5QkMsY0FBVSxFQUFFO0FBQ1YsU0FBRztBQUNEQyxhQUFLLEVBQUU7QUFETixPQURPO0FBSVYsV0FBSztBQUNIQSxhQUFLLEVBQUU7QUFESixPQUpLO0FBT1YsWUFBTTtBQUNKQSxhQUFLLEVBQUU7QUFESDtBQVBJO0FBWmtCLEdBQTlCO0FBMEJELENBM0JELEUsQ0E4QkE7QUFDQTs7QUFDQTdCLENBQUMsQ0FBQ0ksUUFBRCxDQUFELENBQVkwQixRQUFaLENBQXFCLFNBQXJCLEVBQWdDLE9BQWhDLEVBQXlDLFlBQVc7QUFDbEQ3QixRQUFNLENBQUM4QixRQUFQLEdBQWtCL0IsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0MsSUFBUixDQUFhLEdBQWIsRUFBa0JDLElBQWxCLENBQXVCLE1BQXZCLENBQWxCO0FBQ0FsQixTQUFPLENBQUNDLEdBQVIsQ0FBWSxtQkFBWjtBQUNELENBSEQ7QUFNQWhCLENBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9Ca0MsRUFBcEIsQ0FBdUIsbUJBQXZCLEVBQTRDLFlBQVk7QUFJdERuQixTQUFPLENBQUNDLEdBQVIsQ0FBWSxjQUFaO0FBQ0FoQixHQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1Qm1DLElBQXZCO0FBQ0QsQ0FORCxFQU1HRCxFQU5ILENBTU0sa0JBTk4sRUFNMEIsVUFBU0UsQ0FBVCxFQUFZO0FBQ3BDcEMsR0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JxQyxNQUEvQixDQUFzQyxNQUF0QztBQUNELENBUkQ7O0FBV0EsSUFBR3JDLENBQUMsQ0FBQ0MsTUFBRCxDQUFELENBQVVxQyxLQUFWLE1BQXFCLEdBQXhCLEVBQTRCO0FBRTFCdkIsU0FBTyxDQUFDQyxHQUFSLENBQVksZUFBWjtBQUNEOztBQUVEaEIsQ0FBQyxDQUFDQyxNQUFELENBQUQsQ0FBVXNDLE1BQVYsQ0FBaUIsWUFBVTtBQUV6QixNQUFJdkMsQ0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJzQyxLQUE3QixNQUF3QyxHQUE1QyxFQUFnRDtBQUN6QztBQUNIdkIsV0FBTyxDQUFDQyxHQUFSLENBQVksZUFBWjtBQUNIO0FBSUYsQ0FURDtBQWFBLElBQUl3QixLQUFLLEdBQUd4QyxDQUFDLENBQUMsb0JBQUQsQ0FBYjtBQUFBLElBQXFDO0FBQ3JDeUMsT0FBTyxHQUFHLENBRFY7QUFBQSxJQUNhO0FBQ2JDLEtBQUssR0FBRyxDQUZSO0FBQUEsSUFFVztBQUNYQyxRQUFRLEdBQUtILEtBQUssQ0FBQ0ksTUFBTixHQUFlSCxPQUFqQixHQUE2QixDQUh4QyxDLENBRzJDOztBQUUzQ3pDLENBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCYSxLQUF0QixDQUE0QixZQUFVO0FBQ3RDLE1BQUc2QixLQUFLLEdBQUdDLFFBQVgsRUFBcUI7QUFDbkJELFNBQUs7QUFDTEYsU0FBSyxDQUFDSyxPQUFOLENBQWM7QUFBQyxjQUFPO0FBQVIsS0FBZDtBQUNEO0FBQ0EsQ0FMRDtBQU9BN0MsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJhLEtBQXJCLENBQTJCLFlBQVU7QUFDckMsTUFBRzZCLEtBQUssR0FBRyxDQUFYLEVBQWE7QUFDWEEsU0FBSztBQUNMRixTQUFLLENBQUNLLE9BQU4sQ0FBYztBQUFDLGNBQU87QUFBUixLQUFkO0FBQ0Q7QUFDQSxDQUxELEUsQ0FRQTs7QUFDQTdDLENBQUMsQ0FBQyxPQUFELENBQUQsQ0FBV2EsS0FBWCxDQUFpQixZQUFVO0FBQ3pCYixHQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQjhDLFFBQTFCLENBQW1DLE1BQW5DO0FBQ0QsQ0FGRCxFLENBSUE7O0FBQ0EsSUFBSUMsbUJBQW1CLEdBQUcvQyxDQUFDLENBQUMsb0JBQUQsQ0FBM0I7QUFDQSxJQUFJZ0QsT0FBTyxHQUFHaEQsQ0FBQyxDQUFDQyxNQUFELENBQWY7O0FBRUEsU0FBU2dELGdCQUFULEdBQTRCO0FBQzNCLE1BQUlDLGFBQWEsR0FBR0YsT0FBTyxDQUFDRyxNQUFSLEVBQXBCO0FBQ0EsTUFBSUMsbUJBQW1CLEdBQUdKLE9BQU8sQ0FBQzFDLFNBQVIsRUFBMUI7QUFDQSxNQUFJK0Msc0JBQXNCLEdBQUlELG1CQUFtQixHQUFHRixhQUFwRDtBQUVBbEQsR0FBQyxDQUFDc0QsSUFBRixDQUFPUCxtQkFBUCxFQUE0QixZQUFXO0FBQ3RDLFFBQUlRLFFBQVEsR0FBR3ZELENBQUMsQ0FBQyxJQUFELENBQWhCO0FBQ0EsUUFBSXdELGNBQWMsR0FBR0QsUUFBUSxDQUFDRSxXQUFULEVBQXJCO0FBQ0EsUUFBSUMsb0JBQW9CLEdBQUdILFFBQVEsQ0FBQ0ksTUFBVCxHQUFrQkMsR0FBN0M7QUFDQSxRQUFJQyx1QkFBdUIsR0FBSUgsb0JBQW9CLEdBQUdGLGNBQXRELENBSnNDLENBTXRDOztBQUNBLFFBQUtLLHVCQUF1QixJQUFJVCxtQkFBNUIsSUFDRk0sb0JBQW9CLElBQUlMLHNCQUQxQixFQUNtRDtBQUNqREUsY0FBUSxDQUFDL0MsUUFBVCxDQUFrQixTQUFsQjtBQUNELEtBSEQsTUFHTztBQUNMK0MsY0FBUSxDQUFDOUMsV0FBVCxDQUFxQixTQUFyQjtBQUNEO0FBQ0QsR0FiRDtBQWNBOztBQUVEdUMsT0FBTyxDQUFDZCxFQUFSLENBQVcsZUFBWCxFQUE0QmUsZ0JBQTVCO0FBQ0FELE9BQU8sQ0FBQ2MsT0FBUixDQUFnQixRQUFoQixFQUEwQmIsZ0JBQTFCLEUsQ0FHQTtBQUdBOztBQUVBLElBQUljLE9BQU8sR0FBRzNELFFBQVEsQ0FBQzRELGFBQVQsQ0FBdUIsVUFBdkIsQ0FBZDtBQUNBLElBQUlDLFVBQVUsR0FBRyxLQUFqQjtBQUVBaEUsTUFBTSxDQUFDaUUsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsVUFBQzlCLENBQUQsRUFBTztBQUN4QyxNQUFJK0IsYUFBYSxHQUFJbEUsTUFBTSxDQUFDbUUsT0FBUCxHQUFpQm5FLE1BQU0sQ0FBQ29FLFdBQXpCLElBQXlDTixPQUFPLENBQUNPLFNBQXJFOztBQUVBLE1BQUlILGFBQWEsSUFBSSxDQUFDRixVQUF0QixFQUFrQztBQUNoQ0EsY0FBVSxHQUFHLElBQWI7QUFFQ2pFLEtBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWXNELElBQVosQ0FBaUIsWUFBWTtBQUM1QnRELE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXVFLElBQVIsQ0FBYSxTQUFiLEVBQXVCLENBQXZCLEVBQTBCMUIsT0FBMUIsQ0FBa0M7QUFDL0IyQixlQUFPLEVBQUV4RSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVF5RSxJQUFSO0FBRHNCLE9BQWxDLEVBRUc7QUFDQUMsZ0JBQVEsRUFBRSxJQURWO0FBRUFDLGNBQU0sRUFBRSxPQUZSO0FBR0FDLFlBQUksRUFBRSxjQUFVQyxHQUFWLEVBQWU7QUFDakI3RSxXQUFDLENBQUMsSUFBRCxDQUFELENBQVF5RSxJQUFSLENBQWFLLElBQUksQ0FBQ0MsSUFBTCxDQUFVRixHQUFWLENBQWI7QUFDSDtBQUxELE9BRkg7QUFTQSxLQVZEO0FBWUQ7QUFDRixDQW5CRCxFLENBc0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFHQTdFLENBQUMsQ0FBRSxRQUFGLENBQUQsQ0FBY2EsS0FBZCxDQUFvQixZQUFXO0FBQzdCLE1BQUltRSxJQUFJLEdBQUc1RSxRQUFRLENBQUM2RSxjQUFULENBQXdCLE1BQXhCLENBQVg7QUFDQSxNQUFJQyxRQUFRLEdBQUc5RSxRQUFRLENBQUMrRSxzQkFBVCxDQUFnQyxNQUFoQyxDQUFmO0FBQ0EsTUFBSUMsT0FBTyxHQUFHaEYsUUFBUSxDQUFDNkUsY0FBVCxDQUF3QixPQUF4QixDQUFkO0FBQ0FsRSxTQUFPLENBQUNDLEdBQVIsQ0FBWSxPQUFaOztBQUVBLE1BQUlnRSxJQUFJLENBQUNLLEtBQUwsQ0FBV0MsT0FBWCxLQUF1QixNQUEzQixFQUFtQztBQUNqQ04sUUFBSSxDQUFDSyxLQUFMLENBQVdDLE9BQVgsR0FBcUIsT0FBckI7QUFDQUYsV0FBTyxDQUFDRyxTQUFSLEdBQW9CLFdBQXBCOztBQUNBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR04sUUFBUSxDQUFDdEMsTUFBN0IsRUFBcUM0QyxDQUFDLEVBQXRDLEVBQTBDO0FBQzFDTixjQUFRLENBQUNNLENBQUQsQ0FBUixDQUFZSCxLQUFaLENBQWtCQyxPQUFsQixHQUE0QixNQUE1QjtBQUNDO0FBQ0YsR0FORCxNQU1PO0FBQ0xOLFFBQUksQ0FBQ0ssS0FBTCxDQUFXQyxPQUFYLEdBQXFCLE1BQXJCO0FBQ0FGLFdBQU8sQ0FBQ0csU0FBUixHQUFvQixXQUFwQjs7QUFDQSxTQUFLLElBQUlDLEVBQUMsR0FBRyxDQUFiLEVBQWdCQSxFQUFDLEdBQUdOLFFBQVEsQ0FBQ3RDLE1BQTdCLEVBQXFDNEMsRUFBQyxFQUF0QyxFQUEwQztBQUMxQ04sY0FBUSxDQUFDTSxFQUFELENBQVIsQ0FBWUgsS0FBWixDQUFrQkMsT0FBbEIsR0FBNEIsT0FBNUI7QUFDQztBQUNGO0FBQ0YsQ0FuQkQ7QUFxQkE7O0FBQ0EsU0FBU0csRUFBVCxDQUFZQyxRQUFaLEVBQXNCO0FBQUUsU0FBT3RGLFFBQVEsQ0FBQzRELGFBQVQsQ0FBdUIwQixRQUF2QixDQUFQO0FBQXlDOztBQUNqRSxTQUFTQyxHQUFULENBQWFELFFBQWIsRUFBdUI7QUFBRSxTQUFPdEYsUUFBUSxDQUFDd0YsZ0JBQVQsQ0FBMEJGLFFBQTFCLENBQVA7QUFBNEM7O0FBQ3JFLFNBQVN4RCxFQUFULENBQVl3RCxRQUFaLEVBQXNCL0UsS0FBdEIsRUFBNkJrRixNQUE3QixFQUFxQztBQUFFRixLQUFHLENBQUNELFFBQUQsQ0FBSCxDQUFjSSxPQUFkLENBQXNCLFVBQUExRCxDQUFDO0FBQUEsV0FBSUEsQ0FBQyxDQUFDOEIsZ0JBQUYsQ0FBbUJ2RCxLQUFuQixFQUEwQmtGLE1BQTFCLENBQUo7QUFBQSxHQUF2QjtBQUErRDs7QUFDdEcsU0FBU0UsTUFBVCxDQUFnQkMsSUFBaEIsRUFBc0I7QUFDcEIsTUFBSUMsQ0FBQyxHQUFHN0YsUUFBUSxDQUFDMkYsTUFBVCxDQUFnQkcsS0FBaEIsQ0FBc0IsSUFBdEIsRUFBNEJsRSxJQUE1QixDQUFpQyxVQUFBK0QsTUFBTTtBQUFBLFdBQUlBLE1BQU0sSUFBSUEsTUFBTSxDQUFDSSxVQUFQLENBQWtCSCxJQUFJLEdBQUMsR0FBdkIsQ0FBZDtBQUFBLEdBQXZDLENBQVI7QUFDQSxTQUFPQyxDQUFDLEdBQUdBLENBQUMsQ0FBQ0MsS0FBRixDQUFRLEdBQVIsRUFBYSxDQUFiLENBQUgsR0FBcUIsS0FBN0I7QUFDRDtBQUdEOzs7QUFDQWhFLEVBQUUsQ0FBQyxzQkFBRCxFQUF5QixPQUF6QixFQUFrQyxZQUFNO0FBQ3hDdUQsSUFBRSxDQUFDLGVBQUQsQ0FBRixDQUFvQlcsU0FBcEIsQ0FBOEJDLEdBQTlCLENBQWtDLHdCQUFsQztBQUNBakcsVUFBUSxDQUFDMkYsTUFBVDtBQUNELENBSEMsQ0FBRjtBQUtBOztBQUNBLElBQUlBLE1BQU0sQ0FBQyxpQkFBRCxDQUFOLEtBQThCLE1BQWxDLEVBQTBDO0FBQ3hDTixJQUFFLENBQUMsZUFBRCxDQUFGLENBQW9CVyxTQUFwQixDQUE4QkMsR0FBOUIsQ0FBa0MsNEJBQWxDO0FBQ0Q7QUFJRDs7O0FBRUEsU0FBU0MsTUFBVCxHQUFrQjtBQUNoQmxHLFVBQVEsQ0FBQzJGLE1BQVQsR0FBa0IsdUJBQWxCO0FBQ0EzRixVQUFRLENBQUMyQixRQUFULENBQWtCd0UsTUFBbEI7QUFDRDs7QUFFRCxTQUFTQyxXQUFULENBQXFCQyxRQUFyQixFQUErQjtBQUM3QmhCLElBQUUsQ0FBQyxlQUFELENBQUYsQ0FBb0JXLFNBQXBCLENBQThCTSxNQUE5QixDQUFxQ0QsUUFBckM7QUFDRDs7QUFHRHpHLENBQUMsQ0FBQyxZQUFVO0FBQ1ZBLEdBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJhLEtBQW5CLENBQXlCLFlBQVU7QUFFakNFLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLE9BQVo7QUFDRCxHQUhEO0FBSUQsQ0FMQSxDQUFEO0FBTUFELE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGdEQUFaLEU7Ozs7Ozs7Ozs7O0FDM2JBLHVDIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAnb3dsLmNhcm91c2VsL2Rpc3QvYXNzZXRzL293bC5jYXJvdXNlbC5jc3MnO1xuIHJlcXVpcmUoICcuL3N0eWxlcy9hcHAuY3NzJyk7XG5cblxuY29uc3QgJCA9IHJlcXVpcmUoJ2pxdWVyeScpO1xuXG5yZXF1aXJlKCdib290c3RyYXAnKTtcblxuaW1wb3J0ICdvd2wuY2Fyb3VzZWwnO1xuXG5cbndpbmRvdy5vbnNjcm9sbCA9IGZ1bmN0aW9uKCkge3Njcm9sbEZ1bmN0aW9uKCl9O1xuXG5mdW5jdGlvbiBzY3JvbGxGdW5jdGlvbigpIHtcbiAgaWYgKGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wID4gMTUgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCA+IDE1KSB7XG5cbiAgIFxuICAgICQoJy5uYXZiYXInKS5hZGRDbGFzcyhcImZpeGVkLXRvcFwiKTtcbiAgIFxuXG5cbiAgICBcbiAgICAkKCcuYmctZW1wLXRyYW5zcGFyZW50JykuYWRkQ2xhc3MoJ2JnLWVtcC1ibHVlJykucmVtb3ZlQ2xhc3MoJ2JnLWVtcC10cmFuc3BhcmVudCcpO1xuICAgICQoJy5uYXZiYXItYnJhbmQnKS5hZGRDbGFzcyhcIm92YWwtZW1wXCIpLnJlbW92ZUNsYXNzKCdvdmFsLWVtcFRyYW5zcGFyZW50Jyk7O1xuXG4gIH1lbHNlIHtcbiAgICBcbiAgIFxuICAgICQoJy5uYXZiYXIgJykucmVtb3ZlQ2xhc3MoXCJmaXhlZC10b3BcIik7XG4gICBcbiBcbiAgICAkKCcubmF2YmFyJykuYWRkQ2xhc3MoJ2JnLWVtcC10cmFuc3BhcmVudCcpLnJlbW92ZUNsYXNzKCdiZy1lbXAtYmx1ZScpO1xuICAgICQoJy5uYXZiYXItYnJhbmQnKS5hZGRDbGFzcyhcIm92YWwtZW1wVHJhbnNwYXJlbnRcIikucmVtb3ZlQ2xhc3MoJ292YWwtZW1wJyk7O1xuXG4gIH1cblxuICBcbn1cblxud2luZG93Lm9ubG9hZCA9IChldmVudCkgPT4ge1xuICBpZihkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wID4gMjUpe1xuXG4gICAgLy8kKCcubmF2YmFyJykuYWRkQ2xhc3MoXCJmaXhlZC10b3BcIik7XG4gICAgJCgnLm5hdmJhcicpLmFkZENsYXNzKCdiZy1lbXAtYmx1ZScpO1xuICAgIFxuICB9XG4gIFxufTtcblxuXG5cblxuXG5cbi8vSGFtYnVyZ2VyIG1lbnUgY2hhbmdlIG9uIGNsaWNrXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xuICAkKCcjaGFtLWljb24nKS5jbGljayhmdW5jdGlvbigpe1xuICAgICQoJy5pY29uLWhhbScpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbiAgICBjb25zb2xlLmxvZyhcImljb24gY2xpY2tlZFwiKTtcbiAgICAvLyBpZiAoJCgnbmF2YmFyLWNvbGxhcHNlJykuaGFzQ2xhc3MoJ3Nob3cnKSl7XG5cbiAgICAvLyAgICQoJ2JvZHknKS5jc3MoJ292ZXJmbG93LXknLCdoaWRkZW4gIWltcG9ydGFudCcpO1xuICAgIC8vIH07XG4gIH0pXG59KVxuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuXG5cbiAgdmFyIG9uZSA9ICQoJy5vd2wtZmluZS1ob21lcycpO1xuICB2YXIgdHdvID0gJCgnLm93bC1jYXJvdXNlbCcpO1xuICB2YXIgdGhyZWUgPSAkKCcub3dsLXByb3AtdmlkZW9zJyk7XG4gIHZhciBmb3VyID0gJCgnI293bC1wb3J0YWxzJyk7XG5cbiAgb25lLm93bENhcm91c2VsKHtcbiAgICBsb29wOiB0cnVlLFxuICAgIG1hcmdpbjogMTAsXG4gICAgbmF2OiB0cnVlLFxuICAgIG5hdlRleHQ6IFtcbiAgICAgIFwiPGkgY2xhc3M9J2ZhIGZhLWNhcmV0LWxlZnQnPjwvaT5cIixcbiAgICAgIFwiPGkgY2xhc3M9J2ZhIGZhLWNhcmV0LXJpZ2h0Jz48L2k+XCJcbiAgICBdLFxuICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgIGF1dG9wbGF5SG92ZXJQYXVzZTogdHJ1ZSxcbiAgICByZXNwb25zaXZlOiB7XG4gICAgICAwOiB7XG4gICAgICAgIGl0ZW1zOiAxXG4gICAgICB9LFxuICAgICAgNjAwOiB7XG4gICAgICAgIGl0ZW1zOiAxXG4gICAgICB9LFxuICAgICAgMTAwMDoge1xuICAgICAgICBpdGVtczogMVxuICAgICAgfVxuICAgIH1cbiAgfSk7XG5cblxuXG4vLyBjYWxsaW5nIG93bCBjYXJvdXNlbC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuXG50d28ub3dsQ2Fyb3VzZWwoe1xuICBsb29wOiB0cnVlLFxuICBtYXJnaW46IDEwLFxuICBuYXY6IHRydWUsXG4gIG5hdlRleHQ6IFtcbiAgICBcIjxpIGNsYXNzPSdmYSBmYS1jYXJldC1sZWZ0Jz48L2k+XCIsXG4gICAgXCI8aSBjbGFzcz0nZmEgZmEtY2FyZXQtcmlnaHQnPjwvaT5cIlxuICBdLFxuICBhdXRvcGxheTogdHJ1ZSxcbiAgYXV0b3BsYXlIb3ZlclBhdXNlOiB0cnVlLFxuICByZXNwb25zaXZlOiB7XG4gICAgMDoge1xuICAgICAgaXRlbXM6IDFcbiAgICB9LFxuICAgIDYwMDoge1xuICAgICAgaXRlbXM6IDJcbiAgICB9LFxuICAgIDEwMDA6IHtcbiAgICAgIGl0ZW1zOiAzXG4gICAgfSxcbiAgICAxNjAwOiB7XG4gICAgICBpdGVtczogM1xuICAgIH1cbiAgfVxufSk7XG5cbnRocmVlLm93bENhcm91c2VsKHtcbiAgbG9vcDogdHJ1ZSxcbiAgbWFyZ2luOiAxMCxcbiAgbmF2OiB0cnVlLFxuICBuYXZUZXh0OiBbXG4gICAgXCI8aSBjbGFzcz0nZmEgZmEtY2FyZXQtbGVmdCc+PC9pPlwiLFxuICAgIFwiPGkgY2xhc3M9J2ZhIGZhLWNhcmV0LXJpZ2h0Jz48L2k+XCJcbiAgXSxcbiAgYXV0b3BsYXk6IGZhbHNlLFxuICBhdXRvcGxheUhvdmVyUGF1c2U6IGZhbHNlLFxuICByZXNwb25zaXZlOiB7XG4gICAgMDoge1xuICAgICAgaXRlbXM6IDFcbiAgICB9LFxuICAgIDYwMDoge1xuICAgICAgaXRlbXM6IDFcbiAgICB9LFxuICAgIDEwMDA6IHtcbiAgICAgIGl0ZW1zOiAxXG4gICAgfVxuICB9XG59KTtcblxuXG5cbn0pO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICQoXCIjb3dsLXBvcnRhbHNcIikub3dsQ2Fyb3VzZWwoe1xuXG5cbiAgbG9vcDogdHJ1ZSxcbiAgbWFyZ2luOiAxMCxcbiAgbmF2OiB0cnVlLFxuICBuYXZUZXh0OiBbXG4gICAgXCI8aSBjbGFzcz0nZmEgZmEtY2FyZXQtbGVmdCc+PC9pPlwiLFxuICAgIFwiPGkgY2xhc3M9J2ZhIGZhLWNhcmV0LXJpZ2h0Jz48L2k+XCJcbiAgXSxcbiAgYXV0b3BsYXk6IGZhbHNlLFxuICBhdXRvcGxheUhvdmVyUGF1c2U6IGZhbHNlLFxuICByZXNwb25zaXZlOiB7XG4gICAgMDoge1xuICAgICAgaXRlbXM6IDNcbiAgICB9LFxuICAgIDYwMDoge1xuICAgICAgaXRlbXM6IDNcbiAgICB9LFxuICAgIDEwMDA6IHtcbiAgICAgIGl0ZW1zOiAzXG4gICAgfVxuICB9XG5cbiAgXG59KTtcbn0pO1xuXG5cbi8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tPS0tLS0tXG4vLyQoIFwiI2NhcF9vbmVcIiApLmFwcGVuZCggXCI8cD5UZXN0PC9wPlwiICk7XG4kKGRvY3VtZW50KS5kZWxlZ2F0ZShcImNhcF9vbmVcIiwgXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcbiAgd2luZG93LmxvY2F0aW9uID0gJCh0aGlzKS5maW5kKFwiYVwiKS5hdHRyKFwiaHJlZlwiKTtcbiAgY29uc29sZS5sb2coXCJib3R0b24gaXMgY2xpY2tlZFwiKTtcbn0pO1xuXG5cbiQoJyNjYXJvdXNlbC1ob21lJykub24oJ3NsaWRlLmJzLmNhcm91c2VsJywgZnVuY3Rpb24gKCkge1xuXG4gIFxuXG4gIGNvbnNvbGUubG9nKCdpdGVtLWNoYW5nZWQnKTtcbiAgJCgnLmNhcm91c2VsLWNhcHRpb24nKS5oaWRlKCk7XG59KS5vbignc2xpZC5icy5jYXJvdXNlbCcsIGZ1bmN0aW9uKGUpIHtcbiAgJCgnLmFjdGl2ZSAuY2Fyb3VzZWwtY2FwdGlvbicpLmZhZGVJbignc2xvdycpO1xufSk7XG4gIFxuXG5pZigkKHdpbmRvdykud2lkdGgoKSA8PSA3Njcpe1xuXG4gIGNvbnNvbGUubG9nKCdtb2JpbGUgZGV2aWNlJylcbn1cblxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpeyAgICAgXG5cbiAgaWYgKCQoJyNzZXJ2aWNlLWljb24tY29udGFpbmVyJykud2lkdGgoKSA8PSA3Njcpe1xuICAgICAgICAgLy8gaXMgbW9iaWxlIGRldmljZVxuICAgICAgY29uc29sZS5sb2coJ21vYmlsZSBkZXZpY2UnKVxuICB9XG5cblxuXG59KTtcblxuXG5cbnZhciAkaXRlbSA9ICQoJy5zZXJ2aWNlLWltZy1jbGFzcycpLCAvL0NhY2hlIHlvdXIgRE9NIHNlbGVjdG9yXG52aXNpYmxlID0gMiwgLy9TZXQgdGhlIG51bWJlciBvZiBpdGVtcyB0aGF0IHdpbGwgYmUgdmlzaWJsZVxuaW5kZXggPSAwLCAvL1N0YXJ0aW5nIGluZGV4XG5lbmRJbmRleCA9ICggJGl0ZW0ubGVuZ3RoIC8gdmlzaWJsZSApIC0gMTsgLy9FbmQgaW5kZXhcblxuJCgnI2Fycm93LWltZy1yaWdodCcpLmNsaWNrKGZ1bmN0aW9uKCl7XG5pZihpbmRleCA8IGVuZEluZGV4ICl7XG4gIGluZGV4Kys7XG4gICRpdGVtLmFuaW1hdGUoeydsZWZ0JzonLT0yMDBweCd9KTtcbn1cbn0pO1xuXG4kKCcjYXJyb3ctaW1nLWxlZnQnKS5jbGljayhmdW5jdGlvbigpe1xuaWYoaW5kZXggPiAwKXtcbiAgaW5kZXgtLTsgICAgICAgICAgICBcbiAgJGl0ZW0uYW5pbWF0ZSh7J2xlZnQnOicrPTIwMHB4J30pO1xufVxufSk7XG5cblxuLy8gRW5hYmxlIENhcm91c2VsIENvbnRyb2xzXG4kKFwiLmxlZnRcIikuY2xpY2soZnVuY3Rpb24oKXtcbiAgJChcIiNjYXJvdXNlbC1wcm9wZXJ0aWVzXCIpLmNhcm91c2VsKFwicHJldlwiKTtcbn0pO1xuXG4vLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1BbmltYXRlIG9uIHNjcm9sbC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxudmFyICRhbmltYXRpb25fZWxlbWVudHMgPSAkKCcuYW5pbWF0aW9uLWVsZW1lbnQnKTtcbnZhciAkd2luZG93ID0gJCh3aW5kb3cpO1xuXG5mdW5jdGlvbiBjaGVja19pZl9pbl92aWV3KCkge1xuXHR2YXIgd2luZG93X2hlaWdodCA9ICR3aW5kb3cuaGVpZ2h0KCk7XG5cdHZhciB3aW5kb3dfdG9wX3Bvc2l0aW9uID0gJHdpbmRvdy5zY3JvbGxUb3AoKTtcblx0dmFyIHdpbmRvd19ib3R0b21fcG9zaXRpb24gPSAod2luZG93X3RvcF9wb3NpdGlvbiArIHdpbmRvd19oZWlnaHQpO1xuXG5cdCQuZWFjaCgkYW5pbWF0aW9uX2VsZW1lbnRzLCBmdW5jdGlvbigpIHtcblx0XHR2YXIgJGVsZW1lbnQgPSAkKHRoaXMpO1xuXHRcdHZhciBlbGVtZW50X2hlaWdodCA9ICRlbGVtZW50Lm91dGVySGVpZ2h0KCk7XG5cdFx0dmFyIGVsZW1lbnRfdG9wX3Bvc2l0aW9uID0gJGVsZW1lbnQub2Zmc2V0KCkudG9wO1xuXHRcdHZhciBlbGVtZW50X2JvdHRvbV9wb3NpdGlvbiA9IChlbGVtZW50X3RvcF9wb3NpdGlvbiArIGVsZW1lbnRfaGVpZ2h0KTtcblxuXHRcdC8vY2hlY2sgdG8gc2VlIGlmIHRoaXMgY3VycmVudCBjb250YWluZXIgaXMgd2l0aGluIHZpZXdwb3J0XG5cdFx0aWYgKChlbGVtZW50X2JvdHRvbV9wb3NpdGlvbiA+PSB3aW5kb3dfdG9wX3Bvc2l0aW9uKSAmJlxuXHRcdFx0KGVsZW1lbnRfdG9wX3Bvc2l0aW9uIDw9IHdpbmRvd19ib3R0b21fcG9zaXRpb24pKSB7XG5cdFx0ICAkZWxlbWVudC5hZGRDbGFzcygnaW4tdmlldycpO1xuXHRcdH0gZWxzZSB7XG5cdFx0ICAkZWxlbWVudC5yZW1vdmVDbGFzcygnaW4tdmlldycpO1xuXHRcdH1cblx0fSk7XG59XG5cbiR3aW5kb3cub24oJ3Njcm9sbCByZXNpemUnLCBjaGVja19pZl9pbl92aWV3KTtcbiR3aW5kb3cudHJpZ2dlcignc2Nyb2xsJywgY2hlY2tfaWZfaW5fdmlldyk7XG5cblxuLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tQW5pbWF0ZSBvbiBzY3JvbGwtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuXG4vLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1OdW1iZXIgYW5pbWF0aW9uLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbnZhciBzZWN0aW9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm51bWJlcnMnKTtcbnZhciBoYXNFbnRlcmVkID0gZmFsc2U7XG5cbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCAoZSkgPT4ge1xuXHR2YXIgc2hvdWxkQW5pbWF0ZSA9ICh3aW5kb3cuc2Nyb2xsWSArIHdpbmRvdy5pbm5lckhlaWdodCkgPj0gc2VjdGlvbi5vZmZzZXRUb3A7XG5cblx0aWYgKHNob3VsZEFuaW1hdGUgJiYgIWhhc0VudGVyZWQpIHtcbiAgXHRoYXNFbnRlcmVkID0gdHJ1ZTtcbiAgICBcbiAgICAkKCcudmFsdWUnKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICBcdCQodGhpcykucHJvcCgnQ291bnRlcicsMCkuYW5pbWF0ZSh7XG4gICAgICAgIENvdW50ZXI6ICQodGhpcykudGV4dCgpXG4gICAgXHR9LCB7XG4gICAgICAgIGR1cmF0aW9uOiA0MDAwLFxuICAgICAgICBlYXNpbmc6ICdzd2luZycsXG4gICAgICAgIHN0ZXA6IGZ1bmN0aW9uIChub3cpIHtcbiAgICAgICAgICAgICQodGhpcykudGV4dChNYXRoLmNlaWwobm93KSk7XG4gICAgICAgIH1cbiAgIFx0XHR9KTtcbiAgICB9KTtcblxuICB9XG59KTtcblxuXG4vLyBtb3J0Z2FnZSBjYWxjdWxhdGVyXG4vLyBmdW5jdGlvbiBjYWxjdWxhdGVNb3J0Z2FnZShwLCByLCBuKSB7XG4vLyAgIHIgPSBwZXJjZW50VG9EZWNpbWFsKHIpO1xuLy8gICBuID0geWVhcnNUb01vbnRocyhuKTtcbi8vICAgdmFyIHBtdCA9IChyICogcCkgLyAoMSAtIChNYXRoLnBvdygoMSArIHIpLCAoLW4pKSkpO1xuLy8gICByZXR1cm4gcGFyc2VGbG9hdChwbXQudG9GaXhlZCgyKSk7XG4vLyB9XG5cbi8vIGZ1bmN0aW9uIHBlcmNlbnRUb0RlY2ltYWwocGVyY2VudCkge1xuLy8gICByZXR1cm4gKHBlcmNlbnQgLyAxMikgLyAxMDA7XG4vLyB9XG5cbi8vIGZ1bmN0aW9uIHllYXJzVG9Nb250aHMoeWVhcikge1xuLy8gICByZXR1cm4geWVhciAqIDEyO1xuLy8gfVxuLy8gdmFyIGh0bWxFbCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwib3V0TW9udGhseVwiKTtcblxuLy8gZnVuY3Rpb24gcG9zdFBheW1lbnRzKHBtdCkge1xuLy8gICBodG1sRWwuaW5uZXJUZXh0ID0gXCLCo1wiICsgcG10O1xuLy8gfVxuLy8gdmFyIGJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYnRuQ2FsY3VsYXRlXCIpO1xuLy8gYnRuLm9uY2xpY2sgPSBmdW5jdGlvbigpIHtcbi8vICAgdmFyIGNvc3QgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImluQ29zdFwiKS52YWx1ZS5yZXBsYWNlKCfCoycsICcnKTtcbi8vICAgdmFyIGRvd25QYXltZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbkRvd25cIikudmFsdWUucmVwbGFjZSgnwqMnLCAnJyk7XG4vLyAgIHZhciBpbnRlcmVzdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW5JbnRlcmVzdFwiKS52YWx1ZS5yZXBsYWNlKCclJywgJycpO1xuLy8gICB2YXIgdGVybSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW5UZXJtXCIpLnZhbHVlLnJlcGxhY2UoJyB5ZWFycycsICcnKTtcbiAgXG4vLyAgIGlmIChjb3N0ID09IFwiXCIgJiYgZG93blBheW1lbnQgPT0gXCJcIiAmJiBpbnRlcmVzdCA9PSBcIlwiICYmIHRlcm0gPT0gXCJcIikge1xuLy8gICAgIGh0bWxFbC5pbm5lclRleHQgPSBcIlBsZWFzZSBmaWxsIG91dCBhbGwgZmllbGRzLlwiO1xuLy8gICAgIHJldHVybiBmYWxzZTtcbi8vICAgfVxuLy8gICBpZiAoY29zdCA8IDAgfHwgY29zdCA9PSBcIlwiIHx8IGlzTmFOKGNvc3QpKSB7XG4vLyAgICAgaHRtbEVsLmlubmVyVGV4dCA9IFwiUGxlYXNlIGVudGVyIGEgdmFsaWQgbG9hbiBhbW91bnQuXCI7XG4vLyAgICAgcmV0dXJuIGZhbHNlO1xuLy8gICB9XG4vLyAgIGlmIChkb3duUGF5bWVudCA8IDAgfHwgZG93blBheW1lbnQgPT0gXCJcIiB8fCBpc05hTihkb3duUGF5bWVudCkpIHtcbi8vICAgICBodG1sRWwuaW5uZXJUZXh0ID0gXCJQbGVhc2UgZW50ZXIgYSB2YWxpZCBkb3duIHBheW1lbnQuXCI7XG4vLyAgICAgcmV0dXJuIGZhbHNlO1xuLy8gICB9XG4vLyAgIGlmIChpbnRlcmVzdCA8IDAgfHwgaW50ZXJlc3QgPT0gXCJcIiB8fCBpc05hTihpbnRlcmVzdCkpIHtcbi8vICAgICBodG1sRWwuaW5uZXJUZXh0ID0gXCJQbGVhc2UgZW50ZXIgYSB2YWxpZCBpbnRlcmVzdCByYXRlLlwiO1xuLy8gICAgIHJldHVybiBmYWxzZTtcbi8vICAgfVxuLy8gICBpZiAodGVybSA8IDAgfHwgdGVybSA9PSBcIlwiIHx8IGlzTmFOKHRlcm0pKSB7XG4vLyAgICAgaHRtbEVsLmlubmVyVGV4dCA9IFwiUGxlYXNlIGVudGVyIGEgdmFsaWQgbGVuZ3RoIG9mIGxvYW4uXCI7XG4vLyAgICAgcmV0dXJuIGZhbHNlO1xuLy8gICB9XG4vLyAgIHZhciBhbW91bnRCb3Jyb3dlZCA9IGNvc3QgLSBkb3duUGF5bWVudDtcbi8vICAgdmFyIHBtdCA9IGNhbGN1bGF0ZU1vcnRnYWdlKGFtb3VudEJvcnJvd2VkLCBpbnRlcmVzdCwgdGVybSk7XG4vLyAgIHBvc3RQYXltZW50cyhwbXQpO1xuLy8gfTtcblxuLy8galF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiggJCApIHtcbi8vICAgJChcIiNnb29nbGUtcmV2aWV3c1wiKS5nb29nbGVQbGFjZXMoe1xuLy8gICAgICAgIHBsYWNlSWQ6ICdDaElKdnpuMzRIUVRka2dSdGVsVjBJN2VSTGsnIC8vRmluZCBwbGFjZUlEIEA6IGh0dHBzOi8vZGV2ZWxvcGVycy5nb29nbGUuY29tL3BsYWNlcy9wbGFjZS1pZFxuLy8gICAgICAsIHJlbmRlcjogWydyZXZpZXdzJ11cbi8vICAgICAgLCBtaW5fcmF0aW5nOjRcbi8vICAgICAgLCBtYXhfcm93czo1XG4vLyAgICAgICxyb3RhdGVUaW1lOiBmYWxzZVxuLy8gICAgICAsIHNob3J0ZW5fbmFtZXM6IHRydWVcblxuICAgIFxuLy8gICB9KTtcbi8vIH0pO1xuXG5cbi8vICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuLy8gdmlkZW9zID0gJChcInZpZGVvXCIpOyBcbi8vIGZvcih2aWRlbyBvZiB2aWRlb3MpIHtcbi8vICAgdmlkZW8ucGF1c2UoKTsgXG4vLyB9XG4vLyB9KTtcblxuXG4kKCAnI215QnRuJyApLmNsaWNrKGZ1bmN0aW9uKCkge1xuICB2YXIgZG90cyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZG90c1wiKTtcbiAgdmFyIG1vcmVUZXh0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcIm1vcmVcIik7XG4gIHZhciBidG5UZXh0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJteUJ0blwiKTtcbiAgY29uc29sZS5sb2coJ2Nsb3NlJyk7XG5cbiAgaWYgKGRvdHMuc3R5bGUuZGlzcGxheSA9PT0gXCJub25lXCIpIHtcbiAgICBkb3RzLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XG4gICAgYnRuVGV4dC5pbm5lckhUTUwgPSBcIlJlYWQgbW9yZVwiO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbW9yZVRleHQubGVuZ3RoOyBpKyspIHtcbiAgICBtb3JlVGV4dFtpXS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIGRvdHMuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgIGJ0blRleHQuaW5uZXJIVE1MID0gXCJSZWFkIGxlc3NcIjtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IG1vcmVUZXh0Lmxlbmd0aDsgaSsrKSB7XG4gICAgbW9yZVRleHRbaV0uc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgICB9XG4gIH1cbn0pO1xuXG4vKiBjb21tb24gZnVjdGlvbnMgKi9cbmZ1bmN0aW9uIGVsKHNlbGVjdG9yKSB7IHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKSB9XG5mdW5jdGlvbiBlbHMoc2VsZWN0b3IpIHsgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpIH1cbmZ1bmN0aW9uIG9uKHNlbGVjdG9yLCBldmVudCwgYWN0aW9uKSB7IGVscyhzZWxlY3RvcikuZm9yRWFjaChlID0+IGUuYWRkRXZlbnRMaXN0ZW5lcihldmVudCwgYWN0aW9uKSkgfVxuZnVuY3Rpb24gY29va2llKG5hbWUpIHsgXG4gIGxldCBjID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7ICcpLmZpbmQoY29va2llID0+IGNvb2tpZSAmJiBjb29raWUuc3RhcnRzV2l0aChuYW1lKyc9JykpXG4gIHJldHVybiBjID8gYy5zcGxpdCgnPScpWzFdIDogZmFsc2U7IFxufVxuXG5cbi8qIHBvcHVwIGJ1dHRvbiBoYW5sZXIgKi9cbm9uKCcuY29va2llLXBvcHVwIGJ1dHRvbicsICdjbGljaycsICgpID0+IHtcbiAgZWwoJy5jb29raWUtcG9wdXAnKS5jbGFzc0xpc3QuYWRkKCdjb29raWUtcG9wdXAtLWFjY2VwdGVkJyk7XG4gIGRvY3VtZW50LmNvb2tpZSA9IGBjb29raWUtYWNjZXB0ZWQ9dHJ1ZWBcbn0pO1xuXG4vKiBwb3B1cCBpbml0IGhhbmxlciAqL1xuaWYgKGNvb2tpZSgnY29va2llLWFjY2VwdGVkJykgIT09IFwidHJ1ZVwiKSB7XG4gIGVsKCcuY29va2llLXBvcHVwJykuY2xhc3NMaXN0LmFkZCgnY29va2llLXBvcHVwLS1ub3QtYWNjZXB0ZWQnKTtcbn1cblxuXG5cbi8qIHBhZ2UgYnV0dG9ucyBoYW5kbGVycyAqL1xuXG5mdW5jdGlvbiBfcmVzZXQoKSB7XG4gIGRvY3VtZW50LmNvb2tpZSA9ICdjb29raWUtYWNjZXB0ZWQ9ZmFsc2UnOyBcbiAgZG9jdW1lbnQubG9jYXRpb24ucmVsb2FkKCk7XG59XG5cbmZ1bmN0aW9uIF9zd2l0Y2hNb2RlKGNzc0NsYXNzKSB7XG4gIGVsKCcuY29va2llLXBvcHVwJykuY2xhc3NMaXN0LnRvZ2dsZShjc3NDbGFzcyk7XG59XG5cblxuJChmdW5jdGlvbigpe1xuICAkKCcubW9kYWwtZm9vdGVyJykuY2xpY2soZnVuY3Rpb24oKXsgICAgICBcbiAgICAgXG4gICAgY29uc29sZS5sb2coJ2Nsb3NlJyk7XG4gIH0pO1xufSk7XG5jb25zb2xlLmxvZygnSGVsbG8gV2VicGFjayBFbmNvcmUhIEVkaXQgbWUgaW4gYXNzZXRzL2FwcC5qcycpO1xuXG5cblxuIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==