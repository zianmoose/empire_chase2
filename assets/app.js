import 'owl.carousel/dist/assets/owl.carousel.css';
 require( './styles/app.css');


const $ = require('jquery');

require('bootstrap');

import 'owl.carousel';


window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 15 || document.documentElement.scrollTop > 15) {

   
    $('.navbar').addClass("fixed-top");
   


    
    $('.bg-emp-transparent').addClass('bg-emp-blue').removeClass('bg-emp-transparent');
    $('.navbar-brand').addClass("oval-emp").removeClass('oval-empTransparent');;

  }else {
    
   
    $('.navbar ').removeClass("fixed-top");
   
 
    $('.navbar').addClass('bg-emp-transparent').removeClass('bg-emp-blue');
    $('.navbar-brand').addClass("oval-empTransparent").removeClass('oval-emp');;

  }

  
}

window.onload = (event) => {
  if(document.documentElement.scrollTop > 25){

    //$('.navbar').addClass("fixed-top");
    $('.navbar').addClass('bg-emp-blue');
    
  }
  
};






//Hamburger menu change on click
$(document).ready(function(){
  $('#ham-icon').click(function(){
    $('.icon-ham').toggleClass('active');
    console.log("icon clicked");
    // if ($('navbar-collapse').hasClass('show')){

    //   $('body').css('overflow-y','hidden !important');
    // };
  })
})


$(document).ready(function() {


  var one = $('.owl-fine-homes');
  var two = $('.owl-carousel');
  var three = $('.owl-prop-videos');
  var four = $('#owl-portals');

  one.owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-caret-left'></i>",
      "<i class='fa fa-caret-right'></i>"
    ],
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });



// calling owl carousel-------------------------------------------------------------------------------------------------


two.owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  navText: [
    "<i class='fa fa-caret-left'></i>",
    "<i class='fa fa-caret-right'></i>"
  ],
  autoplay: true,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items: 3
    },
    1600: {
      items: 3
    }
  }
});

three.owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  navText: [
    "<i class='fa fa-caret-left'></i>",
    "<i class='fa fa-caret-right'></i>"
  ],
  autoplay: false,
  autoplayHoverPause: false,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items: 1
    }
  }
});



});

$(document).ready(function () {
  $("#owl-portals").owlCarousel({


  loop: true,
  margin: 10,
  nav: true,
  navText: [
    "<i class='fa fa-caret-left'></i>",
    "<i class='fa fa-caret-right'></i>"
  ],
  autoplay: false,
  autoplayHoverPause: false,
  responsive: {
    0: {
      items: 3
    },
    600: {
      items: 3
    },
    1000: {
      items: 3
    }
  }

  
});
});


//------------------------------------------------------------------------------------------------------------------=-----
//$( "#cap_one" ).append( "<p>Test</p>" );
$(document).delegate("cap_one", "click", function() {
  window.location = $(this).find("a").attr("href");
  console.log("botton is clicked");
});


$('#carousel-home').on('slide.bs.carousel', function () {

  

  console.log('item-changed');
  $('.carousel-caption').hide();
}).on('slid.bs.carousel', function(e) {
  $('.active .carousel-caption').fadeIn('slow');
});
  

if($(window).width() <= 767){

  console.log('mobile device')
}

$(window).resize(function(){     

  if ($('#service-icon-container').width() <= 767){
         // is mobile device
      console.log('mobile device')
  }



});



var $item = $('.service-img-class'), //Cache your DOM selector
visible = 2, //Set the number of items that will be visible
index = 0, //Starting index
endIndex = ( $item.length / visible ) - 1; //End index

$('#arrow-img-right').click(function(){
if(index < endIndex ){
  index++;
  $item.animate({'left':'-=200px'});
}
});

$('#arrow-img-left').click(function(){
if(index > 0){
  index--;            
  $item.animate({'left':'+=200px'});
}
});


// Enable Carousel Controls
$(".left").click(function(){
  $("#carousel-properties").carousel("prev");
});

//-------------------------------------------Animate on scroll------------------------------------------------------------
var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
	var window_height = $window.height();
	var window_top_position = $window.scrollTop();
	var window_bottom_position = (window_top_position + window_height);

	$.each($animation_elements, function() {
		var $element = $(this);
		var element_height = $element.outerHeight();
		var element_top_position = $element.offset().top;
		var element_bottom_position = (element_top_position + element_height);

		//check to see if this current container is within viewport
		if ((element_bottom_position >= window_top_position) &&
			(element_top_position <= window_bottom_position)) {
		  $element.addClass('in-view');
		} else {
		  $element.removeClass('in-view');
		}
	});
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll', check_if_in_view);


//-------------------------------------------Animate on scroll------------------------------------------------------------


//-------------------------------------------Number animation------------------------------------------------------------

var section = document.querySelector('.numbers');
var hasEntered = false;

window.addEventListener('scroll', (e) => {
	var shouldAnimate = (window.scrollY + window.innerHeight) >= section.offsetTop;

	if (shouldAnimate && !hasEntered) {
  	hasEntered = true;
    
    $('.value').each(function () {
    	$(this).prop('Counter',0).animate({
        Counter: $(this).text()
    	}, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
   		});
    });

  }
});


// mortgage calculater
// function calculateMortgage(p, r, n) {
//   r = percentToDecimal(r);
//   n = yearsToMonths(n);
//   var pmt = (r * p) / (1 - (Math.pow((1 + r), (-n))));
//   return parseFloat(pmt.toFixed(2));
// }

// function percentToDecimal(percent) {
//   return (percent / 12) / 100;
// }

// function yearsToMonths(year) {
//   return year * 12;
// }
// var htmlEl = document.getElementById("outMonthly");

// function postPayments(pmt) {
//   htmlEl.innerText = "£" + pmt;
// }
// var btn = document.getElementById("btnCalculate");
// btn.onclick = function() {
//   var cost = document.getElementById("inCost").value.replace('£', '');
//   var downPayment = document.getElementById("inDown").value.replace('£', '');
//   var interest = document.getElementById("inInterest").value.replace('%', '');
//   var term = document.getElementById("inTerm").value.replace(' years', '');
  
//   if (cost == "" && downPayment == "" && interest == "" && term == "") {
//     htmlEl.innerText = "Please fill out all fields.";
//     return false;
//   }
//   if (cost < 0 || cost == "" || isNaN(cost)) {
//     htmlEl.innerText = "Please enter a valid loan amount.";
//     return false;
//   }
//   if (downPayment < 0 || downPayment == "" || isNaN(downPayment)) {
//     htmlEl.innerText = "Please enter a valid down payment.";
//     return false;
//   }
//   if (interest < 0 || interest == "" || isNaN(interest)) {
//     htmlEl.innerText = "Please enter a valid interest rate.";
//     return false;
//   }
//   if (term < 0 || term == "" || isNaN(term)) {
//     htmlEl.innerText = "Please enter a valid length of loan.";
//     return false;
//   }
//   var amountBorrowed = cost - downPayment;
//   var pmt = calculateMortgage(amountBorrowed, interest, term);
//   postPayments(pmt);
// };

// jQuery(document).ready(function( $ ) {
//   $("#google-reviews").googlePlaces({
//        placeId: 'ChIJvzn34HQTdkgRtelV0I7eRLk' //Find placeID @: https://developers.google.com/places/place-id
//      , render: ['reviews']
//      , min_rating:4
//      , max_rows:5
//      ,rotateTime: false
//      , shorten_names: true

    
//   });
// });


// $(document).ready(function() {
// videos = $("video"); 
// for(video of videos) {
//   video.pause(); 
// }
// });


$( '#myBtn' ).click(function() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementsByClassName("more");
  var btnText = document.getElementById("myBtn");
  console.log('close');

  if (dots.style.display === "none") {
    dots.style.display = "block";
    btnText.innerHTML = "Read more";
    for (let i = 0; i < moreText.length; i++) {
    moreText[i].style.display = "none";
    }
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    for (let i = 0; i < moreText.length; i++) {
    moreText[i].style.display = "block";
    }
  }
});

/* common fuctions */
function el(selector) { return document.querySelector(selector) }
function els(selector) { return document.querySelectorAll(selector) }
function on(selector, event, action) { els(selector).forEach(e => e.addEventListener(event, action)) }
function cookie(name) { 
  let c = document.cookie.split('; ').find(cookie => cookie && cookie.startsWith(name+'='))
  return c ? c.split('=')[1] : false; 
}


/* popup button hanler */
on('.cookie-popup button', 'click', () => {
  el('.cookie-popup').classList.add('cookie-popup--accepted');
  document.cookie = `cookie-accepted=true`
});

/* popup init hanler */
if (cookie('cookie-accepted') !== "true") {
  el('.cookie-popup').classList.add('cookie-popup--not-accepted');
}



/* page buttons handlers */

function _reset() {
  document.cookie = 'cookie-accepted=false'; 
  document.location.reload();
}

function _switchMode(cssClass) {
  el('.cookie-popup').classList.toggle(cssClass);
}


$(function(){
  $('.modal-footer').click(function(){      
     
    console.log('close');
  });
});
console.log('Hello Webpack Encore! Edit me in assets/app.js');



