<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /services/valuation/virtual-valuation.html.twig */
class __TwigTemplate_c8cb08c76369d2642f445f7b279c7f869efc29a8233dd559468a967d5fd0c6e0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/valuation/virtual-valuation.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/valuation/virtual-valuation.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/services/valuation/virtual-valuation.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"Z2JTZKx3\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/buildtoRentBanner.jpeg');background-size: cover;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Virtual valuation</h1>
\t\t\t\t<p>Find out how much your home is worth.</p>
\t\t\t\t<p>
\t\t\t\t\t<a class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 24
        echo "
\t\t</div>


\t\t<div class=\"container\">
\t\t<br>
\t\t\t<br>

\t<p class=\"review-content-allign lead\">We provide an independent accurate valuation supported by facts, market conditions, previous sold prices and comparable. Please get in touch with our team, if you would like to book a virtual or in person viewing</p>
\t\t<br>
\t\t\t<br>
\t\t\t<div class=\"row\">

\t\t\t\t<div class=\"col\" style=\"
\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
\t\t\t\t\t\t\">
\t\t\t\t\t";
        // line 44
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 44, $this->source); })()), 'form');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<!--Grid column-->


\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"col-md-3 text-center\">
\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t</div>


\t\t\t\t</div>

\t\t\t</div>
\t\t\t<!-- /container -->


\t\t</main>


\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/services/valuation/virtual-valuation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 44,  89 => 24,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"Z2JTZKx3\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/buildtoRentBanner.jpeg');background-size: cover;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Virtual valuation</h1>
\t\t\t\t<p>Find out how much your home is worth.</p>
\t\t\t\t<p>
\t\t\t\t\t<a class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>


\t\t<div class=\"container\">
\t\t<br>
\t\t\t<br>

\t<p class=\"review-content-allign lead\">We provide an independent accurate valuation supported by facts, market conditions, previous sold prices and comparable. Please get in touch with our team, if you would like to book a virtual or in person viewing</p>
\t\t<br>
\t\t\t<br>
\t\t\t<div class=\"row\">

\t\t\t\t<div class=\"col\" style=\"
\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
\t\t\t\t\t\t\">
\t\t\t\t\t{{form(form)}}
\t\t\t\t\t</div>
\t\t\t\t\t<!--Grid column-->


\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"col-md-3 text-center\">
\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t</div>


\t\t\t\t</div>

\t\t\t</div>
\t\t\t<!-- /container -->


\t\t</main>


\t{% endblock %}
", "/services/valuation/virtual-valuation.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\valuation\\virtual-valuation.html.twig");
    }
}
