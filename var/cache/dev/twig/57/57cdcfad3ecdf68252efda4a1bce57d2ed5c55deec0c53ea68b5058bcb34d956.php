<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* services/services.html.twig */
class __TwigTemplate_7319da1f097792b07f05eaa9fce3f13decd8f7eef189d53bfaf75e1457f0ecb9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/services.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/services.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "services/services.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<main role=\"main\">

  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class=\"jumbotron jumbotron-fluid services-jumbo\" >
  
    <div class=\"container services-jumbo-container\">
      <h1 class=\"display-3\">Our services</h1>
      <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content.</p>
      <p><a class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #FFFFFF!important\" role=\"button\">Contact us</a></p>
    </div>
    
    ";
        // line 17
        echo "    
  </div>

  <div class=\"container\">
    <!-- Example row of columns -->
    <div class=\"row\">
      <div class=\"col-md-4\">
        <h2>Sales & Lettings</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Property Management</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Build to rent</h2>
        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
    </div>

    <hr>

     <!-- Example row of columns -->
    <div class=\"row\">
      <div class=\"col-md-4\">
        <h2>Asset Management</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Block Management</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Market Research</h2>
        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
    </div>

    <hr>

  </div> <!-- /container -->

  <div class=\"container-fluid\" style = \"max-width: 800px;\">
  <h1>Our Other Services</h1>
  <img src=\"emp-table.jpg\" alt =\"services table\" class=\"responsive\" style = \"max-width: 800px; margin-right: auto;
    margin-left: auto;\">
  </div>

</main>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "services/services.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 17,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

<main role=\"main\">

  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class=\"jumbotron jumbotron-fluid services-jumbo\" >
  
    <div class=\"container services-jumbo-container\">
      <h1 class=\"display-3\">Our services</h1>
      <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content.</p>
      <p><a class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #FFFFFF!important\" role=\"button\">Contact us</a></p>
    </div>
    
    {# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}
    
  </div>

  <div class=\"container\">
    <!-- Example row of columns -->
    <div class=\"row\">
      <div class=\"col-md-4\">
        <h2>Sales & Lettings</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Property Management</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Build to rent</h2>
        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
    </div>

    <hr>

     <!-- Example row of columns -->
    <div class=\"row\">
      <div class=\"col-md-4\">
        <h2>Asset Management</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Block Management</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
      <div class=\"col-md-4\">
        <h2>Market Research</h2>
        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class=\"btn btn-secondary\" href=\"#\" style = \"background-color:  #2b459c!important; color: #FFFFFF!important\" role=\"button\">View details &raquo;</a></p>
      </div>
    </div>

    <hr>

  </div> <!-- /container -->

  <div class=\"container-fluid\" style = \"max-width: 800px;\">
  <h1>Our Other Services</h1>
  <img src=\"emp-table.jpg\" alt =\"services table\" class=\"responsive\" style = \"max-width: 800px; margin-right: auto;
    margin-left: auto;\">
  </div>

</main>



{% endblock %}", "services/services.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\services.html.twig");
    }
}
