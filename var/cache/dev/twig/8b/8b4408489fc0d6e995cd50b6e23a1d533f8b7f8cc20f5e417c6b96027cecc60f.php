<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* buy.html.twig */
class __TwigTemplate_52aeead164734c0b856e97f52582feb9083bfe64596e80441bc9e563b3a87056 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "buy.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "buy.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "buy.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('buy/banner-image.jpeg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">BUY</h1>
\t\t\t\t<p>Buying the biggest asset in your life maybe challenging, you will need the right professionals by your side.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 21
        echo "
\t\t</div>

\t\t<div class=\"container\">
\t\t<br>
\t\t\t<br>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign \">Why choose Empire Chase?</h2>
\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Our award-winning customer services</li>
\t\t\t\t<li class=\"lead\">After sales services</li>
\t\t\t\t<li class=\"lead\">24-hour property line</li>
\t\t\t\t<li class=\"lead\">Independent genuine guidance</li>
\t\t\t<li class=\"lead\">Fluent local knowledge</li>
\t\t<li class=\"lead\">Experienced sales progression team</li>
\t<li class=\"lead\">Modern technologies to enhance your property journey</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t\t<br>
\t\t
\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign \">Buying process
\t</h2>

\t<div class=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">

\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t<br>
\t\t\t\t
\t\t\t<ul>
\t\t\t\t<li class=\"lead\">Decide on what you need</li>
\t\t\t\t<li class=\"lead\">Choose the area</li>
\t\t\t\t<li class=\"lead\">Work out the finances
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Arrange for a mortgage
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Look for a good solicitor</li>
\t\t\t\t<li class=\"lead\">Property Search</li>
\t\t\t\t<li class=\"lead\">View ample properties</li>
\t\t\t\t<li class=\"lead\">Make the offer</li>
\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t<li class=\"lead\">Exchange contracts</li>
\t\t\t\t<li class=\"lead\">Completion
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Moving day
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">After moving</li>
\t\t\t</ul>
\t\t\t<br>
\t\t</div>


\t\t<div class=\"col-md-6 order-md-6 animation-element slide-right\">
\t\t
\t\t\t<iframe style=\"width:100%;max-height:1000px; min-height:400px; \" src=\"https://www.youtube.com/embed/-Qzq_Y-jV8I\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t</div>

\t\t<!-- /.col-md-6 -->

\t</div>

\t\t\t<br>
\t\t\t<h2 id=\"firsttimebuy\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">First Time Buyers – New homes</h2>
\t\t\t<br>
\t\t\t\t
\t\t\t<p class=\"lead\">There is no better feeling then moving to your first home.  With a wide range of new built properties available with Help to Buy across the nation, it is very important you choose the correct property that suits your requirements.</p>
\t\t\t<br>
\t\t<p class=\"lead\">Our experienced team can point out the pros  and cons of buying a new built property.</p>
\t\t\t<br>
\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">Help To Buy</h2>
\t\t
\t\t\t\t
\t\t\t<p class=\"lead\">The UK government has created the Help to Buy scheme to help hard working people to take a step to own a property.<span id=\"dots\">...</span></p>

\t\t\t<p class=\"lead more\">If you’re a first-time buyer in England, you can apply for a Help to Buy: Equity Loan.</p>

\t\t\t
\t\t\t<p  class=\"lead more\"  >This is a loan from the government that you put towards the cost of buying a newly built home.</p>
\t\t\t
\t\t\t<p class=\"lead more\" >You can borrow a minimum of 5% and up to a maximum of 20% (40% in London) of the full purchase price of a new-build home.</p>
\t\t\t
\t\t\t<p  class=\" lead more\" >You must buy your home from a homebuilder registered for Help to Buy: Equity Loan.</p>
\t\t\t
\t\t\t<p  class=\"lead more\" >The amount you pay for a home depends on where in England you buy it.</p>
\t\t
\t\t<button  id=\"myBtn\" style =\"border-radius: 10px;padding: 3px;\">Read more</button>
\t\t\t<br>
\t\t

\t\t\t<!--  image left  -->

\t\t\t<br>
\t\t\t\t<div class = \"more\">

\t\t\t<div class=\"row \" style=\"min-height: 500px;\">
\t\t\t\t<div style=\"margin-top:20px;\" class=\"col-md-6 animation-element slide-left more\">

\t\t\t\t\t<div class=\"container more\">

\t\t\t\t\t\t<p>Help to Buy Equity Loan price caps – April 2021 to March 2023</p>
\t\t\t\t\t\t<table class=\"table table-bordered\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th>Region</th>
\t\t\t\t\t\t\t\t\t<th>Maximum propertyprice</th>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>North East</td>
\t\t\t\t\t\t\t\t\t<td>£186,100</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>North West</td>
\t\t\t\t\t\t\t\t\t<td>£224,400</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Yorkshire andthe Humber</td>
\t\t\t\t\t\t\t\t\t<td>£228,100</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>East Midlands</td>
\t\t\t\t\t\t\t\t\t<td>£261,900</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>West Midlands</td>
\t\t\t\t\t\t\t\t\t<td>£255,600</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>East of England</td>
\t\t\t\t\t\t\t\t\t<td>£407,400</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>London</td>
\t\t\t\t\t\t\t\t\t<td>£600,000</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>South East</td>
\t\t\t\t\t\t\t\t\t<td>£437,600</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>South West</td>
\t\t\t\t\t\t\t\t\t<td>£349,000</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>

\t\t\t\t\t";
        // line 169
        echo "\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div id=\"col-brad\" class=\"col  more\" style=\"margin-top:20px !important;\">

\t\t\t\t\t\t\t<p class=\"lead\">The equity loan, the deposit you have saved, and your repayment mortgage cover the total cost of buying your newly built home.</p>
\t\t\t\t\t\t\t<p class=\"lead\">The percentage you borrow is based on the market value of your home when you buy it.</p>
\t\t\t\t\t\t\t<p class=\"lead\">You do not pay interest on the equity loan for the first 5 years. You start to pay interest in year 6, on the equity loan amount you borrowed.</p>
\t\t\t\t\t\t\t<p class=\"lead\">The equity loan payments are interest only, so you do not reduce the amount you owe.
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">You can repay all or part of your equity loan at any time. A part payment must be at least 10% of what your home is worth at the time of repayment.</p>
\t\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>

\t\t</div> <!--more--!>
\t\t\t<!-- /.row -->

\t\t\t<div class=\"container\">

\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign \">Cladding</h2>
\t\t\t<p class=\"lead \">Cladding is a material which is wrapped around the outside of a building to improve appearance and energy efficiency.</p>
\t\t<p class=\"lead \">After the devastating Grenfell fire in London, the fire authorities have imposed more and more strict measures over cladding and fire safety for apartment blocks.</p>
\t<p class=\"lead \">Every building needs to have a valid EWS1 certificate passed within the required level.</p>
\t<br>
<h2 class=\"h2-letter-spacing review-content-allign\">What is an EWS1</h2>
\t\t\t\t<div class=\"row\" style=\"margin-top: 10px;margin-right: 10px;margin-left: 10px;\">
\t\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t
\t\t\t\t\t\t<video width=\"640\" controls poster=\"buy/EWS1.png\" preload=\"none\">
\t\t\t\t\t\t\t<source src=\"buy/EWS1.mp4\" type=\"video/mp4\">
\t\t\t\t\t\t</video>
\t\t\t\t\t</div>


\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t\t<div class=\"row align-items-center h-100\" style=\"margin-top: 10px;\">
\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t<p class=\"lead\">The new EWS1 form, or External Wall Fire Review form, is intended to record in a consistent and universal manner what assessments have been carried out on the external wall construction of residential apartment buildings.</p>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br/><br/>
\t\t\t<br/><br/>




\t\t\t<div class=\"row\" style=\"margin-top: 10px;margin-right: 10px;margin-left: 10px;\">

\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t<h1 id=\"finehomes\" class=\"h2-letter-spacing review-content-allign\">Fine homes</h1>
\t\t\t\t\t<br>

\t\t\t\t\t<div id=\"fine-and-invest-home\" class=\"carousel-wrap\">
\t\t\t\t\t\t<div class=\"owl-carousel owl-fine-homes\">


\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/finehome1.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/finehome2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/finehome3.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<p class=\"lead\">Our team strives to acquire some of the best homes in the area.
\t\t\t\t\t</p>

\t\t\t\t\t<p class=\"lead\">From conservation areas and private roads to highly sought after neighbourhoods, we strive to find the best properties for our clients.
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"lead\">Please register your interest with us if you are looking for a fine home. Some of our vendors prefers a discreet sale than advertising their private home. Registering with us gives our clients the opportunity to be the first to view those properties. 
\t\t\t\t\t</p>


\t\t\t\t\t<br/><br/>

\t\t\t\t</div>


\t\t\t\t<div
\t\t\t\t\tclass=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t";
        // line 287
        echo "\t\t\t\t\t<h1 id=\"investmentprop\" class=\"h2-letter-spacing review-content-allign\">Investment property
\t\t\t\t\t</h1>
\t\t\t\t\t<br>

\t\t\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t\t\t<div class=\"owl-carousel owl-fine-homes\">


\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/investprop1.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/investprop2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/investprop3.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<p class=\"lead\">Property development is proven to be a solid way to generate high income and ROI. Our sales team regularly acquires and source unique property deals.
\t\t\t\t</p>

\t\t\t\t<p class=\"lead\">We work with most of the probate lawyers in the nation and various other networks to be the first to bring probate and distressed properties in to the market
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">If you are looking for a probate property, properties with short leases and distressed properties please register your interest. 
\t\t\t\t</p>
\t\t\t\t</div>

\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br/>
\t\t\t

\t\t\t<!-- /.row  affliations -->

\t\t\t<!--  image right  -->
\t\t\t\t<div class=\"container\"> <div
\t\t\t\t\tclass=\"row\">

\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\"></div>

\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t<div class=\"container\">

\t\t\t\t\t\t
\t\t\t\t\t\t<h2 id=\"buytolet\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">Buy to Let</h2>
\t\t\t\t\t\t<br>

\t\t\t\t\t\t<p class=\"lead\">Although Buy to Let is a great investment opportunity, it’s important to understand where you stand in the property market. </p>
\t\t\t\t\t<p class=\"lead\">With the constant new legislation imposed on Buy to Let landlords, it is vital to choose a professional estate agency to find a tenant and manage your property. </p>
\t\t\t\t\t<p class=\"lead\">Buy to Let landlords faces various other threats such as competition from Build to Rent corporate landlords, large supply of new builds and overseas landlords entering in to the market. </p>
\t\t\t\t<p class=\"lead\">Considering all the threats affecting the Buy to Let market, we offer a unique strategy for our Buy to Let clients to stay ahead of the competition  </p>
\t\t\t<p class=\"lead\">Read our latest Blog about the Buy to let property market. </p>
\t\t\t\t\t\t<p class=\"lead\">If you like to learn more read our
\t\t\t\t\t\t\t<a href=\"";
        // line 355
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_/market-research");
        echo "\">MARKET RESEARCH
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\tpage</p>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">Conveyancing</h2>
\t\t\t\t\t\t<br>

\t\t\t\t\t\t<p class=\"lead\">A conveyancer can make or break a sale. A slow solicitor/conveyancer can cause unnecessary delays and may lead to your purchase to fall through. Choose a good solicitor who can act on your behalf.</p>
\t\t\t\t\t\t<p class=\"lead\">When looking for a solicitor it may be a good idea to find someone from recommendation and check their reviews, speed and process before instructing</p>
\t\t\t\t\t\t<p class=\"lead\">We would suggest looking for a solicitor when you start the process of viewings properties</p>


\t\t\t\t\t

\t\t\t\t\t\t<br>

\t\t\t\t\t</div>


\t\t\t\t</div>


\t\t\t</main>


\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "buy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  427 => 355,  357 => 287,  238 => 169,  89 => 21,  82 => 16,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('buy/banner-image.jpeg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">BUY</h1>
\t\t\t\t<p>Buying the biggest asset in your life maybe challenging, you will need the right professionals by your side.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div class=\"container\">
\t\t<br>
\t\t\t<br>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign \">Why choose Empire Chase?</h2>
\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Our award-winning customer services</li>
\t\t\t\t<li class=\"lead\">After sales services</li>
\t\t\t\t<li class=\"lead\">24-hour property line</li>
\t\t\t\t<li class=\"lead\">Independent genuine guidance</li>
\t\t\t<li class=\"lead\">Fluent local knowledge</li>
\t\t<li class=\"lead\">Experienced sales progression team</li>
\t<li class=\"lead\">Modern technologies to enhance your property journey</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t\t<br>
\t\t
\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign \">Buying process
\t</h2>

\t<div class=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">

\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t<br>
\t\t\t\t
\t\t\t<ul>
\t\t\t\t<li class=\"lead\">Decide on what you need</li>
\t\t\t\t<li class=\"lead\">Choose the area</li>
\t\t\t\t<li class=\"lead\">Work out the finances
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Arrange for a mortgage
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Look for a good solicitor</li>
\t\t\t\t<li class=\"lead\">Property Search</li>
\t\t\t\t<li class=\"lead\">View ample properties</li>
\t\t\t\t<li class=\"lead\">Make the offer</li>
\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t<li class=\"lead\">Exchange contracts</li>
\t\t\t\t<li class=\"lead\">Completion
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Moving day
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">After moving</li>
\t\t\t</ul>
\t\t\t<br>
\t\t</div>


\t\t<div class=\"col-md-6 order-md-6 animation-element slide-right\">
\t\t
\t\t\t<iframe style=\"width:100%;max-height:1000px; min-height:400px; \" src=\"https://www.youtube.com/embed/-Qzq_Y-jV8I\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t</div>

\t\t<!-- /.col-md-6 -->

\t</div>

\t\t\t<br>
\t\t\t<h2 id=\"firsttimebuy\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">First Time Buyers – New homes</h2>
\t\t\t<br>
\t\t\t\t
\t\t\t<p class=\"lead\">There is no better feeling then moving to your first home.  With a wide range of new built properties available with Help to Buy across the nation, it is very important you choose the correct property that suits your requirements.</p>
\t\t\t<br>
\t\t<p class=\"lead\">Our experienced team can point out the pros  and cons of buying a new built property.</p>
\t\t\t<br>
\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">Help To Buy</h2>
\t\t
\t\t\t\t
\t\t\t<p class=\"lead\">The UK government has created the Help to Buy scheme to help hard working people to take a step to own a property.<span id=\"dots\">...</span></p>

\t\t\t<p class=\"lead more\">If you’re a first-time buyer in England, you can apply for a Help to Buy: Equity Loan.</p>

\t\t\t
\t\t\t<p  class=\"lead more\"  >This is a loan from the government that you put towards the cost of buying a newly built home.</p>
\t\t\t
\t\t\t<p class=\"lead more\" >You can borrow a minimum of 5% and up to a maximum of 20% (40% in London) of the full purchase price of a new-build home.</p>
\t\t\t
\t\t\t<p  class=\" lead more\" >You must buy your home from a homebuilder registered for Help to Buy: Equity Loan.</p>
\t\t\t
\t\t\t<p  class=\"lead more\" >The amount you pay for a home depends on where in England you buy it.</p>
\t\t
\t\t<button  id=\"myBtn\" style =\"border-radius: 10px;padding: 3px;\">Read more</button>
\t\t\t<br>
\t\t

\t\t\t<!--  image left  -->

\t\t\t<br>
\t\t\t\t<div class = \"more\">

\t\t\t<div class=\"row \" style=\"min-height: 500px;\">
\t\t\t\t<div style=\"margin-top:20px;\" class=\"col-md-6 animation-element slide-left more\">

\t\t\t\t\t<div class=\"container more\">

\t\t\t\t\t\t<p>Help to Buy Equity Loan price caps – April 2021 to March 2023</p>
\t\t\t\t\t\t<table class=\"table table-bordered\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th>Region</th>
\t\t\t\t\t\t\t\t\t<th>Maximum propertyprice</th>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>North East</td>
\t\t\t\t\t\t\t\t\t<td>£186,100</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>North West</td>
\t\t\t\t\t\t\t\t\t<td>£224,400</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Yorkshire andthe Humber</td>
\t\t\t\t\t\t\t\t\t<td>£228,100</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>East Midlands</td>
\t\t\t\t\t\t\t\t\t<td>£261,900</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>West Midlands</td>
\t\t\t\t\t\t\t\t\t<td>£255,600</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>East of England</td>
\t\t\t\t\t\t\t\t\t<td>£407,400</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>London</td>
\t\t\t\t\t\t\t\t\t<td>£600,000</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>South East</td>
\t\t\t\t\t\t\t\t\t<td>£437,600</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>South West</td>
\t\t\t\t\t\t\t\t\t<td>£349,000</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>

\t\t\t\t\t{# <img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6a/Imac_alu.png\" alt=\"\" class=\"w-100\" /> #}
\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div id=\"col-brad\" class=\"col  more\" style=\"margin-top:20px !important;\">

\t\t\t\t\t\t\t<p class=\"lead\">The equity loan, the deposit you have saved, and your repayment mortgage cover the total cost of buying your newly built home.</p>
\t\t\t\t\t\t\t<p class=\"lead\">The percentage you borrow is based on the market value of your home when you buy it.</p>
\t\t\t\t\t\t\t<p class=\"lead\">You do not pay interest on the equity loan for the first 5 years. You start to pay interest in year 6, on the equity loan amount you borrowed.</p>
\t\t\t\t\t\t\t<p class=\"lead\">The equity loan payments are interest only, so you do not reduce the amount you owe.
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">You can repay all or part of your equity loan at any time. A part payment must be at least 10% of what your home is worth at the time of repayment.</p>
\t\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>

\t\t</div> <!--more--!>
\t\t\t<!-- /.row -->

\t\t\t<div class=\"container\">

\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign \">Cladding</h2>
\t\t\t<p class=\"lead \">Cladding is a material which is wrapped around the outside of a building to improve appearance and energy efficiency.</p>
\t\t<p class=\"lead \">After the devastating Grenfell fire in London, the fire authorities have imposed more and more strict measures over cladding and fire safety for apartment blocks.</p>
\t<p class=\"lead \">Every building needs to have a valid EWS1 certificate passed within the required level.</p>
\t<br>
<h2 class=\"h2-letter-spacing review-content-allign\">What is an EWS1</h2>
\t\t\t\t<div class=\"row\" style=\"margin-top: 10px;margin-right: 10px;margin-left: 10px;\">
\t\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t
\t\t\t\t\t\t<video width=\"640\" controls poster=\"buy/EWS1.png\" preload=\"none\">
\t\t\t\t\t\t\t<source src=\"buy/EWS1.mp4\" type=\"video/mp4\">
\t\t\t\t\t\t</video>
\t\t\t\t\t</div>


\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t\t<div class=\"row align-items-center h-100\" style=\"margin-top: 10px;\">
\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t<p class=\"lead\">The new EWS1 form, or External Wall Fire Review form, is intended to record in a consistent and universal manner what assessments have been carried out on the external wall construction of residential apartment buildings.</p>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br/><br/>
\t\t\t<br/><br/>




\t\t\t<div class=\"row\" style=\"margin-top: 10px;margin-right: 10px;margin-left: 10px;\">

\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t<h1 id=\"finehomes\" class=\"h2-letter-spacing review-content-allign\">Fine homes</h1>
\t\t\t\t\t<br>

\t\t\t\t\t<div id=\"fine-and-invest-home\" class=\"carousel-wrap\">
\t\t\t\t\t\t<div class=\"owl-carousel owl-fine-homes\">


\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/finehome1.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/finehome2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/finehome3.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<p class=\"lead\">Our team strives to acquire some of the best homes in the area.
\t\t\t\t\t</p>

\t\t\t\t\t<p class=\"lead\">From conservation areas and private roads to highly sought after neighbourhoods, we strive to find the best properties for our clients.
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"lead\">Please register your interest with us if you are looking for a fine home. Some of our vendors prefers a discreet sale than advertising their private home. Registering with us gives our clients the opportunity to be the first to view those properties. 
\t\t\t\t\t</p>


\t\t\t\t\t<br/><br/>

\t\t\t\t</div>


\t\t\t\t<div
\t\t\t\t\tclass=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t{# <div class= \"container\" style=\"max-width:300px\"> #}
\t\t\t\t\t<h1 id=\"investmentprop\" class=\"h2-letter-spacing review-content-allign\">Investment property
\t\t\t\t\t</h1>
\t\t\t\t\t<br>

\t\t\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t\t\t<div class=\"owl-carousel owl-fine-homes\">


\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/investprop1.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/investprop2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"buy/investprop3.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<p class=\"lead\">Property development is proven to be a solid way to generate high income and ROI. Our sales team regularly acquires and source unique property deals.
\t\t\t\t</p>

\t\t\t\t<p class=\"lead\">We work with most of the probate lawyers in the nation and various other networks to be the first to bring probate and distressed properties in to the market
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">If you are looking for a probate property, properties with short leases and distressed properties please register your interest. 
\t\t\t\t</p>
\t\t\t\t</div>

\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br/>
\t\t\t

\t\t\t<!-- /.row  affliations -->

\t\t\t<!--  image right  -->
\t\t\t\t<div class=\"container\"> <div
\t\t\t\t\tclass=\"row\">

\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\"></div>

\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t<div class=\"container\">

\t\t\t\t\t\t
\t\t\t\t\t\t<h2 id=\"buytolet\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">Buy to Let</h2>
\t\t\t\t\t\t<br>

\t\t\t\t\t\t<p class=\"lead\">Although Buy to Let is a great investment opportunity, it’s important to understand where you stand in the property market. </p>
\t\t\t\t\t<p class=\"lead\">With the constant new legislation imposed on Buy to Let landlords, it is vital to choose a professional estate agency to find a tenant and manage your property. </p>
\t\t\t\t\t<p class=\"lead\">Buy to Let landlords faces various other threats such as competition from Build to Rent corporate landlords, large supply of new builds and overseas landlords entering in to the market. </p>
\t\t\t\t<p class=\"lead\">Considering all the threats affecting the Buy to Let market, we offer a unique strategy for our Buy to Let clients to stay ahead of the competition  </p>
\t\t\t<p class=\"lead\">Read our latest Blog about the Buy to let property market. </p>
\t\t\t\t\t\t<p class=\"lead\">If you like to learn more read our
\t\t\t\t\t\t\t<a href=\"{{ path('emp_services_/market-research') }}\">MARKET RESEARCH
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\tpage</p>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">Conveyancing</h2>
\t\t\t\t\t\t<br>

\t\t\t\t\t\t<p class=\"lead\">A conveyancer can make or break a sale. A slow solicitor/conveyancer can cause unnecessary delays and may lead to your purchase to fall through. Choose a good solicitor who can act on your behalf.</p>
\t\t\t\t\t\t<p class=\"lead\">When looking for a solicitor it may be a good idea to find someone from recommendation and check their reviews, speed and process before instructing</p>
\t\t\t\t\t\t<p class=\"lead\">We would suggest looking for a solicitor when you start the process of viewings properties</p>


\t\t\t\t\t

\t\t\t\t\t\t<br>

\t\t\t\t\t</div>


\t\t\t\t</div>


\t\t\t</main>


\t\t{% endblock %}
", "buy.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\buy.html.twig");
    }
}
