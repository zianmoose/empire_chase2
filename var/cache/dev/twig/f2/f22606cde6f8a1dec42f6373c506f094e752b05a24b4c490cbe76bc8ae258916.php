<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* cookie-policy.html.twig */
class __TwigTemplate_b0245fc0d70c4845bdf69ee52562b2217ed8a08cfd18e149c09c94e71cdfdab3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "cookie-policy.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "cookie-policy.html.twig"));

        // line 1
        echo "<br>
    <br>
        <br>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Privacy and Cookie Policy</span></strong></p>
<p>We at Empire Chase estate agents are committed to respecting your online privacy and recognise your need for appropriate protection and management of any personally identifiable information (&ldquo;Personal Information&rdquo;) you share with us.</p>
<p>Empire Chase Limited (Registered office: 14 Peterborough Road, Harrow, HA1 2BQ, Registration Number: 07924071) is committed to protecting your personal information, being transparent about what data we hold and giving you control over how we use it.</p>
<p>The purpose of this privacy policy is to give you a clear explanation about how Empire Chase use personal and behavioural information we collect from you when you use our websites and mobile sites such as empirechase.co.uk &ndash; collectively called our &ldquo;Digital Channels&rdquo;.</p>
<p>It is important for you to read this policy in full to understand what information we hold about you, how we may use it and how you can access, update and delete your personal information. By accessing and using our Digital Channels, you are agreeing, or have agreed to, be bound by the Website Terms and Privacy Policy, and you consent to Empire Chase holding, processing and transferring any personal information that you provide Empire Chase as set out in this Privacy Policy.</p>
<p>If you don&rsquo;t have time to read the whole policy now, here are the main reasons we collect different types of information about our users:</p>
<ol>
    <li>To help us monitor and improve our Digital Channels</li>
    <li>To provide you with a more personalised experience, such as making property suggestions that are relevant to your search criteria</li>
    <li>To send you details about our related services, where you have signed up to receive such marketing communications</li>
    <li>Keep the Digital Channels secure and prevent fraud</li>
</ol>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Should I read any other information or terms?</span></strong></p>
<p>Whenever you use our Digital Channels, information may be collected through the use of cookies so you should read our Cookie Policy (COOKIES) for more information about this.</p>
<p><br></p>
<p>If you&rsquo;re interested in the legal recommendations, we recommend visiting the Information Commissioner&rsquo;s Office website (http://www.ico.org.uk) The ICO regulates data protection and e-privacy in the UK so there is lots of useful guidance about this on their website.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">What are Empire Chase&rsquo;s privacy principles?</span></strong></p>
<p>Empire Chase privacy principles, in respect of how it collects, uses and manages your personal information, are summarised as follows:</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">When we ask for data it&rsquo;s to give something back.</span></strong></p>
<p>Registering your details on our Digital Channels gives you access to a growing number of benefits, such as being able to read viewing feedback, track sales and lettings progression and save and compare properties that are of interest. Asking for your name means we know how to talk to you. Knowing your address and property status helps us to allocate the most appropriate member of our team to help you with your property needs and your contact details enables us to contact you regarding our services.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">We are clear on what data we hold and what we do with it.</span></strong></p>
<p>In collecting and using your personal information, we will be clear about what we are doing and make sure that you understand the way in which we are using it. Also, we won&rsquo;t sell your information to anyone.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">You control the data we hold on you.</span></strong></p>
<p>If requested, we will delete your personal information securely and swiftly and some of the information we hold on your activity on our Digital Channels will be anonymised, so we won&rsquo;t know it&rsquo;s yours.</p>
<p>We have in place appropriate security measures to protect your information from unauthorised access and will only work with third parties necessary to the completion of any services you requested from us.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Compliance with laws and your rights</span></strong></p>
<p>At Empire Chase, we make sure we stick to these data protection and e-privacy laws. Information about the laws, and your rights, can be found on the ICO&rsquo;s website &ndash; www.ico.org.uk.</p>
<p>You have the right to ask us not to process your personal information for marketing purposes. This can be done by checking or unchecking relevant boxes on our web forms, unsubscribing via any marketing emails we send, or contacting us to request the cessation of data processing or removal of your details at info@empirechase.co.uk</p>
<p>If you are not satisfied with the way in which your information is/has been handled then you have the right to lodge a complaint to the Information Commissioner&rsquo;s Office www.ico.org.uk</p>
<p>What information will Empire Chase collect about me?</p>
<p>Empire Chase may collect and hold the following information about you:</p>
<ul>
    <li>Information you provide to us such as when you complete a digital service enquiry form, or register for an account, email notifications and/or newsletters. On registration with empirechase.co.uk, we currently ask for your first and surname, your email address, telephone, house number and postcode and your property status. We ask you for this information so we can offer you relevant local content, information or services. We will always be clear about how this information will be used.</li>
    <li>Information we collect using cookies or similar technologies stored on your device are about how you use our Digital Channels. Cookies are very useful and do lots of different jobs which help to make your experience on our Digital Channels as smooth as possible. For example, they let you move between web pages efficiently, remembering your preferences, and generally improve your user experience. We provide much more information about how we use cookies and other similar technologies in our Cookies Policy.</li>
    <li>Empire Chase may decide to display 3rd party tracking numbers across marketing channels to identify which services and contact methods are most popular so we can ensure each is adequately serviced. The following is tracked on such tracking numbers; the number dialled, the diallers&rsquo; number, the time of the call, the length of the call and a recording of the call. The call recording is stored by default for a period of 30 days. We track this data as it aids staff training on how best to service our customers. You have the right to ask us not to track your personal information for marketing purposes. This can be done by contacting info@empirechase.co.uk to request a different non-tracking telephone number and / or the removal of your details.</li>
    <li>Any other information that you choose to send to us.</li>
</ul>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Information from our web servers&rsquo; logs, such as IP addresses</span></strong></p>
<p>This is used by us (Empire Chase to as &lsquo;us&rsquo; and &lsquo;our&rsquo;) to analyse activity on all our Digital Channels. For example, this helps us to look at the number of visits to specific pages to evaluate if we have sufficient infrastructure to support and maintain a great experience for our visitors. We may also collect, store and use the following kinds of information: Information about your computer, your visits to and use of this website (including but not limiting to your IP address, browser type and version, operating system, referral source, length of visit, page views).</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Information from social networking sites (such as LinkedIn, Facebook, YouTube, Instagram, Snapchat and Twitter)</span></strong></p>
<p>We collect information from social media activity, including likes, shares and tweets on our Digital Channels and about our content on social networking sites.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Transfers of Information Outside the European Economic Area</span></strong></p>
<p>Your use of our Digital Channels may result in the transfer of personal information provided by you across national boundaries. By visiting our Digital Channels and communicating with Empire Chase electronically, you consent to the transfer of your Personal Information outside the European Economic Area but in line with the guidelines of GDPR i.e. to countries that are approved under GDPR for these transfers.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Personal information of others</span></strong></p>
<p>Before you disclose to us the personal information of another person, you must obtain that person&rsquo;s consent to both the disclosure and the processing of that personal information in accordance with the terms of this privacy policy.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How will we use this information?</span></strong></p>
<p>Empire Chase will only hold and process your data in line with our legal obligations and the legitimate interests of Empire Chase. We use the information we collect about you in the following ways:</p>
<ul>
    <li>To help us to monitor, analyse and improve our Digital Channels. This information helps us to understand which content and services are most interesting, helpful to our users and if they are shared via social networks. The information also helps us to identify when errors occur within our Digital Channels and test different versions of a page or feature to see which one works best. All of which helps us improve our Digital Channels for our users.</li>
    <li>To provide you with a more personalised experience. By looking at the information we have about you, we can create a more tailored experience. For example, we will do this by making property suggestions.</li>
    <li>To inform the way we commission future content at Empire Chase. &nbsp;We will use aggregated and anonymous user data to help our Digital Marketing team understand what type of content our users like so that they can make more in the future.</li>
    <li>To market services to you. We will send you information about our Digital Channels and Services or other marketing activities but only where you have agreed to receive these from us and for the purposes stated at the collection point. We will email you or send you other marketing communications relating to Empire Chase services which we think may be of interest to you, by post or, by email or similar technology such as online display advertising. You can inform us at any time if you no longer require marketing communications.</li>
    <li>We may also invite you to participate in surveys from time to time to help us understand what you think about our programmes, products and services, and in turn make them better.</li>
</ul>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Does Empire Chase&rsquo;s privacy policy apply to third party websites or social networking sites?</span></strong></p>
<p>This policy only applies to our use of your information via the Empire Chase Digital Channels. Therefore, it does not apply to third party websites to which Empire Chase might provide links or on which Empire Chase content is embedded. Empire Chase is not responsible for the privacy practice or content of such websites.</p>
<p>Equally, it is important to note that when you access Empire Chase Digital Channels via a social networking site, such as Facebook or Twitter, that site may also collect and use your information. So, we would advise you to check the privacy policies for those social networking sites and applications to see how your information is being used.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How can I manage cookies?</span></strong></p>
<p>We provide more information about cookies and similar technologies used on our Digital Channels in our Cookies Policy. This also tells you how to control and manage cookies in your browser and how to opt-out of certain types of cookies.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Use of cookies</span></strong></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">COOKIE POLICY</span></strong></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\"><em>WHAT ARE COOKIES?</em></span></strong></p>
<ul>
    <li>Cookies are small files placed on your computer or device by our site when you visit certain parts of it and/or when you use certain features of our site. They are created when you open our website.</li>
    <li>Our website sends information to the browser you use, which then creates a text file.</li>
</ul>
<p>iii. These cookies are then stored as small files on your device for various periods of time.</p>
<ul>
    <li>Every time you come back to the website, the browser retrieves and sends cookies to the server, which helps to improve your experience of the website by remembering settings, so you don&rsquo;t have to re-enter them every time you visit</li>
    <li>Remembering your last properties and searches, helps you keep on top of new listings</li>
    <li>Measuring use of the website so that we can see where we can improve.</li>
</ul>
<p>vii. You can manage and/or delete these small files from your device, but please be aware that this will limit the functionality we then can offer to you.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">What about spam?</span></strong></p>
<p>We will only send targeted messages that we feel are relevant and are of interest to you.</p>
<p>Marketing emails you have signed up to will also tell you how you can unsubscribe should you decide that you no longer wish to receive them.</p>
<p>Please note that active clients may receive important information about Empire Chase Services that they have requested from us (such as changes to the Services themselves or other service administration matters). As an active user, this is not spam.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Will Empire Chase share my information with anyone else?</span></strong></p>
<p>Empire Chase will only hold and process your data in line with our legal obligations and the legitimate interests of Empire Chase. Empire Chase commits to keeping your personal information confidential and safe:</p>
<p>Other than is required by law or as set out in this Privacy Policy, Empire Chase will not disclose, rent or sell the Personal Information to any third party that is not required to fulfil the services requested, without your permission. Employees, contractors and agents of Empire Chase may be given access to any personal information which Empire Chase collect but their use shall be limited to performance of their duties in relation to the services provided by Empire Chase. We contractually require these service providers to safeguard the privacy and security of personal information they process on our behalf in line with GDPR and authorise them to use, disclose and store the information only as necessary to perform services on our behalf or comply with legal requirements.</p>
<p>Empire Chase will take reasonable precautions to prevent the loss, misuse or alteration of your Personal Information.</p>
<p>Those who have access to your personal information are required to keep that information confidential and are not permitted to use it for any purpose other than that to the services and products offered by Empire Chase or the use of our Digital Channels. Once we have received your information, we will use strict procedures and security features to protect data and prevent unauthorised access.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How long does Empire Chase keep my personal information?</span></strong></p>
<p>We will keep your information only for as long as it is relevant and useful for the purpose for which it was originally collected, and in line with the statutory data retention period set by HMRC. Beyond data we are obliged to hold by legal obligation/statute i.e. tenancy and contractual information you have the option to have your data deleted by emailing us on info@empirechase.co.uk</p>
<p>Empire Chase will remove the personal information when requested by you.</p>
<p>When using our Digital Channels anonymised data will be collected to help you navigate the website, we won&rsquo;t be able to recognise that data as yours or contact you from the data collated. Please note if you don&rsquo;t want to continue to receive relevant targeted advertising from our Digital Channels after you have left the Digital Channels, we recommend that you delete any cookies set on your device. If you want more information on how to manage your cookies, our Cookies Policy can help.</p>
<p>Where we hold other information (resulting from, for example, submitted competition answers) we will keep this for the minimum amount of time necessary for the purposes of running the competition or other relevant purpose.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Data Controller</span></strong></p>
<p>Empire Chase will be what is known as the &lsquo;Controller&rsquo; of the personal data you provide to us. Empire Chase company registration number is 07924071and its registered address is: 14 Peterborough Road, Harrow, HA1 2BQ.</p>
<p>The Data Protection Officer is Rasheda Begum and she can be contacted on: propertymanagement@empirechase.co.uk</p>
<p><br></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How can I access and update my personal information?</span></strong></p>
<p>Empire Chase recognises the rights of all of our clients under the EU GDPR, further detailed information on these specific rights can be found at the website of the UK Information Commissioners under www.ico.org.uk</p>
<p>Contact Us and we will be able to update your records on our data management systems &ndash; please let us know if the personal information which we hold about you needs to be corrected or updated.</p>
<p><br></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Access to your information</span></strong></p>
<p>Empire Chase recognises the rights of all of our clients under the EU GDPR, further detailed information on these specific rights can be found at the website of the UK Information Commissioners under www.ico.org.uk</p>
<p>If you would like to know what personal information Empire Chase holds about you, you may make a request to Empire Chase for copies of this information. You should be aware that we will need to see proof of identity before processing any such request. If requested, Empire Chase will use all reasonable efforts to supply, correct or delete information about you on file.</p>
<p>All subject access requests should be made in writing and addressed to Anika Vaghela, The Data Protection Officer, Empire Chase, 14 Peterborough Road, Harrow, HA1 2BQ.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Changes to this policy</span></strong></p>
<p>Our privacy policy may change, update or be altered from time to time. Any changes we may make to our privacy policy in the future will be posted on this page. You should check this page occasionally to ensure you are happy with any changes. We may also notify you of changes to our privacy policy by email.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How do I contact Empire Chase if have a question about privacy issues?</span></strong></p>
<p>If you have any questions or concerns about privacy issues, you can contact the Data Protection and Compliance department via email at info@empirechase.co.uk</p>
<p>Alternatively send your enquiry to: Empire Chase, 14 Peterborough Road, Harrow, HA1 2BQ.</p>
<p><br></p>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "cookie-policy.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<br>
    <br>
        <br>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Privacy and Cookie Policy</span></strong></p>
<p>We at Empire Chase estate agents are committed to respecting your online privacy and recognise your need for appropriate protection and management of any personally identifiable information (&ldquo;Personal Information&rdquo;) you share with us.</p>
<p>Empire Chase Limited (Registered office: 14 Peterborough Road, Harrow, HA1 2BQ, Registration Number: 07924071) is committed to protecting your personal information, being transparent about what data we hold and giving you control over how we use it.</p>
<p>The purpose of this privacy policy is to give you a clear explanation about how Empire Chase use personal and behavioural information we collect from you when you use our websites and mobile sites such as empirechase.co.uk &ndash; collectively called our &ldquo;Digital Channels&rdquo;.</p>
<p>It is important for you to read this policy in full to understand what information we hold about you, how we may use it and how you can access, update and delete your personal information. By accessing and using our Digital Channels, you are agreeing, or have agreed to, be bound by the Website Terms and Privacy Policy, and you consent to Empire Chase holding, processing and transferring any personal information that you provide Empire Chase as set out in this Privacy Policy.</p>
<p>If you don&rsquo;t have time to read the whole policy now, here are the main reasons we collect different types of information about our users:</p>
<ol>
    <li>To help us monitor and improve our Digital Channels</li>
    <li>To provide you with a more personalised experience, such as making property suggestions that are relevant to your search criteria</li>
    <li>To send you details about our related services, where you have signed up to receive such marketing communications</li>
    <li>Keep the Digital Channels secure and prevent fraud</li>
</ol>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Should I read any other information or terms?</span></strong></p>
<p>Whenever you use our Digital Channels, information may be collected through the use of cookies so you should read our Cookie Policy (COOKIES) for more information about this.</p>
<p><br></p>
<p>If you&rsquo;re interested in the legal recommendations, we recommend visiting the Information Commissioner&rsquo;s Office website (http://www.ico.org.uk) The ICO regulates data protection and e-privacy in the UK so there is lots of useful guidance about this on their website.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">What are Empire Chase&rsquo;s privacy principles?</span></strong></p>
<p>Empire Chase privacy principles, in respect of how it collects, uses and manages your personal information, are summarised as follows:</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">When we ask for data it&rsquo;s to give something back.</span></strong></p>
<p>Registering your details on our Digital Channels gives you access to a growing number of benefits, such as being able to read viewing feedback, track sales and lettings progression and save and compare properties that are of interest. Asking for your name means we know how to talk to you. Knowing your address and property status helps us to allocate the most appropriate member of our team to help you with your property needs and your contact details enables us to contact you regarding our services.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">We are clear on what data we hold and what we do with it.</span></strong></p>
<p>In collecting and using your personal information, we will be clear about what we are doing and make sure that you understand the way in which we are using it. Also, we won&rsquo;t sell your information to anyone.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">You control the data we hold on you.</span></strong></p>
<p>If requested, we will delete your personal information securely and swiftly and some of the information we hold on your activity on our Digital Channels will be anonymised, so we won&rsquo;t know it&rsquo;s yours.</p>
<p>We have in place appropriate security measures to protect your information from unauthorised access and will only work with third parties necessary to the completion of any services you requested from us.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Compliance with laws and your rights</span></strong></p>
<p>At Empire Chase, we make sure we stick to these data protection and e-privacy laws. Information about the laws, and your rights, can be found on the ICO&rsquo;s website &ndash; www.ico.org.uk.</p>
<p>You have the right to ask us not to process your personal information for marketing purposes. This can be done by checking or unchecking relevant boxes on our web forms, unsubscribing via any marketing emails we send, or contacting us to request the cessation of data processing or removal of your details at info@empirechase.co.uk</p>
<p>If you are not satisfied with the way in which your information is/has been handled then you have the right to lodge a complaint to the Information Commissioner&rsquo;s Office www.ico.org.uk</p>
<p>What information will Empire Chase collect about me?</p>
<p>Empire Chase may collect and hold the following information about you:</p>
<ul>
    <li>Information you provide to us such as when you complete a digital service enquiry form, or register for an account, email notifications and/or newsletters. On registration with empirechase.co.uk, we currently ask for your first and surname, your email address, telephone, house number and postcode and your property status. We ask you for this information so we can offer you relevant local content, information or services. We will always be clear about how this information will be used.</li>
    <li>Information we collect using cookies or similar technologies stored on your device are about how you use our Digital Channels. Cookies are very useful and do lots of different jobs which help to make your experience on our Digital Channels as smooth as possible. For example, they let you move between web pages efficiently, remembering your preferences, and generally improve your user experience. We provide much more information about how we use cookies and other similar technologies in our Cookies Policy.</li>
    <li>Empire Chase may decide to display 3rd party tracking numbers across marketing channels to identify which services and contact methods are most popular so we can ensure each is adequately serviced. The following is tracked on such tracking numbers; the number dialled, the diallers&rsquo; number, the time of the call, the length of the call and a recording of the call. The call recording is stored by default for a period of 30 days. We track this data as it aids staff training on how best to service our customers. You have the right to ask us not to track your personal information for marketing purposes. This can be done by contacting info@empirechase.co.uk to request a different non-tracking telephone number and / or the removal of your details.</li>
    <li>Any other information that you choose to send to us.</li>
</ul>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Information from our web servers&rsquo; logs, such as IP addresses</span></strong></p>
<p>This is used by us (Empire Chase to as &lsquo;us&rsquo; and &lsquo;our&rsquo;) to analyse activity on all our Digital Channels. For example, this helps us to look at the number of visits to specific pages to evaluate if we have sufficient infrastructure to support and maintain a great experience for our visitors. We may also collect, store and use the following kinds of information: Information about your computer, your visits to and use of this website (including but not limiting to your IP address, browser type and version, operating system, referral source, length of visit, page views).</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Information from social networking sites (such as LinkedIn, Facebook, YouTube, Instagram, Snapchat and Twitter)</span></strong></p>
<p>We collect information from social media activity, including likes, shares and tweets on our Digital Channels and about our content on social networking sites.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Transfers of Information Outside the European Economic Area</span></strong></p>
<p>Your use of our Digital Channels may result in the transfer of personal information provided by you across national boundaries. By visiting our Digital Channels and communicating with Empire Chase electronically, you consent to the transfer of your Personal Information outside the European Economic Area but in line with the guidelines of GDPR i.e. to countries that are approved under GDPR for these transfers.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Personal information of others</span></strong></p>
<p>Before you disclose to us the personal information of another person, you must obtain that person&rsquo;s consent to both the disclosure and the processing of that personal information in accordance with the terms of this privacy policy.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How will we use this information?</span></strong></p>
<p>Empire Chase will only hold and process your data in line with our legal obligations and the legitimate interests of Empire Chase. We use the information we collect about you in the following ways:</p>
<ul>
    <li>To help us to monitor, analyse and improve our Digital Channels. This information helps us to understand which content and services are most interesting, helpful to our users and if they are shared via social networks. The information also helps us to identify when errors occur within our Digital Channels and test different versions of a page or feature to see which one works best. All of which helps us improve our Digital Channels for our users.</li>
    <li>To provide you with a more personalised experience. By looking at the information we have about you, we can create a more tailored experience. For example, we will do this by making property suggestions.</li>
    <li>To inform the way we commission future content at Empire Chase. &nbsp;We will use aggregated and anonymous user data to help our Digital Marketing team understand what type of content our users like so that they can make more in the future.</li>
    <li>To market services to you. We will send you information about our Digital Channels and Services or other marketing activities but only where you have agreed to receive these from us and for the purposes stated at the collection point. We will email you or send you other marketing communications relating to Empire Chase services which we think may be of interest to you, by post or, by email or similar technology such as online display advertising. You can inform us at any time if you no longer require marketing communications.</li>
    <li>We may also invite you to participate in surveys from time to time to help us understand what you think about our programmes, products and services, and in turn make them better.</li>
</ul>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Does Empire Chase&rsquo;s privacy policy apply to third party websites or social networking sites?</span></strong></p>
<p>This policy only applies to our use of your information via the Empire Chase Digital Channels. Therefore, it does not apply to third party websites to which Empire Chase might provide links or on which Empire Chase content is embedded. Empire Chase is not responsible for the privacy practice or content of such websites.</p>
<p>Equally, it is important to note that when you access Empire Chase Digital Channels via a social networking site, such as Facebook or Twitter, that site may also collect and use your information. So, we would advise you to check the privacy policies for those social networking sites and applications to see how your information is being used.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How can I manage cookies?</span></strong></p>
<p>We provide more information about cookies and similar technologies used on our Digital Channels in our Cookies Policy. This also tells you how to control and manage cookies in your browser and how to opt-out of certain types of cookies.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Use of cookies</span></strong></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">COOKIE POLICY</span></strong></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\"><em>WHAT ARE COOKIES?</em></span></strong></p>
<ul>
    <li>Cookies are small files placed on your computer or device by our site when you visit certain parts of it and/or when you use certain features of our site. They are created when you open our website.</li>
    <li>Our website sends information to the browser you use, which then creates a text file.</li>
</ul>
<p>iii. These cookies are then stored as small files on your device for various periods of time.</p>
<ul>
    <li>Every time you come back to the website, the browser retrieves and sends cookies to the server, which helps to improve your experience of the website by remembering settings, so you don&rsquo;t have to re-enter them every time you visit</li>
    <li>Remembering your last properties and searches, helps you keep on top of new listings</li>
    <li>Measuring use of the website so that we can see where we can improve.</li>
</ul>
<p>vii. You can manage and/or delete these small files from your device, but please be aware that this will limit the functionality we then can offer to you.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">What about spam?</span></strong></p>
<p>We will only send targeted messages that we feel are relevant and are of interest to you.</p>
<p>Marketing emails you have signed up to will also tell you how you can unsubscribe should you decide that you no longer wish to receive them.</p>
<p>Please note that active clients may receive important information about Empire Chase Services that they have requested from us (such as changes to the Services themselves or other service administration matters). As an active user, this is not spam.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Will Empire Chase share my information with anyone else?</span></strong></p>
<p>Empire Chase will only hold and process your data in line with our legal obligations and the legitimate interests of Empire Chase. Empire Chase commits to keeping your personal information confidential and safe:</p>
<p>Other than is required by law or as set out in this Privacy Policy, Empire Chase will not disclose, rent or sell the Personal Information to any third party that is not required to fulfil the services requested, without your permission. Employees, contractors and agents of Empire Chase may be given access to any personal information which Empire Chase collect but their use shall be limited to performance of their duties in relation to the services provided by Empire Chase. We contractually require these service providers to safeguard the privacy and security of personal information they process on our behalf in line with GDPR and authorise them to use, disclose and store the information only as necessary to perform services on our behalf or comply with legal requirements.</p>
<p>Empire Chase will take reasonable precautions to prevent the loss, misuse or alteration of your Personal Information.</p>
<p>Those who have access to your personal information are required to keep that information confidential and are not permitted to use it for any purpose other than that to the services and products offered by Empire Chase or the use of our Digital Channels. Once we have received your information, we will use strict procedures and security features to protect data and prevent unauthorised access.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How long does Empire Chase keep my personal information?</span></strong></p>
<p>We will keep your information only for as long as it is relevant and useful for the purpose for which it was originally collected, and in line with the statutory data retention period set by HMRC. Beyond data we are obliged to hold by legal obligation/statute i.e. tenancy and contractual information you have the option to have your data deleted by emailing us on info@empirechase.co.uk</p>
<p>Empire Chase will remove the personal information when requested by you.</p>
<p>When using our Digital Channels anonymised data will be collected to help you navigate the website, we won&rsquo;t be able to recognise that data as yours or contact you from the data collated. Please note if you don&rsquo;t want to continue to receive relevant targeted advertising from our Digital Channels after you have left the Digital Channels, we recommend that you delete any cookies set on your device. If you want more information on how to manage your cookies, our Cookies Policy can help.</p>
<p>Where we hold other information (resulting from, for example, submitted competition answers) we will keep this for the minimum amount of time necessary for the purposes of running the competition or other relevant purpose.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Data Controller</span></strong></p>
<p>Empire Chase will be what is known as the &lsquo;Controller&rsquo; of the personal data you provide to us. Empire Chase company registration number is 07924071and its registered address is: 14 Peterborough Road, Harrow, HA1 2BQ.</p>
<p>The Data Protection Officer is Rasheda Begum and she can be contacted on: propertymanagement@empirechase.co.uk</p>
<p><br></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How can I access and update my personal information?</span></strong></p>
<p>Empire Chase recognises the rights of all of our clients under the EU GDPR, further detailed information on these specific rights can be found at the website of the UK Information Commissioners under www.ico.org.uk</p>
<p>Contact Us and we will be able to update your records on our data management systems &ndash; please let us know if the personal information which we hold about you needs to be corrected or updated.</p>
<p><br></p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Access to your information</span></strong></p>
<p>Empire Chase recognises the rights of all of our clients under the EU GDPR, further detailed information on these specific rights can be found at the website of the UK Information Commissioners under www.ico.org.uk</p>
<p>If you would like to know what personal information Empire Chase holds about you, you may make a request to Empire Chase for copies of this information. You should be aware that we will need to see proof of identity before processing any such request. If requested, Empire Chase will use all reasonable efforts to supply, correct or delete information about you on file.</p>
<p>All subject access requests should be made in writing and addressed to Anika Vaghela, The Data Protection Officer, Empire Chase, 14 Peterborough Road, Harrow, HA1 2BQ.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">Changes to this policy</span></strong></p>
<p>Our privacy policy may change, update or be altered from time to time. Any changes we may make to our privacy policy in the future will be posted on this page. You should check this page occasionally to ensure you are happy with any changes. We may also notify you of changes to our privacy policy by email.</p>
<p><strong><span style=\"color: rgb(41, 105, 176);\">How do I contact Empire Chase if have a question about privacy issues?</span></strong></p>
<p>If you have any questions or concerns about privacy issues, you can contact the Data Protection and Compliance department via email at info@empirechase.co.uk</p>
<p>Alternatively send your enquiry to: Empire Chase, 14 Peterborough Road, Harrow, HA1 2BQ.</p>
<p><br></p>", "cookie-policy.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\cookie-policy.html.twig");
    }
}
