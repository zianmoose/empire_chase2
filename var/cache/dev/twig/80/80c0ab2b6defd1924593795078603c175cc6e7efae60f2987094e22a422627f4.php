<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* aboutUs.html.twig */
class __TwigTemplate_3ea899a35659b0421ec86a219467c3a0a64b3b8e80458af0f6216ed997c5a533 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "aboutUs.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "aboutUs.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "aboutUs.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id=\"about-us\" class=\"jumbotron jumbotron-fluid services-jumbo\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">ABOUT US</h1>
\t\t\t\t<p></p>

\t\t\t</div>

\t\t\t";
        // line 18
        echo "
\t\t</div>

\t\t<div class=\"container\">


\t\t\t<p class=\"text-center lead\">Welcome to Empire Chase, we are an upmarket property firm offering real estate services in Sales, Lettings, Property Management, Asset management and block management services in North West London and Central London.</p>
\t\t\t<p class=\"text-center lead\">Our business has significantly grown since 2012 with a vast number of properties, corporate landlords and institutional clients.</p>
\t\t\t<p class=\"text-center lead\">Our business is based on customer services, trust and relationship.</p>
\t\t\t<p class=\"text-center lead\">‘We are glad to say our first clients from 2012 are still with us and we never had a client who had left us with bitterness’
\t\t\t</p>
\t\t\t<br>

\t\t\t<!--  image left  -->
\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\">Our Brands</h2>
\t\t\t<br>
\t\t\t<div class=\"row\" style=\"min-height: 500px;\">
\t\t\t\t<div
\t\t\t\t\tclass=\"col-md-6 animation-element slide-left\">

\t\t\t\t\t";
        // line 41
        echo "\t\t\t\t\t<div style=\"width: 100%;\"><img src=\"about-us/our-brands.jpeg\" style=\"width: 100%;\" alt=\"Empire Chase Premire\"/></div>

\t\t\t\t\t";
        // line 44
        echo "\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div id=\"col-brad\" class=\"col\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<h3>Empire Chase</h3>

\t\t\t\t\t\t\t<p class=\"lead\">Real estate firm offering real estate service in property sales, rentals, property management and asset management in London</p>

\t\t\t\t\t\t\t<h3>Empire Chase Premier</h3>

\t\t\t\t\t\t\t<p class=\"lead\">Premier, private and luxury real estate services offered to sports personalities, musicians, and high-net-worth individuals.</p>
\t\t\t\t\t\t\t<h3>101 Property Services
\t\t\t\t\t\t\t</h3>
\t\t\t\t\t\t\t<p class=\"lead\">Property services firm offering inventory reports, interior designing, property inspections and property staging.</p>
\t\t\t\t\t\t\t<h3>Empire Chase Hong Kong & China
\t\t\t\t\t\t\t</h3>
\t\t\t\t\t\t\t<p class=\"lead\">Real estate services offered to clients in Hong Kong, China, and the Far east.</p>

\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br>
\t\t\t<br>
\t\t\t<!-- /.row -->

\t\t\t<!--  affliations  --><h2 id=\"registration\" class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\"> Registrations and affliations</h2>
\t\t\t<br>
\t\t\t<p class=\"text-center lead\">You are in safe hand with all our industry affiliations and registrations.</p>
\t\t\t<br>
\t\t\t<div class=\"row\" style\"display: flex;align-items: center;justify-content: center;\">
\t\t\t\t<div
\t\t\t\t\tclass=\"col-md-6 order-md-6 animation-element slide-left\">

\t\t\t\t\t";
        // line 86
        echo "\t\t\t\t\t<img src=\"about-us/CMP-Electronic-Certificate-2021.jpg\" alt=\"\" style=\"width:60%;\"/>
\t\t\t\t</div>

\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<p class=\"lead\">Registered address: Empire Chase Limited, 14 Peterborough Road, Harrow, Middlesex, HA1 2BQ, United Kingdom

\t\t\t\t\t\t\t\t<br/></p>
\t\t\t\t\t\t\t<p class=\"lead\">Company registration: 07924071
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">VAT Registration: 228 5335 05</p>
\t\t\t\t\t\t\t<p class=\"lead\">Property Ombudsman Registration: D7051
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Safe Agents Registration: A6183
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Client Money Protection: Provided by Safe Agent (License number A6183)
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Professional Indemnity Policy PI18W764621
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">HMRC Non-Resident Landlord Registration number: NA 056002
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">HMRC Anti Money Laundering Registration number: 12848573
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Data protection GDPR registration number: Z3103364</p>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br/><br/>
\t\t\t<!----------------------------------------------------------------------------Team-------------------------------------------------------- -->

\t\t\t";
        // line 210
        echo "

\t\t\t<!-- --------------------------------------------------------------------------Team end-------------------------------------------------------- -->


\t\t\t\t<div class=\"container\" style=\"margin-top: 50px;\"> <h2 id=\"testimonials\" class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Reviews</h2>
\t\t\t\t<br>
\t\t\t\t<p class=\"review-content-allign lead\">Over 700 ⭐️⭐️⭐️⭐️⭐️ reviews across various review platforms</p>
\t\t\t\t<p class=\"review-content-allign lead\">Click the icon below to read our reviews</p>
\t\t\t\t<div class=\"row\">

\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"review-content\">
\t\t\t\t\t\t\t";
        // line 225
        echo "\t\t\t\t\t\t\t<a href=\"https://www.theestas.com/reviews/338?u=widget\"><img src=\"estas.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
        // line 229
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>


\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t\t\t<a href=\"https://www.allagents.co.uk/empire-chase/\">
\t\t\t\t\t\t\t\t<img src=\"allagents.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
        // line 241
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/reviews/?ref=page_internal\">
\t\t\t\t\t\t\t\t<img src=\"facebook.png\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
        // line 251
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t\t\t<a href=\"https://www.google.com/search?q=empire+chase&sxsrf=ALeKk029__j1ehm282gooR0K0TteBohXdQ%3A1621291307613&source=hp&ei=K_GiYMv4IYTk5NoPjeCPaA&iflsig=AINFCbYAAAAAYKL_O2ThdMWXFeCRPUUwwuLcGcUNUc9O&gs_ssp=eJzj4tFP1zcsKCnPKSszLTRgtFI1qDCxMDczNDY3STVIMze2TEqzMqhIsjQxSUm1SE0xMDVNtUwy9eJJzS3ILEpVSM5ILE4FAJiiFCY&oq=empire+chase&gs_lcp=Cgdnd3Mtd2l6EAEYADILCC4QxwEQrwEQkwIyAggAMgIIADICCAAyAggAMgIIADoICAAQsQMQgwE6BQgAELEDOggILhCxAxCDAToICC4QxwEQowI6CAguEMcBEK8BOgUILhCxAzoOCC4QsQMQxwEQrwEQkwI6CwguELEDEMcBEK8BOgUIABDJAzoLCC4QsQMQxwEQowI6CAgAELEDEMkDOgUILhCTAjoCCC5QjQpY5CNgvzdoAHAAeACAAWqIAfcHkgEDOS4zmAEAoAEBqgEHZ3dzLXdpeg&sclient=gws-wiz#lrd=0x48761374e0f739bf:0xb944de8ed055e9b5,1,,,\">
\t\t\t\t\t\t\t\t<img src=\"googlereviews.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
        // line 261
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</div>

\t\t\t<br/><br/>
\t\t\t<br/><br/>
\t\t\t<!-- /.row  affliations -->

\t\t\t<!--  image right  -->
\t\t\t\t<div class=\"container\"> <div
\t\t\t\t\tclass=\"row\">

\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\"></div>

\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t</div>
\t\t\t\t<!-- /.row -->
\t\t\t</div>
\t\t\t<!-- /container -->

\t\t\t<div class=\"container\">
\t\t\t\t<h1 id=\"awards\" class=\"h2-letter-spacing review-content-allign\">Awards</h1>
\t\t\t\t<br/><br/>
\t\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t\t<div class=\"owl-carousel\">

\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award4.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award5.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award6.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award7.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award8.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award9.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award10.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award11.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t\t";
        // line 344
        echo "\t\t\t\t\t</div>


\t\t\t\t</div>
\t\t\t</div>

\t\t</div>


\t</main>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "aboutUs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  327 => 344,  246 => 261,  236 => 251,  226 => 241,  214 => 229,  210 => 225,  194 => 210,  152 => 86,  109 => 44,  105 => 41,  83 => 18,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id=\"about-us\" class=\"jumbotron jumbotron-fluid services-jumbo\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">ABOUT US</h1>
\t\t\t\t<p></p>

\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div class=\"container\">


\t\t\t<p class=\"text-center lead\">Welcome to Empire Chase, we are an upmarket property firm offering real estate services in Sales, Lettings, Property Management, Asset management and block management services in North West London and Central London.</p>
\t\t\t<p class=\"text-center lead\">Our business has significantly grown since 2012 with a vast number of properties, corporate landlords and institutional clients.</p>
\t\t\t<p class=\"text-center lead\">Our business is based on customer services, trust and relationship.</p>
\t\t\t<p class=\"text-center lead\">‘We are glad to say our first clients from 2012 are still with us and we never had a client who had left us with bitterness’
\t\t\t</p>
\t\t\t<br>

\t\t\t<!--  image left  -->
\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\">Our Brands</h2>
\t\t\t<br>
\t\t\t<div class=\"row\" style=\"min-height: 500px;\">
\t\t\t\t<div
\t\t\t\t\tclass=\"col-md-6 animation-element slide-left\">

\t\t\t\t\t{# <div id=\"first-logo\"><img src=\"about-us/101logo.jpg\" alt=\"101 Properties\"/></div>
\t\t\t\t\t\t\t\t\t\t<div id=\"second-logo\"><img src=\"about-us/Empire-Logo.jpg\" alt=\"Empire Chase\"/></div>
\t\t\t\t\t\t\t\t\t\t<div id=\"third-logo\"><img src=\"about-us/Empire-premier-Logo.jpg\" alt=\"Empire Chase Premire\"/></div> #}
\t\t\t\t\t<div style=\"width: 100%;\"><img src=\"about-us/our-brands.jpeg\" style=\"width: 100%;\" alt=\"Empire Chase Premire\"/></div>

\t\t\t\t\t{# <img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6a/Imac_alu.png\" alt=\"\" class=\"w-100\" /> #}
\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div id=\"col-brad\" class=\"col\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<h3>Empire Chase</h3>

\t\t\t\t\t\t\t<p class=\"lead\">Real estate firm offering real estate service in property sales, rentals, property management and asset management in London</p>

\t\t\t\t\t\t\t<h3>Empire Chase Premier</h3>

\t\t\t\t\t\t\t<p class=\"lead\">Premier, private and luxury real estate services offered to sports personalities, musicians, and high-net-worth individuals.</p>
\t\t\t\t\t\t\t<h3>101 Property Services
\t\t\t\t\t\t\t</h3>
\t\t\t\t\t\t\t<p class=\"lead\">Property services firm offering inventory reports, interior designing, property inspections and property staging.</p>
\t\t\t\t\t\t\t<h3>Empire Chase Hong Kong & China
\t\t\t\t\t\t\t</h3>
\t\t\t\t\t\t\t<p class=\"lead\">Real estate services offered to clients in Hong Kong, China, and the Far east.</p>

\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br>
\t\t\t<br>
\t\t\t<!-- /.row -->

\t\t\t<!--  affliations  --><h2 id=\"registration\" class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\"> Registrations and affliations</h2>
\t\t\t<br>
\t\t\t<p class=\"text-center lead\">You are in safe hand with all our industry affiliations and registrations.</p>
\t\t\t<br>
\t\t\t<div class=\"row\" style\"display: flex;align-items: center;justify-content: center;\">
\t\t\t\t<div
\t\t\t\t\tclass=\"col-md-6 order-md-6 animation-element slide-left\">

\t\t\t\t\t{# <img src=\"about-us/branch_photo.jpeg\" alt=\"\" class=\"w-100\" /> #}
\t\t\t\t\t<img src=\"about-us/CMP-Electronic-Certificate-2021.jpg\" alt=\"\" style=\"width:60%;\"/>
\t\t\t\t</div>

\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<p class=\"lead\">Registered address: Empire Chase Limited, 14 Peterborough Road, Harrow, Middlesex, HA1 2BQ, United Kingdom

\t\t\t\t\t\t\t\t<br/></p>
\t\t\t\t\t\t\t<p class=\"lead\">Company registration: 07924071
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">VAT Registration: 228 5335 05</p>
\t\t\t\t\t\t\t<p class=\"lead\">Property Ombudsman Registration: D7051
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Safe Agents Registration: A6183
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Client Money Protection: Provided by Safe Agent (License number A6183)
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Professional Indemnity Policy PI18W764621
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">HMRC Non-Resident Landlord Registration number: NA 056002
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">HMRC Anti Money Laundering Registration number: 12848573
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p class=\"lead\">Data protection GDPR registration number: Z3103364</p>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<br/><br/>
\t\t\t<!----------------------------------------------------------------------------Team-------------------------------------------------------- -->

\t\t\t{# <!-- Ui cards -->
\t\t\t\t\t\t\t<section id=\"cards\"> <div class=\"container py-2\">
\t\t\t\t\t\t\t\t<div class=\"row pb-4\">
\t\t\t\t\t\t\t\t\t<div class=\"col-12 text-center\">
\t\t\t\t\t\t\t\t\t\t<h2 id=\"registration\" class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Our team</h2>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</br>
\t\t\t\t\t\t\t\t<!-- cards -->
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t
\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-6 mb-4 pt-5\">
\t\t\t\t\t\t\t\t<div class=\"card shadow-sm border-0\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"user-picture\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"about-us/team/Naz.jpeg\" class=\"shadow-sm rounded-circle\" height=\"130\" width=\"130\"/>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"user-content\">
\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"text-capitalize user-name\">Naz Nizar</h5>
\t\t\t\t\t\t\t\t\t\t\t<p class=\" text-capitalize text-muted small blockquote-footer\">Director</p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"small text-muted mb-0\">With a great passion to set better standards in real estate Naz found Empire Chase in 2012. His seamless handwork, effort and skills has enabled Empire Chase to grow fast to becoming one of the best property firm in Harrow in a short time frame.
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"small text-muted mb-0\">Naz offers a very professional and personalised services to all the clients. He puts himself to every department in the firm to ensure every aspect of the business is running smoothly, accurately and offering exceptional services.</p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"small text-muted mb-0\">Naz and his team constantly strive to perfect the quality of service and the marketing of real estate to the highest level.</p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"small text-muted mb-0\">Hobbies - football, aeroplane, nature and enjoying family time with his wife and son</p>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-6 mb-4 pt-5\">
\t\t\t\t\t\t\t\t\t\t<div class=\"card shadow-sm border-0\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-picture\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"about-us/team/Jasmine.jpeg\" class=\"shadow-sm rounded-circle\" height=\"130\" width=\"130\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-content\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"text-capitalize user-name\">Jasmine Elliston</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\" text-capitalize text-muted small blockquote-footer\">Senior Property Manager</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"small text-muted mb-0\">Our property management department is headed by Jasmine. She is a knowledgeable and organised professional who keeps our property management department up to speed.
\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                    Jasmine is member of ARLA and has been in the lettings sector for over 6 years. Jasmine enjoys providing a good service to tenants and clients to ensure their tenancy is as enjoyable as possible.
\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-6 mb-4 pt-5\">
\t\t\t\t\t\t\t\t\t\t<div class=\"card shadow-sm border-0\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-picture\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"about-us/team/Zee.jpeg\" class=\"shadow-sm rounded-circle\" height=\"130\" width=\"130\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-content\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"text-capitalize user-name\">Zee Cheema</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\" text-capitalize text-muted small blockquote-footer\">Real estate broker</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"small text-muted mb-0\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nemo harum repellendus aut itaque. Temporibus quaerat dolores ut, cupiditate molestiae commodi! Distinctio praesentium, debitis aut minima doloribus earum
\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                        quia commodi.</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-6 mb-4 pt-5\">
\t\t\t\t\t\t\t\t\t\t<div class=\"card shadow-sm border-0\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-picture\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"about-us/team/Anojan.jpeg\" class=\"shadow-sm rounded-circle\" height=\"130\" width=\"130\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-content\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"text-capitalize user-name\">Anojan Chandrarajah</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\" text-capitalize text-muted small blockquote-footer\">Office management administrator</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"small text-muted mb-0\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nemo harum repellendus aut itaque. Temporibus quaerat dolores ut, cupiditate molestiae commodi! Distinctio praesentium, debitis aut minima doloribus earum
\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                        quia commodi.</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t
\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- /cards -->
\t\t\t\t\t\t</section>
\t\t\t\t\t\t<!-- /Ui cards --> #}


\t\t\t<!-- --------------------------------------------------------------------------Team end-------------------------------------------------------- -->


\t\t\t\t<div class=\"container\" style=\"margin-top: 50px;\"> <h2 id=\"testimonials\" class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Reviews</h2>
\t\t\t\t<br>
\t\t\t\t<p class=\"review-content-allign lead\">Over 700 ⭐️⭐️⭐️⭐️⭐️ reviews across various review platforms</p>
\t\t\t\t<p class=\"review-content-allign lead\">Click the icon below to read our reviews</p>
\t\t\t\t<div class=\"row\">

\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"review-content\">
\t\t\t\t\t\t\t{# <p>class=\"col-xs-6 col-sm-6 col-md-3\"</p> #}
\t\t\t\t\t\t\t<a href=\"https://www.theestas.com/reviews/338?u=widget\"><img src=\"estas.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t{# <p class=\"review-content-allign\">97%</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 249 reviews</p> #}

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>


\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t\t\t<a href=\"https://www.allagents.co.uk/empire-chase/\">
\t\t\t\t\t\t\t\t<img src=\"allagents.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t{# <p class=\"review-content-allign\">4.9/5</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 233 reviews</p> #}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/reviews/?ref=page_internal\">
\t\t\t\t\t\t\t\t<img src=\"facebook.png\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t{# <p class=\"review-content-allign\">5/5</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 81 reviews</p> #}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t\t\t<a href=\"https://www.google.com/search?q=empire+chase&sxsrf=ALeKk029__j1ehm282gooR0K0TteBohXdQ%3A1621291307613&source=hp&ei=K_GiYMv4IYTk5NoPjeCPaA&iflsig=AINFCbYAAAAAYKL_O2ThdMWXFeCRPUUwwuLcGcUNUc9O&gs_ssp=eJzj4tFP1zcsKCnPKSszLTRgtFI1qDCxMDczNDY3STVIMze2TEqzMqhIsjQxSUm1SE0xMDVNtUwy9eJJzS3ILEpVSM5ILE4FAJiiFCY&oq=empire+chase&gs_lcp=Cgdnd3Mtd2l6EAEYADILCC4QxwEQrwEQkwIyAggAMgIIADICCAAyAggAMgIIADoICAAQsQMQgwE6BQgAELEDOggILhCxAxCDAToICC4QxwEQowI6CAguEMcBEK8BOgUILhCxAzoOCC4QsQMQxwEQrwEQkwI6CwguELEDEMcBEK8BOgUIABDJAzoLCC4QsQMQxwEQowI6CAgAELEDEMkDOgUILhCTAjoCCC5QjQpY5CNgvzdoAHAAeACAAWqIAfcHkgEDOS4zmAEAoAEBqgEHZ3dzLXdpeg&sclient=gws-wiz#lrd=0x48761374e0f739bf:0xb944de8ed055e9b5,1,,,\">
\t\t\t\t\t\t\t\t<img src=\"googlereviews.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t{# <p class=\"review-content-allign\">4.9/5</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 149 reviews</p> #}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</div>

\t\t\t<br/><br/>
\t\t\t<br/><br/>
\t\t\t<!-- /.row  affliations -->

\t\t\t<!--  image right  -->
\t\t\t\t<div class=\"container\"> <div
\t\t\t\t\tclass=\"row\">

\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\"></div>

\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t</div>
\t\t\t\t<!-- /.row -->
\t\t\t</div>
\t\t\t<!-- /container -->

\t\t\t<div class=\"container\">
\t\t\t\t<h1 id=\"awards\" class=\"h2-letter-spacing review-content-allign\">Awards</h1>
\t\t\t\t<br/><br/>
\t\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t\t<div class=\"owl-carousel\">

\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award4.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award5.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award6.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award7.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award8.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award9.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award10.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award11.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t\t{# <div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> #}
\t\t\t\t\t</div>


\t\t\t\t</div>
\t\t\t</div>

\t\t</div>


\t</main>


{% endblock %}
", "aboutUs.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\aboutUs.html.twig");
    }
}
