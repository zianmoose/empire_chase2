<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landlords.html.twig */
class __TwigTemplate_0a4f46b0c24b9b61d9efee1e560df962c512763f1cfbd7044a4ec754226e8222 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landlords.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landlords.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "landlords.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id=\"lanlord-banner\" class=\"jumbotron jumbotron-fluid services-jumbo\" >

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">LANDLORDS</h1>
\t\t\t\t<p>Our award-winning rental and property management services offers a safe, transparent and hassle-free services to all landlords.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 20
        echo "
\t\t</div>

\t\t<div class=\"container\">

\t\t\t<p class=\"lead review-content-allign\">Our award-winning rental and property management services offers a safe, transparent and hassle-free services to all landlords.</p>
\t\t\t<p class=\"lead review-content-allign\">Whether you are a corporate landlord, or first time buy to let landlord we offer a hands on professional services to call clients.</p>
\t\t\t<p id=\"buytolet\" class=\"lead review-content-allign\">Our lettings team has won multiple awards for ‘Best letting agent in Harrow’ since 2016 consecutively each year.</p>
\t\t\t<br>

\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Why choose us
\t\t\t</h2>
\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Comprehensive marketing strategy and modern techniques
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">24 Hour property enquiry line
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Our connections with corporate firms
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Premier Property Listings on Rightmove, Zoopla, On the Market</li>
\t\t\t\t<li class=\"lead\">Stunning property photography</li>
\t\t\t\t<li class=\"lead\">Unique pricing stratergy
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Award winning services
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Honest, friendly and professional services
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Full referencing & rent guarantee</li>
\t\t\t\t<li class=\"lead\">Property managers working round the clock</li>
\t\t\t\t<li class=\"lead\">Qualified property management team</li>
\t\t\t\t<li class=\"lead\">Fluent with the new legislations and compliance</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<p class=\"lead review-content-allign\">Check out our reviews from our landlords.</p>
\t\t\t<div class=\"container\">
\t\t\t\t<div id=\"wd_id\" style=\"    max-width: 200px;display: block;margin-left: auto;margin-right: auto;\"></div>
\t\t\t\t<script type=\"text/javascript\" src=\"https://www.allagents.co.uk/widgets/ratings.js\"></script>
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\tinit_widget(200, 150, 9542, 'plain', '000', '')
\t\t\t\t</script>
\t\t\t</div>
\t\t\t<br>


\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Lettings tenant finder services
\t\t\t</h2>
\t\t\t<br>
\t\t\t<p class=\"lead \">With over 690 lettings properties under our lettings portfolio our proactive team covers Harrow, Pinner, Wembley, Stanmore, Edgware, Northwood and Central London.</p>
\t\t\t<p class=\"lead \">We utilise all the modern marketing tools and techniques to market your property to gain maximum exposure.
\t\t\t</p>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Professional photography and videography with floor plans
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Virtual viewings
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">We advertise on Rightmove, Zoopla, On the market, Find a property and Boomin
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Social media marketing (Facebook, Instagram, Linked in)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Local advertising</li>
\t\t\t\t<li class=\"lead\">24 hour property enquiry line – We don’t miss any calls
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Services includes
\t\t\t</h2>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Proactive marketing and advertising
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Virtual viewings
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Conduct viewings and pass on offers to the landlord
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Arrange full referencing through Homelet referencing</li>
\t\t\t\t<li class=\"lead\">Right to rent checks</li>
\t\t\t\t<li class=\"lead\">Prepare contact and serve statutory document
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Arrange inventory check in</li>
\t\t\t\t<li class=\"lead\">Deposit registration</li>
\t\t\t\t<li class=\"lead\">Transfer utility bills
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>

\t\t\t<br>

\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Property management services</h2>
\t\t\t<br>
\t\t\t<p class=\"lead \">From Harrow to Canary wharf and from Edgware to Central London our property management team covers a wide area of properties in London.</p>
\t\t\t<p class=\"lead \">Our super-efficient property management team is highly appreciated by all our existing landlords and tenants. Offering a 24-hour property management services to all our managed properties below are some of our USP
\t\t\t</p>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">24 hour emergency line
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Virtual viewings
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Experienced and qualified property managers
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Efficient and prompt response services
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Complaint and up to date with all the new regulations</li>
\t\t\t\t<li class=\"lead\">Monthly/quarterly client reporting
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Work within client SLA and budget g
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Honest, transparent and efficient service
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<p class=\"lead \">In addition to the lettings tenant finder services below are the services we offer in our property management services
\t\t\t</p>

\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Services includes
\t\t\t</h2>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Organise inventory check ins and check outs
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Move the tenants in
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Register tenants for utility bills
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Register the deposit
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">24 hour property emergency line for tenants</li>
\t\t\t\t<li class=\"lead\">Organise contractors
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Organise safety check such as electric and gas
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Rent collection</li>
\t\t\t\t<li class=\"lead\">Produce statement of accounts each quarter
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Deposit dispute resolution
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>

\t\t\t<section class=\"numbers white-txt justify-content-center\" style=\"padding:0px; background-color: #484848!important;\">
\t\t\t\t<div
\t\t\t\t\tclass=\"marged flex\">
\t\t\t\t\t<!-- item 01 -->
\t\t\t\t\t<div class=\"number-item\" style=\"text-align: center;\">
\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t<span class=\"value\">414</span>
\t\t\t\t\t\t</h2>
\t\t\t\t\t\t<h6>Properties managed</h6>
\t\t\t\t\t</div>

\t\t\t\t\t<!-- item 02 -->
\t\t\t\t\t<div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t<span class=\"value\">99</span>%</h2>
\t\t\t\t\t\t<h6>of our clients paid full rent and didn’t fall in to rent arrears in the past 12 months</h6>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
\t\t\t\t\t<h2>£
\t\t\t\t\t\t<span class=\"value\">85</span>,<span class=\"value\">639</span>,<span class=\"value\">202</span>
\t\t\t\t\t</h2>
\t\t\t\t\t<h6>worth of asset and properties managed</h6>
\t\t\t\t</div>

\t\t\t</section>
\t\t\t<br>

\t\t\t<p class=\"lead \">Our property management services can be signed up by any landlord whether the property is self-managed or managed by any other managing agent. We offer a hassle-free switching process.
\t\t\t</p>
\t\t\t<p class=\"lead \">If you would like to know how proactive our property management services are and why we are better than others, please contact our property management team
\t\t\t</p>
\t\t\t

\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 id=\"premierprop\" class=\"review-content-allign h2-letter-spacing\">Premier property management services</h2>
\t\t\t\t<br>


\t\t\t\t<p class=\"lead\">We have a premier service for clients who seek a complete management for their single asset.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">This is mainly designed for overseas landlords who do not live in the UK</p>
\t\t\t\t<br>
\t\t\t\t<h2 id=\"premierprop\" class=\"review-content-allign h2-letter-spacing\">Services includes
\t\t\t\t</h2>
\t\t\t\t<ul style = \"margin-left:30px;\">
\t\t\t\t\t<li class=\"lead\">Organise inventory check in</li>
\t\t\t\t\t<li class=\"lead\">Move the tenants in</li>
\t\t\t\t\t<li class=\"lead\">Register tenants for utility bills</li>
\t\t\t\t\t<li class=\"lead\">Register the deposit</li>
\t\t\t\t\t<li class=\"lead\">24 hour property emergency line for tenants</li>
\t\t\t\t\t<li class=\"lead\">Look after the day-to-day maintenance issues</li>
\t\t\t\t\t<li class=\"lead\">Organise contractors</li>
\t\t\t\t\t<li class=\"lead\">Organise safety check such as electric and gas</li>
\t\t\t\t\t<li class=\"lead\">Rent collection</li>
\t\t\t\t\t<li class=\"lead\">Produce statement of accounts each quarter</li>
\t\t\t\t\t<li class=\"lead\">Deposit depute resolution</li>
\t\t\t\t\t<li class=\"lead\">Dedicated account manager with 24-hour access</li>
\t\t\t\t\t<li class=\"lead\">Monthly statement of accounts</li>
\t\t\t\t\t<li class=\"lead\">Pay any outgoings on behalf of the landlord and reimburse via rent collection</li>
\t\t\t\t\t<li class=\"lead\">Represent the landlord</li>
\t\t\t\t\t<li class=\"lead\">Register for Non-Residents landlords</li>
\t\t\t\t\t<li class=\"lead\">Assist with any other property related issues and matters</li>

\t\t\t\t</ul>

\t\t\t\t<br>


\t\t\t</div>

\t\t\t<div class=\"animation-element slide-left\">
\t\t<iframe src=\"https://www.facebook.com/plugins/video.php?height=315&href=https%3A%2F%2Fwww.facebook.com%2Fempirechaseestateagents%2Fvideos%2F4954274441280932%2F&show_text=false&width=560\" width=\"100%\" max-width=\"560\" height=\"315\" style=\"border:none;overflow:hidden display: block;margin-left: auto;margin-right: auto;\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" allow=\"autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share\" allowFullScreen=\"true\"></iframe>
\t\t\t</div>

\t\t\t<br>
\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\">Bespoke Rental Marketing</h2>

\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\" style=\"margin-top: 20px;\">


\t\t\t\t\t<br>
\t\t\t\t\t<ul style=\"margin-left:30px;\">
\t\t\t\t\t\t<li class=\"lead\">Premier Property Listings on Rightmove, Zoopla, On the Market
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Stunning property photography</li>
\t\t\t\t\t\t<li class=\"lead\">Paid social media advertising</li>
\t\t\t\t\t\t<li class=\"lead\">Property Movie video (large properties)
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Our connections with cooperate firms who relocate staff
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Our unique pricing strategy</li>

\t\t\t\t\t</ul>

\t\t\t\t\t<br>


\t\t\t\t</div>
\t\t\t</br>

\t\t\t<!-- /.col-md-6 -->
\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t<embed src=\"landlords/flat16.pdf\" width=\"100%\" style=\"min-height:250px\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>

\t\t\t<!-- /.col-md-6 -->

\t\t</div>


\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Boomin-logo.jpg\" style=\"max-width:120px;min-width:50px;margin:3px;\"></div>


\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Rightmove_logo.png\" alt=\"Rightmove logo\" style=\"max-width:150px;min-width:50px;margin:3px;margin-top: 15px;\"></div>


\t\t</div>
\t\t<br>
\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Prime-Location-Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>


\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/OnTheMarket_Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>

\t\t</div>
\t\t<br>
\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/zoopla-logo.jpg\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>
\t\t</div>

\t\t<br>

\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Buy to let landlords</h2>
\t\t<br>
\t\t<p class=\"lead \">Due to the large supply of new properties in every corner of London and PRS corporate firms entering to the rental market it has become more challenging for individual buy to let landlords to cope with the current competition.</p>
\t\t<p class=\"lead \">With a fluent knowledge about the local property market, our team will device a marketing strategy to market your property in the correct channels to find the right tenant.</p>
\t\t<p class=\"lead \">In order to sustain and level up from the competition, you will need a bespoke marketing to find the correct tenant.</p>

\t\t<br>


\t\t<br>

\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Portfolio landlords (under 25 units)</h2>
\t\t\t<p class=\"lead \">If you are a mid-range landlord with a portfolio of up to 25 properties, we can offer a bespoke service. Our firm manages properties for various portfolio landlord for the past 10 years
\t\t\t</p>
\t\t\t<br>


\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Corporate landlords (over 25 units)</h2>

\t\t\t<p class=\"lead\">We specialise in letting bulk apartments for PRS firms in the local area. Our team has rented several PRS blocks from buildings of 38 apartments to – 185 apartments in Harrow and Wembley</p>

\t\t\t<p class=\"lead\">We produce a unique marketing strategy for large firms with over 25 apartments available to let covering the below;</p>
\t\t\t<br>
\t
\t\t\t<h4 style=\"letter-spacing: -3px;\">Bradstowe House – Harrow
\t\t\t</h4>
\t\t\t<p class=\"lead \">Building of 177 rental apartments</p>
\t\t\t<br>
\t\t\t<h4 style=\"letter-spacing: -3px;\">Metro Apartments – Wembley</h4>
\t\t\t<p class=\"lead \">Building of 38 apartments</p>
\t\t\t<br>
\t\t\t<h4 style=\"letter-spacing: -3px;\">88 – 98 College Road - Harrow</h4>
\t\t\t<p class=\"lead \">Building of 37 apartments</p>
\t\t\t<br>
\t\t\t
\t\t</div>

\t\t<br>


\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Fees</h2>


\t\t<ul style=\"margin-left:30px;\">
\t\t\t<li class=\"lead\">Let only 9.6% of the annual rent including VAT (8% plus VAT)</li>
\t\t\t<li class=\"lead\">Management – 6% of the annual rent including VAT (5% plus VAT)</li>
\t\t\t<li class=\"lead\">Let and Management – 16.8% of the annual rent including VAT (14% plus VAT)</li>
\t\t\t<li class=\"lead\">Premier Lettings and Management – 19.20% of the annual rent including VAT (16% plus VAT)</li>
\t\t\t<li class=\"lead\">Inventory check in £140 including VAT (1- 2 beds)</li>
\t\t\t<li class=\"lead\">Inventory check out £140 including VAT</li>
\t\t\t<li class=\"lead\">Gas safety check - £82.20 including VAT</li>
\t\t\t<li class=\"lead\">Electric safety check - £140 plus VAT</li>
\t\t\t<li class=\"lead\">EPC - £82.20 including VAT</li>
\t\t\t<li class=\"lead\">Additional property inspection £82.80 including VAT</li>
\t\t\t<li class=\"lead\">Deposit registration – Free (with DPS)</li>
\t\t\t<li class=\"lead\">Tenancy contract – Free</li>
\t\t\t<li class=\"lead\">Tenant referencing – Free
\t\t\t</li>
\t\t</ul>
\t\t<br>
\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Other services for landlords</h2>
\t\t<br>
\t\t<ul style=\"margin-left:30px;\">
\t\t\t<li class=\"lead \">Interior designing
\t\t\t</li>
\t\t\t<li class=\"lead \">We have an inhouse interior design firm and work with various other famous interior
\t\t\t</li>
\t\t\t<li class=\"lead \">design firms in London.
\t\t\t</li>
\t\t\t<li class=\"lead\">Cleaning</li>
\t\t\t<li class=\"lead \">Furnishing</li>
\t\t\t<li class=\"lead\">Non-Resident landlords
\t\t\t</li>
\t\t</ul>
\t</div>


</div></main>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "landlords.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 20,  81 => 15,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id=\"lanlord-banner\" class=\"jumbotron jumbotron-fluid services-jumbo\" >

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">LANDLORDS</h1>
\t\t\t\t<p>Our award-winning rental and property management services offers a safe, transparent and hassle-free services to all landlords.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div class=\"container\">

\t\t\t<p class=\"lead review-content-allign\">Our award-winning rental and property management services offers a safe, transparent and hassle-free services to all landlords.</p>
\t\t\t<p class=\"lead review-content-allign\">Whether you are a corporate landlord, or first time buy to let landlord we offer a hands on professional services to call clients.</p>
\t\t\t<p id=\"buytolet\" class=\"lead review-content-allign\">Our lettings team has won multiple awards for ‘Best letting agent in Harrow’ since 2016 consecutively each year.</p>
\t\t\t<br>

\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Why choose us
\t\t\t</h2>
\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Comprehensive marketing strategy and modern techniques
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">24 Hour property enquiry line
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Our connections with corporate firms
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Premier Property Listings on Rightmove, Zoopla, On the Market</li>
\t\t\t\t<li class=\"lead\">Stunning property photography</li>
\t\t\t\t<li class=\"lead\">Unique pricing stratergy
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Award winning services
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Honest, friendly and professional services
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Full referencing & rent guarantee</li>
\t\t\t\t<li class=\"lead\">Property managers working round the clock</li>
\t\t\t\t<li class=\"lead\">Qualified property management team</li>
\t\t\t\t<li class=\"lead\">Fluent with the new legislations and compliance</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<p class=\"lead review-content-allign\">Check out our reviews from our landlords.</p>
\t\t\t<div class=\"container\">
\t\t\t\t<div id=\"wd_id\" style=\"    max-width: 200px;display: block;margin-left: auto;margin-right: auto;\"></div>
\t\t\t\t<script type=\"text/javascript\" src=\"https://www.allagents.co.uk/widgets/ratings.js\"></script>
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\tinit_widget(200, 150, 9542, 'plain', '000', '')
\t\t\t\t</script>
\t\t\t</div>
\t\t\t<br>


\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Lettings tenant finder services
\t\t\t</h2>
\t\t\t<br>
\t\t\t<p class=\"lead \">With over 690 lettings properties under our lettings portfolio our proactive team covers Harrow, Pinner, Wembley, Stanmore, Edgware, Northwood and Central London.</p>
\t\t\t<p class=\"lead \">We utilise all the modern marketing tools and techniques to market your property to gain maximum exposure.
\t\t\t</p>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Professional photography and videography with floor plans
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Virtual viewings
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">We advertise on Rightmove, Zoopla, On the market, Find a property and Boomin
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Social media marketing (Facebook, Instagram, Linked in)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Local advertising</li>
\t\t\t\t<li class=\"lead\">24 hour property enquiry line – We don’t miss any calls
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Services includes
\t\t\t</h2>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Proactive marketing and advertising
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Virtual viewings
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Conduct viewings and pass on offers to the landlord
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Arrange full referencing through Homelet referencing</li>
\t\t\t\t<li class=\"lead\">Right to rent checks</li>
\t\t\t\t<li class=\"lead\">Prepare contact and serve statutory document
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Arrange inventory check in</li>
\t\t\t\t<li class=\"lead\">Deposit registration</li>
\t\t\t\t<li class=\"lead\">Transfer utility bills
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>

\t\t\t<br>

\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Property management services</h2>
\t\t\t<br>
\t\t\t<p class=\"lead \">From Harrow to Canary wharf and from Edgware to Central London our property management team covers a wide area of properties in London.</p>
\t\t\t<p class=\"lead \">Our super-efficient property management team is highly appreciated by all our existing landlords and tenants. Offering a 24-hour property management services to all our managed properties below are some of our USP
\t\t\t</p>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">24 hour emergency line
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Virtual viewings
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Experienced and qualified property managers
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Efficient and prompt response services
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Complaint and up to date with all the new regulations</li>
\t\t\t\t<li class=\"lead\">Monthly/quarterly client reporting
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Work within client SLA and budget g
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Honest, transparent and efficient service
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<p class=\"lead \">In addition to the lettings tenant finder services below are the services we offer in our property management services
\t\t\t</p>

\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Services includes
\t\t\t</h2>

\t\t\t<br>
\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Organise inventory check ins and check outs
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Move the tenants in
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Register tenants for utility bills
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Register the deposit
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">24 hour property emergency line for tenants</li>
\t\t\t\t<li class=\"lead\">Organise contractors
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Organise safety check such as electric and gas
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Rent collection</li>
\t\t\t\t<li class=\"lead\">Produce statement of accounts each quarter
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Deposit dispute resolution
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<br>

\t\t\t<section class=\"numbers white-txt justify-content-center\" style=\"padding:0px; background-color: #484848!important;\">
\t\t\t\t<div
\t\t\t\t\tclass=\"marged flex\">
\t\t\t\t\t<!-- item 01 -->
\t\t\t\t\t<div class=\"number-item\" style=\"text-align: center;\">
\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t<span class=\"value\">414</span>
\t\t\t\t\t\t</h2>
\t\t\t\t\t\t<h6>Properties managed</h6>
\t\t\t\t\t</div>

\t\t\t\t\t<!-- item 02 -->
\t\t\t\t\t<div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t<span class=\"value\">99</span>%</h2>
\t\t\t\t\t\t<h6>of our clients paid full rent and didn’t fall in to rent arrears in the past 12 months</h6>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
\t\t\t\t\t<h2>£
\t\t\t\t\t\t<span class=\"value\">85</span>,<span class=\"value\">639</span>,<span class=\"value\">202</span>
\t\t\t\t\t</h2>
\t\t\t\t\t<h6>worth of asset and properties managed</h6>
\t\t\t\t</div>

\t\t\t</section>
\t\t\t<br>

\t\t\t<p class=\"lead \">Our property management services can be signed up by any landlord whether the property is self-managed or managed by any other managing agent. We offer a hassle-free switching process.
\t\t\t</p>
\t\t\t<p class=\"lead \">If you would like to know how proactive our property management services are and why we are better than others, please contact our property management team
\t\t\t</p>
\t\t\t

\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 id=\"premierprop\" class=\"review-content-allign h2-letter-spacing\">Premier property management services</h2>
\t\t\t\t<br>


\t\t\t\t<p class=\"lead\">We have a premier service for clients who seek a complete management for their single asset.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">This is mainly designed for overseas landlords who do not live in the UK</p>
\t\t\t\t<br>
\t\t\t\t<h2 id=\"premierprop\" class=\"review-content-allign h2-letter-spacing\">Services includes
\t\t\t\t</h2>
\t\t\t\t<ul style = \"margin-left:30px;\">
\t\t\t\t\t<li class=\"lead\">Organise inventory check in</li>
\t\t\t\t\t<li class=\"lead\">Move the tenants in</li>
\t\t\t\t\t<li class=\"lead\">Register tenants for utility bills</li>
\t\t\t\t\t<li class=\"lead\">Register the deposit</li>
\t\t\t\t\t<li class=\"lead\">24 hour property emergency line for tenants</li>
\t\t\t\t\t<li class=\"lead\">Look after the day-to-day maintenance issues</li>
\t\t\t\t\t<li class=\"lead\">Organise contractors</li>
\t\t\t\t\t<li class=\"lead\">Organise safety check such as electric and gas</li>
\t\t\t\t\t<li class=\"lead\">Rent collection</li>
\t\t\t\t\t<li class=\"lead\">Produce statement of accounts each quarter</li>
\t\t\t\t\t<li class=\"lead\">Deposit depute resolution</li>
\t\t\t\t\t<li class=\"lead\">Dedicated account manager with 24-hour access</li>
\t\t\t\t\t<li class=\"lead\">Monthly statement of accounts</li>
\t\t\t\t\t<li class=\"lead\">Pay any outgoings on behalf of the landlord and reimburse via rent collection</li>
\t\t\t\t\t<li class=\"lead\">Represent the landlord</li>
\t\t\t\t\t<li class=\"lead\">Register for Non-Residents landlords</li>
\t\t\t\t\t<li class=\"lead\">Assist with any other property related issues and matters</li>

\t\t\t\t</ul>

\t\t\t\t<br>


\t\t\t</div>

\t\t\t<div class=\"animation-element slide-left\">
\t\t<iframe src=\"https://www.facebook.com/plugins/video.php?height=315&href=https%3A%2F%2Fwww.facebook.com%2Fempirechaseestateagents%2Fvideos%2F4954274441280932%2F&show_text=false&width=560\" width=\"100%\" max-width=\"560\" height=\"315\" style=\"border:none;overflow:hidden display: block;margin-left: auto;margin-right: auto;\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" allow=\"autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share\" allowFullScreen=\"true\"></iframe>
\t\t\t</div>

\t\t\t<br>
\t\t\t<h2 id=\"brands\" class=\"review-content-allign h2-letter-spacing\">Bespoke Rental Marketing</h2>

\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\" style=\"margin-top: 20px;\">


\t\t\t\t\t<br>
\t\t\t\t\t<ul style=\"margin-left:30px;\">
\t\t\t\t\t\t<li class=\"lead\">Premier Property Listings on Rightmove, Zoopla, On the Market
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Stunning property photography</li>
\t\t\t\t\t\t<li class=\"lead\">Paid social media advertising</li>
\t\t\t\t\t\t<li class=\"lead\">Property Movie video (large properties)
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Our connections with cooperate firms who relocate staff
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Our unique pricing strategy</li>

\t\t\t\t\t</ul>

\t\t\t\t\t<br>


\t\t\t\t</div>
\t\t\t</br>

\t\t\t<!-- /.col-md-6 -->
\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t<embed src=\"landlords/flat16.pdf\" width=\"100%\" style=\"min-height:250px\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>

\t\t\t<!-- /.col-md-6 -->

\t\t</div>


\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Boomin-logo.jpg\" style=\"max-width:120px;min-width:50px;margin:3px;\"></div>


\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Rightmove_logo.png\" alt=\"Rightmove logo\" style=\"max-width:150px;min-width:50px;margin:3px;margin-top: 15px;\"></div>


\t\t</div>
\t\t<br>
\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Prime-Location-Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>


\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/OnTheMarket_Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>

\t\t</div>
\t\t<br>
\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/zoopla-logo.jpg\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>
\t\t</div>

\t\t<br>

\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Buy to let landlords</h2>
\t\t<br>
\t\t<p class=\"lead \">Due to the large supply of new properties in every corner of London and PRS corporate firms entering to the rental market it has become more challenging for individual buy to let landlords to cope with the current competition.</p>
\t\t<p class=\"lead \">With a fluent knowledge about the local property market, our team will device a marketing strategy to market your property in the correct channels to find the right tenant.</p>
\t\t<p class=\"lead \">In order to sustain and level up from the competition, you will need a bespoke marketing to find the correct tenant.</p>

\t\t<br>


\t\t<br>

\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Portfolio landlords (under 25 units)</h2>
\t\t\t<p class=\"lead \">If you are a mid-range landlord with a portfolio of up to 25 properties, we can offer a bespoke service. Our firm manages properties for various portfolio landlord for the past 10 years
\t\t\t</p>
\t\t\t<br>


\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Corporate landlords (over 25 units)</h2>

\t\t\t<p class=\"lead\">We specialise in letting bulk apartments for PRS firms in the local area. Our team has rented several PRS blocks from buildings of 38 apartments to – 185 apartments in Harrow and Wembley</p>

\t\t\t<p class=\"lead\">We produce a unique marketing strategy for large firms with over 25 apartments available to let covering the below;</p>
\t\t\t<br>
\t
\t\t\t<h4 style=\"letter-spacing: -3px;\">Bradstowe House – Harrow
\t\t\t</h4>
\t\t\t<p class=\"lead \">Building of 177 rental apartments</p>
\t\t\t<br>
\t\t\t<h4 style=\"letter-spacing: -3px;\">Metro Apartments – Wembley</h4>
\t\t\t<p class=\"lead \">Building of 38 apartments</p>
\t\t\t<br>
\t\t\t<h4 style=\"letter-spacing: -3px;\">88 – 98 College Road - Harrow</h4>
\t\t\t<p class=\"lead \">Building of 37 apartments</p>
\t\t\t<br>
\t\t\t
\t\t</div>

\t\t<br>


\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Fees</h2>


\t\t<ul style=\"margin-left:30px;\">
\t\t\t<li class=\"lead\">Let only 9.6% of the annual rent including VAT (8% plus VAT)</li>
\t\t\t<li class=\"lead\">Management – 6% of the annual rent including VAT (5% plus VAT)</li>
\t\t\t<li class=\"lead\">Let and Management – 16.8% of the annual rent including VAT (14% plus VAT)</li>
\t\t\t<li class=\"lead\">Premier Lettings and Management – 19.20% of the annual rent including VAT (16% plus VAT)</li>
\t\t\t<li class=\"lead\">Inventory check in £140 including VAT (1- 2 beds)</li>
\t\t\t<li class=\"lead\">Inventory check out £140 including VAT</li>
\t\t\t<li class=\"lead\">Gas safety check - £82.20 including VAT</li>
\t\t\t<li class=\"lead\">Electric safety check - £140 plus VAT</li>
\t\t\t<li class=\"lead\">EPC - £82.20 including VAT</li>
\t\t\t<li class=\"lead\">Additional property inspection £82.80 including VAT</li>
\t\t\t<li class=\"lead\">Deposit registration – Free (with DPS)</li>
\t\t\t<li class=\"lead\">Tenancy contract – Free</li>
\t\t\t<li class=\"lead\">Tenant referencing – Free
\t\t\t</li>
\t\t</ul>
\t\t<br>
\t\t<h2 class=\"review-content-allign h2-letter-spacing\">Other services for landlords</h2>
\t\t<br>
\t\t<ul style=\"margin-left:30px;\">
\t\t\t<li class=\"lead \">Interior designing
\t\t\t</li>
\t\t\t<li class=\"lead \">We have an inhouse interior design firm and work with various other famous interior
\t\t\t</li>
\t\t\t<li class=\"lead \">design firms in London.
\t\t\t</li>
\t\t\t<li class=\"lead\">Cleaning</li>
\t\t\t<li class=\"lead \">Furnishing</li>
\t\t\t<li class=\"lead\">Non-Resident landlords
\t\t\t</li>
\t\t</ul>
\t</div>


</div></main>{% endblock %}
", "landlords.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\landlords.html.twig");
    }
}
