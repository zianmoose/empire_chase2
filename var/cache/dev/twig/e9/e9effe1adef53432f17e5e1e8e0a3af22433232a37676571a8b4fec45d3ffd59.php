<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* services/market-research.html.twig */
class __TwigTemplate_5bdbdfe0d461686f803275f1ff6ec83a70f78becff71e0d5c154188c82688f18 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/market-research.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/market-research.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "services/market-research.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"bHsYtqIV\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/researchMagGlass.jpeg');background-size: cover;background-position: center;
\t\t\t\t\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Market Research</h1>
\t\t\t\t<p>Get the latest and most comprehensive real estate statistics.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 24
        echo "
\t\t</div>

\t\t<div class=\"container\">
\t\t\t<br>
\t\t\t<p class=\"lead review-content-allign\">Want to know about the rapidly changing lettings market in your area? Grab a copy of the latest market research report produced by our research team.</p>
\t\t\t<br>

\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t<div class=\"owl-carousel\">
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2669377489770650\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/greenford.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/1850126751695732\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/harrow-lettings.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2146815078693563\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/harrow-summer.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/3588131087895281\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/harrow.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2863478857027178\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/wembley-lettings.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/3588131087895281\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/wembley.jpg\" alt=\"Card image cap\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>



\t\t\t\t\t";
        // line 83
        echo "\t\t\t\t</div>


\t\t\t</div>

\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<h2 class=\"h2-letter-spacing  review-content-allign\">Market Research</h2>
\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\t\tOur market research team regularly produces market insight reports about the local property market. Most major surveyors, and development lender consults our research team for a property market appraisal.
\t\t\t\t\t\t<br/>
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\t\tWe have an Indepth knowledge of the local property market backed with various supporting facts and figures.
\t\t\t\t\t\t<br/>
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\t\tIf you like to obtain a copy of a market research reports or any information, please feel free to contact us.
\t\t\t\t\t\t<br/>
\t\t\t\t\t</p>


\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<div class=\"fb-post\" data-href=\"https://www.facebook.com/empirechaseestateagents/posts/2834015073306890\" data-width=\"500\" data-show-text=\"true\">
\t\t\t\t\t\t\t\t<blockquote cite=\"https://www.facebook.com/empirechaseestateagents/posts/2834015073306890\" class=\"fb-xfbml-parse-ignore\">
\t\t\t\t\t\t\t\t\t<p>Want to know about the rapidly changing lettings market in your area? Grab a copy of the latest market research report produced by our research team. #lettings #reports #london #rentals #propertymanagement</p>Posted by
\t\t\t\t\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/\">Empire Chase</a>
\t\t\t\t\t\t\t\t\ton&nbsp;<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2834015073306890\">Thursday, July 4, 2019</a>
\t\t\t\t\t\t\t\t</blockquote>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<!-- /contact form -->
\t\t\t<div
\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t<section
\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t<!--Section description-->
\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"row\">

\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-7 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t     background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">

\t\t\t\t\t\t\t";
        // line 154
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 154, $this->source); })()), 'form');
        echo "

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t</div>

\t\t\t\t\t</section>


\t\t\t\t</div>
\t\t\t\t<!-- /.row -->


\t\t\t</div>
\t\t\t<!-- /container -->


\t\t</main>


\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "services/market-research.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 154,  149 => 83,  92 => 24,  85 => 19,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"bHsYtqIV\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/researchMagGlass.jpeg');background-size: cover;background-position: center;
\t\t\t\t\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Market Research</h1>
\t\t\t\t<p>Get the latest and most comprehensive real estate statistics.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div class=\"container\">
\t\t\t<br>
\t\t\t<p class=\"lead review-content-allign\">Want to know about the rapidly changing lettings market in your area? Grab a copy of the latest market research report produced by our research team.</p>
\t\t\t<br>

\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t<div class=\"owl-carousel\">
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2669377489770650\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/greenford.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/1850126751695732\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/harrow-lettings.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2146815078693563\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/harrow-summer.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/3588131087895281\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/harrow.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2863478857027178\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/wembley-lettings.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/posts/3588131087895281\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-research/wembley.jpg\" alt=\"Card image cap\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>



\t\t\t\t\t{# <div class=\"item\">
\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"about-us/award2.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t</div> #}
\t\t\t\t</div>


\t\t\t</div>

\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<h2 class=\"h2-letter-spacing  review-content-allign\">Market Research</h2>
\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\t\tOur market research team regularly produces market insight reports about the local property market. Most major surveyors, and development lender consults our research team for a property market appraisal.
\t\t\t\t\t\t<br/>
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\t\tWe have an Indepth knowledge of the local property market backed with various supporting facts and figures.
\t\t\t\t\t\t<br/>
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\t\tIf you like to obtain a copy of a market research reports or any information, please feel free to contact us.
\t\t\t\t\t\t<br/>
\t\t\t\t\t</p>


\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<div class=\"fb-post\" data-href=\"https://www.facebook.com/empirechaseestateagents/posts/2834015073306890\" data-width=\"500\" data-show-text=\"true\">
\t\t\t\t\t\t\t\t<blockquote cite=\"https://www.facebook.com/empirechaseestateagents/posts/2834015073306890\" class=\"fb-xfbml-parse-ignore\">
\t\t\t\t\t\t\t\t\t<p>Want to know about the rapidly changing lettings market in your area? Grab a copy of the latest market research report produced by our research team. #lettings #reports #london #rentals #propertymanagement</p>Posted by
\t\t\t\t\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/\">Empire Chase</a>
\t\t\t\t\t\t\t\t\ton&nbsp;<a href=\"https://www.facebook.com/empirechaseestateagents/posts/2834015073306890\">Thursday, July 4, 2019</a>
\t\t\t\t\t\t\t\t</blockquote>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<!-- /contact form -->
\t\t\t<div
\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t<section
\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t<!--Section description-->
\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"row\">

\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-7 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t     background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">

\t\t\t\t\t\t\t{{form(form)}}

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t</div>

\t\t\t\t\t</section>


\t\t\t\t</div>
\t\t\t\t<!-- /.row -->


\t\t\t</div>
\t\t\t<!-- /container -->


\t\t</main>


\t{% endblock %}
", "services/market-research.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\market-research.html.twig");
    }
}
