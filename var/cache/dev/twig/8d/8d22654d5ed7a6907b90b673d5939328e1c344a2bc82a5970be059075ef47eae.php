<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sell.html.twig */
class __TwigTemplate_c567baa5e4141a507adeefd5d0269ceef3f5b42501de306383dbd73b01b9ba57 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sell.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sell.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "sell.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "\t<main
\t\trole=\"main\">
\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id =\"sell-banner\"class=\"jumbotron jumbotron-fluid services-jumbo\" >
\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">
\t\t\t\t\tSell
\t\t\t\t</h1>
\t\t\t\t<p>
\t\t\t\t\tWhen selling your biggest asset, choose an agent wisely
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">
\t\t\t\t\t\tContact us
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 22
        echo "\t\t</div>

\t\t<div class=\"container\">
\t\t\t<p class=\"lead review-content-allign\">
\t\t\t\tAs a forward thinking and very active real estate firm we device a
\t\t\t\t\t\t\t\t        unique marketing strategy to sell every home. Our comprehensive
\t\t\t\t\t\t\t\t        marketing strategy will enable your property to reach out to a very
\t\t\t\t\t\t\t\t        large audience and achieve maximum market value.
\t\t\t</p>
\t\t\t<br/>
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\t\tWhy choose Empire Chase
\t\t\t</h2>
\t\t\t<br/>
\t\t\t<ul style= \"margin-left:30px;\">
\t\t\t\t<li class=\"lead\">Bespoke marketing strategy for every property</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tWe only take a handful or properties at a time which means we can
\t\t\t\t\t\t\t\t\t\t          dedicate our 100% focus on selling them
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tPaid social media advertising (Through our premier sales brand Empire
\t\t\t\t\t\t\t\t\t\t          Chase Premier)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tAmerican open house style viewings (Through our premier sales brand
\t\t\t\t\t\t\t\t\t\t          Empire Chase Premier)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Local marketing campaign</li>
\t\t\t\t<li class=\"lead\">Premier property portal listings</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tOverseas marketing (Through our brand in Hong Kong Empire Chase HK,
\t\t\t\t\t\t\t\t\t\t          China, middle east and various other countries)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">24-hour property inquiry line</li>
\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t<li class=\"lead\">Award winning customer service</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tSales progression and ongoing support during the process.
\t\t\t\t</li>
\t\t\t</ul>

\t\t\t<br/>
\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 id=\"marketingexpert\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\t\tOur Marketing Expertise
\t\t\t\t</h2>
\t\t\t\t<br/>

\t\t\t\t<p class=\"lead\">
\t\t\t\tAs a real estate agent, our favourite part of the business is marketing. We believe the correct marketing and correct pricing strategy will sell a home for the best possible price
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tWe have invested ample time and revenue in adopting into every marketing platform to offer our clients the best marketing service.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tWe use some of the best photographers/videographers to produce the best quality images including twilight photography, property movie videos and drone footages to create a bespoke branding for your property.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tWe are one of the most active local agents in social media with over 15000 followers. Our marketing team runs paid advertising on Facebook, Instagram and linked showing properties to a large audience
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\tPlease visit our <a href=\"";
        // line 84
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_marketexpert");
        echo "\"> Marketing Expertise </a> page for more information.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tIf you are looking to sell your property, please have a chat with our team who will explain the current market conditions and device a plan to sell your biggest asset to the highest price.
\t\t\t\t</p>
\t\t\t\t
\t\t\t</div>

\t\t\t<br/>

\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">
\t\t\t\t\tSellers guide (Step by Step process)
\t\t\t\t</h2>
\t\t\t\t<br/>

\t\t\t\t<ul style=\"margin-left:30px;\">
\t\t\t\t\t<li class=\"lead\">Prepare for sale</li>
\t\t\t\t\t<li class=\"lead\">Value your home</li>
\t\t\t\t\t<li class=\"lead\">Choose an estate agent</li>
\t\t\t\t\t<li class=\"lead\">Choose a conveyancer</li>
\t\t\t\t\t<li class=\"lead\">Prepare your home for sale</li>
\t\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t\t<li class=\"lead\">Exchange</li>
\t\t\t\t\t<li class=\"lead\">Completion</li>
\t\t\t\t\t<li class=\"lead\">Move in out</li>
\t\t\t\t\t<li class=\"lead\">After sales</li>
\t\t\t\t</ul>
\t\t\t</div>

\t\t\t<br/>
\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 id=\"privateagent\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\t\tPremier Sales through our Premier Brand EMPIRE CHASE PREMIER
\t\t\t\t</h2>
\t\t\t\t<br/>

\t\t\t\t<p class=\"lead\">
\t\t\t\t\tEmpire Chase Premier is our luxury sales brand offering a very bespoke
\t\t\t\t\t\t\t\t\t\t          premier sales services for luxury and upscale properties.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\t\tSales services offers American style bespoke marketing services, with
\t\t\t\t\t\t\t\t\t\t          property movies, luxury magazine marketing and paid social media
\t\t\t\t\t\t\t\t\t\t          advertising in additional to all other marketing
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\t\tPrivate estate agency services for sports personalities, musicians and
\t\t\t\t\t\t\t\t\t\t          high net-worth individuals
\t\t\t\t</p>
\t\t\t</div>

\t\t\t<br/>
\t\t</div>
\t</main>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "sell.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 84,  91 => 22,  82 => 15,  68 => 3,  58 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block body %}
\t<main
\t\trole=\"main\">
\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id =\"sell-banner\"class=\"jumbotron jumbotron-fluid services-jumbo\" >
\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">
\t\t\t\t\tSell
\t\t\t\t</h1>
\t\t\t\t<p>
\t\t\t\t\tWhen selling your biggest asset, choose an agent wisely
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">
\t\t\t\t\t\tContact us
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}
\t\t</div>

\t\t<div class=\"container\">
\t\t\t<p class=\"lead review-content-allign\">
\t\t\t\tAs a forward thinking and very active real estate firm we device a
\t\t\t\t\t\t\t\t        unique marketing strategy to sell every home. Our comprehensive
\t\t\t\t\t\t\t\t        marketing strategy will enable your property to reach out to a very
\t\t\t\t\t\t\t\t        large audience and achieve maximum market value.
\t\t\t</p>
\t\t\t<br/>
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\t\tWhy choose Empire Chase
\t\t\t</h2>
\t\t\t<br/>
\t\t\t<ul style= \"margin-left:30px;\">
\t\t\t\t<li class=\"lead\">Bespoke marketing strategy for every property</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tWe only take a handful or properties at a time which means we can
\t\t\t\t\t\t\t\t\t\t          dedicate our 100% focus on selling them
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tPaid social media advertising (Through our premier sales brand Empire
\t\t\t\t\t\t\t\t\t\t          Chase Premier)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tAmerican open house style viewings (Through our premier sales brand
\t\t\t\t\t\t\t\t\t\t          Empire Chase Premier)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">Local marketing campaign</li>
\t\t\t\t<li class=\"lead\">Premier property portal listings</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tOverseas marketing (Through our brand in Hong Kong Empire Chase HK,
\t\t\t\t\t\t\t\t\t\t          China, middle east and various other countries)
\t\t\t\t</li>
\t\t\t\t<li class=\"lead\">24-hour property inquiry line</li>
\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t<li class=\"lead\">Award winning customer service</li>
\t\t\t\t<li class=\"lead\">
\t\t\t\t\tSales progression and ongoing support during the process.
\t\t\t\t</li>
\t\t\t</ul>

\t\t\t<br/>
\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 id=\"marketingexpert\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\t\tOur Marketing Expertise
\t\t\t\t</h2>
\t\t\t\t<br/>

\t\t\t\t<p class=\"lead\">
\t\t\t\tAs a real estate agent, our favourite part of the business is marketing. We believe the correct marketing and correct pricing strategy will sell a home for the best possible price
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tWe have invested ample time and revenue in adopting into every marketing platform to offer our clients the best marketing service.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tWe use some of the best photographers/videographers to produce the best quality images including twilight photography, property movie videos and drone footages to create a bespoke branding for your property.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tWe are one of the most active local agents in social media with over 15000 followers. Our marketing team runs paid advertising on Facebook, Instagram and linked showing properties to a large audience
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\tPlease visit our <a href=\"{{ path('emp_marketexpert') }}\"> Marketing Expertise </a> page for more information.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\tIf you are looking to sell your property, please have a chat with our team who will explain the current market conditions and device a plan to sell your biggest asset to the highest price.
\t\t\t\t</p>
\t\t\t\t
\t\t\t</div>

\t\t\t<br/>

\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">
\t\t\t\t\tSellers guide (Step by Step process)
\t\t\t\t</h2>
\t\t\t\t<br/>

\t\t\t\t<ul style=\"margin-left:30px;\">
\t\t\t\t\t<li class=\"lead\">Prepare for sale</li>
\t\t\t\t\t<li class=\"lead\">Value your home</li>
\t\t\t\t\t<li class=\"lead\">Choose an estate agent</li>
\t\t\t\t\t<li class=\"lead\">Choose a conveyancer</li>
\t\t\t\t\t<li class=\"lead\">Prepare your home for sale</li>
\t\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t\t<li class=\"lead\">Exchange</li>
\t\t\t\t\t<li class=\"lead\">Completion</li>
\t\t\t\t\t<li class=\"lead\">Move in out</li>
\t\t\t\t\t<li class=\"lead\">After sales</li>
\t\t\t\t</ul>
\t\t\t</div>

\t\t\t<br/>
\t\t\t<div class=\"animation-element slide-left\">
\t\t\t\t<h2 id=\"privateagent\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\t\tPremier Sales through our Premier Brand EMPIRE CHASE PREMIER
\t\t\t\t</h2>
\t\t\t\t<br/>

\t\t\t\t<p class=\"lead\">
\t\t\t\t\tEmpire Chase Premier is our luxury sales brand offering a very bespoke
\t\t\t\t\t\t\t\t\t\t          premier sales services for luxury and upscale properties.
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\t\tSales services offers American style bespoke marketing services, with
\t\t\t\t\t\t\t\t\t\t          property movies, luxury magazine marketing and paid social media
\t\t\t\t\t\t\t\t\t\t          advertising in additional to all other marketing
\t\t\t\t</p>
\t\t\t\t<p class=\"lead\">
\t\t\t\t\tPrivate estate agency services for sports personalities, musicians and
\t\t\t\t\t\t\t\t\t\t          high net-worth individuals
\t\t\t\t</p>
\t\t\t</div>

\t\t\t<br/>
\t\t</div>
\t</main>
{% endblock %}
", "sell.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\sell.html.twig");
    }
}
