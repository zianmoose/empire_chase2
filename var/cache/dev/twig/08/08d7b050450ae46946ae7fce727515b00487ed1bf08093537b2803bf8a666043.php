<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /properties/properties.html.twig */
class __TwigTemplate_6da463bf6aaf1e3fb2a95ff3dc28aa8a38203ecb11b36442bc26d6c6e68d5acb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascripts' => [$this, 'block_javascripts'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/properties/properties.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/properties/properties.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/properties/properties.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 4
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo " 
        ";
        // line 5
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("pagination");
        echo "
        ";
        // line 6
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("pagecustom");
        echo "

    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo " 
    ";
        // line 12
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("properties");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 16
        echo "
<main role=\"main\">

  <!-- Main jumbotron for a primary marketing message or call to action -->
   <div class=\"jumbotron jumbotron-fluid services-jumbo\" style =\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/empImg3.jpg');background-size: cover;background-position: center;
  height:500px;\" >
  
    <div class=\"container services-jumbo-container display-3-top\" style=\"margin-top: 200px!important;\">
      <h1 class=\"display-3\">Properties</h1>
      <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content.</p>
      <p><a href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\"class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a></p>
    </div>
    
    ";
        // line 30
        echo "    
  </div>




<div id=\"properties-container\" class=\"container\" style=\"margin-top: 50px;\">

<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem; min-width: 18rem; min-height:10rem;background-color: #2b459c!important;
      color: white;\">
  ";
        // line 41
        echo "  <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('properties/rent.jpg');background-size: cover;background-position: center;\">

    <h5 class=\"card-title\" style=\"color:white\"></h5>
    <br>
    <a href=\"https://www.rightmove.co.uk/property-to-rent/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&propertyStatus=all&includeLetAgreed=true&_includeLetAgreed=on\">
      <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important; margin-top:80%\">Find a property for rent</button>
    </a>
  </div>
</div>

<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem; min-width: 18rem; min-height:10rem;background-color: #2b459c!important;
      color: white;\">
  ";
        // line 54
        echo "  <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('properties/sale.jpg');background-size: cover;background-position: center;\">

    <h5 class=\"card-title\" style=\"color:white\"></h5>
    <br>
    <a href=\"https://www.rightmove.co.uk/property-for-sale/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&includeSSTC=true&_includeSSTC=on\">
      <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important; margin-top:80%\">Find a property for sale</button>
    </a>
  </div>
</div>


</div>

</div>


  </div> <!--/container -->
 


</main> 


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/properties/properties.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 54,  159 => 41,  147 => 30,  141 => 26,  129 => 16,  119 => 15,  107 => 12,  102 => 11,  92 => 10,  79 => 6,  75 => 5,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

 {% block javascripts %}
        {{ parent() }} 
        {{ encore_entry_script_tags('pagination') }}
        {{ encore_entry_script_tags('pagecustom') }}

    {% endblock %}

{% block stylesheets %}
    {{ parent() }} 
    {{ encore_entry_link_tags('properties') }}
{% endblock %}

{% block body %}

<main role=\"main\">

  <!-- Main jumbotron for a primary marketing message or call to action -->
   <div class=\"jumbotron jumbotron-fluid services-jumbo\" style =\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/empImg3.jpg');background-size: cover;background-position: center;
  height:500px;\" >
  
    <div class=\"container services-jumbo-container display-3-top\" style=\"margin-top: 200px!important;\">
      <h1 class=\"display-3\">Properties</h1>
      <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content.</p>
      <p><a href=\"{{ path('emp_contact-us') }}\"class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a></p>
    </div>
    
    {# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}
    
  </div>




<div id=\"properties-container\" class=\"container\" style=\"margin-top: 50px;\">

<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem; min-width: 18rem; min-height:10rem;background-color: #2b459c!important;
      color: white;\">
  {# <div class=\"card-header\">Landlords</div> #}
  <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('properties/rent.jpg');background-size: cover;background-position: center;\">

    <h5 class=\"card-title\" style=\"color:white\"></h5>
    <br>
    <a href=\"https://www.rightmove.co.uk/property-to-rent/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&propertyStatus=all&includeLetAgreed=true&_includeLetAgreed=on\">
      <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important; margin-top:80%\">Find a property for rent</button>
    </a>
  </div>
</div>

<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem; min-width: 18rem; min-height:10rem;background-color: #2b459c!important;
      color: white;\">
  {# <div class=\"card-header\">Landlords</div> #}
  <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('properties/sale.jpg');background-size: cover;background-position: center;\">

    <h5 class=\"card-title\" style=\"color:white\"></h5>
    <br>
    <a href=\"https://www.rightmove.co.uk/property-for-sale/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&includeSSTC=true&_includeSSTC=on\">
      <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important; margin-top:80%\">Find a property for sale</button>
    </a>
  </div>
</div>


</div>

</div>


  </div> <!--/container -->
 


</main> 


{% endblock %}", "/properties/properties.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\properties\\properties.html.twig");
    }
}
