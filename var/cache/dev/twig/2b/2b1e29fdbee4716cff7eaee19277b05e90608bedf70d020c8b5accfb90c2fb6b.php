<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* services/asset-management.html.twig */
class __TwigTemplate_e226f208a77233859765b0f806165da9e48d26df96bded58daac911125590e14 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/asset-management.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/asset-management.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "services/asset-management.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/AssetManBanner.jpeg');background-size: cover;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Asset Management</h1>
\t\t\t\t<p>A service with real care.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;background-position: center;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 21
        echo "
\t\t</div>

\t\t<div class=\"container\" style=\"margin-top: 100px\">


\t\t\t<!-- <h1 class=\"text-center\">Asset Management</h1> -->

\t\t\t<!--  image left  -->
\t\t\t\t<div class=\"row\" style=\"margin-right: 10px;margin-left: 10px;\"> <div class=\"col-md-6 animation-element slide-left\">

\t\t\t\t\t<img src=\"assetMan-keys.jpg\" alt=\"\" class=\"w-100\"/>
\t\t\t\t</div>

\t\t\t\t<br/><br/>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">
<br>
\t\t\t\t\t\t\t<p class=\"lead\">We offer a comprehensive portfolio and asset management services for landlords with a property portfolio. Our services includes full property management, tenancy management, refinancing, maximising ROI, cost cutting and exit strategies
\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<!--  image right  -->
\t\t\t<div class=\"row\" style=\"margin-top: 50px;margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t<iframe style=\"width:100%;height:100%;\" src=\"https://www.youtube.com/embed/x8dE0hOwzJw\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t</div>


\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t<h2 class=\"h2-letter-spacing\">Why choose our asset management</h2>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<ul style=\"margin-left:30px;\">
\t\t\t\t\t\t\t\t<li class = \"lead\">Dedicated account manager.</li>
\t\t\t\t\t\t\t\t<li  class = \"lead\">Experienced team.</li>
\t\t\t\t\t\t\t\t<li  class = \"lead\">Efficient services</li>
\t\t\t\t\t\t\t\t<li  class = \"lead\">Proactive approach</li>
\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<!-- /contact form -->
\t\t\t<div
\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t<section
\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t<!--Section description-->
\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t        a matter of hours to help you.</p>
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"row\">


\t\t\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t\t\t<div class=\"col-md-6\" style=\"
\t\t\t\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t\t\t";
        // line 112
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 112, $this->source); })()), 'form');
        echo "

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!---container-->

\t\t\t\t\t</section>


\t\t\t\t</div>
\t\t\t\t<!-- /.row -->

\t\t\t</div>
\t\t\t<!-- /container -->


\t\t</main>


\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "services/asset-management.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 112,  89 => 21,  82 => 16,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/AssetManBanner.jpeg');background-size: cover;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Asset Management</h1>
\t\t\t\t<p>A service with real care.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;background-position: center;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div class=\"container\" style=\"margin-top: 100px\">


\t\t\t<!-- <h1 class=\"text-center\">Asset Management</h1> -->

\t\t\t<!--  image left  -->
\t\t\t\t<div class=\"row\" style=\"margin-right: 10px;margin-left: 10px;\"> <div class=\"col-md-6 animation-element slide-left\">

\t\t\t\t\t<img src=\"assetMan-keys.jpg\" alt=\"\" class=\"w-100\"/>
\t\t\t\t</div>

\t\t\t\t<br/><br/>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">
<br>
\t\t\t\t\t\t\t<p class=\"lead\">We offer a comprehensive portfolio and asset management services for landlords with a property portfolio. Our services includes full property management, tenancy management, refinancing, maximising ROI, cost cutting and exit strategies
\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<!--  image right  -->
\t\t\t<div class=\"row\" style=\"margin-top: 50px;margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t<iframe style=\"width:100%;height:100%;\" src=\"https://www.youtube.com/embed/x8dE0hOwzJw\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t</div>


\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t<h2 class=\"h2-letter-spacing\">Why choose our asset management</h2>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<ul style=\"margin-left:30px;\">
\t\t\t\t\t\t\t\t<li class = \"lead\">Dedicated account manager.</li>
\t\t\t\t\t\t\t\t<li  class = \"lead\">Experienced team.</li>
\t\t\t\t\t\t\t\t<li  class = \"lead\">Efficient services</li>
\t\t\t\t\t\t\t\t<li  class = \"lead\">Proactive approach</li>
\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<!-- /contact form -->
\t\t\t<div
\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t<section
\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t<!--Section description-->
\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t        a matter of hours to help you.</p>
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"row\">


\t\t\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t\t\t<div class=\"col-md-6\" style=\"
\t\t\t\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t\t\t{{ form(form) }}

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!---container-->

\t\t\t\t\t</section>


\t\t\t\t</div>
\t\t\t\t<!-- /.row -->

\t\t\t</div>
\t\t\t<!-- /container -->


\t\t</main>


\t{% endblock %}
", "services/asset-management.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\asset-management.html.twig");
    }
}
