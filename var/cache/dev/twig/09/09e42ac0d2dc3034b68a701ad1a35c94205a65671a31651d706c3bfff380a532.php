<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* services/build-to-rent.html.twig */
class __TwigTemplate_5f38b2cb455b1da9876f0b988dfc7ad4315a5fe482a2cc2291406e9c001efca6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/build-to-rent.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/build-to-rent.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "services/build-to-rent.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"Z2JTZKx3\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id=\"build-to-rent-banner\" class=\"jumbotron jumbotron-fluid services-jumbo\" >

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Build to rent</h1>
\t\t\t\t<p>We are one of the only agents in the area with a fluent knowledge about the PRS build to rent sector.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 24
        echo "
\t\t</div>

\t\t<div class=\"container\">
\t\t\t<br>

\t\t\t<p class=\"lead review-content-allign\">We are one of the only local agents in the area with a fluent knowledge about the PRS build to rent sector. Our firm has been researching the American style Build to rent system since 2015 and have worked various firms to establish their projects
\t\t\t</p>
\t\t\t<p class=\"lead review-content-allign\">We have worked with some of the largest build to rent firms such as Greystar, St Modwen properties, BGRS and various other investment institutional landlords.</p>
\t\t\t<br>
\t\t\t<div id=\"btr-3img\" class=\"row justify-content-center\">

      <div id=\"btr-3img-inner\"><img src=\"build-to-rent/img1.jpeg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

      <div id=\"btr-3img-inner\"><img src=\"build-to-rent/img2.jpeg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

      <div id=\"btr-3img-inner\"><img src=\"build-to-rent/img3.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t</div>
\t\t\t<br>
\t\t\t<p class=\"lead\">We currently fully manage a large portfolio of apartments over 7 PRS blocks. Our services include, Tenant sourcing, property management, asset management, financing, rent collection, parking management, retail and commercial management.
\t\t\t\t\t\t\t\t     
\t\t\t</p>
\t\t\t<br>

\t\t</div>


\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<img src=\"buildtoRentpic1.jpg\" alt=\"\" class=\"w-100\"/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t<br>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">What is Build to Rent?</h2>
\t\t\t<p class=\"lead\">This is a term used for the residential schemes being designed, built and managed specifically for rent.
\t\t\t\t<br/><br/>
\t\t\t</p>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">What is PRS?</h2>
\t\t\t<p class=\"lead\">PRS (Private Rented Sector) is a catch for all the rent homes including Built to rent  (BTR) and Buy to let (BTL)
\t\t\t\t<br/><br/>
\t\t\t</p>


\t\t</div>

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<br/><br/>
\t\t\t<!--  image right  -->
\t\t\t<div
\t\t\t\tclass=\"row\">


\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t";
        // line 107
        echo "\t\t\t\t\t\t\t<img src=\"/build-to-rent/middle-img.jpeg\" alt=\"\" class=\"w-100\"/>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<br>


\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">
\t\t\t\t<br>
\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Our services includes</h2>
\t\t\t\t\t
          
          <div style=\"margin-left:50px;\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li class=\"lead\">Valuation and consultancy
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Bespoke marketing plan</li>
\t\t\t\t\t\t<li class=\"lead\">Tenancy management
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Property Management</li>
\t\t\t\t\t\t<li class=\"lead\">Block and building management
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Tenant retention
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Rent uplift</li>
\t\t\t\t\t\t<li class=\"lead\">Valuation and refinancing
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Exit strategy
\t\t\t\t\t\t</li>

\t\t\t\t\t</ul>
\t\t\t\t\t<br>
        </div>
\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t</div>
\t\t\t\t<br>
          <br>


        
  \t<!-- SECTION NUMBERS -->

\t<!-- END OF SECTION NUMBERS --> 
\t
 
       

     
     
      </div>
\t\t\t\t<!-- /contact form -->
\t\t\t\t<div
\t\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t\t<section
\t\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t\t<!--Section description-->
\t\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"row\">

\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t<div class=\"col-md-7 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t     background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t\t\t";
        // line 182
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 182, $this->source); })()), 'form');
        echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column-->


\t\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</section>


\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.row contact form-->

\t\t\t\t</div>
\t\t\t\t<!-- /container -->


\t\t\t</main>


\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "services/build-to-rent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 182,  169 => 107,  92 => 24,  85 => 19,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"Z2JTZKx3\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id=\"build-to-rent-banner\" class=\"jumbotron jumbotron-fluid services-jumbo\" >

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Build to rent</h1>
\t\t\t\t<p>We are one of the only agents in the area with a fluent knowledge about the PRS build to rent sector.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div class=\"container\">
\t\t\t<br>

\t\t\t<p class=\"lead review-content-allign\">We are one of the only local agents in the area with a fluent knowledge about the PRS build to rent sector. Our firm has been researching the American style Build to rent system since 2015 and have worked various firms to establish their projects
\t\t\t</p>
\t\t\t<p class=\"lead review-content-allign\">We have worked with some of the largest build to rent firms such as Greystar, St Modwen properties, BGRS and various other investment institutional landlords.</p>
\t\t\t<br>
\t\t\t<div id=\"btr-3img\" class=\"row justify-content-center\">

      <div id=\"btr-3img-inner\"><img src=\"build-to-rent/img1.jpeg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

      <div id=\"btr-3img-inner\"><img src=\"build-to-rent/img2.jpeg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

      <div id=\"btr-3img-inner\"><img src=\"build-to-rent/img3.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t</div>
\t\t\t<br>
\t\t\t<p class=\"lead\">We currently fully manage a large portfolio of apartments over 7 PRS blocks. Our services include, Tenant sourcing, property management, asset management, financing, rent collection, parking management, retail and commercial management.
\t\t\t\t\t\t\t\t     
\t\t\t</p>
\t\t\t<br>

\t\t</div>


\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<img src=\"buildtoRentpic1.jpg\" alt=\"\" class=\"w-100\"/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t<br>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">What is Build to Rent?</h2>
\t\t\t<p class=\"lead\">This is a term used for the residential schemes being designed, built and managed specifically for rent.
\t\t\t\t<br/><br/>
\t\t\t</p>
\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">What is PRS?</h2>
\t\t\t<p class=\"lead\">PRS (Private Rented Sector) is a catch for all the rent homes including Built to rent  (BTR) and Buy to let (BTL)
\t\t\t\t<br/><br/>
\t\t\t</p>


\t\t</div>

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<br/><br/>
\t\t\t<!--  image right  -->
\t\t\t<div
\t\t\t\tclass=\"row\">


\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t{# <div class=\"fb-video\" data-href=\"https://www.facebook.com/empirechaseestateagents/videos/484925698756788/\" data-width=\"500\" data-show-text=\"false\">
\t\t\t\t\t\t\t\t<blockquote cite=\"https://developers.facebook.com/empirechaseestateagents/videos/484925698756788/\" class=\"fb-xfbml-parse-ignore\">
\t\t\t\t\t\t\t\t\t<a href=\"https://developers.facebook.com/empirechaseestateagents/videos/484925698756788/\"></a>
\t\t\t\t\t\t\t\t\t<p>Build to rent #PRS #lettings #buildtorent</p>Posted by
\t\t\t\t\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/\">Empire Chase</a>
\t\t\t\t\t\t\t\t\ton Tuesday, September 10, 2019</blockquote>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div> #}
\t\t\t\t\t\t\t<img src=\"/build-to-rent/middle-img.jpeg\" alt=\"\" class=\"w-100\"/>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<br>


\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">
\t\t\t\t<br>
\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Our services includes</h2>
\t\t\t\t\t
          
          <div style=\"margin-left:50px;\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li class=\"lead\">Valuation and consultancy
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Bespoke marketing plan</li>
\t\t\t\t\t\t<li class=\"lead\">Tenancy management
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Property Management</li>
\t\t\t\t\t\t<li class=\"lead\">Block and building management
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Tenant retention
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Rent uplift</li>
\t\t\t\t\t\t<li class=\"lead\">Valuation and refinancing
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"lead\">Exit strategy
\t\t\t\t\t\t</li>

\t\t\t\t\t</ul>
\t\t\t\t\t<br>
        </div>
\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t</div>
\t\t\t\t<br>
          <br>


        
  \t<!-- SECTION NUMBERS -->

\t<!-- END OF SECTION NUMBERS --> 
\t
 
       

     
     
      </div>
\t\t\t\t<!-- /contact form -->
\t\t\t\t<div
\t\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t\t<section
\t\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t\t<!--Section description-->
\t\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"row\">

\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t<div class=\"col-md-7 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t     background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t\t\t{{form(form)}}
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column-->


\t\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</section>


\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.row contact form-->

\t\t\t\t</div>
\t\t\t\t<!-- /container -->


\t\t\t</main>


\t\t{% endblock %}
", "services/build-to-rent.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\build-to-rent.html.twig");
    }
}
