<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* marketexpert.html.twig */
class __TwigTemplate_110c0f6977e72ebb4b00fc4c20a2eddd28180eca36ce2e428451fcfd4a26a2c5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "marketexpert.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "marketexpert.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "marketexpert.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('market-expert/market-expert-banner.jpeg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">


\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Marketing Expertise</h1>
\t\t\t\t<p>Marketing being the forefront of our business we constantly strive perfecting our marketing platforms.</p>
\t\t\t\t";
        // line 17
        echo "\t\t\t</div>

\t\t\t";
        // line 20
        echo "
\t\t</div>

\t\t<div
\t\t\tclass=\"container\">
\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We believe marketing is a key element for selling or letting a property and a real estate agent should be a pro in marketing.</p>
\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We have adopted into every marketing and advertising platforms.</p>
<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">Our marketing team will design a unique marketing strategy after carefully understanding your property and its location. Our knowledge about the local market, modern advertising techniques, bespoke presentation and promotional staff enables us to put our expertise in the forefront.</p>
\t\t\t";
        // line 33
        echo "

\t\t\t<h2 id=\"firsttimebuy\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:20px;\">Property movie videos
\t\t\t</h2>
\t\t\t<br>

\t\t\t<div class=\" animation-element slide-right\">


\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We are one of the agents in the area to start producing bespoke professional property movies with presenters showcasing a lifestyle.</p>

\t\t\t\t<br/><br/>

\t\t\t</div>
\t\t\t<br>


\t\t\t<!--  image left  -->

\t\t\t<!-- Grid row -->
\t\t\t\t<div
\t\t\t\tclass=\"row\"> <!-- Grid column -->
\t\t\t\t<div
\t\t\t\t\tclass=\"col-lg-4 col-md-12 mb-4\">

\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t<div class=\"modal fade\" id=\"modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"modal-dialog modal-lg\" role=\"document\">

\t\t\t\t\t\t\t<!--Content-->
\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\tclass=\"modal-content\">

\t\t\t\t\t\t\t\t<!--Body-->
\t\t\t\t\t\t\t\t<div class=\"modal-body mb-0 p-0\">

\t\t\t\t\t\t\t\t\t<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
\t\t\t\t\t\t\t\t\t\t<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/gfhLHqqyO9w\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<!--Footer-->
\t\t\t\t\t\t\t\t<div class=\"modal-footer justify-content-center\">


\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--/.Content-->

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t<div>
\t\t\t\t\t\t<a><img src=\"market-expert/play-button.png\" alt=\"play button\" style=\"width:100px; position:absolute;\" data-toggle=\"modal\" data-target=\"#modal1\"><a></div>
\t\t\t\t\t\t\t<a><img class=\"img-fluid z-depth-1\" src=\"greenroof.jpg\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal1\"></a>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- Grid column -->

\t\t\t\t\t\t<!-- Grid column -->
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"col-lg-4 col-md-6 mb-4\">

\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t<div class=\"modal fade\" id=\"modal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\tclass=\"modal-dialog modal-lg\" role=\"document\">

\t\t\t\t\t\t\t\t\t<!--Content-->
\t\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\t\tclass=\"modal-content\">

\t\t\t\t\t\t\t\t\t\t<!--Body-->
\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body mb-0 p-0\">

\t\t\t\t\t\t\t\t\t\t\t<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
\t\t\t\t\t\t\t\t\t\t\t\t<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/tqWhuPwhtN8\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t<!--Footer-->
\t\t\t\t\t\t\t\t\t\t<div class=\"modal-footer justify-content-center\">


\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>

\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!--/.Content-->

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<a><img src=\"market-expert/play-button.png\" alt=\"play button\" style=\"width:100px; position:absolute;\" data-toggle=\"modal\" data-target=\"#modal2\"><a></div>
\t\t\t\t\t\t\t\t\t<a><img class=\"img-fluid z-depth-1\" src=\"market-expert/chasewood-inner.jpg\"  alt=\"video\" data-toggle=\"modal\" data-target=\"#modal2\"></a>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- Grid column -->

\t\t\t\t\t\t\t\t<!-- Grid column -->
\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\tclass=\"col-lg-4 col-md-6 mb-4\">

\t\t\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t\t\t<div class=\"modal fade\" id=\"modal3\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\t\t\tclass=\"modal-dialog modal-lg\" role=\"document\">

\t\t\t\t\t\t\t\t\t\t\t<!--Content-->
\t\t\t\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\t\t\t\tclass=\"modal-content\">

\t\t\t\t\t\t\t\t\t\t\t\t<!--Body-->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body mb-0 p-0\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/hNG0byL-Di4\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t<!--Footer-->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-footer justify-content-center\">


\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<!--/.Content-->

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<a><img src=\"market-expert/play-button.png\" alt=\"play button\" style=\"width:100px; position:absolute;\" data-toggle=\"modal\" data-target=\"#modal3\"><a></div>
\t\t\t\t\t\t\t\t\t\t\t<a><img class=\"img-fluid z-depth-1\" src=\"bovis.jpeg\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal3\"></a>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- Grid column -->

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!-- Grid row -->
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- /.row -->
\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t<div class=\"container\">

\t\t\t\t\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign \">Professional photography</h2>

\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t<div class=\"animation-element slide-left\">


\t\t\t\t\t\t\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We hire some of the best tenanted photographers in town to capture the best images. We use various methods including twilight images, drones and 360 cameras featuring different options for your property.
\t\t\t\t\t\t\t\t\t\t</p>


\t\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t\t\t\t\t\t\t<div class=\"owl-carousel\">


\t\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/photography-01.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/photography-02.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/photography-03.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t<div class=\"container\">

\t\t\t\t\t\t\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign \">Property portals
\t\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Boomin-logo.jpg\" style=\"max-width:120px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Rightmove_logo.png\" alt=\"Rightmove logo\" style=\"max-width:150px;min-width:50px;margin:3px;margin-top: 15px;\"></div>


\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Prime-Location-Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/OnTheMarket_Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/zoopla-logo.jpg\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t\t<div class=\" animation-element slide-left\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Social media</h2>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem; \">We are one of the most active agent in social media with over 15000 followers.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">Our marketing team runs paid advertising on Facebook, Instagram and linked showing properties to a large audience .</p>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">

\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/facebook-logo.png\" alt=\"facebook logo\" style=\"max-width:140px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/instragram-logo.png\" style=\"max-width:140px;min-width:50px;margin:3px;\"></div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/linkedin-logo.png\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/tiktok-logo.png\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t";
        // line 299
        echo "
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t\t<!-- /.row  affliations -->



\t\t\t\t\t\t<!--  image right  -->
\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Open house</h2>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/openhouse.jpeg\" alt=\"OPEN HOUSE\" style = \"width: 70%;margin-left: 50px; margin-right: 50px;\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">
\t
\t\t\t\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t\t<p class=\"lead\">Empire Chase hosts some of the unique open houses. The event will be hosted in style with invited buyers, music, food and drinks. </p>
\t\t\t\t\t\t\t\t\t<p class=\"lead\">Our unique 'Open House' strategy generates high interest and competition which usually result in properties being sold above the asking price </p>
\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- /.col-md-6 -->
\t
\t\t\t\t\t\t</div>

\t\t\t\t
              <br>
                <br>


\t\t\t\t\t\t</main>


\t\t\t\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "marketexpert.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 299,  96 => 33,  86 => 20,  82 => 17,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('market-expert/market-expert-banner.jpeg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">


\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Marketing Expertise</h1>
\t\t\t\t<p>Marketing being the forefront of our business we constantly strive perfecting our marketing platforms.</p>
\t\t\t\t{# <p><a href=\"{{ path('emp_contact-us') }}\"class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a></p> #}
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div
\t\t\tclass=\"container\">
\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We believe marketing is a key element for selling or letting a property and a real estate agent should be a pro in marketing.</p>
\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We have adopted into every marketing and advertising platforms.</p>
<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">Our marketing team will design a unique marketing strategy after carefully understanding your property and its location. Our knowledge about the local market, modern advertising techniques, bespoke presentation and promotional staff enables us to put our expertise in the forefront.</p>
\t\t\t{# 
\t\t\t<h2 id=\"firsttimebuy\"class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:70px;\">Market Expertise </h2>
\t\t\t<br>
\t\t\t   
\t\t\t   <p class=\"lead review-content-allign\">Marketing being the forefront of our business we constantly strive perfecting our marketing platforms.</p> #}


\t\t\t<h2 id=\"firsttimebuy\" class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:20px;\">Property movie videos
\t\t\t</h2>
\t\t\t<br>

\t\t\t<div class=\" animation-element slide-right\">


\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We are one of the agents in the area to start producing bespoke professional property movies with presenters showcasing a lifestyle.</p>

\t\t\t\t<br/><br/>

\t\t\t</div>
\t\t\t<br>


\t\t\t<!--  image left  -->

\t\t\t<!-- Grid row -->
\t\t\t\t<div
\t\t\t\tclass=\"row\"> <!-- Grid column -->
\t\t\t\t<div
\t\t\t\t\tclass=\"col-lg-4 col-md-12 mb-4\">

\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t<div class=\"modal fade\" id=\"modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"modal-dialog modal-lg\" role=\"document\">

\t\t\t\t\t\t\t<!--Content-->
\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\tclass=\"modal-content\">

\t\t\t\t\t\t\t\t<!--Body-->
\t\t\t\t\t\t\t\t<div class=\"modal-body mb-0 p-0\">

\t\t\t\t\t\t\t\t\t<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
\t\t\t\t\t\t\t\t\t\t<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/gfhLHqqyO9w\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<!--Footer-->
\t\t\t\t\t\t\t\t<div class=\"modal-footer justify-content-center\">


\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--/.Content-->

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t<div>
\t\t\t\t\t\t<a><img src=\"market-expert/play-button.png\" alt=\"play button\" style=\"width:100px; position:absolute;\" data-toggle=\"modal\" data-target=\"#modal1\"><a></div>
\t\t\t\t\t\t\t<a><img class=\"img-fluid z-depth-1\" src=\"greenroof.jpg\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal1\"></a>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- Grid column -->

\t\t\t\t\t\t<!-- Grid column -->
\t\t\t\t\t\t<div
\t\t\t\t\t\t\tclass=\"col-lg-4 col-md-6 mb-4\">

\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t<div class=\"modal fade\" id=\"modal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\tclass=\"modal-dialog modal-lg\" role=\"document\">

\t\t\t\t\t\t\t\t\t<!--Content-->
\t\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\t\tclass=\"modal-content\">

\t\t\t\t\t\t\t\t\t\t<!--Body-->
\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body mb-0 p-0\">

\t\t\t\t\t\t\t\t\t\t\t<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
\t\t\t\t\t\t\t\t\t\t\t\t<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/tqWhuPwhtN8\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t<!--Footer-->
\t\t\t\t\t\t\t\t\t\t<div class=\"modal-footer justify-content-center\">


\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>

\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!--/.Content-->

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<a><img src=\"market-expert/play-button.png\" alt=\"play button\" style=\"width:100px; position:absolute;\" data-toggle=\"modal\" data-target=\"#modal2\"><a></div>
\t\t\t\t\t\t\t\t\t<a><img class=\"img-fluid z-depth-1\" src=\"market-expert/chasewood-inner.jpg\"  alt=\"video\" data-toggle=\"modal\" data-target=\"#modal2\"></a>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- Grid column -->

\t\t\t\t\t\t\t\t<!-- Grid column -->
\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\tclass=\"col-lg-4 col-md-6 mb-4\">

\t\t\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t\t\t<div class=\"modal fade\" id=\"modal3\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\t\t\tclass=\"modal-dialog modal-lg\" role=\"document\">

\t\t\t\t\t\t\t\t\t\t\t<!--Content-->
\t\t\t\t\t\t\t\t\t\t\t<div
\t\t\t\t\t\t\t\t\t\t\t\tclass=\"modal-content\">

\t\t\t\t\t\t\t\t\t\t\t\t<!--Body-->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body mb-0 p-0\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/hNG0byL-Di4\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t<!--Footer-->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-footer justify-content-center\">


\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<!--/.Content-->

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!--Modal: Name-->
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<a><img src=\"market-expert/play-button.png\" alt=\"play button\" style=\"width:100px; position:absolute;\" data-toggle=\"modal\" data-target=\"#modal3\"><a></div>
\t\t\t\t\t\t\t\t\t\t\t<a><img class=\"img-fluid z-depth-1\" src=\"bovis.jpeg\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal3\"></a>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- Grid column -->

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!-- Grid row -->
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- /.row -->
\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t<div class=\"container\">

\t\t\t\t\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign \">Professional photography</h2>

\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t<div class=\"animation-element slide-left\">


\t\t\t\t\t\t\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">We hire some of the best tenanted photographers in town to capture the best images. We use various methods including twilight images, drones and 360 cameras featuring different options for your property.
\t\t\t\t\t\t\t\t\t\t</p>


\t\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t\t\t<div class=\"carousel-wrap\">
\t\t\t\t\t\t\t\t\t\t<div class=\"owl-carousel\">


\t\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/photography-01.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/photography-02.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/photography-03.jpeg\" alt=\"Card image cap\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t<div class=\"container\">

\t\t\t\t\t\t\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign \">Property portals
\t\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Boomin-logo.jpg\" style=\"max-width:120px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Rightmove_logo.png\" alt=\"Rightmove logo\" style=\"max-width:150px;min-width:50px;margin:3px;margin-top: 15px;\"></div>


\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Prime-Location-Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/OnTheMarket_Logo.png\" style=\"max-width:200px;min-width:50px;margin:3px;\"></div>

\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/zoopla-logo.jpg\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t\t<div class=\" animation-element slide-left\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Social media</h2>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem; \">We are one of the most active agent in social media with over 15000 followers.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"lead review-content-allign\" style=\"font-size: 1.5rem;\">Our marketing team runs paid advertising on Facebook, Instagram and linked showing properties to a large audience .</p>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">

\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/facebook-logo.png\" alt=\"facebook logo\" style=\"max-width:140px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/instragram-logo.png\" style=\"max-width:140px;min-width:50px;margin:3px;\"></div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<div id=\"portal-logos\" class=\"container-fluid\" style=\"display:flex;justify-content: space-around;\">
\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/linkedin-logo.png\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>


\t\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/tiktok-logo.png\" style=\"max-width:100px;min-width:50px;margin:3px;\"></div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t{# <div id=\"portal-logos\" class= \"container-fluid\" style=\"display:flex;justify-content: space-around;\"> 
\t\t\t\t\t\t\t\t\t\t<div><img class=\"card-img-top img-responsive\" src=\"market-expert/Boomin-logo.jpg\"style =\"max-width:100px;min-width:50px;margin:3px;\"></div>
\t\t\t\t\t\t\t\t\t\t </div> #}

\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t\t<!-- /.row  affliations -->



\t\t\t\t\t\t<!--  image right  -->
\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<h2 class=\"h2-letter-spacing review-content-allign\">Open house</h2>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"market-expert/openhouse.jpeg\" alt=\"OPEN HOUSE\" style = \"width: 70%;margin-left: 50px; margin-right: 50px;\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">
\t
\t\t\t\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t\t<p class=\"lead\">Empire Chase hosts some of the unique open houses. The event will be hosted in style with invited buyers, music, food and drinks. </p>
\t\t\t\t\t\t\t\t\t<p class=\"lead\">Our unique 'Open House' strategy generates high interest and competition which usually result in properties being sold above the asking price </p>
\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- /.col-md-6 -->
\t
\t\t\t\t\t\t</div>

\t\t\t\t
              <br>
                <br>


\t\t\t\t\t\t</main>


\t\t\t\t\t{% endblock %}
", "marketexpert.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\marketexpert.html.twig");
    }
}
