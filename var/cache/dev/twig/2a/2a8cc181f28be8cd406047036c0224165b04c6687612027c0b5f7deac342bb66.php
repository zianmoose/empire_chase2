<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home.html.twig */
class __TwigTemplate_a08530364bdf5cb1f4d31ae0171a82d69df4d19370a7f49c2053820ac8515502 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/home.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "\t<div id=\"car-container\">
\t\t<div class=\"container-fluid\" style=\"padding-right: 0px;
\t\t\t\t\t\t\t\t    padding-left: 0px;\">


\t\t\t<div id=\"carousel-home\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t<div id=\"carousel-inner\">

\t\t\t\t\t\t<div class=\"carousel-item active\">
\t\t\t\t\t\t\t<div class=\"overlay-background\">
\t\t\t\t\t\t\t\t<div class=\"overlay\">
\t\t\t\t\t\t\t\t\t<img class=\"d-block w-100 carousel-home-img\" src=\"empImg1.jpg\" alt=\"First slide\">

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div id=\"cap_one\" class=\"carousel-caption \">
\t\t\t\t\t\t\t\t<div id=\"cap-head\">
\t\t\t\t\t\t\t\t\t<h5 id=\"h5\">Welcome Aboard</h5>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p>Customer service being the centre of our business we offer a white gloves conceirge service in real estate.

\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_properties");
        echo "\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #2b459c!important; color: #FFFFFF!important\">Find a property</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 31
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_valuation_main");
        echo "\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\">Valuation</a>
\t\t\t\t\t\t\t\t\t";
        // line 33
        echo "\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t<div class=\"overlay-background\">
\t\t\t\t\t\t\t\t<div class=\"overlay\">
\t\t\t\t\t\t\t\t\t<img class=\"d-block w-100 carousel-home-img\" src=\"home/property2.jpeg\" alt=\"Second slide\">

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"carousel-caption\">
\t\t\t\t\t\t\t\t<div id=\"cap-head\">
\t\t\t\t\t\t\t\t\t<h5 id=\"h5\">Exceptional Marketing</h5>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p>We use the most innovative marketing strategies and modern marketing platforms
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_properties");
        echo "\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #2b459c!important; color: #FFFFFF!important\">Find a property</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_valuation_main");
        echo "\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\">Valuation</a>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t<div class=\"overlay-background\">
\t\t\t\t\t\t\t\t<div class=\"overlay\">
\t\t\t\t\t\t\t\t\t<img class=\"d-block w-100 carousel-home-img\" src=\"home/chasewood-home.jpeg\" alt=\"Third slide\">

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"carousel-caption\">
\t\t\t\t\t\t\t\t<div id=\"cap-head\">
\t\t\t\t\t\t\t\t\t<h5 id=\"h5\">Awards & Reviews</h5>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p>Multiple Award winning services with over 700 ⭐️⭐️⭐️⭐️⭐️ reviews.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_properties");
        echo "\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #2b459c!important; color: #FFFFFF!important\">Find a property</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 74
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_valuation_main");
        echo "\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\">Valuation</a>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</div>

\t</div>


\t<div id=\"home-content\">

\t\t<div id=\"jumbotron\" class=\"jumbotron jumbotron-fluid\">
\t\t\t<div id=\"service-jumbotron-container\" class=\"container\">
\t\t\t\t<h4 class=\"services-p\">Our services</h4>
\t\t\t\t<div id=\"arrow-img-left\">
\t\t\t\t\t<img src=\"arrow-right.png\" alt=\"Arrow-Left\"></div>
\t\t\t\t<div id=\"arrow-img-right\">
\t\t\t\t\t<img src=\"arrow-left.png\" alt=\"Arrow-Right\"></div>

\t\t\t\t<div
\t\t\t\t\tid=\"service-icon-container\">

\t\t\t\t\t";
        // line 103
        echo "\t\t\t\t\t<a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_marketexpert");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"MarketExpert.png\" alt=\"Avatar\"><h6>Marketing expertise</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"";
        // line 107
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_salep");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"saleSign.png\" alt=\"Avatar\"><h6>Sales</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_rentalp");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"rentSign.png\" alt=\"Avatar\"><h6>Lettings</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"";
        // line 115
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_property-management");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"propertyM.png\" alt=\"Avatar\"><h6>Property Management</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"";
        // line 119
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_build-to-rent");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"buildtoRent.png\" alt=\"Avatar\"><h6>Build to rent</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"";
        // line 123
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_asset-management");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"assetMan.png\" alt=\"Avatar\"><h6>Asset Management</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"";
        // line 127
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_block-management");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"blockMan.png\" alt=\"Avatar\"><h6>Block Management</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"";
        // line 131
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_/market-research");
        echo "\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"marketRes.png\" alt=\"Avatar\"><h6>Market Research</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>


\t\t\t\t</div>

\t\t\t</div>
\t\t</div>
\t</div>
\t<div id=\"featured-properties\" class=\"container\">
\t\t<h2 class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">
\t\t\tFeatured properties
\t\t</h2>
\t\t<br>

\t</div>
\t";
        // line 151
        echo "<div class=\"container-fluid\" style=\"padding-right: 0px;
\t\t\t\t\t\t    padding-left: 0px; max-width:1600px;\">

\t\t<div id=\"home-owl-carousel\" class=\"carousel-wrap\">
\t\t\t<div class=\"owl-carousel\">
\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/105615170#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">

\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/carnegie.jpg\" alt=\"Card image cap\">

\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-yellow\">Sale</h6>
\t\t\t\t\t\t\t\t<h5 class=\"card-title\">£635,000</h5>
\t\t\t\t\t\t\t\t<h6 class=\"card-title\">3 bed flat for sale
\t\t\t\t\t\t\t\t</h6>
\t\t\t\t\t\t\t\t<p class=\"card-text\">Carnegie House, Peterborough Road, Harrow
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>

\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/106465871#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t\t<div class=\"item\">

\t\t\t\t\t\t<div class=\"card\">


\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/chasewood.jpg\" alt=\"Card image cap\">

\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-yellow\">Sale</h6>
\t\t\t\t\t\t\t\t<h5 class=\"card-title\">£575000</h5>
\t\t\t\t\t\t\t\t<h6 class=\"card-title\">2 bed end 2 bath apartment</h6>
\t\t\t\t\t\t\t\t<p class=\"card-text\">Chasewood Park, Harrow on the hill, HA1</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t</a>

\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/105518507#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/bovis.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-red\">Sold</h6>
\t\t\t\t\t\t\t<h5 class=\"card-title\">£550,000</h5>
\t\t\t\t\t\t\t<h6 class=\"card-title\">2 bed 2 bath apartment
\t\t\t\t\t\t\t</h6>
\t\t\t\t\t\t\t<p class=\"card-text\">Bovis House, Northolt Road, Harrow, HA2</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
\t\t\t<a href=\"https://www.rightmove.co.uk/properties/105897314#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/crawford.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-blue\">Rent</h6>
\t\t\t\t\t\t\t<h5 class=\"card-title\">£1,400 pcm</h5>
\t\t\t\t\t\t\t<h6 class=\"card-title\">1 bed 1 bath to rent</h6>
\t\t\t\t\t\t\t<p class=\"card-text\">Crawford Avenue, Wembley, Middlesex, HA0</p>
\t\t\t\t\t\t\t";
        // line 218
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/99024659#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/college.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-blue\">Rent</h6>
\t\t\t\t\t\t\t<h5 class=\"card-title\">£1,300 pcm</h5>
\t\t\t\t\t\t\t<h6 class=\"card-title\">1 bed 1 bath to rent
\t\t\t\t\t\t\t</h6>
\t\t\t\t\t\t\t<p class=\"card-text\">88 - 98 College Road, Harrow, Middlesex, HA1
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
\t\t\t</div>
\t\t</div>

\t</div>";
        // line 263
        echo "\t\t<div class=\"container\"> <h2 class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Awards</h2>
\t</div>
\t<br>
\t<p class=\"review-content-allign lead\">Our exceptional customer services has earned us multiple awards over the years from some of the industry experts.</p>
\t<div style=\"background-color: #484848; padding:20px; \">
\t\t<div id=\"items-container\" class=\"container justify-content-center\">
\t\t\t<img
\t\t\tsrc=\"awards-banner.jpeg\" class=\"img-fluid center\" alt=\"CorporateLiveWire trophy 2020\" style=\"box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);display: block;
\t\t\t    margin-left: auto;
\t\t\t    margin-right: auto;\">
\t\t";
        // line 281
        echo "

\t\t</div>
\t</div>

\t<!-- 4 columns -->

\t<div class=\"container\" style=\"margin-top: 50px;\">
\t\t<h2 class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Reviews</h2>
\t\t<br>
\t\t<p class=\"review-content-allign lead\">Over 700 ⭐️⭐️⭐️⭐️⭐️ reviews across various review platforms</p>
\t\t<p class=\"review-content-allign lead\">Click the icon below to read our reviews</p>
\t\t<div class=\"row\">

\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div
\t\t\t\t\tclass=\"review-content\">
\t\t\t\t\t";
        // line 299
        echo "\t\t\t\t\t<a href=\"https://www.theestas.com/reviews/338?u=widget\"><img src=\"estas.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t";
        // line 303
        echo "
\t\t\t\t</div>
\t\t\t</div>


\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t<a href=\"https://www.allagents.co.uk/empire-chase/\">
\t\t\t\t\t\t<img src=\"allagents.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t";
        // line 315
        echo "\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/reviews/?ref=page_internal\">
\t\t\t\t\t\t<img src=\"facebook.png\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t";
        // line 325
        echo "\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t<a href=\"https://www.google.com/search?q=empire+chase&sxsrf=ALeKk029__j1ehm282gooR0K0TteBohXdQ%3A1621291307613&source=hp&ei=K_GiYMv4IYTk5NoPjeCPaA&iflsig=AINFCbYAAAAAYKL_O2ThdMWXFeCRPUUwwuLcGcUNUc9O&gs_ssp=eJzj4tFP1zcsKCnPKSszLTRgtFI1qDCxMDczNDY3STVIMze2TEqzMqhIsjQxSUm1SE0xMDVNtUwy9eJJzS3ILEpVSM5ILE4FAJiiFCY&oq=empire+chase&gs_lcp=Cgdnd3Mtd2l6EAEYADILCC4QxwEQrwEQkwIyAggAMgIIADICCAAyAggAMgIIADoICAAQsQMQgwE6BQgAELEDOggILhCxAxCDAToICC4QxwEQowI6CAguEMcBEK8BOgUILhCxAzoOCC4QsQMQxwEQrwEQkwI6CwguELEDEMcBEK8BOgUIABDJAzoLCC4QsQMQxwEQowI6CAgAELEDEMkDOgUILhCTAjoCCC5QjQpY5CNgvzdoAHAAeACAAWqIAfcHkgEDOS4zmAEAoAEBqgEHZ3dzLXdpeg&sclient=gws-wiz#lrd=0x48761374e0f739bf:0xb944de8ed055e9b5,1,,,\">
\t\t\t\t\t\t<img src=\"googlereviews.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t";
        // line 335
        echo "\t\t\t\t</div>
\t\t\t</div>

\t\t</div>
\t</div>


\t<div id=\"emp_video\" classs=\"container\">
\t\t<div class=\"container\">
\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:50px;background-color:\t#D8D8D8;\">Property Movies</h2>
\t\t
\t\t<br>
\t\t<p class=\"review-content-allign lead\">We produce the most innovative property movie style videos with drone footages, actors, and twilight photography showcasing a lifestyle in the property.</p>
\t</div>
\t\t<video width=\"640\" controls poster=\"bradstow-thumbnail.png\" preload=\"none\">
\t\t\t<source src=\"emp_video.mp4\" type=\"video/mp4\">

\t\t</video>

\t</div>


\t";
        // line 384
        echo "\t";
        // line 395
        echo "

\t<!-- SECTION NUMBERS -->
\t";
        // line 417
        echo "
\t";
        // line 424
        echo "

\t<!-- BEGIN ESTAS WIDGET -->
\t";
        // line 446
        echo "

\t";
        // line 452
        echo "

\t";
        // line 481
        echo "

\t";
        // line 487
        echo "
\t\t
  
\t\t<div class=\"cookie-popup cookie-popup--short cookie-popup--dark\" style=\"z-index: 100;\">
\t\t  
\t\t  <div>
\t\t  This website uses cookies to provide you with a great user experience. By using it, you accept our <a href=\"";
        // line 493
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_cookie-policy");
        echo "\">use of cookies</a>
\t\t  </div>
\t\t  <div class=\"cookie-popup-actions\">
\t\t\t<button>Okay</button>
\t\t  </div>
\t  
\t\t</div>
\t\t
\t  </div>


</div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  474 => 493,  466 => 487,  462 => 481,  458 => 452,  454 => 446,  449 => 424,  446 => 417,  441 => 395,  439 => 384,  415 => 335,  405 => 325,  395 => 315,  383 => 303,  379 => 299,  360 => 281,  348 => 263,  324 => 218,  256 => 151,  235 => 131,  228 => 127,  221 => 123,  214 => 119,  207 => 115,  200 => 111,  193 => 107,  185 => 103,  154 => 74,  150 => 73,  126 => 52,  122 => 51,  102 => 33,  98 => 31,  94 => 30,  68 => 6,  58 => 5,  35 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("{# templates/blog/layout.html.twig #}

{% extends 'base.html.twig' %}

{% block body %}
\t<div id=\"car-container\">
\t\t<div class=\"container-fluid\" style=\"padding-right: 0px;
\t\t\t\t\t\t\t\t    padding-left: 0px;\">


\t\t\t<div id=\"carousel-home\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t<div id=\"carousel-inner\">

\t\t\t\t\t\t<div class=\"carousel-item active\">
\t\t\t\t\t\t\t<div class=\"overlay-background\">
\t\t\t\t\t\t\t\t<div class=\"overlay\">
\t\t\t\t\t\t\t\t\t<img class=\"d-block w-100 carousel-home-img\" src=\"empImg1.jpg\" alt=\"First slide\">

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div id=\"cap_one\" class=\"carousel-caption \">
\t\t\t\t\t\t\t\t<div id=\"cap-head\">
\t\t\t\t\t\t\t\t\t<h5 id=\"h5\">Welcome Aboard</h5>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p>Customer service being the centre of our business we offer a white gloves conceirge service in real estate.

\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_properties') }}\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #2b459c!important; color: #FFFFFF!important\">Find a property</a>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_valuation_main') }}\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\">Valuation</a>
\t\t\t\t\t\t\t\t\t{# <button type=\"button\" class=\"btn btn-primary btn-lg btn-md\" style = \"background-color:  #d39e00!important; color: #FFFFFF!important\">Valuation</button> #}
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t<div class=\"overlay-background\">
\t\t\t\t\t\t\t\t<div class=\"overlay\">
\t\t\t\t\t\t\t\t\t<img class=\"d-block w-100 carousel-home-img\" src=\"home/property2.jpeg\" alt=\"Second slide\">

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"carousel-caption\">
\t\t\t\t\t\t\t\t<div id=\"cap-head\">
\t\t\t\t\t\t\t\t\t<h5 id=\"h5\">Exceptional Marketing</h5>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p>We use the most innovative marketing strategies and modern marketing platforms
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_properties') }}\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #2b459c!important; color: #FFFFFF!important\">Find a property</a>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_valuation_main') }}\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\">Valuation</a>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"carousel-item\">
\t\t\t\t\t\t\t<div class=\"overlay-background\">
\t\t\t\t\t\t\t\t<div class=\"overlay\">
\t\t\t\t\t\t\t\t\t<img class=\"d-block w-100 carousel-home-img\" src=\"home/chasewood-home.jpeg\" alt=\"Third slide\">

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"carousel-caption\">
\t\t\t\t\t\t\t\t<div id=\"cap-head\">
\t\t\t\t\t\t\t\t\t<h5 id=\"h5\">Awards & Reviews</h5>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p>Multiple Award winning services with over 700 ⭐️⭐️⭐️⭐️⭐️ reviews.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_properties') }}\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #2b459c!important; color: #FFFFFF!important\">Find a property</a>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_valuation_main') }}\" class=\"btn btn-primary btn-lg btn-md\" role=\"button\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\">Valuation</a>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</div>

\t</div>


\t<div id=\"home-content\">

\t\t<div id=\"jumbotron\" class=\"jumbotron jumbotron-fluid\">
\t\t\t<div id=\"service-jumbotron-container\" class=\"container\">
\t\t\t\t<h4 class=\"services-p\">Our services</h4>
\t\t\t\t<div id=\"arrow-img-left\">
\t\t\t\t\t<img src=\"arrow-right.png\" alt=\"Arrow-Left\"></div>
\t\t\t\t<div id=\"arrow-img-right\">
\t\t\t\t\t<img src=\"arrow-left.png\" alt=\"Arrow-Right\"></div>

\t\t\t\t<div
\t\t\t\t\tid=\"service-icon-container\">

\t\t\t\t\t{# <a href=\"{{ path('emp_properties') }}\"><div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"salesandLetting.png\" alt=\"Avatar\" ><h6>Sales &amp; Lettings</h6></div></a> #}
\t\t\t\t\t<a href=\"{{ path('emp_marketexpert') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"MarketExpert.png\" alt=\"Avatar\"><h6>Marketing expertise</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"{{ path('emp_salep') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"saleSign.png\" alt=\"Avatar\"><h6>Sales</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"{{ path('emp_rentalp') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"rentSign.png\" alt=\"Avatar\"><h6>Lettings</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"{{ path('emp_services_property-management') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"propertyM.png\" alt=\"Avatar\"><h6>Property Management</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"{{ path('emp_services_build-to-rent') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"buildtoRent.png\" alt=\"Avatar\"><h6>Build to rent</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"{{ path('emp_services_asset-management') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"assetMan.png\" alt=\"Avatar\"><h6>Asset Management</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"{{ path('emp_services_block-management') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"blockMan.png\" alt=\"Avatar\"><h6>Block Management</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"{{ path('emp_services_/market-research') }}\">
\t\t\t\t\t\t<div id=\"service-img\" class=\"col service-img-class\"><img id=\"emp-services\" src=\"marketRes.png\" alt=\"Avatar\"><h6>Market Research</h6>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>


\t\t\t\t</div>

\t\t\t</div>
\t\t</div>
\t</div>
\t<div id=\"featured-properties\" class=\"container\">
\t\t<h2 class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">
\t\t\tFeatured properties
\t\t</h2>
\t\t<br>

\t</div>
\t{# <img src=\"https://picsum.photos/285/200/?image=4&random\" alt=\"Second slide\"> #}
\t{#----------------------------------------------------------------------------------------------------------------------------------#}
\t<div class=\"container-fluid\" style=\"padding-right: 0px;
\t\t\t\t\t\t    padding-left: 0px; max-width:1600px;\">

\t\t<div id=\"home-owl-carousel\" class=\"carousel-wrap\">
\t\t\t<div class=\"owl-carousel\">
\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/105615170#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t<div class=\"card\">

\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/carnegie.jpg\" alt=\"Card image cap\">

\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-yellow\">Sale</h6>
\t\t\t\t\t\t\t\t<h5 class=\"card-title\">£635,000</h5>
\t\t\t\t\t\t\t\t<h6 class=\"card-title\">3 bed flat for sale
\t\t\t\t\t\t\t\t</h6>
\t\t\t\t\t\t\t\t<p class=\"card-text\">Carnegie House, Peterborough Road, Harrow
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>

\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/106465871#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t\t<div class=\"item\">

\t\t\t\t\t\t<div class=\"card\">


\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/chasewood.jpg\" alt=\"Card image cap\">

\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-yellow\">Sale</h6>
\t\t\t\t\t\t\t\t<h5 class=\"card-title\">£575000</h5>
\t\t\t\t\t\t\t\t<h6 class=\"card-title\">2 bed end 2 bath apartment</h6>
\t\t\t\t\t\t\t\t<p class=\"card-text\">Chasewood Park, Harrow on the hill, HA1</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t</a>

\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/105518507#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/bovis.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-red\">Sold</h6>
\t\t\t\t\t\t\t<h5 class=\"card-title\">£550,000</h5>
\t\t\t\t\t\t\t<h6 class=\"card-title\">2 bed 2 bath apartment
\t\t\t\t\t\t\t</h6>
\t\t\t\t\t\t\t<p class=\"card-text\">Bovis House, Northolt Road, Harrow, HA2</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
\t\t\t<a href=\"https://www.rightmove.co.uk/properties/105897314#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/crawford.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-blue\">Rent</h6>
\t\t\t\t\t\t\t<h5 class=\"card-title\">£1,400 pcm</h5>
\t\t\t\t\t\t\t<h6 class=\"card-title\">1 bed 1 bath to rent</h6>
\t\t\t\t\t\t\t<p class=\"card-text\">Crawford Avenue, Wembley, Middlesex, HA0</p>
\t\t\t\t\t\t\t{# <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p> #}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
\t\t\t\t<a href=\"https://www.rightmove.co.uk/properties/99024659#/\" style=\"text-decoration:none!important; color:black\">
\t\t\t\t<div class=\"item\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<img class=\"card-img-top img-responsive\" src=\"featured-prop/college.jpg\" alt=\"Card image cap\">
\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<h6 id=\"h6-ribbon\" class=\"ribbon-blue\">Rent</h6>
\t\t\t\t\t\t\t<h5 class=\"card-title\">£1,300 pcm</h5>
\t\t\t\t\t\t\t<h6 class=\"card-title\">1 bed 1 bath to rent
\t\t\t\t\t\t\t</h6>
\t\t\t\t\t\t\t<p class=\"card-text\">88 - 98 College Road, Harrow, Middlesex, HA1
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
\t\t\t</div>
\t\t</div>

\t</div>
\t{#----------------------------------------------------------------------------------------------------------------------------------#}

\t{#-----Grid Contents----------------------------------------------------------------------------------------------------------------#}

\t{# <div id=\"items-container\" class=\"container-fluid\">
\t\t<div class=\"items-container-1\">
\t\t
\t\t<div class =\"items-overlay\">
\t\t  <div class=\"awards\">
\t\t  <img  class=\"responsive\" src = \"CorporateLiveWire.jpg\" style=\"width: 100%;\"><h4>Our Acheivements</h4></img>
\t\t  </div>
\t\t  </div>
\t\t
\t\t  <div class=\"reviews\">
\t\t  <img  class=\"responsive\" src = \"trophy2020.png\"></img>
\t\t  </div>
\t\t
\t\t</div>
\t\t
\t\t </div>
\t\t</div>
\t\t</div> #}
\t\t<div class=\"container\"> <h2 class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Awards</h2>
\t</div>
\t<br>
\t<p class=\"review-content-allign lead\">Our exceptional customer services has earned us multiple awards over the years from some of the industry experts.</p>
\t<div style=\"background-color: #484848; padding:20px; \">
\t\t<div id=\"items-container\" class=\"container justify-content-center\">
\t\t\t<img
\t\t\tsrc=\"awards-banner.jpeg\" class=\"img-fluid center\" alt=\"CorporateLiveWire trophy 2020\" style=\"box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);display: block;
\t\t\t    margin-left: auto;
\t\t\t    margin-right: auto;\">
\t\t{# <div class=\"d-flex justify-content-around c-container\">
\t\t\t\t\t\t<div class=\"w-50\">
\t\t\t\t\t\t\t<img src=\"CorporateLiveWire.jpg\" class=\"img-fluid\" alt=\"CorporateLiveWire trophy 2020\" style=\"box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"w-50\" style=\"padding: 5% 5% 5% 10%;\">
\t\t\t\t\t\t\t<img src=\"trophy2020.png\" class=\"img-fluid\" alt=\"Allagent trophy 2020\" style=\"margin-left:25%;\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div> #}


\t\t</div>
\t</div>

\t<!-- 4 columns -->

\t<div class=\"container\" style=\"margin-top: 50px;\">
\t\t<h2 class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Reviews</h2>
\t\t<br>
\t\t<p class=\"review-content-allign lead\">Over 700 ⭐️⭐️⭐️⭐️⭐️ reviews across various review platforms</p>
\t\t<p class=\"review-content-allign lead\">Click the icon below to read our reviews</p>
\t\t<div class=\"row\">

\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div
\t\t\t\t\tclass=\"review-content\">
\t\t\t\t\t{# <p>class=\"col-xs-6 col-sm-6 col-md-3\"</p> #}
\t\t\t\t\t<a href=\"https://www.theestas.com/reviews/338?u=widget\"><img src=\"estas.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t{# <p class=\"review-content-allign\">97%</p>
\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 249 reviews</p> #}

\t\t\t\t</div>
\t\t\t</div>


\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t<a href=\"https://www.allagents.co.uk/empire-chase/\">
\t\t\t\t\t\t<img src=\"allagents.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t{# <p class=\"review-content-allign\">4.9/5</p>
\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 233 reviews</p> #}
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/reviews/?ref=page_internal\">
\t\t\t\t\t\t<img src=\"facebook.png\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t{# <p class=\"review-content-allign\">5/5</p>
\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 81 reviews</p> #}
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-xs-6 col-sm-3 col-md-3 review-box\">
\t\t\t\t<div class=\"review-content\">
\t\t\t\t\t<a href=\"https://www.google.com/search?q=empire+chase&sxsrf=ALeKk029__j1ehm282gooR0K0TteBohXdQ%3A1621291307613&source=hp&ei=K_GiYMv4IYTk5NoPjeCPaA&iflsig=AINFCbYAAAAAYKL_O2ThdMWXFeCRPUUwwuLcGcUNUc9O&gs_ssp=eJzj4tFP1zcsKCnPKSszLTRgtFI1qDCxMDczNDY3STVIMze2TEqzMqhIsjQxSUm1SE0xMDVNtUwy9eJJzS3ILEpVSM5ILE4FAJiiFCY&oq=empire+chase&gs_lcp=Cgdnd3Mtd2l6EAEYADILCC4QxwEQrwEQkwIyAggAMgIIADICCAAyAggAMgIIADoICAAQsQMQgwE6BQgAELEDOggILhCxAxCDAToICC4QxwEQowI6CAguEMcBEK8BOgUILhCxAzoOCC4QsQMQxwEQrwEQkwI6CwguELEDEMcBEK8BOgUIABDJAzoLCC4QsQMQxwEQowI6CAgAELEDEMkDOgUILhCTAjoCCC5QjQpY5CNgvzdoAHAAeACAAWqIAfcHkgEDOS4zmAEAoAEBqgEHZ3dzLXdpeg&sclient=gws-wiz#lrd=0x48761374e0f739bf:0xb944de8ed055e9b5,1,,,\">
\t\t\t\t\t\t<img src=\"googlereviews.jpg\" width=\"100%\" class=\"review-content-allign\">
\t\t\t\t\t</a>
\t\t\t\t\t{# <p class=\"review-content-allign\">4.9/5</p>
\t\t\t\t\t\t\t\t\t\t       <p class=\"review-content-allign\">Based on 149 reviews</p> #}
\t\t\t\t</div>
\t\t\t</div>

\t\t</div>
\t</div>


\t<div id=\"emp_video\" classs=\"container\">
\t\t<div class=\"container\">
\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\" style=\"margin-top:50px;background-color:\t#D8D8D8;\">Property Movies</h2>
\t\t
\t\t<br>
\t\t<p class=\"review-content-allign lead\">We produce the most innovative property movie style videos with drone footages, actors, and twilight photography showcasing a lifestyle in the property.</p>
\t</div>
\t\t<video width=\"640\" controls poster=\"bradstow-thumbnail.png\" preload=\"none\">
\t\t\t<source src=\"emp_video.mp4\" type=\"video/mp4\">

\t\t</video>

\t</div>


\t{# </div> 
\t\t
\t\t</div>
\t\t</div> <!--Container-fluid--> #}
{#----------------------------------------------------------------------------------------------------------------------------------#}

\t{# <div id=\"wd_id\">
\t\t</div>
\t\t<script type=\"text/javascript\" src=\"https://www.allagents.co.uk/widgets/ratings.js\"></script>
\t\t<script type=\"text/javascript\">init_widget(200,150,9542,'plain', '000', '')</script>
\t\t
\t\t<!-- BEGIN ESTAS WIDGET -->
\t\t<script>
\t\t    (function (w,d,s,o,f,js,fjs) {
\t\t        w['ESTAS-Widget']=o;w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) };
\t\t        js = d.createElement(s), fjs = d.getElementsByTagName(s)[0];
\t\t        js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs);
\t\t    }(window, document, 'script', 'estas', 'https://www.theestas.com/assets/js/min/widget.bundle.js'));
\t\t    estas('init', { el: 'estas-widget', key: 'kqIfEHGP8Yiy9G9S4EQUBSQQqBhNFbo6mMeLODVl9rYDqfcH7UQzs8bRu5bX', showComments: false });
\t\t</script>
\t\t<div id=\"estas-widget\"></div>
\t\t<!-- END ESTAS WIDGET -->
\t\t
\t\t  <script async defer src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2\"></script>  
\t\t  <div class=\"fb-post\" 
\t\t      data-href=\"https://www.facebook.com/kishor.gangawani/posts/3986695861354253\"
\t\t      data-width=\"300\"></div> #}
\t{# <div id = \"ratings\" style=\"margin-top:30px;margin-right:10px; \">
\t\t      <div class=\"listing container\" style=\"max-width: 1200px;\">
\t\t
\t\t   <div id =\"review-counter\" class = \"container\">
\t\t   <div id=\"wd_id\">
\t\t
\t\t</div>
\t\t<script type=\"text/javascript\" src=\"https://www.allagents.co.uk/widgets/ratings.js\"></script>
\t\t<script type=\"text/javascript\">init_widget(200,150,9542,'plain', '000', '')</script>
\t\t<div style=\"width: 100%;height: 35%;\">
\t\t   #}


\t<!-- SECTION NUMBERS -->
\t{# <section class=\"numbers dark-blue-bg white-txt\" style = \"padding:0px;\">
\t\t\t\t<div class=\"marged flex\">
\t\t  <!-- item 01 -->
\t\t\t\t\t<div class=\"number-item\" style=\"text-align: center;\">
\t\t\t\t\t\t<h2><span class=\"value\">782</span></h2>
\t\t\t\t\t\t<h6>Houses Sold</h6>
\t\t\t\t\t</div>
\t\t
\t\t\t\t<!-- item 02 -->
\t\t\t\t\t<div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
\t\t\t\t\t\t<h2 class=\"value\">1388</h2>
\t\t\t\t\t\t<h6>clients</h6>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</section>
\t\t<!-- END OF SECTION NUMBERS -->
\t\t
\t\t</div>
\t\t</div> #}

\t{# <div class=\"wide\">
\t\t   <script async defer src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2\"></script>  
\t\t  <div class=\"fb-post\" 
\t\t      data-href=\"https://www.facebook.com/kishor.gangawani/posts/3986695861354253\"
\t\t      data-width=\"100\"></div>
\t\t    </div> #}


\t<!-- BEGIN ESTAS WIDGET -->
\t{# <div style=\"
\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 30%);
\t\t    border-radius: 5%;
\t\t    padding: 10px;
\t\t\">
\t\t    <script>
\t\t    (function (w,d,s,o,f,js,fjs) {
\t\t        w['ESTAS-Widget']=o;w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) };
\t\t        js = d.createElement(s), fjs = d.getElementsByTagName(s)[0];
\t\t        js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs);
\t\t    }(window, document, 'script', 'estas', 'https://www.theestas.com/assets/js/min/widget.bundle.js'));
\t\t    estas('init', { el: 'estas-widget', key: 'kqIfEHGP8Yiy9G9S4EQUBSQQqBhNFbo6mMeLODVl9rYDqfcH7UQzs8bRu5bX', showComments: false });
\t\t</script>
\t\t<div id=\"estas-widget\"></div>
\t\t<!-- END ESTAS WIDGET -->
\t\t  </div>
\t\t
\t\t    </div>
\t\t  </div> #}


\t{# 
\t\t<div id=\"google-reviews\">
\t\t<div id=\"revv-widget-container-3173d7b8-1cc6-4c7e-86d7-9c6c34cad640\"></div><script type=\"text/javascript\" src=\"https://api.addreviews.co/static/widget.js\" defer></script>
\t\t</div> #}


\t{# <div class=\"items-container-2\">
\t\t   
\t\t  
\t\t  <div class=\"calcContainer\">
\t\t  <div class=\"calcSub\">
\t\t    <div class=\"calcHeading\">Calculate your Monthly Payment</div>
\t\t    <div style=\" padding-left: auto;padding-left: 37px; padding-right: 37px;\">
\t\t      <form action=\"#\" name=\"mortgage_calculator\">
\t\t      <input type=\"text\" class=\"calcInput\" id=\"inCost\" placeholder=\"Loan Amount (£)\" />
\t\t
\t\t
\t\t      <input type=\"text\" class=\"calcInput\" id=\"inDown\" placeholder=\"Down Payment (£)\" />
\t\t
\t\t
\t\t      <input type=\"text\" class=\"calcInput\" id=\"inInterest\" placeholder=\"Interest (%)\" />
\t\t
\t\t
\t\t      <input type=\"text\" class=\"calcInput\" id=\"inTerm\" placeholder=\"Length of Loan (years)\" />
\t\t    </div>
\t\t    <p>
\t\t      <button type=\"button\" class=\"calcButton\" id=\"btnCalculate\">Calculate</button></form>
\t\t    </p>
\t\t    <p class=\"calcAnswer\"><b>Total Monthly Payment</b>:
\t\t      <br/>
\t\t      <br/><span id=\"outMonthly\">Please enter the values above.</span></p>
\t\t  </div>
\t\t</div> #}


\t{# <div class=\"blog col col-lg-6\">
\t\t  <img  class=\"responsive2\" src = \"blog-pic.png\"></img>
\t\t  <h4>Check out our blog</h4>
\t\t  </div> #}

\t\t
  
\t\t<div class=\"cookie-popup cookie-popup--short cookie-popup--dark\" style=\"z-index: 100;\">
\t\t  
\t\t  <div>
\t\t  This website uses cookies to provide you with a great user experience. By using it, you accept our <a href=\"{{ path('emp_cookie-policy') }}\">use of cookies</a>
\t\t  </div>
\t\t  <div class=\"cookie-popup-actions\">
\t\t\t<button>Okay</button>
\t\t  </div>
\t  
\t\t</div>
\t\t
\t  </div>


</div>{% endblock %}
", "/home.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\home.html.twig");
    }
}
