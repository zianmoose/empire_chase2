<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact-us.html.twig */
class __TwigTemplate_fa49c6adbab11ffdd11683482baff7c3b259828a6452a9a9c083a7605594d15a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact-us.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact-us.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contact-us.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('contact-us/block.jpg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Contact us</h1>
\t\t\t\t<p>Communication is key. With the ample platforms to communicate we are very responsive in communication</p>
\t\t\t\t";
        // line 16
        echo "\t\t\t</div>

\t\t\t";
        // line 19
        echo "
\t\t</div>

\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!-- /contact form -->
\t\t\t<div
\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t<section
\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t<!--Section description-->
\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"row\">

\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-7 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t     background-color: #D8D8D8;
\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t\t";
        // line 47
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 47, $this->source); })()), 'form');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t</div>

\t\t\t\t\t</section>
\t\t\t\t\t<div class=\"column mcb-column one column_column \" style=\"margin-right: auto;margin-left: auto;\">
\t\t\t\t\t\t<div class=\"column_attr clearfix\" style=\"margin-right: auto;margin-left: auto;\">
\t\t\t\t\t\t\t<div class=\"mb-4\">
\t\t\t\t\t\t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2479.4840543957807!2d-0.33353409814758284!3d51.577691312099894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761374e0f739bf%3A0xb944de8ed055e9b5!2sEmpire+Chase+Estate+Agents!5e0!3m2!1sen!2slk!4v1525769405757\" width=\"100%\" frameborder=\"0\" style=\"border:0; margin-right:auto;margin-left:auto;\" allowfullscreen=\"\"></iframe>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"container\">
\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">COMPLAINTS PROCEDURE</h2>
\t\t\t\t<p class=\"lead\">We are committed to providing a professional service to all our clients and customers.Whensomething goes wrong, we need you to tell us about it.  This will help us to improve ourstandards.</p>
\t\t\t\t<p class=\"lead\">If you have a complaint, please put it in writing, including as much detail as possible.  We willthen respond in line with the timeframes set out below (if you feel we have not sought toaddress your complaints within eight weeks, you may be able to refer your complaint to theProperty Ombudsman to consider without our final viewpoint on the matter).</p>
\t\t\t\t<p class=\"lead\">What will happen next?</p>
\t\t\t\t<ul style = \"margin-left:20px;\">
\t\t\t\t\t<li class=\"lead\">We   will   send   you   a   letter   acknowledging   receipt   of   your   complaint   within   threeworking days of receiving it, enclosing a copy of this procedure.</li>
\t\t\t\t\t<li class=\"lead\">We will then investigate your complaint. This will normally be dealt with by the officemanager who will review your file and speak to the member of staff who dealt withyou.  A  formal   written  outcome   of   our   investigation  will  be   sent  to  you  within   15working days of sending the acknowledgement letter.</li>
\t\t\t\t\t<li class=\"lead\">If, at this stage, you are still not satisfied, you should contact us again and we willarrange for a separate review to take place by a senior member of staff.</li>
\t\t\t\t\t<li class=\"lead\">We will write to you within 15 working days of receiving your request for a review,confirming our final viewpoint on the matter.</li>
\t\t\t\t</ul>
\t\t\t\t<p class=\"lead\">If you remain dissatisfied, you can then contact The Property Ombudsman to request anindependent review:</p>
\t\t\t\t<address>

\t\t\t\t\tThe Property Ombudsman Ltd<br>
\t\t\t\t\tMilford House<br>
\t\t\t\t\t43-45 Milford Street<br>
\t\t\t\t\tSalisbury<br>
\t\t\t\t\tWiltshire<br>
\t\t\t\t\tSP1 2BP<br>
\t\t\t\t\tUSA
\t\t\t\t\t<br><br>
\t\t\t\t\t<a href=\"www.tpos.co.uk \">www.tpos.co.uk
\t\t\t\t\t</a><br>
\t\t\t\t\t<a href=\"tel:01722 333 306\">01722 333 306</a>
\t\t\t\t</address>
\t\t\t\t<p class=\"lead\">Please note the following:</p>
\t\t\t\t<p class=\"lead\">You will need to submit your complaint to The Property Ombudsman within 12 months of receiving our final viewpoint letter, including any evidence to support your case.</p>
\t\t\t\t<p class=\"lead\">The Property Ombudsman requires that all complaints are addressed through this in-house complaints procedure, before being submitted for an independent review.</p>
\t\t\t</div>

\t\t</main>


\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact-us.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 47,  85 => 19,  81 => 16,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('contact-us/block.jpg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Contact us</h1>
\t\t\t\t<p>Communication is key. With the ample platforms to communicate we are very responsive in communication</p>
\t\t\t\t{# <p><a class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #FFFFFF!important\" role=\"button\">Contact us</a></p> #}
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!-- /contact form -->
\t\t\t<div
\t\t\t\tclass=\"row\" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t\t<!--Section: Contact v.2-->
\t\t\t\t<section
\t\t\t\t\tclass=\"mb-4\">

\t\t\t\t\t<!--Section heading-->
\t\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t\t<!--Section description-->
\t\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"row\">

\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-7 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t     background-color: #D8D8D8;
\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t\t{{form(form)}}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t\t</div>

\t\t\t\t\t</section>
\t\t\t\t\t<div class=\"column mcb-column one column_column \" style=\"margin-right: auto;margin-left: auto;\">
\t\t\t\t\t\t<div class=\"column_attr clearfix\" style=\"margin-right: auto;margin-left: auto;\">
\t\t\t\t\t\t\t<div class=\"mb-4\">
\t\t\t\t\t\t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2479.4840543957807!2d-0.33353409814758284!3d51.577691312099894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761374e0f739bf%3A0xb944de8ed055e9b5!2sEmpire+Chase+Estate+Agents!5e0!3m2!1sen!2slk!4v1525769405757\" width=\"100%\" frameborder=\"0\" style=\"border:0; margin-right:auto;margin-left:auto;\" allowfullscreen=\"\"></iframe>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"container\">
\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">COMPLAINTS PROCEDURE</h2>
\t\t\t\t<p class=\"lead\">We are committed to providing a professional service to all our clients and customers.Whensomething goes wrong, we need you to tell us about it.  This will help us to improve ourstandards.</p>
\t\t\t\t<p class=\"lead\">If you have a complaint, please put it in writing, including as much detail as possible.  We willthen respond in line with the timeframes set out below (if you feel we have not sought toaddress your complaints within eight weeks, you may be able to refer your complaint to theProperty Ombudsman to consider without our final viewpoint on the matter).</p>
\t\t\t\t<p class=\"lead\">What will happen next?</p>
\t\t\t\t<ul style = \"margin-left:20px;\">
\t\t\t\t\t<li class=\"lead\">We   will   send   you   a   letter   acknowledging   receipt   of   your   complaint   within   threeworking days of receiving it, enclosing a copy of this procedure.</li>
\t\t\t\t\t<li class=\"lead\">We will then investigate your complaint. This will normally be dealt with by the officemanager who will review your file and speak to the member of staff who dealt withyou.  A  formal   written  outcome   of   our   investigation  will  be   sent  to  you  within   15working days of sending the acknowledgement letter.</li>
\t\t\t\t\t<li class=\"lead\">If, at this stage, you are still not satisfied, you should contact us again and we willarrange for a separate review to take place by a senior member of staff.</li>
\t\t\t\t\t<li class=\"lead\">We will write to you within 15 working days of receiving your request for a review,confirming our final viewpoint on the matter.</li>
\t\t\t\t</ul>
\t\t\t\t<p class=\"lead\">If you remain dissatisfied, you can then contact The Property Ombudsman to request anindependent review:</p>
\t\t\t\t<address>

\t\t\t\t\tThe Property Ombudsman Ltd<br>
\t\t\t\t\tMilford House<br>
\t\t\t\t\t43-45 Milford Street<br>
\t\t\t\t\tSalisbury<br>
\t\t\t\t\tWiltshire<br>
\t\t\t\t\tSP1 2BP<br>
\t\t\t\t\tUSA
\t\t\t\t\t<br><br>
\t\t\t\t\t<a href=\"www.tpos.co.uk \">www.tpos.co.uk
\t\t\t\t\t</a><br>
\t\t\t\t\t<a href=\"tel:01722 333 306\">01722 333 306</a>
\t\t\t\t</address>
\t\t\t\t<p class=\"lead\">Please note the following:</p>
\t\t\t\t<p class=\"lead\">You will need to submit your complaint to The Property Ombudsman within 12 months of receiving our final viewpoint letter, including any evidence to support your case.</p>
\t\t\t\t<p class=\"lead\">The Property Ombudsman requires that all complaints are addressed through this in-house complaints procedure, before being submitted for an independent review.</p>
\t\t\t</div>

\t\t</main>


\t{% endblock %}
", "contact-us.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\contact-us.html.twig");
    }
}
