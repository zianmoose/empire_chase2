<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /services/rent.html.twig */
class __TwigTemplate_8f3b3c87a1b9398ba90598cc3ae3b455d79199170db43f1e5d099f21217fef59 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/rent.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/rent.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/services/rent.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "\t<main
\t\trole=\"main\">
\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('rent/rent-banner.jpeg');background-size: cover;background-position: center;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  height:600px;\">
\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">
\t\t\t\t\tRent
\t\t\t\t</h1>
\t\t\t\t<p>
\t\t\t\t\tWe are a customer service oriented firm focusing on best customer services.

\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">
\t\t\t\t\t\tContact us
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 24
        echo "\t\t</div>

\t\t<div class=\"container\">
\t\t\t<br>
\t\t\t\t<p class=\"lead review-content-allign\">
\t\t\t\t\tOur lettings team source the best properties in the area from modern apartments to fine homes.
\t\t\t\t</p>

\t\t\t</br>

\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\t\tWhy rent from us
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<div style=\"margin-left:50px\" ;>

\t\t\t\t<ul>

\t\t\t\t\t<li class=\"lead2\">Genuine guidance</li>
\t\t\t\t\t<li class=\"lead2\">24 hour property enquiry line
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Award winning customer services</li>
\t\t\t\t\t<li class=\"lead2\">Selection of best properties in the area</li>
\t\t\t\t\t<li class=\"lead2\">Qualified and fully regulated agents
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Fully complaint
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Stunning modern properties</li>
\t\t\t\t\t<li class=\"lead2\">After sales services
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Assistants to clients during the entire tenancy</li>
\t\t\t\t</ul>


\t\t\t</div>

\t\t\t<br>
\t\t\t<div id=\"btr-3img\" class=\"row justify-content-center\">

\t\t\t\t<div id=\"btr-3img-inner\"><img src=\"rent/move.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t\t<div id=\"btr-3img-inner\"><img src=\"rent/award.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t\t<div id=\"btr-3img-inner\"><img src=\"rent/24-7.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t</div>
\t\t\t<br>
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\t\tTenant fees
\t\t\t</h2>
\t\t\t<br>
\t\t\t\t<p class=\"lead review-content-allign\">
\t\t\t\t\tWe do not charge any admin related fees at all. However according to The Tenant fee ban act below is our general fee information
\t\t\t\t</p>

\t\t\t</br>
\t\t\t<p class=\"lead\">
\t\t\tRelevant letting fees and tenant protection information in line with the tenant fee ban 2019. 
\t\t\t
\t\t\tAs well as paying the rent, you may also be required to make the following permitted payments.
\t\t\t</p>

\t\t

\t\t\t<br>

\t\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Holding Deposit:1 week's rent </li>
\t\t\t\t<p class=\"lead\" style=\"margin-left: 40px;\">
\t\t\t\tThis is to reserve a property. Please Note: This will be withheld if any relevant person (including any guarantor(s)) withdraws from the tenancy, fail a Right to Rent check, provide materially significant false or misleading information, or fail to sign their tenancy agreement (and / or Deed of Guarantee) within 15 calendar days (or other Deadline for Agreement as mutually agreed in writing). If the tenant is a company (PLC, Ltd or LLP) an administration fee is payable to reserve the property instead of a holding deposit. 
\t\t\t
\t\t\t\t</p>
\t
\t\t\t\t<li class=\"lead\">Deposit: 5 weeks' rent - This covers damages or defaults on the part of the tenant during the tenancy  </li>
\t\t\t\t<li class=\"lead\">Changes to the tenancy agreement: None</li>
\t\t\t\t<li class=\"lead\">Payment of interest for the late payment of rent at a rate of 3%</li>
\t\t\t\t<li class=\"lead\">Charge for the reasonably incurred costs for the loss of keys/security devices: £70 labour plus the key cost and any cost related to a fob.</li>
\t\t\t\t<li class=\"lead\">Payment of any unpaid rent or other reasonable costs associated with your early termination of the tenancy.  </li>
\t\t\t\t<li class=\"lead\">During the tenancy (payable to the provider)</li>
\t\t\t\t<li class=\"lead\">Utilities - gas, electricity, water</li>
\t\t\t\t<li class=\"lead\">Installation of cable/satellite</li>
\t\t\t\t<li class=\"lead\">Subscription to cable/satellite supplier </li>
\t\t\t\t<li class=\"lead\">Television licence </li>
\t\t\t\t<li class=\"lead\">Council Tax </li>
\t\t\t<li class=\"lead\">Other permitted payments </li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<p class=\"lead\">
\t\t\tAny other permitted payments, not included above, under the relevant legislation including contractual damages
\t\t\t</p>
\t\t\t<br>
\t\t\t<p class=\"lead\">
\t\t\tTenant protection
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\tEmpire Chase is a member of Safe Agents Client Money Protection which is a client money protection scheme, and a member of The Property Ombudsman which is a redress scheme.
\t\t\t</p>

\t
\t\t\t\t\t
\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t
\t\t

\t\t\t<br/>
\t\t\t<div class=\"row\">

\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t<div id=\"btr-spec\"><a href=\"https://spec.co/s/6yxj\"><img src=\"rent/virtual-viewings-360.png\" class=\"img-responsive center\" width=\"100%\" style=\"margin:10px; display:block;margin-right:auto;margin-left:auto\" alt=\"Responsive image\"></a></div>
\t\t\t\t\t\t

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t

\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing  review-content-allign\">
\t\t\t\t\tVirtual viewings
\t\t\t\t\t</h2>

\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\tWe offer virtual viewings facility on most of our properties. Clients can view our properties through 360 live virtual tour or video call before a face to face viewing.
\t\t\t\t\t</p>


\t\t\t\t</div>


\t\t\t</div>
\t\t\t<br>
\t\t\t<br>
\t\t\t<div class=\"row\">

\t\t\t
\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing  review-content-allign\">
\t\t\tRight to check
\t\t\t</h2>

\t\t\t<p class=\"lead\">
\t\t\tThe Right to Rent scheme requires landlords to check that all tenants who occupy their properties have legal status to live in the UK. This means that before you can rent a home in England, a landlord or letting agent must undertake passport and immigration checks prior to letting out the property.
\t\t\t</p>


\t\t</div>

\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t<div id=\"btr-right-to-check\"><a href=\"\"><img src=\"rent/right-to-check.jpeg\" class=\"img-responsive center\" width=\"100%\" style=\"margin:10px; display:block;margin-right:auto;margin-left:auto\" alt=\"Responsive image\"></a></div>
\t\t\t\t\t\t

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>

\t\t\t</div>

\t\t\t<br>
\t\t\t\t<br>

\t\t\t
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\tLetting Process
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<div style=\"margin-left:50px\" ;>

\t\t\t\t<ol>

\t\t\t\t\t<li class=\"lead2\">View a property</li>
\t\t\t\t\t<li class=\"lead2\">Make an offer through our colleague</li>
\t\t\t\t\t<li class=\"lead2\"> If your offer is accepted by the landlord we will send you the offer in writing via email</li>
\t\t\t\t\t<li class=\"lead2\"> Clarify any questions you may have before proceeding </li>
\t\t\t\t\t<li class=\"lead2\">Pay a holding deposit</li>
\t\t\t\t\t<li class=\"lead2\">Referencing apply through Homelet </li>
\t\t\t\t\t<li class=\"lead2\"> Once references passed you will be notified </li>
\t\t\t\t\t<li class=\"lead2\">Right to rent check </li>
\t\t\t\t\t<li class=\"lead2\">We will serve the tenancy agreement, safety certificate and statutory documents via email </li>
\t\t\t\t<li class=\"lead2\"> Clarify any questions you may have before proceeding </li>
\t\t\t<li class=\"lead2\">Clarify any questions you may have </li>
\t\t\t<li class=\"lead2\">Sign contract </li>
\t\t\t<li class=\"lead2\"> Pay the balance funds 48 hours before your moved in date </li>
\t\t\t<li class=\"lead2\">Key collection and inventory check in </li>
\t\t\t<li class=\"lead2\">Move in to your property  </li>
\t\t<li class=\"lead2\">Notify utility suppliers\t</li>
\t\t\t\t</ol>


\t\t\t</div>

\t\t</div><!-- /container -->




\t\t\t<br/>
\t\t</div>
\t</main>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/services/rent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 24,  84 => 17,  68 => 3,  58 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block body %}
\t<main
\t\trole=\"main\">
\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('rent/rent-banner.jpeg');background-size: cover;background-position: center;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  height:600px;\">
\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">
\t\t\t\t\tRent
\t\t\t\t</h1>
\t\t\t\t<p>
\t\t\t\t\tWe are a customer service oriented firm focusing on best customer services.

\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">
\t\t\t\t\t\tContact us
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}
\t\t</div>

\t\t<div class=\"container\">
\t\t\t<br>
\t\t\t\t<p class=\"lead review-content-allign\">
\t\t\t\t\tOur lettings team source the best properties in the area from modern apartments to fine homes.
\t\t\t\t</p>

\t\t\t</br>

\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\t\tWhy rent from us
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<div style=\"margin-left:50px\" ;>

\t\t\t\t<ul>

\t\t\t\t\t<li class=\"lead2\">Genuine guidance</li>
\t\t\t\t\t<li class=\"lead2\">24 hour property enquiry line
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Award winning customer services</li>
\t\t\t\t\t<li class=\"lead2\">Selection of best properties in the area</li>
\t\t\t\t\t<li class=\"lead2\">Qualified and fully regulated agents
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Fully complaint
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Stunning modern properties</li>
\t\t\t\t\t<li class=\"lead2\">After sales services
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"lead2\">Assistants to clients during the entire tenancy</li>
\t\t\t\t</ul>


\t\t\t</div>

\t\t\t<br>
\t\t\t<div id=\"btr-3img\" class=\"row justify-content-center\">

\t\t\t\t<div id=\"btr-3img-inner\"><img src=\"rent/move.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t\t<div id=\"btr-3img-inner\"><img src=\"rent/award.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t\t<div id=\"btr-3img-inner\"><img src=\"rent/24-7.jpg\" class=\"img-responsive \" width=\"30%\" style=\"margin:10px;\" alt=\"Responsive image\"></div>

\t\t\t</div>
\t\t\t<br>
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\t\tTenant fees
\t\t\t</h2>
\t\t\t<br>
\t\t\t\t<p class=\"lead review-content-allign\">
\t\t\t\t\tWe do not charge any admin related fees at all. However according to The Tenant fee ban act below is our general fee information
\t\t\t\t</p>

\t\t\t</br>
\t\t\t<p class=\"lead\">
\t\t\tRelevant letting fees and tenant protection information in line with the tenant fee ban 2019. 
\t\t\t
\t\t\tAs well as paying the rent, you may also be required to make the following permitted payments.
\t\t\t</p>

\t\t

\t\t\t<br>

\t\t\t\t<ul style=\"margin-left: 20px;\">
\t\t\t\t<li class=\"lead\">Holding Deposit:1 week's rent </li>
\t\t\t\t<p class=\"lead\" style=\"margin-left: 40px;\">
\t\t\t\tThis is to reserve a property. Please Note: This will be withheld if any relevant person (including any guarantor(s)) withdraws from the tenancy, fail a Right to Rent check, provide materially significant false or misleading information, or fail to sign their tenancy agreement (and / or Deed of Guarantee) within 15 calendar days (or other Deadline for Agreement as mutually agreed in writing). If the tenant is a company (PLC, Ltd or LLP) an administration fee is payable to reserve the property instead of a holding deposit. 
\t\t\t
\t\t\t\t</p>
\t
\t\t\t\t<li class=\"lead\">Deposit: 5 weeks' rent - This covers damages or defaults on the part of the tenant during the tenancy  </li>
\t\t\t\t<li class=\"lead\">Changes to the tenancy agreement: None</li>
\t\t\t\t<li class=\"lead\">Payment of interest for the late payment of rent at a rate of 3%</li>
\t\t\t\t<li class=\"lead\">Charge for the reasonably incurred costs for the loss of keys/security devices: £70 labour plus the key cost and any cost related to a fob.</li>
\t\t\t\t<li class=\"lead\">Payment of any unpaid rent or other reasonable costs associated with your early termination of the tenancy.  </li>
\t\t\t\t<li class=\"lead\">During the tenancy (payable to the provider)</li>
\t\t\t\t<li class=\"lead\">Utilities - gas, electricity, water</li>
\t\t\t\t<li class=\"lead\">Installation of cable/satellite</li>
\t\t\t\t<li class=\"lead\">Subscription to cable/satellite supplier </li>
\t\t\t\t<li class=\"lead\">Television licence </li>
\t\t\t\t<li class=\"lead\">Council Tax </li>
\t\t\t<li class=\"lead\">Other permitted payments </li>
\t\t\t</ul>
\t\t\t<br>
\t\t\t<p class=\"lead\">
\t\t\tAny other permitted payments, not included above, under the relevant legislation including contractual damages
\t\t\t</p>
\t\t\t<br>
\t\t\t<p class=\"lead\">
\t\t\tTenant protection
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\tEmpire Chase is a member of Safe Agents Client Money Protection which is a client money protection scheme, and a member of The Property Ombudsman which is a redress scheme.
\t\t\t</p>

\t
\t\t\t\t\t
\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t
\t\t

\t\t\t<br/>
\t\t\t<div class=\"row\">

\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t<div id=\"btr-spec\"><a href=\"https://spec.co/s/6yxj\"><img src=\"rent/virtual-viewings-360.png\" class=\"img-responsive center\" width=\"100%\" style=\"margin:10px; display:block;margin-right:auto;margin-left:auto\" alt=\"Responsive image\"></a></div>
\t\t\t\t\t\t

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t

\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing  review-content-allign\">
\t\t\t\t\tVirtual viewings
\t\t\t\t\t</h2>

\t\t\t\t\t<p class=\"lead\">
\t\t\t\t\tWe offer virtual viewings facility on most of our properties. Clients can view our properties through 360 live virtual tour or video call before a face to face viewing.
\t\t\t\t\t</p>


\t\t\t\t</div>


\t\t\t</div>
\t\t\t<br>
\t\t\t<br>
\t\t\t<div class=\"row\">

\t\t\t
\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing  review-content-allign\">
\t\t\tRight to check
\t\t\t</h2>

\t\t\t<p class=\"lead\">
\t\t\tThe Right to Rent scheme requires landlords to check that all tenants who occupy their properties have legal status to live in the UK. This means that before you can rent a home in England, a landlord or letting agent must undertake passport and immigration checks prior to letting out the property.
\t\t\t</p>


\t\t</div>

\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t<div id=\"btr-right-to-check\"><a href=\"\"><img src=\"rent/right-to-check.jpeg\" class=\"img-responsive center\" width=\"100%\" style=\"margin:10px; display:block;margin-right:auto;margin-left:auto\" alt=\"Responsive image\"></a></div>
\t\t\t\t\t\t

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>

\t\t\t</div>

\t\t\t<br>
\t\t\t\t<br>

\t\t\t
\t\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\tLetting Process
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<div style=\"margin-left:50px\" ;>

\t\t\t\t<ol>

\t\t\t\t\t<li class=\"lead2\">View a property</li>
\t\t\t\t\t<li class=\"lead2\">Make an offer through our colleague</li>
\t\t\t\t\t<li class=\"lead2\"> If your offer is accepted by the landlord we will send you the offer in writing via email</li>
\t\t\t\t\t<li class=\"lead2\"> Clarify any questions you may have before proceeding </li>
\t\t\t\t\t<li class=\"lead2\">Pay a holding deposit</li>
\t\t\t\t\t<li class=\"lead2\">Referencing apply through Homelet </li>
\t\t\t\t\t<li class=\"lead2\"> Once references passed you will be notified </li>
\t\t\t\t\t<li class=\"lead2\">Right to rent check </li>
\t\t\t\t\t<li class=\"lead2\">We will serve the tenancy agreement, safety certificate and statutory documents via email </li>
\t\t\t\t<li class=\"lead2\"> Clarify any questions you may have before proceeding </li>
\t\t\t<li class=\"lead2\">Clarify any questions you may have </li>
\t\t\t<li class=\"lead2\">Sign contract </li>
\t\t\t<li class=\"lead2\"> Pay the balance funds 48 hours before your moved in date </li>
\t\t\t<li class=\"lead2\">Key collection and inventory check in </li>
\t\t\t<li class=\"lead2\">Move in to your property  </li>
\t\t<li class=\"lead2\">Notify utility suppliers\t</li>
\t\t\t\t</ol>


\t\t\t</div>

\t\t</div><!-- /container -->




\t\t\t<br/>
\t\t</div>
\t</main>
{% endblock %}
", "/services/rent.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\rent.html.twig");
    }
}
