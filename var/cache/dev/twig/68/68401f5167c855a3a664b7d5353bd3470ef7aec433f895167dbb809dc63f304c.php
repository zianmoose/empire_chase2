<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /services/lettings.html.twig */
class __TwigTemplate_2fe8ac740edf1b301a82e8e9251d38729d61fbb334ff8d4bece4f72629f89d93 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/lettings.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/lettings.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/services/lettings.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "\t<main
\t\trole=\"main\">
\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('about-us/bradstowe.jpg');background-size: cover;background-position: center;
\t\t\t\t\t\t\t\t\t\t\t\t  height:600px;\">
\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">
\t\t\t\t\tRent
\t\t\t\t</h1>
\t\t\t\t<p>
\t\t\t\t\tWe are a customer service oriented firm focusing on best customer services.

\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">
\t\t\t\t\t\tContact us
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 24
        echo "\t\t</div>

\t\t<div class=\"container\">
    <h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
    Why rent from Empire Chase
  </h2>
\t\t\t<br/>

      <div style= \"margin-left:50px\";>

\t\t\t<ul>
\t
\t\t\t\t<li class=\"lead2\">Genuine guidance</li>
\t\t\t\t<li class=\"lead2\">24 hour property enquiry line </li>
\t\t\t\t<li class=\"lead2\">Award winning customer services</li>
\t\t\t\t<li class=\"lead2\">Selection of best properties in the area</li>
      <li class=\"lead2\">Qualified and fully regulated agents </li>
    <li class=\"lead2\">Fully complaint  </li>
\t\t\t</ul>
  

\t\t</div>
\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\tWhy choose Empire Chase
\t\t</h2>
\t\t<br/>
\t\t<ul>
\t\t\t<li class=\"lead2\">Bespoke Marketing Strategy for every property</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tWe only take a handful or properties at a time which means we can
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          dedicate our 100% focus on selling them
\t\t\t</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tPaid social media advertising (Through our premier sales brand Empire
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          Chase Premier)
\t\t\t</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tAmerican Open House style viewings (Through our premier sales brand
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          Empire Chase Premier)
\t\t\t</li>
\t\t\t<li class=\"lead2\">Local marketing campaign</li>
\t\t\t<li class=\"lead2\">Premier Property portal listings</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tOverseas Marketing (Through our brand in Hong Kong Empire Chase HK,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          China, middle east and various other countries)
\t\t\t</li>
\t\t\t<li class=\"lead2\">24-hour property inquiry line</li>
\t\t\t<li class=\"lead2\">Sales progression</li>
\t\t\t<li class=\"lead2\">Award winning customer service</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tSales progression and ongoing support during the process.
\t\t\t</li>
\t\t</ul>

  </div>

\t\t<br/>
\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 id=\"marketingexpert\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\tOur Marketing Expertise
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<p class=\"lead\">
\t\t\t\tBeing a real estate agent, our favourite part is marketing. We believe
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          the correct marketing and correct pricing strategy will sell a home
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          for the best possible price.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tWe have invested ample time and revenue in adopting into every
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          marketing platform to offer our clients the best marketing service.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tWe use some of the best photographers to produce the best quality
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          images We will produce an attractive video (Featuring presenters for
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          larger properties) with drone images to advertise on all social media
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          platforms.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tOur marketing team will produce a 360 view virtual tour of the
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          property.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tIf you are looking to sell your home please have a chat with our team
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          who will explain the current market conditions with facts, spot the
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          treats and opportunities and device a plan to sell your biggest asset
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          to the highest price.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tAdvertising platforms
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tRightmove, Zoopla, On the Market, Instagram, Facebook, Linkedin, Tik
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          Tok, YouTube, Sports magazine, Open house style viewings, Videos
\t\t\t</p>
\t\t</div>

\t\t<br/>

\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">
\t\t\t\tSellers guide (Step by Step process)
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<ul>
\t\t\t\t<li class=\"lead\">Prepare for sale</li>
\t\t\t\t<li class=\"lead\">Value your home</li>
\t\t\t\t<li class=\"lead\">Choose an estate agent</li>
\t\t\t\t<li class=\"lead\">Choose a conveyancer</li>
\t\t\t\t<li class=\"lead\">Prepare your home for sale</li>
\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t<li class=\"lead\">Exchange</li>
\t\t\t\t<li class=\"lead\">Completion</li>
\t\t\t\t<li class=\"lead\">Move in out</li>
\t\t\t\t<li class=\"lead\">After sales</li>
\t\t\t</ul>
\t\t</div>

\t\t<br/>
\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 id=\"privateagent\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\tPremier Sales through our Premier Brand EMPIRE CHASE PREMIER
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<p class=\"lead\">
\t\t\t\tEmpire Chase Premier is our luxury sales brand offering a very bespoke
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          premier sales services for luxury and upscale properties.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tSales services offers American style bespoke marketing services, with
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          property movies, luxury magazine marketing and paid social media
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          advertising in additional to all other marketing
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tPrivate estate agency services for sports personalities, musicians and
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          high net-worth individuals
\t\t\t</p>
\t\t</div>

\t\t<br/>
\t</div>
</main>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/services/lettings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 24,  84 => 17,  68 => 3,  58 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block body %}
\t<main
\t\trole=\"main\">
\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('about-us/bradstowe.jpg');background-size: cover;background-position: center;
\t\t\t\t\t\t\t\t\t\t\t\t  height:600px;\">
\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">
\t\t\t\t\tRent
\t\t\t\t</h1>
\t\t\t\t<p>
\t\t\t\t\tWe are a customer service oriented firm focusing on best customer services.

\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">
\t\t\t\t\t\tContact us
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}
\t\t</div>

\t\t<div class=\"container\">
    <h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
    Why rent from Empire Chase
  </h2>
\t\t\t<br/>

      <div style= \"margin-left:50px\";>

\t\t\t<ul>
\t
\t\t\t\t<li class=\"lead2\">Genuine guidance</li>
\t\t\t\t<li class=\"lead2\">24 hour property enquiry line </li>
\t\t\t\t<li class=\"lead2\">Award winning customer services</li>
\t\t\t\t<li class=\"lead2\">Selection of best properties in the area</li>
      <li class=\"lead2\">Qualified and fully regulated agents </li>
    <li class=\"lead2\">Fully complaint  </li>
\t\t\t</ul>
  

\t\t</div>
\t\t<h2 id=\"whychooseus\" class=\"h2-letter-spacing review-content-allign\">
\t\t\tWhy choose Empire Chase
\t\t</h2>
\t\t<br/>
\t\t<ul>
\t\t\t<li class=\"lead2\">Bespoke Marketing Strategy for every property</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tWe only take a handful or properties at a time which means we can
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          dedicate our 100% focus on selling them
\t\t\t</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tPaid social media advertising (Through our premier sales brand Empire
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          Chase Premier)
\t\t\t</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tAmerican Open House style viewings (Through our premier sales brand
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          Empire Chase Premier)
\t\t\t</li>
\t\t\t<li class=\"lead2\">Local marketing campaign</li>
\t\t\t<li class=\"lead2\">Premier Property portal listings</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tOverseas Marketing (Through our brand in Hong Kong Empire Chase HK,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          China, middle east and various other countries)
\t\t\t</li>
\t\t\t<li class=\"lead2\">24-hour property inquiry line</li>
\t\t\t<li class=\"lead2\">Sales progression</li>
\t\t\t<li class=\"lead2\">Award winning customer service</li>
\t\t\t<li class=\"lead2\">
\t\t\t\tSales progression and ongoing support during the process.
\t\t\t</li>
\t\t</ul>

  </div>

\t\t<br/>
\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 id=\"marketingexpert\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\tOur Marketing Expertise
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<p class=\"lead\">
\t\t\t\tBeing a real estate agent, our favourite part is marketing. We believe
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          the correct marketing and correct pricing strategy will sell a home
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          for the best possible price.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tWe have invested ample time and revenue in adopting into every
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          marketing platform to offer our clients the best marketing service.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tWe use some of the best photographers to produce the best quality
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          images We will produce an attractive video (Featuring presenters for
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          larger properties) with drone images to advertise on all social media
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          platforms.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tOur marketing team will produce a 360 view virtual tour of the
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          property.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tIf you are looking to sell your home please have a chat with our team
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          who will explain the current market conditions with facts, spot the
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          treats and opportunities and device a plan to sell your biggest asset
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          to the highest price.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tAdvertising platforms
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tRightmove, Zoopla, On the Market, Instagram, Facebook, Linkedin, Tik
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          Tok, YouTube, Sports magazine, Open house style viewings, Videos
\t\t\t</p>
\t\t</div>

\t\t<br/>

\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 class=\"review-content-allign h2-letter-spacing\">
\t\t\t\tSellers guide (Step by Step process)
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<ul>
\t\t\t\t<li class=\"lead\">Prepare for sale</li>
\t\t\t\t<li class=\"lead\">Value your home</li>
\t\t\t\t<li class=\"lead\">Choose an estate agent</li>
\t\t\t\t<li class=\"lead\">Choose a conveyancer</li>
\t\t\t\t<li class=\"lead\">Prepare your home for sale</li>
\t\t\t\t<li class=\"lead\">Sales progression</li>
\t\t\t\t<li class=\"lead\">Exchange</li>
\t\t\t\t<li class=\"lead\">Completion</li>
\t\t\t\t<li class=\"lead\">Move in out</li>
\t\t\t\t<li class=\"lead\">After sales</li>
\t\t\t</ul>
\t\t</div>

\t\t<br/>
\t\t<div class=\"animation-element slide-left\">
\t\t\t<h2 id=\"privateagent\" class=\"review-content-allign h2-letter-spacing\">
\t\t\t\tPremier Sales through our Premier Brand EMPIRE CHASE PREMIER
\t\t\t</h2>
\t\t\t<br/>

\t\t\t<p class=\"lead\">
\t\t\t\tEmpire Chase Premier is our luxury sales brand offering a very bespoke
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          premier sales services for luxury and upscale properties.
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tSales services offers American style bespoke marketing services, with
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          property movies, luxury magazine marketing and paid social media
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          advertising in additional to all other marketing
\t\t\t</p>
\t\t\t<p class=\"lead\">
\t\t\t\tPrivate estate agency services for sports personalities, musicians and
\t\t\t\t\t\t\t\t\t\t\t\t\t\t          high net-worth individuals
\t\t\t</p>
\t\t</div>

\t\t<br/>
\t</div>
</main>{% endblock %}
", "/services/lettings.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\lettings.html.twig");
    }
}
