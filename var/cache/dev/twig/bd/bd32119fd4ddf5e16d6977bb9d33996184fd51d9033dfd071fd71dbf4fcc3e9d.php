<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /services/sales.html.twig */
class __TwigTemplate_04ccfc6ba6cd6044eb77e07ddae4968c2d27a6c25e9337ca594253033dd52e7e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/sales.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/services/sales.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/services/sales.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Our services</h1>
\t\t\t\t<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content.</p>
\t\t\t\t<p>
\t\t\t\t\t<a class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 20
        echo "
\t\t</div>

\t\t<div class=\"container\">
\t\t\t<h1>Sales<h1>


\t\t\t\t\t<h1 class=\"text-center\">Apple Like Content Section</h1>

\t\t\t\t\t<!--  image left  -->
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">

\t\t\t\t\t\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6a/Imac_alu.png\" alt=\"\" class=\"w-100\"/>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t<h1 class=\"display-3\">Vertically Centered Text</h1>
\t\t\t\t\t\t\t\t\t<p class=\"lead\">Flexbox grids help you build some really nice layouts.
\t\t\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t\t\t<a href=\"\" class=\"\">Learn More</a>
\t\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.row -->

\t\t\t\t\t<!--  image right  -->
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">

\t\t\t\t\t\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6a/Imac_alu.png\" alt=\"\" class=\"w-100\"/>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t<h1 class=\"display-3\">Vertically Centered Text</h1>
\t\t\t\t\t\t\t\t\t<p class=\"lead\">Push and pull classes were added to change the order on desktop but still have the image before the text on mobile.
\t\t\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t\t\t<a href=\"\" class=\"\">Learn More</a>
\t\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.row -->

\t\t\t\t</div>
\t\t\t\t<!-- /container -->


\t\t\t</main>


\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/services/sales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 20,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Our services</h1>
\t\t\t\t<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content.</p>
\t\t\t\t<p>
\t\t\t\t\t<a class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #FFFFFF!important\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div class=\"container\">
\t\t\t<h1>Sales<h1>


\t\t\t\t\t<h1 class=\"text-center\">Apple Like Content Section</h1>

\t\t\t\t\t<!--  image left  -->
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">

\t\t\t\t\t\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6a/Imac_alu.png\" alt=\"\" class=\"w-100\"/>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t<h1 class=\"display-3\">Vertically Centered Text</h1>
\t\t\t\t\t\t\t\t\t<p class=\"lead\">Flexbox grids help you build some really nice layouts.
\t\t\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t\t\t<a href=\"\" class=\"\">Learn More</a>
\t\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.row -->

\t\t\t\t\t<!--  image right  -->
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\">

\t\t\t\t\t\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6a/Imac_alu.png\" alt=\"\" class=\"w-100\"/>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t<h1 class=\"display-3\">Vertically Centered Text</h1>
\t\t\t\t\t\t\t\t\t<p class=\"lead\">Push and pull classes were added to change the order on desktop but still have the image before the text on mobile.
\t\t\t\t\t\t\t\t\t\t<br/><br/>
\t\t\t\t\t\t\t\t\t\t<a href=\"\" class=\"\">Learn More</a>
\t\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-6 -->

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.row -->

\t\t\t\t</div>
\t\t\t\t<!-- /container -->


\t\t\t</main>


\t\t{% endblock %}
", "/services/sales.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\sales.html.twig");
    }
}
