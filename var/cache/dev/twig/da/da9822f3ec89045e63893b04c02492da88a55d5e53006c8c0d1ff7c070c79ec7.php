<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_fb3e0a4a7d7beac0e4c96788c9953919416b7f44a16bb46b4313585939cb4fb5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'javascripts' => [$this, 'block_javascripts'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 2
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<meta charset=\"UTF-8\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0 shrink-to-fit=no\">

\t\t<title>
\t\t\t";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        // line 11
        echo "\t\t</title>
\t\t";
        // line 12
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "\t\t";
        // line 22
        echo "\t\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css\" rel=\"stylesheet\">
\t\t";
        // line 23
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 26
        echo "

\t</head>
\t<body>

\t\t";
        // line 31
        $this->displayBlock('navbar', $context, $blocks);
        // line 226
        echo "\t\t";
        $this->displayBlock('body', $context, $blocks);
        // line 227
        echo "
\t\t<footer class=\"footer\">
\t\t\t<div class=\"container\">

\t\t\t\t<div class=\"navbar-text pull-left\">


\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"http://facebook.com/empirechaseestateagents\"><i class=\"fab fa-facebook\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"http://linkedin.com/company/empire-chase/\"><i class=\"fab fa-linkedin\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"http://twitter.com/empirechase?lang=en\"><i class=\"fab fa-twitter\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"http://youtube.com/channel/UCJVaERxsB83h2-UiZrBH6cA\"><i class=\"fab fa-youtube\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"https://wa.me/+447890285162\"><i class=\"fab fa-whatsapp\" aria-hidden=\"true\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"https://www.instagram.com/empire_chase/\"><i class=\"fab fa-instagram\"></i></a></li>


\t\t\t\t\t</ul>

\t\t\t\t</div>

\t\t\t</div>
\t\t</footer>
\t\t<!-- =============== Stickyfooter End ====================-->

\t\t<section class=\"contact-area animation-element slide-left\" id=\"contact\">
\t\t\t<div class=\"container\" style=\"margin-bottom=20px;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6 offset-lg-3\">
\t\t\t\t\t\t<div class=\"contact-content text-center\">
\t\t\t\t\t\t\t<a href=\"#\"><img src=\"pOmbudsman-logo.png\" alt=\"logo\"></a>
\t\t\t\t\t\t\t<a href=\"#\"><img src=\"tsi-code-logo.jpg\" alt=\"logo\"></a>
\t\t\t\t\t\t\t<a href=\"#\"><img src=\"safeagent_Logo.jpg\" alt=\"logo\"></a>
\t\t\t\t\t\t\t<div class=\"hr\"></div>
\t\t\t\t\t\t\t<h6>14 Peterborough Road,Harrow,HA1 2BQ</h6>
\t\t\t\t\t\t\t<h6>0208 4227722<span></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t\t<!-- =============== 1.9 Contact Area End ====================-->
\t\t\t\t<!-- =============== 1.9 Footer-2 Area Start ====================-->
\t\t\t\t<footer-2>
\t\t\t\t\t<p style=\"margin-bottom: -40px;\">Copyright &copy; 2021
\t\t\t\t\t\t<img src=\"Emp-Logo-tiny.png\" alt=\"logo\">
\t\t\t\t\t\tAll Rights Reserved.</p>
\t\t\t\t</footer-2>


\t\t\t</body>
\t\t</html>
\t</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "My Application
\t\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 13
        echo "\t\t\t";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "

\t\t\t<!-- Renders app.js & a webpack runtime.js file
\t\t\t                <script src=\"/build/runtime.js\" defer></script>
\t\t\t                <script src=\"/build/app.js\" defer></script>
\t\t\t                See note below about the \"defer\" attribute -->
\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 23
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 24
        echo "\t\t\t";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 31
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 32
        echo "\t\t\t<nav id=\"bg-light\" class=\"navbar navbar-expand-lg navbar-light \">
\t\t\t\t<a id=\"navbar-brand-oval\" class=\"navbar-brand\" href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_home");
        echo "\">
\t\t\t\t\t<img src=\"/EmpLogo3.png\" class=\"d-inline-block\" alt=\" Empire Chase\">
\t\t\t\t</a>
\t\t\t\t<a id=\"ham-menu\" data-toggle=\"collapse\" data-target=\"#navbarText\" aria-controls=\"navbarText\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">

\t\t\t\t\t<div id=\"ham-icon\">
\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t<div class=\"icon-ham\">
\t\t\t\t\t\t\t\t<div class=\"hamburger\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</span>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarText\">
\t\t\t\t\t<ul class=\"navbar-nav mr-auto\">
\t\t\t\t\t\t<li class=\"nav-item active\">
\t\t\t\t\t\t\t<a class=\"nav-link\" href=\"";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_home");
        echo "\">HOME<span class=\"sr-only\">(current)</span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">ABOUT US<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_about_us");
        echo "\">About us</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_about_us");
        echo "\">Our Brands</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_about_us");
        echo "\">Registrations</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 66
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_about_us");
        echo "\">Awards</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_about_us");
        echo "\">Testimonials</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"#!\">Marketing expertise</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">BUY<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 81
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">Buying guide</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 84
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">Why chose us</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t";
        // line 88
        echo "\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">First time buyers</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 92
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">Investment property</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 95
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">Buy to Let</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">Fine homes</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 101
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">Private estate agency</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">SELL
\t\t\t\t\t\t\t\t<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">Selling guide</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 114
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">Why chose us</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 117
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">Marketing expertise</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 120
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">Marketing tools</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 123
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">Premier sales</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 126
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">Private estate agency</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>

            <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"";
        // line 132
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_rent");
        echo "\">RENT<span class=\"sr-only\">(current)</span>
          </a>
        </li>

\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">LANDLORDS<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 141
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Our lettings services</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 144
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Property Management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 147
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Buy to let landlords</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 150
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Portfolio landlords</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 153
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Corporate landlords</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 156
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Asset Management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 159
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Premier property management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 162
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">Fees</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">BUILD TO RENT<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 172
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_build-to-rent");
        echo "\">Research reports</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 175
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_build-to-rent");
        echo "\">PRS management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 178
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_build-to-rent");
        echo "\">Asset management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 181
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_build-to-rent");
        echo "\">Our clients</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 184
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_build-to-rent");
        echo "\">Our numbers</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t";
        // line 192
        echo "
\t\t\t\t\t\t";
        // line 203
        echo "

\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">VALUATION<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 210
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_virtual_valuation");
        echo "\">Virtual Valuation</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 213
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_in_person_valuation");
        echo "\">In Person Valuation</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<span class=\"navbar-text\">
\t\t\t\t\t\t<a href=\"";
        // line 219
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\">
\t\t\t\t\t\t\tCONTACT US
\t\t\t\t\t\t</a>
\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t</nav>
\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 226
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  531 => 226,  514 => 219,  505 => 213,  499 => 210,  490 => 203,  487 => 192,  479 => 184,  473 => 181,  467 => 178,  461 => 175,  455 => 172,  442 => 162,  436 => 159,  430 => 156,  424 => 153,  418 => 150,  412 => 147,  406 => 144,  400 => 141,  388 => 132,  379 => 126,  373 => 123,  367 => 120,  361 => 117,  355 => 114,  349 => 111,  336 => 101,  330 => 98,  324 => 95,  318 => 92,  312 => 89,  309 => 88,  304 => 84,  298 => 81,  283 => 69,  277 => 66,  271 => 63,  265 => 60,  259 => 57,  248 => 49,  229 => 33,  226 => 32,  216 => 31,  203 => 24,  193 => 23,  175 => 13,  165 => 12,  145 => 9,  83 => 227,  80 => 226,  78 => 31,  71 => 26,  69 => 23,  66 => 22,  64 => 20,  62 => 12,  59 => 11,  57 => 9,  48 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# templates/base.html.twig #}
<!DOCTYPE html>
<html>
\t<head>
\t\t<meta charset=\"UTF-8\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0 shrink-to-fit=no\">

\t\t<title>
\t\t\t{% block title %}My Application
\t\t\t{% endblock %}
\t\t</title>
\t\t{% block javascripts %}
\t\t\t{{ encore_entry_script_tags('app') }}

\t\t\t<!-- Renders app.js & a webpack runtime.js file
\t\t\t                <script src=\"/build/runtime.js\" defer></script>
\t\t\t                <script src=\"/build/app.js\" defer></script>
\t\t\t                See note below about the \"defer\" attribute -->
\t\t{% endblock %}
\t\t{# <script src=\"/node_modules/owl.carousel/dist/owl.carousel.min.js\"></script>
\t\t        <link rel=\"stylesheet\" href=\"/node_modules/owl.carousel/dist/assets/owl.carousel.min.css\" /> #}
\t\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css\" rel=\"stylesheet\">
\t\t{% block stylesheets %}
\t\t\t{{ encore_entry_link_tags('app') }}
\t\t{% endblock %}


\t</head>
\t<body>

\t\t{% block navbar %}
\t\t\t<nav id=\"bg-light\" class=\"navbar navbar-expand-lg navbar-light \">
\t\t\t\t<a id=\"navbar-brand-oval\" class=\"navbar-brand\" href=\"{{ path('emp_home') }}\">
\t\t\t\t\t<img src=\"/EmpLogo3.png\" class=\"d-inline-block\" alt=\" Empire Chase\">
\t\t\t\t</a>
\t\t\t\t<a id=\"ham-menu\" data-toggle=\"collapse\" data-target=\"#navbarText\" aria-controls=\"navbarText\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">

\t\t\t\t\t<div id=\"ham-icon\">
\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t<div class=\"icon-ham\">
\t\t\t\t\t\t\t\t<div class=\"hamburger\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</span>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarText\">
\t\t\t\t\t<ul class=\"navbar-nav mr-auto\">
\t\t\t\t\t\t<li class=\"nav-item active\">
\t\t\t\t\t\t\t<a class=\"nav-link\" href=\"{{ path('emp_home') }}\">HOME<span class=\"sr-only\">(current)</span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">ABOUT US<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_about_us') }}\">About us</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_about_us') }}\">Our Brands</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_about_us') }}\">Registrations</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_about_us') }}\">Awards</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_about_us') }}\">Testimonials</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"#!\">Marketing expertise</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">BUY<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_buy') }}\">Buying guide</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_buy') }}\">Why chose us</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t{# <li><a href=\"{{ path('emp_about_us') }}#brands\">Stamp duty calculator</a></li>
\t\t\t\t\t\t\t\t          <li><a href=\"{{ path('emp_about_us') }}#brands\">Morgage Calculator </a></li> #}
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_buy') }}\">First time buyers</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_buy') }}\">Investment property</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_buy') }}\">Buy to Let</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_buy') }}\">Fine homes</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">Private estate agency</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">SELL
\t\t\t\t\t\t\t\t<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">Selling guide</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">Why chose us</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">Marketing expertise</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">Marketing tools</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">Premier sales</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">Private estate agency</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>

            <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"{{ path('emp_services_rent') }}\">RENT<span class=\"sr-only\">(current)</span>
          </a>
        </li>

\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">LANDLORDS<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Our lettings services</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Property Management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Buy to let landlords</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Portfolio landlords</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Corporate landlords</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Asset Management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Premier property management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_landlords') }}\">Fees</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">BUILD TO RENT<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_services_build-to-rent') }}\">Research reports</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_services_build-to-rent') }}\">PRS management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_services_build-to-rent') }}\">Asset management</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_services_build-to-rent') }}\">Our clients</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_services_build-to-rent') }}\">Our numbers</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t{# <li class=\"nav-item\">
\t\t\t\t\t\t        <a class=\"nav-link\" href=\"#\">EMPIRE CHASE PREMIER</a>
\t\t\t\t\t\t      </li> #}

\t\t\t\t\t\t{# <li class=\"dropdown \" id=\"drop-down-sub\"><a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">MARKETING EXPERTISE<span class=\"caret\"></span></a>
\t\t\t\t\t\t        <ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t          <li><a href=\"#!\">Future of marketing</a></li>
\t\t\t\t\t\t          <li><a href=\"#!\">Videos</a></li>
\t\t\t\t\t\t          <li><a href=\"#!\">Social media</a></li>
\t\t\t\t\t\t          <li><a href=\"#!\">Magazines</a></li>
\t\t\t\t\t\t          <li><a href=\"#!\">Local presence</a></li>
\t\t\t\t\t\t          <li><a href=\"#!\">International marketing</a></li>
\t\t\t\t\t\t        </ul>
\t\t\t\t\t\t        </li> #}


\t\t\t\t\t\t<li class=\"dropdown \" id=\"drop-down-sub\">
\t\t\t\t\t\t\t<a href=\"#!\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-target=\"#\">VALUATION<span class=\"caret\"></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_virtual_valuation') }}\">Virtual Valuation</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"{{ path('emp_in_person_valuation') }}\">In Person Valuation</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<span class=\"navbar-text\">
\t\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\">
\t\t\t\t\t\t\tCONTACT US
\t\t\t\t\t\t</a>
\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t</nav>
\t\t{% endblock %}
\t\t{% block body %}{% endblock %}

\t\t<footer class=\"footer\">
\t\t\t<div class=\"container\">

\t\t\t\t<div class=\"navbar-text pull-left\">


\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"http://facebook.com/empirechaseestateagents\"><i class=\"fab fa-facebook\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"http://linkedin.com/company/empire-chase/\"><i class=\"fab fa-linkedin\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"http://twitter.com/empirechase?lang=en\"><i class=\"fab fa-twitter\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"http://youtube.com/channel/UCJVaERxsB83h2-UiZrBH6cA\"><i class=\"fab fa-youtube\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"https://wa.me/+447890285162\"><i class=\"fab fa-whatsapp\" aria-hidden=\"true\"></i></a></li>
\t\t\t\t\t\t<li><a href=\"https://www.instagram.com/empire_chase/\"><i class=\"fab fa-instagram\"></i></a></li>


\t\t\t\t\t</ul>

\t\t\t\t</div>

\t\t\t</div>
\t\t</footer>
\t\t<!-- =============== Stickyfooter End ====================-->

\t\t<section class=\"contact-area animation-element slide-left\" id=\"contact\">
\t\t\t<div class=\"container\" style=\"margin-bottom=20px;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6 offset-lg-3\">
\t\t\t\t\t\t<div class=\"contact-content text-center\">
\t\t\t\t\t\t\t<a href=\"#\"><img src=\"pOmbudsman-logo.png\" alt=\"logo\"></a>
\t\t\t\t\t\t\t<a href=\"#\"><img src=\"tsi-code-logo.jpg\" alt=\"logo\"></a>
\t\t\t\t\t\t\t<a href=\"#\"><img src=\"safeagent_Logo.jpg\" alt=\"logo\"></a>
\t\t\t\t\t\t\t<div class=\"hr\"></div>
\t\t\t\t\t\t\t<h6>14 Peterborough Road,Harrow,HA1 2BQ</h6>
\t\t\t\t\t\t\t<h6>0208 4227722<span></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t\t<!-- =============== 1.9 Contact Area End ====================-->
\t\t\t\t<!-- =============== 1.9 Footer-2 Area Start ====================-->
\t\t\t\t<footer-2>
\t\t\t\t\t<p style=\"margin-bottom: -40px;\">Copyright &copy; 2021
\t\t\t\t\t\t<img src=\"Emp-Logo-tiny.png\" alt=\"logo\">
\t\t\t\t\t\tAll Rights Reserved.</p>
\t\t\t\t</footer-2>


\t\t\t</body>
\t\t</html>
\t</body>
</html>
", "base.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\base.html.twig");
    }
}
