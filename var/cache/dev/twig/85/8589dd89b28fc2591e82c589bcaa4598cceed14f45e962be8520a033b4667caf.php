<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* services/property-management.html.twig */
class __TwigTemplate_9c035807ed8561d7b17be9c18b8d16332825544617ad4fc08942b34751c9d884 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/property-management.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/property-management.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "services/property-management.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"BwxMlIWp\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id = \"property-man-banner\"class=\"jumbotron jumbotron-fluid services-jumbo\" >

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Property Management</h1>
\t\t\t\t<p>Your property is in safe hands</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 23
        echo "
\t\t</div>

\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<h1 class=\"h2-letter-spacing review-content-allign \">Property management services</h1>
\t\t\t\t\t<br>
\t\t\t\t<p class=\"lead\">From Harrow to Central London and Edgware to Chelsea our property management team covers a wide area of properties in London.</p>
\t\t\t<p class=\"lead\">Our super-efficient property management team is highly appreciated by all our existing landlords and tenants. Offering a 24-hour property management services to all our managed properties below are some of our USP</p>
\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t</p>


\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<img src=\"blockMan-pic.jpg\" alt=\"\" class=\"w-100\"/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\" style=\"margin-top: 20px;\">

\t\t<h1 class=\"h2-letter-spacing review-content-allign \">Our USP</h1>
\t\t\t\t\t<br>
\t\t\t\t\t\t<ul style = \"margin-left:30px;\">
\t\t\t\t\t\t<li class=\"lead\">
\t\t\t\t\t24 hour emergency line </li>
\t\t\t\t\t\t<li class=\"lead\">Experienced and qualified property managers .</li>
\t\t\t\t\t\t<li class=\"lead\">Efficient and prompt response services </li>
\t\t\t\t\t\t<li class=\"lead\">Complaint and up to date with all the new regulations </li>
\t\t\t\t\t\t<li class=\"lead\">Monthly/quarterly client reporting  </li>
\t\t\t\t\t\t<li class=\"lead\">Work within client SLA and budget</li>
\t\t\t\t\t\t<li class=\"lead\">Honest, transparent and efficient service</li>
\t\t\t\t\t</ul>

\t\t\t\t\t<br>

\t\t\t

\t\t\t\t</div>
\t\t\t</br>

\t\t\t<!-- /.col-md-6 -->
\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t<div class=\"fb-video\" data-href=\"https://www.facebook.com/empirechaseestateagents/videos/4954274441280932/\" data-width=\"500\" data-show-text=\"false\">
\t\t\t\t\t\t\t<blockquote cite=\"https://developers.facebook.com/empirechaseestateagents/videos/4954274441280932/\" class=\"fb-xfbml-parse-ignore\">
\t\t\t\t\t\t\t\t<a href=\"https://developers.facebook.com/empirechaseestateagents/videos/4954274441280932/\"></a>
\t\t\t\t\t\t\t\t<p>If you own a property in London - You got to try this.  
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tProfessional property management  3 months Free Trial - Get in touch   
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tpropertymanagement&#064;empirechase.co.uk 02084227722  
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tapplies for selected properties only in selected areas only.</p>Posted by
\t\t\t\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/\">Empire Chase</a>
\t\t\t\t\t\t\t\ton Saturday, January 9, 2021</blockquote>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t\t<!-- /.col-md-6 -->

\t\t</div>
\t\t<!-- /.row -->
<br>
\t<h1 class=\"h2-letter-spacing review-content-allign \">Services Covered </h1>

\t<br>
\t\t<ul style = \"margin-left:30px;\">
\t\t<li class=\"lead\">Organise inventory check ins and check outs   </li>
\t\t<li class=\"lead\">Move the tenants in </li>
\t\t<li class=\"lead\">Register tenants for utility bills </li>
\t\t<li class=\"lead\">Register the deposit</li>
\t\t<li class=\"lead\">24 hour property emergency line for tenants   </li>
\t\t<li class=\"lead\">Look after the day-to-day maintenance issues</li>
\t\t<li class=\"lead\">Organise contractors</li>
\t\t<li class=\"lead\">Organise safety check such as electric and gas</li>
\t\t<li class=\"lead\">Rent collection </li>
\t\t<li class=\"lead\">Monthly/quarterly client reporting</li>
\t\t<li class=\"lead\">Produce statement of accounts each quarter</li>
\t\t<li class=\"lead\">Deposit dispute resolution</li>
\t</ul>
\t<br>

\t<section class=\"numbers white-txt justify-content-center\" style = \"padding:0px; background-color: #484848!important;\">
\t<div class=\"marged flex\">
  <!-- item 01 -->
\t  <div class=\"number-item\" style=\"text-align: center;\">
\t\t<h2><span class=\"value\">414</span></h2>
\t\t<h6>Properties managed</h6>
\t  </div>

\t<!-- item 02 -->
\t  <div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
\t\t<h2 ><span class=\"value\">99</span>%</h2>
\t\t<h6>of our clients paid full rent and didn’t fall in to rent arrears in the past 12 months</h6>
\t  </div>
\t</div>
\t<div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
<h2 >£ <span class=\"value\">85</span>,<span class=\"value\">639</span>,<span class=\"value\">202</span></h2>
<h6>worth of asset and properties managed</h6>
</div>
</div>
  </section>
  <br>
\t<br>


\t\t<!-- /contact form -->
\t\t<div class =\"container\">
\t<h1 class=\"h2-letter-spacing review-content-allign \">Premier property management</h1>
\t<br>
<p class=\"lead review-content-allign\">We have a premier service for clients who seek a complete management for their asset. </p>
<p class=\"lead review-content-allign\">This service is mainly designed for overseas landlords who do not live in the UK</p>
<p class=\"lead review-content-allign\">In addition to the lettings tenant finder services below are the services we offer in our premier property management services</p>
<br>
<h1 class=\"h2-letter-spacing review-content-allign \">Services includes</h1>
<ul style = \"margin-left:30px;\">
<li class=\"lead\">Organise inventory check in    </li>
<li class=\"lead\">Move the tenants in </li>
<li class=\"lead\">Register tenants for utility bills </li>
<li class=\"lead\">Register the deposit</li>
<li class=\"lead\">24 hour property emergency line for tenants   </li>
<li class=\"lead\">Organise contractors</li>
<li class=\"lead\">Organise safety check such as electric and gas</li>
<li class=\"lead\">Rent collection </li>
<li class=\"lead\">Monthly/quarterly client reporting</li>
<li class=\"lead\">Produce statement of accounts each quarter</li>
<li class=\"lead\">Deposit dispute resolution</li>
<li class=\"lead\">Dedicated account manager with 24-hour access</li>
<li class=\"lead\">Monthly statement of accounts</li>
<li class=\"lead\">Pay any outgoings on behalf of the landlord and reimburse via rent collection</li>
<li class=\"lead\">Represent the landlord </li>
<li class=\"lead\">Register for Non-Residents landlords</li>
<li class=\"lead\">Assist with any other property related issues and matters</li>
</ul>
<br>
<h1 class=\"h2-letter-spacing review-content-allign \">Rent guarantee</h1>
<br>
<p class=\"lead \">We offer rent guarantee insurances from some of the leading rent guarantee insurance providers such as Homelet offering maximum cover to landlords. </p>
<p class=\"lead\">Currently there are policies covering up to 12 – 15 months on new tenancies. Please contact our property management team for further questions</p>
<p class=\"lead\">If you would like to know how proactive our property management services are and why we are better than others, please contact our property management team</p>

\t\t<div
\t\t\tclass=\"row \" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t<!--Section: Contact v.2-->
\t\t\t<section
\t\t\t\tclass=\"mb-4\">

\t\t\t\t<!--Section heading-->
\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t<!--Section description-->
\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t<div
\t\t\t\t\tclass=\"row\">

\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t<div class=\"col-md-6 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    font-weight: 300;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t";
        // line 210
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 210, $this->source); })()), 'form');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->


\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t</div>

\t\t\t\t</section>


\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t</div>
\t\t<!-- /container -->


\t</main>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "services/property-management.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 210,  91 => 23,  84 => 18,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<div id=\"fb-root\"></div>
\t<script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=565842800257522&autoLogAppEvents=1\" nonce=\"BwxMlIWp\"></script>

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div id = \"property-man-banner\"class=\"jumbotron jumbotron-fluid services-jumbo\" >

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Property Management</h1>
\t\t\t\t<p>Your property is in safe hands</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<h1 class=\"h2-letter-spacing review-content-allign \">Property management services</h1>
\t\t\t\t\t<br>
\t\t\t\t<p class=\"lead\">From Harrow to Central London and Edgware to Chelsea our property management team covers a wide area of properties in London.</p>
\t\t\t<p class=\"lead\">Our super-efficient property management team is highly appreciated by all our existing landlords and tenants. Offering a 24-hour property management services to all our managed properties below are some of our USP</p>
\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t</p>


\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<img src=\"blockMan-pic.jpg\" alt=\"\" class=\"w-100\"/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 order-md-6 animation-element slide-left\" style=\"margin-top: 20px;\">

\t\t<h1 class=\"h2-letter-spacing review-content-allign \">Our USP</h1>
\t\t\t\t\t<br>
\t\t\t\t\t\t<ul style = \"margin-left:30px;\">
\t\t\t\t\t\t<li class=\"lead\">
\t\t\t\t\t24 hour emergency line </li>
\t\t\t\t\t\t<li class=\"lead\">Experienced and qualified property managers .</li>
\t\t\t\t\t\t<li class=\"lead\">Efficient and prompt response services </li>
\t\t\t\t\t\t<li class=\"lead\">Complaint and up to date with all the new regulations </li>
\t\t\t\t\t\t<li class=\"lead\">Monthly/quarterly client reporting  </li>
\t\t\t\t\t\t<li class=\"lead\">Work within client SLA and budget</li>
\t\t\t\t\t\t<li class=\"lead\">Honest, transparent and efficient service</li>
\t\t\t\t\t</ul>

\t\t\t\t\t<br>

\t\t\t

\t\t\t\t</div>
\t\t\t</br>

\t\t\t<!-- /.col-md-6 -->
\t\t\t<div class=\"col-md-6 order-md-1 animation-element slide-right\">

\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t<div class=\"fb-video\" data-href=\"https://www.facebook.com/empirechaseestateagents/videos/4954274441280932/\" data-width=\"500\" data-show-text=\"false\">
\t\t\t\t\t\t\t<blockquote cite=\"https://developers.facebook.com/empirechaseestateagents/videos/4954274441280932/\" class=\"fb-xfbml-parse-ignore\">
\t\t\t\t\t\t\t\t<a href=\"https://developers.facebook.com/empirechaseestateagents/videos/4954274441280932/\"></a>
\t\t\t\t\t\t\t\t<p>If you own a property in London - You got to try this.  
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tProfessional property management  3 months Free Trial - Get in touch   
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tpropertymanagement&#064;empirechase.co.uk 02084227722  
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tapplies for selected properties only in selected areas only.</p>Posted by
\t\t\t\t\t\t\t\t<a href=\"https://www.facebook.com/empirechaseestateagents/\">Empire Chase</a>
\t\t\t\t\t\t\t\ton Saturday, January 9, 2021</blockquote>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t\t<!-- /.col-md-6 -->

\t\t</div>
\t\t<!-- /.row -->
<br>
\t<h1 class=\"h2-letter-spacing review-content-allign \">Services Covered </h1>

\t<br>
\t\t<ul style = \"margin-left:30px;\">
\t\t<li class=\"lead\">Organise inventory check ins and check outs   </li>
\t\t<li class=\"lead\">Move the tenants in </li>
\t\t<li class=\"lead\">Register tenants for utility bills </li>
\t\t<li class=\"lead\">Register the deposit</li>
\t\t<li class=\"lead\">24 hour property emergency line for tenants   </li>
\t\t<li class=\"lead\">Look after the day-to-day maintenance issues</li>
\t\t<li class=\"lead\">Organise contractors</li>
\t\t<li class=\"lead\">Organise safety check such as electric and gas</li>
\t\t<li class=\"lead\">Rent collection </li>
\t\t<li class=\"lead\">Monthly/quarterly client reporting</li>
\t\t<li class=\"lead\">Produce statement of accounts each quarter</li>
\t\t<li class=\"lead\">Deposit dispute resolution</li>
\t</ul>
\t<br>

\t<section class=\"numbers white-txt justify-content-center\" style = \"padding:0px; background-color: #484848!important;\">
\t<div class=\"marged flex\">
  <!-- item 01 -->
\t  <div class=\"number-item\" style=\"text-align: center;\">
\t\t<h2><span class=\"value\">414</span></h2>
\t\t<h6>Properties managed</h6>
\t  </div>

\t<!-- item 02 -->
\t  <div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
\t\t<h2 ><span class=\"value\">99</span>%</h2>
\t\t<h6>of our clients paid full rent and didn’t fall in to rent arrears in the past 12 months</h6>
\t  </div>
\t</div>
\t<div class=\"number-item\" style=\"text-align: center; padding: 3px;\">
<h2 >£ <span class=\"value\">85</span>,<span class=\"value\">639</span>,<span class=\"value\">202</span></h2>
<h6>worth of asset and properties managed</h6>
</div>
</div>
  </section>
  <br>
\t<br>


\t\t<!-- /contact form -->
\t\t<div class =\"container\">
\t<h1 class=\"h2-letter-spacing review-content-allign \">Premier property management</h1>
\t<br>
<p class=\"lead review-content-allign\">We have a premier service for clients who seek a complete management for their asset. </p>
<p class=\"lead review-content-allign\">This service is mainly designed for overseas landlords who do not live in the UK</p>
<p class=\"lead review-content-allign\">In addition to the lettings tenant finder services below are the services we offer in our premier property management services</p>
<br>
<h1 class=\"h2-letter-spacing review-content-allign \">Services includes</h1>
<ul style = \"margin-left:30px;\">
<li class=\"lead\">Organise inventory check in    </li>
<li class=\"lead\">Move the tenants in </li>
<li class=\"lead\">Register tenants for utility bills </li>
<li class=\"lead\">Register the deposit</li>
<li class=\"lead\">24 hour property emergency line for tenants   </li>
<li class=\"lead\">Organise contractors</li>
<li class=\"lead\">Organise safety check such as electric and gas</li>
<li class=\"lead\">Rent collection </li>
<li class=\"lead\">Monthly/quarterly client reporting</li>
<li class=\"lead\">Produce statement of accounts each quarter</li>
<li class=\"lead\">Deposit dispute resolution</li>
<li class=\"lead\">Dedicated account manager with 24-hour access</li>
<li class=\"lead\">Monthly statement of accounts</li>
<li class=\"lead\">Pay any outgoings on behalf of the landlord and reimburse via rent collection</li>
<li class=\"lead\">Represent the landlord </li>
<li class=\"lead\">Register for Non-Residents landlords</li>
<li class=\"lead\">Assist with any other property related issues and matters</li>
</ul>
<br>
<h1 class=\"h2-letter-spacing review-content-allign \">Rent guarantee</h1>
<br>
<p class=\"lead \">We offer rent guarantee insurances from some of the leading rent guarantee insurance providers such as Homelet offering maximum cover to landlords. </p>
<p class=\"lead\">Currently there are policies covering up to 12 – 15 months on new tenancies. Please contact our property management team for further questions</p>
<p class=\"lead\">If you would like to know how proactive our property management services are and why we are better than others, please contact our property management team</p>

\t\t<div
\t\t\tclass=\"row \" style=\"margin-right: 10px;margin-left: 10px;\">
\t\t\t<!--Section: Contact v.2-->
\t\t\t<section
\t\t\t\tclass=\"mb-4\">

\t\t\t\t<!--Section heading-->
\t\t\t\t<h2 class=\"h1-responsive font-weight-bold text-center my-4 h2-letter-spacing\">Contact us</h2>
\t\t\t\t<!--Section description-->
\t\t\t\t<p class=\"text-center w-responsive mx-auto mb-5\">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
\t\t\t\t\t\t\t\t\t\t        a matter of hours to help you.</p>

\t\t\t\t<div
\t\t\t\t\tclass=\"row\">

\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t<div class=\"col-md-6 mb-md-0 mb-5\" style=\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    font-weight: 300;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t{{form(form)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->


\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t</div>

\t\t\t\t</section>


\t\t\t</div>
\t\t\t<!-- /.row -->

\t\t</div>
\t\t<!-- /container -->


\t</main>


{% endblock %}
", "services/property-management.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\property-management.html.twig");
    }
}
