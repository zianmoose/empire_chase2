<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /properties/rentalp.html.twig */
class __TwigTemplate_3a100efccd7f23d32e90b5c63a1f570827543edf6182c4e02502cf42a8e7c691 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascripts' => [$this, 'block_javascripts'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/properties/rentalp.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/properties/rentalp.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/properties/rentalp.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 4
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo " 
        ";
        // line 5
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("pagination");
        echo "
        ";
        // line 6
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("pagecustom");
        echo "

    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    ";
        // line 12
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("properties");
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 17
        echo "
<main role=\"main\">

  <!-- Main jumbotron for a primary marketing message or call to action -->
   <div class=\"jumbotron jumbotron-fluid services-jumbo\" style =\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/rent-banner.jpeg');background-size: cover;background-position: center;
  height:500px;\" >
  
    <div class=\"container services-jumbo-container display-3-top\" style = \"margin-top:200px !important;\">
      <h1 class=\"display-3\">Lettings</h1>
      <br>
      <p><a href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\"class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a></p>

    </div>
    
    ";
        // line 32
        echo "    
  </div>

  <div class=\"justify-content-center\">
<div class=\"container\">
<p class=\"lead review-content-allign\">Our firm has a proactive lettings department offering a comprehensive lettings services to both tenants and landlords.</p>
<p class=\"lead review-content-allign\">If you are a landlord and looking to let your property, please view our <a href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">LANDLORDS</a> page  </p>
<p class=\"lead review-content-allign\">If you are a tenant and looking to rent a property, please view our  <a href=\"";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_services_rent");
        echo "\">RENT</a> page  </p>

</div>
<br>
  <div id=\"properties-container\" class=\"container\" style=\"margin-top: 50px;\">

    <div class=\"card border-secondary mb-3\" style=\"max-width: 18rem;background-color: #2b459c!important;
          color: white;\">
      <div class=\"card-header\">Landlords</div>
      <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/salep-button.jpg');background-size: cover;background-position: center;\">

        <h5 class=\"card-title\" style=\"color:white\">Check out our in-detail page</h5>
        <br>
        <a href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_landlords");
        echo "\">
          <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
        </a>
      </div>
    </div>

    <div class=\"card border-secondary mb-3\" style=\"width: 18rem;background-color: #2b459c!important;
          color: white;\">
      <div class=\"card-header\">Rent</div>
      <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/buyp-button.jpeg');background-size: cover;background-position: center;\">

        <h5 class=\"card-title\" style=\"color:white\">Find a property</h5>
        <br>
          <br>
        <a href=\"https://www.rightmove.co.uk/property-to-rent/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&propertyStatus=all&includeLetAgreed=true&_includeLetAgreed=on\">
          <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
        </a>
      </div>
    </div>


  </div>

</div>

";
        // line 83
        echo "
<br>

";
        // line 225
        echo "


</main> 


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/properties/rentalp.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 225,  204 => 83,  176 => 52,  160 => 39,  156 => 38,  148 => 32,  141 => 27,  129 => 17,  119 => 16,  107 => 12,  102 => 11,  92 => 10,  79 => 6,  75 => 5,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

 {% block javascripts %}
        {{ parent() }} 
        {{ encore_entry_script_tags('pagination') }}
        {{ encore_entry_script_tags('pagecustom') }}

    {% endblock %}

    {% block stylesheets %}
    {{ parent() }}
    {{ encore_entry_link_tags('properties') }}
    {% endblock %}
  

{% block body %}

<main role=\"main\">

  <!-- Main jumbotron for a primary marketing message or call to action -->
   <div class=\"jumbotron jumbotron-fluid services-jumbo\" style =\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/rent-banner.jpeg');background-size: cover;background-position: center;
  height:500px;\" >
  
    <div class=\"container services-jumbo-container display-3-top\" style = \"margin-top:200px !important;\">
      <h1 class=\"display-3\">Lettings</h1>
      <br>
      <p><a href=\"{{ path('emp_contact-us') }}\"class=\"btn btn-primary btn-lg\" href=\"#\" style = \"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a></p>

    </div>
    
    {# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}
    
  </div>

  <div class=\"justify-content-center\">
<div class=\"container\">
<p class=\"lead review-content-allign\">Our firm has a proactive lettings department offering a comprehensive lettings services to both tenants and landlords.</p>
<p class=\"lead review-content-allign\">If you are a landlord and looking to let your property, please view our <a href=\"{{ path('emp_landlords') }}\">LANDLORDS</a> page  </p>
<p class=\"lead review-content-allign\">If you are a tenant and looking to rent a property, please view our  <a href=\"{{ path('emp_services_rent') }}\">RENT</a> page  </p>

</div>
<br>
  <div id=\"properties-container\" class=\"container\" style=\"margin-top: 50px;\">

    <div class=\"card border-secondary mb-3\" style=\"max-width: 18rem;background-color: #2b459c!important;
          color: white;\">
      <div class=\"card-header\">Landlords</div>
      <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/salep-button.jpg');background-size: cover;background-position: center;\">

        <h5 class=\"card-title\" style=\"color:white\">Check out our in-detail page</h5>
        <br>
        <a href=\"{{ path('emp_landlords') }}\">
          <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
        </a>
      </div>
    </div>

    <div class=\"card border-secondary mb-3\" style=\"width: 18rem;background-color: #2b459c!important;
          color: white;\">
      <div class=\"card-header\">Rent</div>
      <div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/buyp-button.jpeg');background-size: cover;background-position: center;\">

        <h5 class=\"card-title\" style=\"color:white\">Find a property</h5>
        <br>
          <br>
        <a href=\"https://www.rightmove.co.uk/property-to-rent/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&propertyStatus=all&includeLetAgreed=true&_includeLetAgreed=on\">
          <button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
        </a>
      </div>
    </div>


  </div>

</div>

{# <div class=\"text-center\">
  <br>
  <a href=\"https://www.rightmove.co.uk/property-for-sale/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&includeSSTC=true&_includeSSTC=on\">
    <button type=\"button\" class=\"btn btn-primary\" style=\"background:#2b459c!important;margin-right:auto;margin-left:auto\">Find a property</button>
  </a>
</div> #}

<br>

{# <div class=\"container\" style = \"margin-top: 50px;\">



     <div id=\"wrapper\">
          <div class=\"contents\">
    <!-- Example row of columns -->
             <div class=\"row\">
   
        <div class=\"card mb-3\" style=\"max-width: 1080px;\">
  <div class=\"row no-gutters\">
    <div class=\"col-md-4\">
      <img src=\"prop1.jpg\" class=\"card-img\" alt=\"...\">
    </div>
    <div class=\"col-md-8\">
      <div class=\"card-body\">
        <h5 class=\"card-title\">Card title</h5>
        <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>
</div>

        </div><!-- /row -->
    <hr>

     <!-- Example row of columns -->
         <div class=\"row\">

        <div class=\"card mb-3\" style=\"max-width: 1080px;\">
  <div class=\"row no-gutters\">
    <div class=\"col-md-4\">
      <img src=\"prop1.jpg\" class=\"card-img\" alt=\"...\">
    </div>
    <div class=\"col-md-8\">
      <div class=\"card-body\">
        <h5 class=\"card-title\">Card title</h5>
        <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>
</div>

        </div><!-- /row -->

    <hr>

                 <div class=\"row\">
   
        <div class=\"card mb-3\" style=\"max-width: 1080px;\">
  <div class=\"row no-gutters\">
    <div class=\"col-md-4\">
      <img src=\"prop2.jpg\" class=\"card-img\" alt=\"...\">
    </div>
    <div class=\"col-md-8\">
      <div class=\"card-body\">
        <h5 class=\"card-title\">Card title</h5>
        <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>
</div>

        </div><!-- /row -->
    <hr>

     <!-- Example row of columns -->
         <div class=\"row\">

        <div class=\"card mb-3\" style=\"max-width: 1080px;\">
  <div class=\"row no-gutters\">
    <div class=\"col-md-4\">
      <img src=\"prop3.jpg\" class=\"card-img\" alt=\"...\">
    </div>
    <div class=\"col-md-8\">
      <div class=\"card-body\">
        <h5 class=\"card-title\">Card title</h5>
        <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>
</div>

        </div><!-- /row -->

    <hr>

                 <div class=\"row\">
   
        <div class=\"card mb-3\" style=\"max-width: 1080px;\">
  <div class=\"row no-gutters\">
    <div class=\"col-md-4\">
      <img src=\"prop5.jpg\" class=\"card-img\" alt=\"...\">
    </div>
    <div class=\"col-md-8\">
      <div class=\"card-body\">
        <h5 class=\"card-title\">Card title</h5>
        <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>
</div>

        </div><!-- /row -->
    <hr>

     <!-- Example row of columns -->
         <div class=\"row\">

        <div class=\"card mb-3\" style=\"max-width: 1080px;\">
  <div class=\"row no-gutters\">
    <div class=\"col-md-4\">
      <img src=\"prop1.jpg\" class=\"card-img\" alt=\"...\">
    </div>
    <div class=\"col-md-8\">
      <div class=\"card-body\">
        <h5 class=\"card-title\">Card title</h5>
        <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>
</div>

        </div><!-- /row -->

        </div><!--/content-->

        </div><!--/wrapper-->

    <hr>

  </div> <!--/container -->
  #}



</main> 


{% endblock %}", "/properties/rentalp.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\properties\\rentalP.html.twig");
    }
}
