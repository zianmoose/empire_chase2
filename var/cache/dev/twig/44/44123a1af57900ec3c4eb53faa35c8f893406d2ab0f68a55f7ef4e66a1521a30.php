<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /properties/salep.html.twig */
class __TwigTemplate_c1e92a7226c4b0dd3c587ef34a25e754474481f30158c476808758b050497176 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascripts' => [$this, 'block_javascripts'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/properties/salep.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/properties/salep.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/properties/salep.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 4
        echo "\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t";
        // line 5
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("pagination");
        echo "
\t";
        // line 6
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("pagecustom");
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t";
        // line 12
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("properties");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 16
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/empImg3.jpg');background-size: cover;background-position: center;
\t\t\t\t  height:500px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\" style = \"margin-top:200px !important;\">
\t\t\t\t<h1 class=\"display-3\">Sales</h1>
\t\t\t\t<br>
\t\t\t\t<p>Buying or selling your biggest asset can be challenging, choose your agent wisely.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 34
        echo "
\t\t</div>
\t\t<div class = \"container\">
\t<p class=\"lead review-content-allign\">Selling or buying a property can be a challenging process. Having professional, customer focused and efficient estate agents is vital in this process.</p>
\t<p class=\"lead review-content-allign\">We offer bespoke sales services to both sellers and buyers. In addition to our unique marketing strategy we offer a comprehensive sales profession services. </p>\t
<p class=\"lead review-content-allign\">A property progresser can make or break a sale. From the moment a sale is agreed our sales progression team will liaise with all parties to keep the sale on track</p>
<p class=\"lead review-content-allign\">If you are looking to sell your property please view our <a href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\"> SELL</a> page  </p>
<p class=\"lead review-content-allign\">If you are looking to buy a property please view our <a href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">BUY</a> page  </p>
\t\t<div>

\t\t<div class=\"justify-content-center\">

\t\t\t<div id=\"properties-container\" class=\"container\" style=\"margin-top: 50px;\">

\t\t\t\t<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem;background-color: #2b459c!important;
\t\t\t\t\t\t  color: white;\">
\t\t\t\t\t<div class=\"card-header\">Sell your property</div>
\t\t\t\t\t<div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/salep-button.jpg');background-size: cover;background-position: center;\">

\t\t\t\t\t\t<h5 class=\"card-title\" style=\"color:white\">Check out our in-detail page</h5>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<a href=\"";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_sell");
        echo "\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem;background-color: #2b459c!important;
\t\t\t\t\t\t  color: white;\">
\t\t\t\t\t<div class=\"card-header\">Buy a property</div>
\t\t\t\t\t<div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/buyp-button.jpeg');background-size: cover;background-position: center;\">

\t\t\t\t\t\t<h5 class=\"card-title\" style=\"color:white\">Check out our in-detail page</h5>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<a href=\"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_buy");
        echo "\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>


\t\t\t</div>

\t\t</div>

\t\t<div class=\"text-center\">
\t\t\t<br>
\t\t\t<a href=\"https://www.rightmove.co.uk/property-for-sale/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&includeSSTC=true&_includeSSTC=on\">
\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"background:#2b459c!important;margin-right:auto;margin-left:auto\">Find a property</button>
\t\t\t</a>
\t\t</div>

\t\t<br>


\t\t";
        // line 249
        echo "\t\t<!--/container -->


\t</main>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/properties/salep.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 249,  196 => 68,  180 => 55,  163 => 41,  159 => 40,  151 => 34,  144 => 29,  129 => 16,  119 => 15,  107 => 12,  102 => 11,  92 => 10,  79 => 6,  75 => 5,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block javascripts %}
\t{{ parent() }}
\t{{ encore_entry_script_tags('pagination') }}
\t{{ encore_entry_script_tags('pagecustom') }}

{% endblock %}

{% block stylesheets %}
\t{{ parent() }}
\t{{ encore_entry_link_tags('properties') }}
{% endblock %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/empImg3.jpg');background-size: cover;background-position: center;
\t\t\t\t  height:500px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\" style = \"margin-top:200px !important;\">
\t\t\t\t<h1 class=\"display-3\">Sales</h1>
\t\t\t\t<br>
\t\t\t\t<p>Buying or selling your biggest asset can be challenging, choose your agent wisely.</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>
\t\t<div class = \"container\">
\t<p class=\"lead review-content-allign\">Selling or buying a property can be a challenging process. Having professional, customer focused and efficient estate agents is vital in this process.</p>
\t<p class=\"lead review-content-allign\">We offer bespoke sales services to both sellers and buyers. In addition to our unique marketing strategy we offer a comprehensive sales profession services. </p>\t
<p class=\"lead review-content-allign\">A property progresser can make or break a sale. From the moment a sale is agreed our sales progression team will liaise with all parties to keep the sale on track</p>
<p class=\"lead review-content-allign\">If you are looking to sell your property please view our <a href=\"{{ path('emp_sell') }}\"> SELL</a> page  </p>
<p class=\"lead review-content-allign\">If you are looking to buy a property please view our <a href=\"{{ path('emp_buy') }}\">BUY</a> page  </p>
\t\t<div>

\t\t<div class=\"justify-content-center\">

\t\t\t<div id=\"properties-container\" class=\"container\" style=\"margin-top: 50px;\">

\t\t\t\t<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem;background-color: #2b459c!important;
\t\t\t\t\t\t  color: white;\">
\t\t\t\t\t<div class=\"card-header\">Sell your property</div>
\t\t\t\t\t<div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/salep-button.jpg');background-size: cover;background-position: center;\">

\t\t\t\t\t\t<h5 class=\"card-title\" style=\"color:white\">Check out our in-detail page</h5>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<a href=\"{{ path('emp_sell') }}\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"card border-secondary mb-3\" style=\"max-width: 18rem;background-color: #2b459c!important;
\t\t\t\t\t\t  color: white;\">
\t\t\t\t\t<div class=\"card-header\">Buy a property</div>
\t\t\t\t\t<div class=\"card-body text-secondary text-center\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('salep/buyp-button.jpeg');background-size: cover;background-position: center;\">

\t\t\t\t\t\t<h5 class=\"card-title\" style=\"color:white\">Check out our in-detail page</h5>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<a href=\"{{ path('emp_buy') }}\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"background:#d39e00!important\">Click here</button>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>


\t\t\t</div>

\t\t</div>

\t\t<div class=\"text-center\">
\t\t\t<br>
\t\t\t<a href=\"https://www.rightmove.co.uk/property-for-sale/find/Empire-Chase-Estate-Agent/Harrow.html?locationIdentifier=BRANCH%5E128062&includeSSTC=true&_includeSSTC=on\">
\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"background:#2b459c!important;margin-right:auto;margin-left:auto\">Find a property</button>
\t\t\t</a>
\t\t</div>

\t\t<br>


\t\t{# <div class=\"container\" style=\"margin-top: 50px;\">

\t\t\t<h2 id=\"registration\" class=\"review-content-allign h2-letter-spacing\" style=\"background-color:\t#D8D8D8;\">Featured Properties</h2>

\t\t\t<br>
\t\t\t<div id=\"wrapper\">
\t\t\t\t<div
\t\t\t\t\tclass=\"contents\">
\t\t\t\t\t<!-- Example row of columns -->
\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"card mb-3\" style=\"max-width: 1080px;\">
\t\t\t\t\t\t\t<div class=\"row no-gutters\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<img src=\"prop1.jpg\" class=\"card-img\" alt=\"...\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">Card title</h5>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">
\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-muted\">Last updated 3 mins ago</small>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /row -->
\t\t\t\t\t<hr>

\t\t\t\t\t<!-- Example row of columns -->
\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"card mb-3\" style=\"max-width: 1080px;\">
\t\t\t\t\t\t\t<div class=\"row no-gutters\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<img src=\"prop1.jpg\" class=\"card-img\" alt=\"...\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">Card title</h5>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">
\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-muted\">Last updated 3 mins ago</small>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /row -->

\t\t\t\t\t<hr>

\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"card mb-3\" style=\"max-width: 1080px;\">
\t\t\t\t\t\t\t<div class=\"row no-gutters\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<img src=\"prop2.jpg\" class=\"card-img\" alt=\"...\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">Card title</h5>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">
\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-muted\">Last updated 3 mins ago</small>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /row -->
\t\t\t\t\t<hr>

\t\t\t\t\t<!-- Example row of columns -->
\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"card mb-3\" style=\"max-width: 1080px;\">
\t\t\t\t\t\t\t<div class=\"row no-gutters\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<img src=\"prop3.jpg\" class=\"card-img\" alt=\"...\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">Card title</h5>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">
\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-muted\">Last updated 3 mins ago</small>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /row -->

\t\t\t\t\t<hr>

\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"card mb-3\" style=\"max-width: 1080px;\">
\t\t\t\t\t\t\t<div class=\"row no-gutters\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<img src=\"prop5.jpg\" class=\"card-img\" alt=\"...\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">Card title</h5>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">
\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-muted\">Last updated 3 mins ago</small>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /row -->
\t\t\t\t\t<hr>

\t\t\t\t\t<!-- Example row of columns -->
\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"card mb-3\" style=\"max-width: 1080px;\">
\t\t\t\t\t\t\t<div class=\"row no-gutters\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t<img src=\"prop1.jpg\" class=\"card-img\" alt=\"...\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">Card title</h5>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
\t\t\t\t\t\t\t\t\t\t<p class=\"card-text\">
\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-muted\">Last updated 3 mins ago</small>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<!-- /row -->

\t\t\t\t</div>
\t\t\t\t<!--/content-->

\t\t\t</div>
\t\t\t<!--/wrapper-->

\t\t\t<hr>

\t\t</div> #}
\t\t<!--/container -->


\t</main>


{% endblock %}
", "/properties/salep.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\properties\\salep.html.twig");
    }
}
