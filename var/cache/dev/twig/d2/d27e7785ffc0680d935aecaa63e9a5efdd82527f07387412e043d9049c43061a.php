<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* services/block-management.html.twig */
class __TwigTemplate_b5a447d602d78801bb0e452fda4e8b57e055e803d59774a5e26a5322fc27c310 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/block-management.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/block-management.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "services/block-management.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/blockManBanner.jpg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Block Management</h1>
\t\t\t\t<p>Safety and compliance is preeminent in our business</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emp_contact-us");
        echo "\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t";
        // line 21
        echo "
\t\t</div>

\t\t<br/><br/>


\t\t<!--<h1 class=\"text-center\">Block Management</h1> -->
\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<p class=\"lead\">We are currently managing a number of apartment blocks for various corporate landlords offering a fully comprehensive service including arranging:
\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t</p>
\t\t\t\t\t
\t\t\t\t\t\t<ul style = \"margin-left:30px;\">
\t\t\t\t\t<li class=\"lead\">Fire risk assessments.</li>
\t\t\t\t\t<li class=\"lead\">Weekly fire alarm and AOV system test .</li>
\t\t\t\t\t<li class=\"lead\">Lightning tests</li>
\t\t\t\t\t<li class=\"lead\">Emergency lighting tests  .</li>
\t\t\t\t\t<li class=\"lead\">Lift maintenance management </li>
\t\t\t\t\t<li class=\"lead\">Building insurance .</li>
\t\t\t\t<li class=\"lead\">Parking management  </li>
\t\t\t<li class=\"lead\">Gym and Facilities management  .</li>
\t\t\t<li class=\"lead\">Security and concierge management</li>
\t\t\t<li class=\"lead\">Waste management and much more .</li>
\t\t</ul>
\t\t\t\t


\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<img src=\"block-man/block-man-pic.jpeg\" alt=\"\" class=\"w-100\"/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->
\t\t\t<br/><br/>

\t\t
\t\t\t<br/><br/>

\t\t\t<div class=\"container\">
\t\t\t\t<div
\t\t\t\t\tclass=\"row\">


\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t<div class=\"col-md-6\" style=\"
\t\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t";
        // line 89
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 89, $this->source); })()), 'form');
        echo "

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</section>


\t\t</div>
\t\t<!-- /.row contact-->

\t</div>
\t<!-- /container -->


</main>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "services/block-management.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 89,  89 => 21,  82 => 16,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

\t<main
\t\trole=\"main\">

\t\t<!-- Main jumbotron for a primary marketing message or call to action -->
\t\t<div class=\"jumbotron jumbotron-fluid services-jumbo\" style=\" background:  linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/blockManBanner.jpg');background-size: cover;background-position: center;
\t\t\t\t  height:600px;\">

\t\t\t<div class=\"container services-jumbo-container display-3-top\">
\t\t\t\t<h1 class=\"display-3\">Block Management</h1>
\t\t\t\t<p>Safety and compliance is preeminent in our business</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"{{ path('emp_contact-us') }}\" class=\"btn btn-primary btn-lg\" href=\"#\" style=\"background-color:  #d39e00!important; color: #000!important; border-color: transparent;\" role=\"button\">Contact us</a>
\t\t\t\t</p>
\t\t\t</div>

\t\t\t{# <img  class=\"service-img-wrap\" src=\"services-banner.jpg\" alt=\"First slide\"> #}

\t\t</div>

\t\t<br/><br/>


\t\t<!--<h1 class=\"text-center\">Block Management</h1> -->
\t\t<div
\t\t\tclass=\"container\">
\t\t\t<!--  image left  -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 animation-element slide-left\">
\t\t\t\t\t<p class=\"lead\">We are currently managing a number of apartment blocks for various corporate landlords offering a fully comprehensive service including arranging:
\t\t\t\t\t\t<br/><br/>

\t\t\t\t\t</p>
\t\t\t\t\t
\t\t\t\t\t\t<ul style = \"margin-left:30px;\">
\t\t\t\t\t<li class=\"lead\">Fire risk assessments.</li>
\t\t\t\t\t<li class=\"lead\">Weekly fire alarm and AOV system test .</li>
\t\t\t\t\t<li class=\"lead\">Lightning tests</li>
\t\t\t\t\t<li class=\"lead\">Emergency lighting tests  .</li>
\t\t\t\t\t<li class=\"lead\">Lift maintenance management </li>
\t\t\t\t\t<li class=\"lead\">Building insurance .</li>
\t\t\t\t<li class=\"lead\">Parking management  </li>
\t\t\t<li class=\"lead\">Gym and Facilities management  .</li>
\t\t\t<li class=\"lead\">Security and concierge management</li>
\t\t\t<li class=\"lead\">Waste management and much more .</li>
\t\t</ul>
\t\t\t\t


\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->
\t\t\t\t<div class=\"col-md-6 animation-element slide-right\">

\t\t\t\t\t<div class=\"row align-items-center h-100\">
\t\t\t\t\t\t<div class=\"col\">

\t\t\t\t\t\t\t<img src=\"block-man/block-man-pic.jpeg\" alt=\"\" class=\"w-100\"/>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<!-- /.col-md-6 -->

\t\t\t</div>
\t\t\t<!-- /.row -->
\t\t\t<br/><br/>

\t\t
\t\t\t<br/><br/>

\t\t\t<div class=\"container\">
\t\t\t\t<div
\t\t\t\t\tclass=\"row\">


\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t<div class=\"col-md-6\" style=\"
\t\t\t\t\t\t\t    background-color: #D8D8D8;
\t\t\t\t\t\t\t    margin: 10px 10px 30px 10px;
\t\t\t\t\t\t\t    padding: 20px;
\t\t\t\t\t\t\t    border-radius: 3%;
\t\t\t\t\t\t\t    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);\">
\t\t\t\t\t\t{{ form(form) }}

\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column form-->


\t\t\t\t\t\t<!--Grid column-->
\t\t\t\t\t\t<div class=\"col-md-3 text-center\">
\t\t\t\t\t\t\t<ul class=\"list-unstyled mb-0\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>14 Peterborough Road,Harrow,HA1 2BQ</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-phone mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>0208 4227722</p>
\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope mt-4 fa-2x\"></i>
\t\t\t\t\t\t\t\t\t<p>info@empirechase.co.uk
\t\t\t\t\t\t\t\t\t\t<info@empirechase.co.uk></p>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--Grid column-->

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</section>


\t\t</div>
\t\t<!-- /.row contact-->

\t</div>
\t<!-- /container -->


</main>{% endblock %}
", "services/block-management.html.twig", "C:\\Users\\SAJEE\\Desktop\\empsym\\emp_chase\\templates\\services\\block-management.html.twig");
    }
}
