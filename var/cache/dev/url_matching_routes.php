<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/test' => [[['_route' => 'app_contact_new', '_controller' => 'App\\Controller\\ContactController::new'], null, null, null, false, false, null]],
        '/home' => [
            [['_route' => 'app_default_index', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null],
            [['_route' => 'emp_home', 'template' => '/home.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null],
        ],
        '/email' => [[['_route' => 'app_mailer_sendemail', '_controller' => 'App\\Controller\\MailerController::sendEmail'], null, null, null, false, false, null]],
        '/inperson/type' => [[['_route' => 'inperson_type', '_controller' => 'App\\Controller\\VirtualTypeController::buildForm'], null, null, null, false, false, null]],
        '/services' => [[['_route' => 'emp_services', 'template' => '/services/services.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/rent' => [[['_route' => 'emp_services_rent', 'template' => '/services/rent.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/sales' => [[['_route' => 'emp_services_sales', 'template' => '/services/sales.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/property-management' => [[['_route' => 'emp_services_property-management', 'template' => '/services/property-management.html.twig', '_controller' => 'App\\Controller\\Emp_services\\PropertyManController::new'], null, null, null, false, false, null]],
        '/build-to-rent' => [[['_route' => 'emp_services_build-to-rent', 'template' => '/services/build-to-rent.html.twig', '_controller' => 'App\\Controller\\Emp_services\\BuildRentController::new'], null, null, null, false, false, null]],
        '/asset-management' => [[['_route' => 'emp_services_asset-management', 'template' => '/services/asset-management.html.twig', '_controller' => 'App\\Controller\\Emp_services\\AssetManController::new'], null, null, null, false, false, null]],
        '/block-management' => [[['_route' => 'emp_services_block-management', 'template' => '/services/block-management.html.twig', '_controller' => 'App\\Controller\\Emp_services\\BlockManContoller::new'], null, null, null, false, false, null]],
        '/market-research' => [[['_route' => 'emp_services_/market-research', 'template' => '/services/market-research.html.twig', '_controller' => 'App\\Controller\\Emp_services\\MarktReController::new'], null, null, null, false, false, null]],
        '/properties' => [[['_route' => 'emp_properties', 'template' => '/properties/properties.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/lettings' => [[['_route' => 'emp_rentalp', 'template' => '/properties/rentalp.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/sale' => [[['_route' => 'emp_salep', 'template' => '/properties/salep.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/contact-us' => [[['_route' => 'emp_contact-us', 'template' => '/contact-us.html.twig', '_controller' => 'App\\Controller\\ContactController::new'], null, null, null, false, false, null]],
        '/valuation' => [[['_route' => 'emp_valuation_main', 'template' => '/services/valuation/valuation-main.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/valuation/inperson' => [[['_route' => 'emp_in_person_valuation', 'template' => '/services/valuation/in-person-valuation.html.twig', '_controller' => 'App\\Controller\\Valuation\\InpersonController:index'], null, null, null, false, false, null]],
        '/valuation/virtual' => [[['_route' => 'emp_virtual_valuation', 'template' => '/services/valuation/virtual-valuation.html.twig', '_controller' => 'App\\Controller\\Valuation\\VirtualController:index'], null, null, null, false, false, null]],
        '/thank_you_contacting' => [[['_route' => 'emp_thank_you', 'template' => 'thank_you.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/email_send' => [[['_route' => 'email_send', 'template' => 'email_send.html.twig', '_controller' => 'App\\Controller\\ContactController'], null, null, null, false, false, null]],
        '/about-us' => [[['_route' => 'emp_about_us', 'template' => 'aboutUs.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/buy' => [[['_route' => 'emp_buy', 'template' => 'buy.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/sell' => [[['_route' => 'emp_sell', 'template' => 'sell.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/landlords' => [[['_route' => 'emp_landlords', 'template' => 'landlords.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/market-expertise' => [[['_route' => 'emp_marketexpert', 'template' => 'marketexpert.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
        '/cookie-policy' => [[['_route' => 'emp_cookie-policy', 'template' => 'cookie-policy.html.twig', '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
