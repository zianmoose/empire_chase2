<?php

namespace ContainerVtx4Fzy;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_RFm6ghHService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.RFm6ghH' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.RFm6ghH'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'builder' => ['privates', '.errored..service_locator.RFm6ghH.Symfony\\Component\\Form\\FormBuilderInterface', NULL, 'Cannot autowire service ".service_locator.RFm6ghH": it references interface "Symfony\\Component\\Form\\FormBuilderInterface" but no such service exists. Did you create a class that implements this interface?'],
        ], [
            'builder' => 'Symfony\\Component\\Form\\FormBuilderInterface',
        ]);
    }
}
