<?php

namespace ContainerVtx4Fzy;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getFormViewService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.errored..service_locator.mQJ4S6J.Symfony\Component\Form\FormView' shared service.
     *
     * @return \Symfony\Component\Form\FormView
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->throw('Cannot autowire service ".service_locator.mQJ4S6J": it references class "Symfony\\Component\\Form\\FormView" but no such service exists.');
    }
}
